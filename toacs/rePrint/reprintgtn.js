var header_reprintgtn = function()
{
	return {
        view: "scrollview",
        scroll: "native-y",
        id:"header_reprintgtn",
        body: 
        {
        	id:"reprintgtn_id",
        	type:"clean",
    		rows:
    		[
    		    {
                    view:"form",
                    paddingY:20,
                    id:"reprintgtn_form1",
                    elements:
                    [
                        {                            
                            cols:                                    
                                [    
                                   
                                    {
                                        view:"text",label:'GTN (เลขที่เอกสารขาออก)',required:true,name:"gtn_doc_gtn",id:"gtn_doc_gtn",labelPosition:"top", value:"",width:230
                                    },
                                    {}                          
                                ]
                        },
                        {
                            cols:
                            [
                                {                                             
                                    view:"button", label:"View (ดูตัวอย่างเอกสาร)",width:230,id:"view_print",
                                    on:
                                    {                                                   
                                        onItemClick:function(id, e)
                                        {
                                            if($$('reprintgtn_form1').validate())
                                            {
                                                var gtnno = $$('gtn_doc_gtn').getValue();
                                               
                                                window.open("print/gtnview.php?data="+gtnno, '_blank'); 
                                            }                                                                      
                                        }
                                    }       
                                },
                                {                                             
                                    view:"button", label:"Print (ปริ้นเอกสาร)", type:"form",width:230,id:"Print_gtn",
                                    on:
                                    {                                                   
                                        onItemClick:function(id, e)
                                        {
                                            if ($$('reprintgtn_form1').validate())
                                                {
                                                    var gtnno = $$('gtn_doc_gtn').getValue();
                                                  
                                                    $.post( "print/gtn.php", {data1:gtnno})
                                                    .done(function( data ) 
                                                    {
                                                        var data = eval('('+data+')');
                                                        if(data.ch == 1)
                                                        {
                                                          webix.message({ type:"default",expire:7000, text:'ปริ้น GTN สำเร็จ'});
                                                        }
                                                        else webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});
                                                    });
                                                            
                                                }                                                    
                                        }
                                    }       
                                },{}
                            ]
                        }                                              
                    ]
                    
                }
            ],on:
            {
                onHide:function()
                {
                    
                },
                onShow:function()
                {

                },
                onAddView:function()
                {

                }
            }
        }
    };
};