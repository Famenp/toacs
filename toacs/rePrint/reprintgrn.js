var header_reprintgrn = function()
{
	return {
        view: "scrollview",
        scroll: "native-y",
        id:"header_reprintgrn",
        body: 
        {
        	id:"reprintgrn_id",
        	type:"clean",
    		rows:
    		[
    		    {
                    view:"form",
                    paddingY:20,
                    id:"reprintgrn_form1",
                    elements:
                    [
                        {                            
                            cols:                                    
                                [    
                                    
                                    {
                                        view:"text",label:'GRN (เลขที่เอกสารขาเข้า)',required:true,name:"grn_redoc_grn",id:"grn_redoc_grn",labelPosition:"top", value:"",width:230
                                    },
                                    {}                          
                                ]
                        },
                        {
                            cols:
                            [
                                {                                             
                                    view:"button", label:"View (ดูตัวอย่างเอกสาร)",width:230,id:"view_print",
                                    on:
                                    {                                                   
                                        onItemClick:function(id, e)
                                        {
                                            if($$('reprintgrn_form1').validate())
                                            {
                                                var grnno = $$('grn_redoc_grn').getValue();
                                                window.open("print/grnview.php?grnno="+grnno, '_blank');   
                                            }                               
                                        }
                                    }       
                                },
                                {                                             
                                    view:"button", label:"Print (ปริ้นเอกสาร)", type:"form",width:230,id:"Print_grn",
                                    on:
                                    {                                                   
                                        onItemClick:function(id, e)
                                        {
                                            if ($$('reprintgrn_form1').validate())
                                                {
                                                    var grnno = $$('grn_redoc_grn').getValue();
                                                    $.post( "print/grn.php", {data1:grnno})
                                                    .done(function( data ) 
                                                    {
                                                        var data = eval('('+data+')');
                                                        if(data.ch == 1)
                                                        {
                                                          webix.message({ type:"default",expire:7000, text:'ปริ้น GRN สำเร็จ'});
                                                        }
                                                        else webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});
                                                    });      
                                                }             
                                                                                   
                                        }
                                    }       
                                },{}
                            ]
                        }                                              
                    ]
                    
                }
            ],on:
            {
                onHide:function()
                {
                    
                },
                onShow:function()
                {

                },
                onAddView:function()
                {

                }
            }
        }
    };
};