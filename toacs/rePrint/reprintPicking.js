var header_reprintPicking = function()
{
	return {
        view: "scrollview",
        scroll: "native-y",
        id:"header_reprintPicking",
        body: 
        {
        	id:"reprintPicking_id",
        	type:"clean",
    		rows:
    		[
    		    {
                    view:"form",
                    paddingY:20,
                    id:"reprintPicking_form1",
                    elements:
                    [
                        {                            
                            cols:                                    
                                [    
                                    
                                    {
                                        view:"text",label:'Picking No',required:true,name:"pl_pick_no",id:"pl_pick_no",labelPosition:"top", value:"",width:230
                                    },
                                    {}                          
                                ]
                        },
                        {
                            cols:
                            [
                                {                                             
                                    view:"button", label:"View (ดูตัวอย่างเอกสาร)",width:230,id:"view_print_pl",
                                    on:
                                    {                                                   
                                        onItemClick:function(id, e)
                                        {
                                            if($$('reprintPicking_form1').validate())
                                            {
                                                var pickno = $$('pl_pick_no').getValue();
                                            
                                                window.open("print/pickinglistview.php?data="+pickno, '_blank');   
                                            }                                              
                                        }
                                    }       
                                },
                                {                                             
                                    view:"button", label:"Print (ปริ้นเอกสาร)", type:"form",width:230,id:"Print_Pick_pl",
                                    on:
                                    {                                                   
                                        onItemClick:function(id, e)
                                        {
                                            if ($$('reprintPicking_form1').validate())
                                                {
                                                    var pickno = $$('pl_pick_no').getValue();
                                                  
                                                    $.post( "print/pickinglist.php", {data1:pickno})
                                                    .done(function( data ) 
                                                    {
                                                        var data = eval('('+data+')');
                                                        if(data.ch == 1)
                                                        {
                                                          webix.message({ type:"default",expire:7000, text:'ปริ้น PICK สำเร็จ'});
                                                        }
                                                        else webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});
                                                    });
                                                            
                                                }                                                    
                                        }
                                    }       
                                },{}
                            ]
                        }                                              
                    ]
                    
                }
            ],on:
            {
                onHide:function()
                {
                    
                },
                onShow:function()
                {

                },
                onAddView:function()
                {

                }
            }
        }
    };
};