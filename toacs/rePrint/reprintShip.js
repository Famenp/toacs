var header_reprintShip = function()
{
	return {
        view: "scrollview",
        scroll: "native-y",
        id:"header_reprintShip",
        body: 
        {
        	id:"reprintShip_id",
        	type:"clean",
    		rows:
    		[
    		    {
                    view:"form",
                    paddingY:20,
                    id:"reprintShip_form1",
                    elements:
                    [
                        {                            
                            cols:                                    
                                [    
                                   
                                    {
                                        view:"text",label:'SHP No',required:true,name:"shp_pick_no",id:"shp_pick_no",labelPosition:"top", value:"",width:230
                                    },
                                    {}                          
                                ]
                        },
                        {
                            cols:
                            [
                                {                                             
                                    view:"button", label:"View (ดูตัวอย่างเอกสาร)",width:230,id:"viewshp_print",
                                    on:
                                    {                                                   
                                        onItemClick:function(id, e)
                                        {
                                            if($$('reprintShip_form1').validate())
                                            {
                                                var shpno = $$('shp_pick_no').getValue();
                                              
                                                window.open("print/shpmview.php?data="+shpno, '_blank');   
                                            }                                              
                                        }
                                    }       
                                },
                                {                                             
                                    view:"button", label:"Print (ปริ้นเอกสาร)", type:"form",width:230,id:"viewPrint_Pick",
                                    on:
                                    {                                                   
                                        onItemClick:function(id, e)
                                        {
                                            if ($$('reprintShip_form1').validate())
                                                {
                                                    var shpno = $$('shp_print_name').getValue();
                                                 
                                                    $.post( "print/shpm.php", {data1:shpno})
                                                    .done(function( data ) 
                                                    {
                                                        var data = eval('('+data+')');
                                                        if(data.ch == 1)
                                                        {
                                                          webix.message({ type:"default",expire:7000, text:'ปริ้น SHIP สำเร็จ'});
                                                        }
                                                        else webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});
                                                    });
                                                            
                                                }                                                    
                                        }
                                    }       
                                },{}
                            ]
                        } 
                    ]
                }
            ],on:
            {
                onHide:function()
                {
                    
                },
                onShow:function()
                {

                },
                onAddView:function()
                {

                }
            }
        }
    };
};