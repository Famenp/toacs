<?php
include('../start.php');
include('../php/connection.php');
require_once('tcpdf/tcpdf.php');
session_start();
$permision = $_SESSION['xxxPermission'];
if ($permision=="ADMIN") {
		$group = "group 1";
	}
if ($permision == "SUPPORT") {
	$group = "group 1";
}
if ($permision == "PICKG1") {
	$group = "group 1";
}
if ($permision == "PICKG2") {
	$group = "group 2";
}
if ($permision == "PICKG3") {
	$group = "group 3";
}
if ($permision == "PICKG4") {
	$group = "group 4";
}
$data = $_REQUEST['data'];

$printgroup = strtoupper(preg_replace("/\s/", '',$group));

$pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetMargins(10, 5, 10,5);
$pdf->SetAutoPageBreak(TRUE, 0);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

$pdf->setFontSubsetting(true);

$pdf->SetFont('freeserif', '');

$pdf->AddPage();

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

if ($group == "group 1") {
	$query  = "CALL SP_Picking_PrintSummarySheet('$data') ";
	if ($result = $mysqli->query($query)) 
	{
		while ($row = $result->fetch_assoc()) 
		{
			$trip = $row["Trip_Number"];
			$RotationNo = $row["Rotation_Number"];
			$partG = $row["Part_Group"];
			$dollyNo =$row["Dolly_Number"];
			$dDate = $row["Delivery_Date"];
			$refnum =$row["Reference_Number"];
			$page = $row["Page_Number"];
			$round = $row["Round_Trip"];
			if ($dDate =='') {
				echo "PICK PART ยังไม่สำเร็จ";
				exit();
			}
			if ($mysqli->next_result()) 
			{
				if ($result = $mysqli->store_result()) 
                	{
                		//headdetail partname
                		while ($row2 = $result->fetch_assoc())
                		{
                			$colh1 = $row2["Column_1"];
                			$colh2 = $row2["Column_2"];
                			$colh3 = $row2["Column_3"];
                			$colh4 = $row2["Column_4"];
                			$colh5 = $row2["Column_5"];
                			$colh6 = $row2["Column_6"];
                			if ($mysqli->next_result()) 
                			{
                				if ($result = $mysqli->store_result()) 
                				{	
                					while ($row3 = $result->fetch_assoc())
	                				{
	                					$cold1 = $row3["Column_1"];
			                			$cold2 = $row3["Column_2"];
			                			$cold3 = $row3["Column_3"];
			                			$cold4 = $row3["Column_4"];
			                			$cold5 = $row3["Column_5"];
			                			$cold6 = $row3["Column_6"];
			                			if ($mysqli->next_result()) 
			                			{
			                				$arsummdetail = array();
											 if ($result = $mysqli->store_result()) 
											 {
											 	 while ($row4 = $result->fetch_assoc()) 
											 	 {
											 	 	//dataset2
											 	 	$arsummdetail[] = $row4["Rotation_Number"].",".$row4["Line"].",".$row4["Supplier_Code"].","
											 	 					.$row4["Part_Number_1"].",".$row4["Part_Number_2"].",".$row4["Part_Number_3"].",".$row4["Part_Number_4"]
										            				.",".$row4["Part_Number_5"].",".$row4["Part_Number_6"].",".$row4["Part_Number_7"];
											 	 }
											 }
			                			}
	                				}
                				}
         
                			}
                		}
                	}
			}
		}
	}
	$pdf->SetTitle('SUMMARYG1');
	$barcodeSUM= TCPDF_STATIC::serializeTCPDFtagParameters(array($refnum, 'C128', '', '', 0, 10, 0.4, array('position'=>'R', 'border'=>false, 'padding'=>1, 'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>true, 'font'=>'helvetica', 'fontsize'=>7, 'stretchtext'=>4,'cellfitalign'=>'R','stretch'=>true), 'N'));
	$n=0;
	$r=1;
	$html = createHead($barcodeSUM,$trip,$RotationNo,$partG,$dollyNo,$dDate,$refnum,$page,$round);
	$headdata = createheaddata($colh1,$colh2,$colh3,$colh4,$colh5,$colh6,$cold1,$cold2,$cold3,$cold4,$cold5,$cold6);
	while ( $n < 16) {
		$data= explode(",",$arsummdetail[$n]);
		$headdata .= createdata($r,$data[0],$data[1],$data[2],$data[3],$data[4],$data[5],$data[6],$data[7],$data[8],$data[9]);
		$n++;$r++;
	}
	$html .= $headdata;
	$html .= footter();
	$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
}

else if ($group == "group 2") {
	$query  = "CALL SP_Picking_PrintSummarySheet('$data') ";
	if ($result = $mysqli->query($query)) 
	{
		while ($row = $result->fetch_assoc()) 
		{
			$trip = $row["Trip_Number"];
			$RotationNo = $row["Rotation_Number"];
			$partG = $row["Part_Group"];
			$dollyNo =$row["Dolly_Number"];
			$dDate = $row["Delivery_Date"];
			$refnum =$row["Reference_Number"];
			$page = $row["Page_Number"];
			$round = $row["Round_Trip"];
			if ($dDate =='') {
				echo "PICK PART ยังไม่สำเร็จ";
				exit();
			}
			if ($mysqli->next_result()) 
			{
				if ($result = $mysqli->store_result()) 
                	{
                		//headdetail partname
                		while ($row2 = $result->fetch_assoc())
                		{
                			$colh1 = $row2["Column_1"];
                			$colh2 = $row2["Column_2"];
                		
                			if ($mysqli->next_result()) 
                			{
                				if ($result = $mysqli->store_result()) 
                				{	
                					while ($row3 = $result->fetch_assoc())
	                				{
	                					$cold1 = $row3["Column_1"];
			                			$cold2 = $row3["Column_2"];
			                			
			                			if ($mysqli->next_result()) 
			                			{
			                				$arsummdetail = array();
											 if ($result = $mysqli->store_result()) 
											 {
											 	 while ($row4 = $result->fetch_assoc()) 
											 	 {
											 	 	//dataset2
											 	 	$arsummdetail[] = $row4["Rotation_Number"].",".$row4["Line"].",".$row4["Supplier_Code"].","
											 	 					.$row4["Part_Number_1"].",".$row4["Part_Number_2"].",".$row4["Part_Number_3"];
											 	 }
											 }
			                			}
	                				}
                				}
         
                			}
                		}
                	}
			}
		}
	}
	$pdf->SetTitle('SUMMARYG2');
	$barcodeSUM= TCPDF_STATIC::serializeTCPDFtagParameters(array($refnum, 'C128', '', '', 0, 10, 0.4, array('position'=>'R', 'border'=>false, 'padding'=>1, 'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>true, 'font'=>'helvetica', 'fontsize'=>7, 'stretchtext'=>4,'cellfitalign'=>'R','stretch'=>true), 'N'));
	$n=0;
	$r=1;
	$html = createHead($barcodeSUM,$trip,$RotationNo,$partG,$dollyNo,$dDate,$refnum,$page,$round);
	$headdata = createheaddata2($colh1,$colh2,$cold1,$cold2);
	while ( $n < 16) {
		$data= explode(",",$arsummdetail[$n]);
		$headdata .= createdata2($r,$data[0],$data[1],$data[2],$data[3],$data[4],$data[5]);
		$n++;$r++;
	}
	$html .= $headdata;
	$html .= footter();
	$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
}
else if ($group == "group 3") {
	$query  = "CALL SP_Picking_PrintSummarySheet('$data') ";
	if ($result = $mysqli->query($query)) 
	{
		while ($row = $result->fetch_assoc()) 
		{
			$trip = $row["Trip_Number"];
			$RotationNo = $row["Rotation_Number"];
			$partG = $row["Part_Group"];
			$dollyNo =$row["Dolly_Number"];
			$dDate = $row["Delivery_Date"];
			$refnum =$row["Reference_Number"];
			$page = $row["Page_Number"];
			$round = $row["Round_Trip"];
			if ($dDate =='') {
				echo "PICK PART ยังไม่สำเร็จ";
				exit();
			}
			if ($mysqli->next_result()) 
			{
				if ($result = $mysqli->store_result()) 
                	{
                		//headdetail partname
                		while ($row2 = $result->fetch_assoc())
                		{
                			$colh1 = $row2["Column_1"];
                			$colh2 = $row2["Column_2"];
                		
                			if ($mysqli->next_result()) 
                			{
                				if ($result = $mysqli->store_result()) 
                				{	
                					while ($row3 = $result->fetch_assoc())
	                				{
	                					$cold1 = $row3["Column_1"];
			                			$cold2 = $row3["Column_2"];
			                			
			                			if ($mysqli->next_result()) 
			                			{
			                				$arsummdetail = array();
											 if ($result = $mysqli->store_result()) 
											 {
											 	 while ($row4 = $result->fetch_assoc()) 
											 	 {
											 	 	//dataset2
											 	 	$arsummdetail[] = $row4["Rotation_Number"].",".$row4["Line"].",".$row4["Supplier_Code"].","
											 	 					.$row4["Part_Number_1"].",".$row4["Part_Number_2"].",".$row4["Part_Number_3"];
											 	 }
											 }
			                			}
	                				}
                				}
         
                			}
                		}
                	}
			}
		}
	}
	$pdf->SetTitle('SUMMARYG3');
	$barcodeSUM= TCPDF_STATIC::serializeTCPDFtagParameters(array($refnum, 'C128', '', '', 0, 10, 0.4, array('position'=>'R', 'border'=>false, 'padding'=>1, 'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>true, 'font'=>'helvetica', 'fontsize'=>7, 'stretchtext'=>4,'cellfitalign'=>'R','stretch'=>true), 'N'));
	$n=0;
	$r=1;
	$html = createHead($barcodeSUM,$trip,$RotationNo,$partG,$dollyNo,$dDate,$refnum,$page,$round);
	$headdata = createheaddata3($colh1,$colh2,$cold1,$cold2);
	while ( $n < 16) {
		$data= explode(",",$arsummdetail[$n]);
		$headdata .= createdata3($r,$data[0],$data[1],$data[2],$data[3],$data[4],$data[5]);
		$n++;$r++;
	}
	$html .= $headdata;
	$html .= footter();
	$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
}
else if ($group == "group 4"){
	$query  = "CALL SP_Picking_PrintSummarySheet('$data') ";
	if ($result = $mysqli->query($query)) 
	{
		while ($row = $result->fetch_assoc()) 
		{
			$trip = $row["Trip_Number"];
			$RotationNo = $row["Rotation_Number"];
			$partG = $row["Part_Group"];
			$dollyNo =$row["Dolly_Number"];
			$dDate = $row["Delivery_Date"];
			$refnum =$row["Reference_Number"];
			$page = $row["Page_Number"];
			$round = $row["Round_Trip"];
			if ($dDate =='') {
				echo "PICK PART ยังไม่สำเร็จ";
				exit();
			}
			if ($mysqli->next_result()) 
			{
				if ($result = $mysqli->store_result()) 
                	{
                		//headdetail partname
                		while ($row2 = $result->fetch_assoc())
                		{
                			$colh1 = $row2["Column_1"];
                			$colh2 = $row2["Column_2"];
                			$colh3 = $row2["Column_3"];
                			$colh4 = $row2["Column_4"];
                			$colh5 = $row2["Column_5"];
                		
                			if ($mysqli->next_result()) 
                			{
                				if ($result = $mysqli->store_result()) 
                				{	
                					while ($row3 = $result->fetch_assoc())
	                				{
	                					$cold1 = $row3["Column_1"];
			                			$cold2 = $row3["Column_2"];
			                			$cold3 = $row3["Column_3"];
			                			$cold4 = $row3["Column_4"];
			                			$cold5 = $row3["Column_5"];
			                			
			                			if ($mysqli->next_result()) 
			                			{
			                				$arsummdetail = array();
											 if ($result = $mysqli->store_result()) 
											 {
											 	 while ($row4 = $result->fetch_assoc()) 
											 	 {
											 	 	//dataset2
											 	 	$arsummdetail[] = $row4["Rotation_Number"].",".$row4["Line"].",".$row4["Supplier_Code_1"].","
											 	 					.$row4["Part_Number_1"].",".$row4["Supplier_Code_2"].",".$row4["Part_Number_2"].","
											 	 					.$row4["Supplier_Code_3"].",".$row4["Part_Number_3"].",".$row4["Item_No"];
											 	 }
											 }
			                			}
	                				}
                				}
         
                			}
                		}
                	}
			}
		}
	}
	$pdf->SetTitle('SUMMARYG3');
	$barcodeSUM= TCPDF_STATIC::serializeTCPDFtagParameters(array($refnum, 'C128', '', '', 0, 10, 0.4, array('position'=>'R', 'border'=>false, 'padding'=>1, 'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>true, 'font'=>'helvetica', 'fontsize'=>7, 'stretchtext'=>4,'cellfitalign'=>'R','stretch'=>true), 'N'));
	$n=0;
	$r=1;
	$html = createHead($barcodeSUM,$trip,$RotationNo,$partG,$dollyNo,$dDate,$refnum,$page,$round);
	$headdata = createheaddata4($colh1,$colh2,$colh3,$colh4,$colh5,$cold1,$cold2,$cold3,$cold4,$cold5);
	while ( $n < 8) {
		$data= explode(",",$arsummdetail[$n]);
		$headdata .= createdata4($r,$data[0],$data[1],$data[2],$data[3],$data[4],$data[5],$data[6],$data[7],$data[8]);
		$n++;$r++;
	}
	$html .= $headdata;
	$html .= footter();
	$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
}

		

// $tableData .='</table>';
// $html .= $tableData;
// $html .= createbox();
// $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
// echo $html;

// 	$c++;
// }
$randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0,2);
$pdf->Output("C:\\Report\\SUM".$printgroup.$refnum.$randomString.'.pdf', 'F');
echo '{"ch":1,"data":"DONE"}';


function createHead($barcodeSUM,$trip,$RotationNo,$partG,$dollyNo,$dDate,$refnum,$page,$round)
{
	$headData = '<table border="0">
					<tr style="font-size:400px;" >
						<td align="center" width="670"><b>'.$trip.'</b></td>
					</tr>
					<tr style="font-size:24px;">
						<td align="right"><p >Page :'.$page.'</p></td>
					</tr>
					<tr style="font-size:30px;" >
						<td align="left" width="350"><b>Rotation No. '.$RotationNo.'</b></td>
						<td align="left" width="120"></td>
						<td align="left" width="150" style="font-size: 20px;"><b>TTV Round No.</b></td>
						<td align="center" style="width: 50px; height: 40px; border: 3px solid #000;vertical-align: top;">'.$round.'</td>
					</tr>
					<tr style="font-size:13px;" >
						<td align="left" width="90"><b>Dolly Number :</b></td>
						<td align="left" width="70">'.$dollyNo.'</td>
						<td align="left" width="90"><b>Delivery Date :</b></td>
						<td align="left" width="80">'.$dDate.'</td>
						<td align="center" width="140"><b>Summary Sheet No. : </b></td>
						<td align="left" width="205" rowspan="2"><tcpdf method="write1DBarcode" params="'.$barcodeSUM.'"/></td>
					</tr>
					<tr style="font-size:12px;" >
						<td align="left" width="90"><b>Part Group: </b></td>
						<td align="left" width="580">'.$partG.'</td>
					</tr></table>';
	return $headData;
}
function createheaddata($colh1,$colh2,$colh3,$colh4,$colh5,$colh6,$cold1,$cold2,$cold3,$cold4,$cold5,$cold6)
{
	$headbox = '<table style="font-size: 9px;" border="1">
					<tbody>
						<tr style="background-color:#b5b5b5;">
							<td align="left" width="15" style="font-size: 6px;"><b>No</b></td>
							<td align="center" width="35" style="font-size: 7px;"><b>Rotation</b></td>
							<td align="center" width="33" style="font-size: 7px;"><b>Model</b></td>
							<td align="center" width="38" style="font-size: 8px;"><b>Supplier</b></td>
							<td align="center" width="87" style="font-size: 8px;" ><b>'.$colh1.'<br>'.$cold1.'</b></td>
							<td align="center" width="87" style="font-size: 8px;" ><b>'.$colh2.'<br>'.$cold2.'</b></td>
							<td align="center" width="93" style="font-size: 8px;" ><b>'.$colh3.'<br>'.$cold3.'</b></td>
							<td align="center" width="93" style="font-size: 8px;" ><b>'.$colh4.'<br>'.$cold4.'</b></td>
							<td align="center" width="93" style="font-size: 8px;" ><b>'.$colh5.'<br>'.$cold5.'</b></td>
							<td align="center" width="95" style="font-size: 8px;" ><b>'.$colh6.'<br>'.$cold6.'</b></td>
						</tr>
					</tbody>
			</table>';
		return $headbox;
}
function createheaddata2($colh1,$colh2,$cold1,$cold2)
{
	$headbox = '<table style="font-size: 10px;" border="1">
					<tbody>
						<tr style="background-color:#b5b5b5;">
							<td align="left" width="25" rowspan="2"><b>No</b></td>
							<td align="center" width="60" rowspan="2"><b>Rotation</b></td>
							<td align="center" width="60" rowspan="2"><b>Model</b></td>
							<td align="center" width="65" ><b>Supplier</b></td>
							<td align="center" width="230"><b>'.$colh1.'<br>'.$cold1.'</b></td>
							<td align="center" width="230"><b>'.$colh2.'<br>'.$cold2.'</b></td>
						</tr>
					</tbody>
			</table>';
		return $headbox;
}
function createheaddata3($colh1,$colh2,$cold1,$cold2)
{
	$headbox = '<table style="font-size: 10px;" border="1">
					<tbody>
						<tr style="background-color:#b5b5b5;">
							<td align="left" width="25" rowspan="2"><b>No</b></td>
							<td align="center" width="60" rowspan="2"><b>Rotation</b></td>
							<td align="center" width="60" rowspan="2"><b>Model</b></td>
							<td align="center" width="65" ><b>Supplier</b></td>
							<td align="center" width="230"><b>'.$colh1.'<br>'.$cold1.'</b></td>
							<td align="center" width="230"><b>'.$colh2.'<br>'.$cold2.'</b></td>
						</tr>
					</tbody>
			</table>';
		return $headbox;
}
function createheaddata4($colh1,$colh2,$colh3,$colh4,$colh5,$cold1,$cold2,$cold3,$cold4,$cold5)
{
	$headbox = '<table style="font-size: 10px;" border="1">
					<tbody>
						<tr style="background-color:#b5b5b5;">
							<td align="left" width="25" rowspan="2"><b>No</b></td>
							<td align="center" width="60" rowspan="2"><b>Rotation</b></td>
							<td align="center" width="60" rowspan="2"><b>Model</b></td>
							<td align="center" width="65" ><b>'.$colh1.'</b></td>
							<td align="center" width="200"><b>'.$colh2.'<br>'.$cold2.'</b></td>
							<td align="center" width="65" ><b>'.$colh3.'</b></td>
							<td align="center" width="200"><b>'.$colh4.'<br>'.$cold4.'</b></td>
						</tr>
					</tbody>
			</table>';
		return $headbox;
}
function createdata($r,$Rotation_Number,$Line,$Supplier_Code,$Part_Number_1,$Part_Number_2,$Part_Number_3,$Part_Number_4,$Part_Number_5,$Part_Number_6,$Part_Number_7)
{
	$boxdata = '
			<table style="font-size: 10px;" border="1">
					<tbody>
					<tr >
						<td align="center" width="15" style="font-size: 7px;">'.$r.'</td>
						<td align="center" width="35">'.$Rotation_Number.'</td>
						<td align="center" width="33" style="font-size: 7px;">'.$Line.'</td>
						<td align="center" width="38" style="font-size: 7px;">'.$Supplier_Code.'</td>
						<td align="left" width="87">'.$Part_Number_1.'</td>
						<td align="left" width="87">'.$Part_Number_2.'</td>
						<td align="left" width="93">'.$Part_Number_3.'</td>
						<td align="left" width="93">'.$Part_Number_4.'</td>
						<td align="left" width="93">'.$Part_Number_5.'</td>
						<td align="left" width="95">'.$Part_Number_6.'</td>
					</tr>
					</tbody>
			</table>';
	return $boxdata;
}
function createdata2($r,$Rotation_Number,$Line,$Supplier_Code,$Part_Number_1,$Part_Number_2,$Part_Number_3)
{
	$boxdata = '
			<table style="font-size: 12px;" border="1">
					<tbody>
					<tr >
						<td align="center" width="25">'.$r.'</td>
						<td align="center" width="60">'.$Rotation_Number.'</td>
						<td align="center" width="60">'.$Line.'</td>
						<td align="center" width="65">'.$Supplier_Code.'</td>
						<td align="center" width="230">'.$Part_Number_1.'</td>
						<td align="center" width="230">'.$Part_Number_2.'</td>
					</tr>
					</tbody>
			</table>';
	return $boxdata;
}
function createdata3($r,$Rotation_Number,$Line,$Supplier_Code,$Part_Number_1,$Part_Number_2,$Part_Number_3)
{
	$boxdata = '
			<table style="font-size: 12px;" border="1">
					<tbody>
					<tr >
						<td align="center" width="25">'.$r.'</td>
						<td align="center" width="60">'.$Rotation_Number.'</td>
						<td align="center" width="60">'.$Line.'</td>
						<td align="center" width="65">'.$Supplier_Code.'</td>
						<td align="center" width="230">'.$Part_Number_1.'</td>
						<td align="center" width="230">'.$Part_Number_2.'</td>
					</tr>
					</tbody>
			</table>';
	return $boxdata;
}
function createdata4($r,$Rotation_Number,$Line,$Supplier_Code1,$Part_Number_1,$Supplier_Code2,$Part_Number_2,$Supplier_Code3,$Part_Number_3,$Item_No)
{
	$boxdata = '
			<table style="font-size: 12px;" border="1">
					<tbody>
					<tr >
						<td align="center" width="25">'.$Item_No.'</td>
						<td align="center" width="60">'.$Rotation_Number.'</td>
						<td align="center" width="60">'.$Line.'</td>
						<td align="center" width="65">'.$Supplier_Code1.'</td>
						<td align="center" width="200">'.$Part_Number_1.'</td>
						<td align="center" width="65">'.$Supplier_Code2.'</td>
						<td align="center" width="200">'.$Part_Number_2.'</td>
					</tr>
					</tbody>
			</table>';
	return $boxdata;
}
function footter()
{
	$foot = '<br><br>
			<table style="font-size: 14px;" border="0" >
					<tbody>
					<tr  >
						<td align="center" width="330">
							<p>
								<span>&nbsp;</span><br>
								<span>Picker By (TTV)</span><br>
								<span>&nbsp;</span><br>
								<span>___________________</span><br>
								<span>Operation</span><br>
							</p>
						</td>
						<td align="center" width="330">
							<p>
								<span>&nbsp;</span><br>
								<span>Check By (TTV)</span><br>
								<span>&nbsp;</span><br>
								<span>___________________</span><br>
								<span>QA Final Check</span><br>
							</p>
						</td>
					</tr>
					</tbody>
			</table>';

	return $foot;
}
