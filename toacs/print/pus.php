<?php
include('../php/connection.php');
// $data = $_REQUEST['data'];
// $pus = explode(",", $data);
// if ($pus[0]=='') {
// 	echo "กรุณาเลือกPickup Sheet";
// 	exit();
// }
// /*print_r($pus);*/
// $query  = "CALL SP_PUS_PrintPUS('$pus[0]')";
// if ($result = $mysqli->query($query)) 
// {
//     while ($row = $result->fetch_assoc()) 
//     {
//     	$PUSNO =  $row["PUS_Number"];
//     	$COMP =  $row["Company_Name"];
//     	$COMPADDS =  $row["Company_Address"];
//     	$SHPF =  $row["Ship_Form"];
//     	$SHPT =  $row["Ship_To"];
//     	$PDATE =  $row["Pickup_Date"];
//     	$ETDW =  $row["ETD_Warehouse"];
//     	$ETAS =  $row["ETA_Supplier"];
//     	$ETAW =  $row["ETA_Warehouse"];
//     	$TNUM =  $row["Truck_Number"];
//     	$TTYPE =  $row["Truck_Type"];
//     	$DNAME =  $row["Driver_Name"];
//     	$DPHONE =  $row["Telephone"];
//     	$REMARK =  $row["Remarks"];
//     	$PET =  $row["Petition_Number"];
//     	$FDATE =  $row["Forecast_Date"];
//     	$TTQTY =  $row["Total_Qty"];
//     	$TTBOXES =  $row["Total_Boxes"];
//     	$TTPALLETS =  $row["Total_Pallets"];
//     	$TTCBM =  $row["Total_CBM"];
//     	$BPTB =  $row["Boxes_PTB"];
//     	$BCRB =  $row["Boxes_CRB"];
//     	$BPTP =  $row["Boxes_PTP"];
//     	$BSTR =  $row["Boxes_STR"];
//         if ($mysqli->next_result()) 
//         	{
// 	            // printf("-----------------\n");
// 	            $arpusdetail = array();
// 	            if ($result = $mysqli->store_result()) 
// 		        {
// 		            while ($row2 = $result->fetch_assoc()) 
// 		            {
// 		            	$arpusdetail[] =  $row2["Item_No"].",".$row2["Part_Number"].",".$row2["Part_Name"].",".$row2["Qty"].",".$row2["SNP"].",".$row2["Boxes"].",".$row2["Pallets"]
// 		            						.",".$row2["CBM"].",".$row2["Package_Type"];
// 		            }
// 						// print_r($arpusdetail);
// 		            if ($mysqli->next_result()) 
// 		                {
// 		                	// printf("-----------------\n");
// 		                	$arparttag = array();
// 		                	if ($result = $mysqli->store_result()) 
// 		                	{
// 		                		while ($row3 = $result->fetch_assoc())
// 		                		{
// 		                			$arparttag[] = $row3["Tag_Number"].",".$row3["Pallet_No"].",".$row3["Part_Number"].",".$row3["Part_Name"].",".$row3["Qty"].",".$row3["SNP"].","
// 		                			.$row3["Supplier_Part_Number"].",".$row3["Supplier_Dock"];
// 		                		}
// 		                		// print_r($arparttag);
// 		                		if ($mysqli->next_result()) 
// 					                {
// 					                	// printf("-----------------\n");
// 					                	if ($result = $mysqli->store_result()) 
// 					                	{
// 					                		while ($row4 = $result->fetch_assoc())
// 					                		{
// 					                			$pitemnum = $row4["Item_Number"];
// 					                			$pttitem = $row4["Total_Items"];
// 					                			$pQTY = $row4["Total_Qty"];
// 					                			$pWeight = $row4["Total_Weight_Kg"];
// 					                			$pPrice = $row4["Total_Price"];
// 					                			$petNo = $row4["Petition_Number"];
// 					                			$plantName =$row4["Plant_Name_Th"];
// 					                			$petDate = $row4["Petition_Date_Th"];
// 					                			$cuslicen1 = $row4["Customs_License_1"];
// 					                			$cuslicen2 =$row4["Customs_License_2"];
// 					                			$wKeep = $row4["Warehouse_Kepping"];
// 					                			$wADD =$row4["Warehouse_Address_Th"];
// 					                			$petReturn =$row4["Petition_Return_Date_Th"];
// 					                			$cOfficer =$row4["Customs_Officer"];
// 					                			$desp =$row4["Description"];
// 					                		}
// 					                	}
// 					                }
// 		                	}
// 		                }
		           
// 		        }
// 	        }
	
//     }
//     $result->free();
// }

// print_r($arpusdetail);
// print_r($arparttag);
require_once('tcpdf/tcpdf.php');
$pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetTitle('PUSNO');
$pdf->SetMargins(10, 5, 10,5);
$pdf->SetAutoPageBreak(TRUE, 0);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

$pdf->setFontSubsetting(true);

$pdf->SetFont('freeserif', '');

// $pdf->AddPage();

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
//
$thead = '<table border="1" cellspacing="0" cellpadding="2">
	<tr style="font-size:10px;background-color:#C8C8C8;" >
		<td align="center" width="20"><b>No. </b></td>
		<td align="center" width="100"><b>Part Number</b></td>
		<td align="center" width="100"><b>Part Name</b></td>
		<td align="center" width="60"><b>Qty.</b></td>
		<td align="center" width="60"><b>SNP</b></td>
		<td align="center" width="60"><b>Boxes</b></td>
		<td align="center" width="60"><b>Pallets</b></td>
		<td align="center" width="60"><b>CBM</b></td>
		<td align="center" width="50"><b>Pkg.Type</b></td>
		<td align="center" width="100"><b>Remarks</b></td>
	</tr>';
// Set some content to print


$barcodePUS= TCPDF_STATIC::serializeTCPDFtagParameters(array('1234', 'C128', '', '', 0, 10, 0.4, array('position'=>'R', 'border'=>false, 'padding'=>1, 'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>true, 'font'=>'helvetica', 'fontsize'=>5, 'stretchtext'=>4,'cellfitalign'=>'R','stretch'=>true), 'N'));

$ndetail = 40;
$rpus = 0;
$c = 0 ;
$field = 0;
$n = 1;
$d = 30;
$p = $d*$n;
$tableData = $thead;
		while ($c <= $ndetail) 
		{
			$c++;
			if ($field == 0) 
			{
				$field = 1;
				$pdf->AddPage();
				$html = createHead('page 1/',$barcodePUS);
			}
			if($c > $p)
    	      {
    	      	$c++;
    	      	$n++;
    	      	$p = $d*$n;
    	      	$tableData .='</table>';
    	      	$html .= $tableData;
    	      	$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
    	      	$pdf->AddPage();
    	      	$html = createHead('page 1/',$barcodePUS);
    	      	$tableData = $thead;
    	      }
    	      	$tableData .= '<tr style="font-size:10px" >';
				$tableData .= '<td align="center" width="20">'.$rpus.'</td>';
				$tableData .= '<td align="center" width="100"></td>';
				$tableData .= '<td align="center" width="100"></td>';
				$tableData .= '<td align="center" width="60"></td>';
				$tableData .= '<td align="center" width="60"></td>';
				$tableData .= '<td align="center" width="60"></td>';
				$tableData .= '<td align="center" width="60"></td>';
				$tableData .= '<td align="center" width="60"></td>';
				$tableData .= '<td align="center" width="50"></td>';
				$tableData .= '<td align="center" width="100"></td>';
				$tableData .= '</tr>';
				$rpus++;
		}
			$tableData .= '<tr style="font-size:12px" >';
			$tableData .= '<td align="center" width="120"></td>';
			$tableData .= '<td align="right" width="100">Total:</td>';
			$tableData .= '<td align="center" width="60">TTQTY</td>';
			$tableData .= '<td align="center" width="60"></td>';
			$tableData .= '<td align="center" width="60"></td>';
			$tableData .= '<td align="center" width="60"></td>';
			$tableData .= '<td align="center" width="60"></td>';
			$tableData .= '<td align="center" width="50"></td>';
			$tableData .= '<td align="center" width="100"></td>';
			$tableData .= '</tr>';
			$tableData .='</table>';
			$html .= $tableData;
			$html .= createbox();
			$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
		

// $tableData .='</table>';
// $html .= $tableData;
// $html .= createbox();
// $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
// echo $html;

// 	$c++;
// }

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('PUSNO'.'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+

function createHead($page,$barcodePUS)
{
	$headData = str_format('<table border="0">
	<tr>
		<td align="right" style="font-size:11px">{1} </td>
	</tr>
</table>',$page.'1');

	$headData .= '<table border="0">
	<tr>
		<td align="left" width="460" style="font-size:11px"><b></b><br/>
		<br/>
		</td>
		<td width="100"><img src="images/aat-logo.gif" width="160"  height="46"/></td>
		<td width="100"><img src="images/ttv-logo.gif" width="160"  height="46"/></td>
	</tr>
</table>
<hr>
<br>
<table  border="0">
	<tr>
		<td align="center"><b style="font-size:24px; margin-left:300px;">PICKUP SHEET - AAT WIRING</b></td>
	</tr>
</table>
<table border="0" style="margin-top:10px;" cellspacing="" cellpadding="2" style="font-size:11px">
	<tr>
		<td align="left" width="480"></td>
		<td align="left" width="190"><tcpdf method="write1DBarcode" params="'.$barcodePUS.'"/></td>
	</tr>
	<tr>
		<td align="left" width="80"><b>Ship From :</b></td>
		<td align="left" width="90"></td>
		<td align="left" width="80"><b>ETD W/H:</b></td>
		<td align="left" width="70"></td>
		<td align="left" width="80"><b>Truck Number:</b></td>
		<td align="left" width="100"></td>
		<td align="left" width="80"><b>PUS Number :</b></td>
		<td align="left" width="90"></td>
	</tr>
	<tr>
		<td align="left" width="80"><b>Ship To :</b></td>
		<td align="left" width="90"></td>
		<td align="left" width="80"><b>ETA Supplier:</b></td>
		<td align="left" width="70"></td>
		<td align="left" width="80"><b>Truck Type:</b></td>
		<td align="left" width="100"></td>
		<td align="center" width="170"></td>
	</tr>
	<tr>
		<td align="left" width="80"><b>Pickdup Date:</b></td>
		<td align="left" width="90"></td>
		<td align="left" width="80"><b>ETA W/H:</b></td>
		<td align="left" width="70"></td>
		<td align="left" width="80"><b>Driver Name :</b></td>
		<td align="left" width="100"></td>
		<td align="center" width="170"></td>
	</tr>
	<tr>
		<td align="left" width="80"><b>Forecast Date:</b></td>
		<td align="left" width="90"></td>
		<td align="left" width="80"></td>
		<td align="left" width="70"></td>
		<td align="left" width="80"><b>Phone No :</b></td>
		<td align="left" width="100"></td>
		<td align="left" width="80"><b>Petition #</b></td>
		<td align="left" width="90"></td>
	</tr>
	<tr>
		<td align="left" width="80"><b>Remark :</b></td>
		<td align="left" width="400"></td>
		<td align="center" width="190"></td>
	</tr>

</table><br>';
return $headData;
}
function createbox()
{
	$boxdata = '<br><br>
	<table style="font-size: 12px;" border="1">
		<tbody>
		<tr style="background-color:#C8C8C8;" >
			<th width="170">Remarks / หมายเหตุ</th>
			<th align="center" width="120">ชื่อผู้จ่ายสินค้า (Supplier)</th>
			<th align="center" width="120">ชื่อผู้รับสินค้า (Driver)</th>
			<th colspan="5" align="center" width="260">&nbsp; Packaging Type Record</th>
		</tr>
		<tr >
			<th rowspan="4"  align="center">&nbsp;&nbsp;&nbsp;&nbsp;</th><th rowspan="4" align="center">
			<p>&nbsp;</p>
			<p>_____________</p>
			<p><br />(____/____/____)</p>
			</th><th rowspan="4" align="center">
			<p>&nbsp;</p>
			<p>&nbsp;_____________</p>
			<p><br />(____/____/____)</p>
			</th>
			<th style="background-color:#C8C8C8;" rowspan="2" align="center">ยอดรับ</th>
			<th style="background-color:#C8C8C8;" align="center"></th>
			<th style="background-color:#C8C8C8;" align="center"></th>
			<th style="background-color:#C8C8C8;" align="center"></th>
			<th style="background-color:#C8C8C8;" align="center"></th>
		</tr>
		<tr style="background-color:#C8C8C8;">
			<th align="center">PTB</th>
			<th align="center">CBM</th>
			<th align="center">PTP</th>
			<th align="center">STD</th>
		</tr>
		<tr>
			<th style="background-color:#C8C8C8;font-size: 10px;" align="center">
				<p>แผนรับ</p>
				<p>&nbsp;</p>
			</th>
			<td><p align="center"><span><br></span></p></td>
			<td><p align="center"><span><br></span></p></td>
			<td><p align="center"><span><br></span></p></td>
			<td><p align="center"><span><br></span></p></td>
		</tr>
		<tr>
			<th style="background-color:#C8C8C8;font-size: 10px;" align="center">
				<p>รับจริง</p>
				<p>&nbsp;</p>
			</th>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		</tbody>
</table>';
	return $boxdata;
}
