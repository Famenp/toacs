<?php
require_once('tcpdf/tcpdf.php');
$pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetTitle('PUSNO');
$pdf->SetMargins(10, 5, 10,5);
$pdf->SetAutoPageBreak(TRUE, 0);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

$pdf->setFontSubsetting(true);

$pdf->SetFont('freeserif', '');

$barcodePUS= TCPDF_STATIC::serializeTCPDFtagParameters(array('12 34', 'C128', '', '', 0, 10, 0.4, array('position'=>'R', 'border'=>false, 'padding'=>1, 'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>true, 'font'=>'helvetica', 'fontsize'=>5, 'stretchtext'=>4,'cellfitalign'=>'R','stretch'=>true), 'N'));

$html = '<table  border="0">
			<tr>
				<td><tcpdf method="write1DBarcode" params="'.$barcodePUS.'"/></td>
			</tr>
		 </table>';

$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
$pdf->Output('PUSNO'.'.pdf', 'I');
?>