<?php
// ini_set('max_execution_time', 300);
include('../php/connection.php');
$data = $_REQUEST['data'];
$pus = explode(",", $data);
if ($pus[0]=='') {
	echo "กรุณาเลือกPickup Sheet";
	exit();
}
/*print_r($pus);*/
$query  = "CALL SP_PUS_PrintPUS('$pus[0]')";
if ($result = $mysqli->query($query)) 
{
	if ($result->num_rows == 0) 
	{
		echo 'ไม่พบ PUS No. '.$pus[0].' ในระบบ';
		$mysqli->close();
		exit();
	}
    while ($row = $result->fetch_assoc()) 
    {
    	$PUSNO =  $row["PUS_Number"];
    	$COMP =  $row["Company_Name"];
    	$COMPADDS =  $row["Company_Address"];
    	$SHPF =  $row["Ship_Form"];
    	$SHPT =  $row["Ship_To"];
    	$PDATE =  $row["Pickup_Date"];
    	$ETDW =  $row["ETD_Warehouse"];
    	$ETAS =  $row["ETA_Supplier"];
    	$ETAW =  $row["ETA_Warehouse"];
    	$TNUM =  $row["Truck_Number"];
    	$TTYPE =  $row["Truck_Type"];
    	$DNAME =  $row["Driver_Name"];
    	$DPHONE =  $row["Telephone"];
    	$REMARK =  $row["Remarks"];
    	$PET =  $row["Petition_Number"];
    	$FDATE =  $row["Forecast_Date"];
    	$TTQTY =  $row["Total_Qty"];
    	$TTBOXES =  $row["Total_Boxes"];
    	$TTPALLETS =  $row["Total_Pallets"];
    	$TTCBM =  $row["Total_CBM"];
    	$BPTB =  $row["Boxes_PTB"];
    	$BCRB =  $row["Boxes_CRB"];
    	$BPTP =  $row["Boxes_PTP"];
    	$BSTR =  $row["Boxes_STR"];
        if ($mysqli->next_result()) 
        	{
	            // printf("-----------------\n");
	            $arpusdetail = array();
	            if ($result = $mysqli->store_result()) 
		        {
		            while ($row2 = $result->fetch_assoc()) 
		            {
		            	$crow = $result->num_rows;
		            	$arpusdetail[] =  $row2["Item_No"].",".$row2["Part_Number"].",".$row2["Part_Name"].",".$row2["Qty"].",".$row2["SNP"].",".$row2["Boxes"].",".$row2["Pallets"]
		            						.",".$row2["CBM"].",".$row2["Package_Type"];
		            }
						// print_r($arpusdetail);
		            if ($mysqli->next_result()) 
		                {
		                	// printf("-----------------\n");
		                	$arparttag = array();
		                	if ($result = $mysqli->store_result()) 
		                	{
		                		while ($row3 = $result->fetch_assoc())
		                		{
		                			$arparttag[] = $row3["Tag_Number"].",".$row3["Pallet_No"].",".$row3["Part_Number"].",".$row3["Part_Name"].",".$row3["Qty"].",".$row3["SNP"].","
		                			.$row3["Supplier_Part_Number"].",".$row3["Supplier_Dock"].",".$row3["Pick_Location"];
		                		}
		                		// print_r($arparttag);
		                		if ($mysqli->next_result()) 
					                {
					                	// printf("-----------------\n");
					                	if ($result = $mysqli->store_result()) 
					                	{
					                		while ($row4 = $result->fetch_assoc())
					                		{
					                			$pitemnum = $row4["Item_Number"];
					                			$pttitem = $row4["Total_Items"];
					                			$pQTY = $row4["Total_Qty"];
					                			$pWeight = $row4["Total_Weight_Kg"];
					                			$pPrice = $row4["Total_Price"];
					                			$petNo = $row4["Petition_Number"];
					                			$plantName =$row4["Plant_Name_Th"];
					                			$petDate = $row4["Petition_Date_Th"];
					                			$cuslicen1 = $row4["Customs_License_1"];
					                			$cuslicen2 =$row4["Customs_License_2"];
					                			$wKeep = $row4["Warehouse_Kepping"];
					                			$wADD =$row4["Warehouse_Address_Th"];
					                			$petReturn =$row4["Petition_Return_Date_Th"];
					                			$cOfficer =$row4["Customs_Officer"];
					                			$desp =$row4["Description"];
					                		}
					                	}
					                }
		                	}
		                }
		           
		        }
	        }
	
    }
    $result->free();
}

// print_r($arpusdetail);
// print_r($arparttag);
require_once('tcpdf/tcpdf.php');
$pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetTitle($PUSNO);
$pdf->SetMargins(10, 5, 10,5);
$pdf->SetAutoPageBreak(TRUE, 0);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

$pdf->setFontSubsetting(true);

$pdf->SetFont('freeserif', '');

// $pdf->AddPage();

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
//
$thead = '<table border="1" cellspacing="0" cellpadding="2">
	<tr style="font-size:10px;background-color:#C8C8C8;" >
		<td align="center" width="20"><b>No. </b></td>
		<td align="center" width="100"><b>Part Number</b></td>
		<td align="center" width="160"><b>Part Name</b></td>
		<td align="center" width="50"><b>Qty.</b></td>
		<td align="center" width="50"><b>SNP</b></td>
		<td align="center" width="50"><b>Boxes</b></td>
		
		<td align="center" width="60"><b>CBM</b></td>
		<td align="center" width="50"><b>Pkg.Type</b></td>
		<td align="center" width="130"><b>Remarks</b></td>
	</tr>';
// Set some content to print

$data = 40;
$c = 0 ;
$field = 0;
$n = 1;
$d = 30;
$p = $d*$n;

$tableData = $thead;

$barcodePUS= $pdf->serializeTCPDFtagParameters(array($PUSNO, 'C128', '', '', 0, 10, 0.4, array('position'=>'R', 'border'=>false, 'padding'=>1, 'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>true, 'font'=>'helvetica', 'fontsize'=>5, 'stretchtext'=>4,'cellfitalign'=>'R','stretch'=>true), 'N'));

// $html = createHead('page 1/',$PUSNO,$COMP,$COMPADDS,$barcodePUS,$SHPF,$SHPT,$PDATE,$ETDW,$ETAS,$ETAW,$TNUM,$TTYPE,$DNAME,$DPHONE,$REMARK,$PET,$FDATE,$TTQTY,
// 	$TTBOXES,$TTPALLETS,$TTCBM,$BPTB,$BCRB,$BPTP,$BSTR);

// $allpage = ceil($ndetail/$d);
// $n = 0;
$rpus = 1;
$ndetail = count($arpusdetail);
// echo $ndetail;
$d = 29;
$ccpus = 0 ;
$field = 0;
$nn = 1;
$p = $d*$nn;
$allPage = ceil($crow/$d);
$tableData = $thead;
	while ( $ccpus <= $ndetail-1) {
		// echo $ccpus;
		// 	print_r($arpusdetail[$ccpus]);
			// $ccpus++;
			if ($field == 0) 
			{
				$field = 1;
				$pdf->AddPage();
				$html = createHead('page 1/'.$allPage,$PUSNO,$COMP,$COMPADDS,$barcodePUS,$SHPF,$SHPT,$PDATE,$ETDW,$ETAS,$ETAW,$TNUM,$TTYPE,$DNAME,$DPHONE,$REMARK,$PET,$FDATE,$TTQTY,
									$TTBOXES,$TTPALLETS,$TTCBM,$BPTB,$BCRB,$BPTP,$BSTR);
			}
			if($ccpus > $p)
    	      {
    	      	// $ccpus++;
    	      	$n++;
    	      	$nn++;
    	      	$p = $d*$nn;
    	      	$tableData .='</table>';
    	      	$html .= $tableData;
    	      	$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
    	      	$pdf->AddPage();
    	      	$html = createHead('page'.$n.'/'.$allPage,$PUSNO,$COMP,$COMPADDS,$barcodePUS,$SHPF,$SHPT,$PDATE,$ETDW,$ETAS,$ETAW,$TNUM,$TTYPE,$DNAME,$DPHONE,$REMARK,$PET,$FDATE,$TTQTY,
									$TTBOXES,$TTPALLETS,$TTCBM,$BPTB,$BCRB,$BPTP,$BSTR);
    	      	$tableData = $thead;
    	      }
    	      
				$data= explode(",",$arpusdetail[$ccpus]);
				// print_r($data);
				$tableData .= '<tr style="font-size:10px" >';
				$tableData .= '<td align="center" width="20">'.$rpus.'</td>';
				$tableData .= '<td align="center" width="100">'.$data[1].'</td>';
			  	$tableData .= '<td align="center" width="160">'.$data[2].'</td>';
			  	$tableData .= '<td align="center" width="50">'.$data[3].'</td>';
			  	$tableData .= '<td align="center" width="50">'.$data[4].'</td>';
			  	$tableData .= '<td align="center" width="50">'.$data[5].'</td>';
			  	// $tableData .= '<td align="center" width="60">'.$data[6].'</td>';
			  	$tableData .= '<td align="center" width="60">'.$data[7].'</td>';
			  	$tableData .= '<td align="center" width="50">'.$data[8].'</td>';
			  	$tableData .= '<td align="center" width="130"></td>';
			  	$tableData .= '</tr>';
			  	$ccpus++;$rpus++;
			}
		$tableData .= '<tr style="font-size:12px" >';
		$tableData .= '<td align="center" width="120"></td>';
		$tableData .= '<td align="right" width="160">Total:</td>';
		$tableData .= '<td align="center" width="50">'.$TTQTY .'</td>';
		$tableData .= '<td align="center" width="50"></td>';
		$tableData .= '<td align="center" width="50">'.$TTBOXES.'</td>';
		// $tableData .= '<td align="center" width="60">'.$TTPALLETS.'</td>';
		$tableData .= '<td align="center" width="60">'.$TTCBM.'</td>';
		$tableData .= '<td align="center" width="50"></td>';
		$tableData .= '<td align="center" width="130"></td>';
		$tableData .= '</tr>';
		$tableData .='</table>';
		$html .= $tableData;
		$html .= createbox($BPTB,$BCRB,$BPTP,$BSTR);
		$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// Print text using writeHTMLCell()
//parttag
if ($arparttag[0][4]!='0') {
	$nparttag = count($arparttag);
	$pagetag = ($nparttag/3);
	$c = 0 ;
	$npart = 0 ;
	// while ( $c < $pagetag) {
		$pdf->AddPage();
		while ( $npart < $nparttag) {
			$data2= explode(",",$arparttag[$npart]);
			$barcodeTAG= $pdf->serializeTCPDFtagParameters(array($data2[0], 'C128', '', '', 80, 10, 0.4, array('position'=>'C', 'border'=>false, 'padding'=>1, 'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>true, 'font'=>'helvetica', 'fontsize'=>5, 'stretchtext'=>4,'cellfitalign'=>'R','stretch'=>true), 'N'));

			$html = createmastertag($data2[0],$data2[1],$data2[2],$data2[3],$data2[4],$data2[5],$data2[6],$data2[7],$data2[8],$barcodeTAG,$FDATE,$SHPF,$PDATE,$PUSNO,$SHPT);
			$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
			$npart++;$c++;
			if ($c%3 == 0 && $c!=$nparttag ) {
				$pdf->AddPage();
			}
		}
}

$barcodeTRUCK= $pdf->serializeTCPDFtagParameters(array($PUSNO, 'C128', '', '', 60, 13, 0.4, array('position'=>'C', 'border'=>false, 'padding'=>1, 'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>true, 'font'=>'helvetica', 'fontsize'=>8, 'stretchtext'=>4,'cellfitalign'=>'C','stretch'=>false), 'N'));
$pdf->AddPage('L');
$html = truckheadder();
$html .= truckdetail($barcodeTRUCK,$PDATE,$PUSNO,$TNUM,$TTYPE,$DNAME,$DPHONE,$SHPF,$SHPT,$ETDW,$ETAS,$ETAW);
$html .= truckfoot();
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

if ($petNo != '') {
	$pdf->AddPage('P');
	$html = pettitionn($petNo,$plantName,$petDate,$cuslicen1,$cuslicen2,$wKeep,$wADD,$petReturn,$cOfficer,$pitemnum,$pttitem,$pQTY,$pWeight,$pPrice,$desp);
	$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
	$pdf->AddPage('P');
	$html = pettitionn2();
	$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
}

	// $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
// 	$c++;
// }

// ----------------------e-----------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output($PUSNO.'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+

function createHead($page,$PUSNO,$COMP,$COMPADDS,$barcodePUS,$SHPF,$SHPT,$PDATE,$ETDW,$ETAS,$ETAW,$TNUM,$TTYPE,$DNAME,$DPHONE,$REMARK,$PET,$FDATE,$TTQTY,
	$TTBOXES,$TTPALLETS,$TTCBM,$BPTB,$BCRB,$BPTP,$BSTR)
{
	$headData = str_format('<table border="0">
	<tr>
		<td align="right" style="font-size:11px">{1} </td>
	</tr>
</table>',$page);

	$headData .= '<table border="0">
	<tr>
		<td align="left" width="460" style="font-size:11px"><b>'.$COMP.'</b><br/>
		'.$COMPADDS.'<br/>
		</td>
		<td width="100"><img src="images/aat-logo.gif" width="160"  height="46"/></td>
		<td width="100"><img src="images/ttv-logo.gif" width="160"  height="46"/></td>
	</tr>
</table>
<hr>
<br>
<table  border="0">
	<tr>
		<td align="center"><b style="font-size:24px; margin-left:300px;">PICKUP SHEET - AAT WIRING / ใบรับฝากสินค้า </b></td>
	</tr>
</table>
<table border="0" style="margin-top:10px;" cellspacing="" cellpadding="2" style="font-size:11px">
	<tr>
		<td align="left" width="480"></td>
		<td align="left" width="190"><tcpdf method="write1DBarcode" params="'.$barcodePUS.'"/></td>
	</tr>
	<tr>
		<td align="left" width="80"><b>Ship From :</b></td>
		<td align="left" width="90">'.$SHPF.'</td>
		<td align="left" width="80"><b>ETD W/H:</b></td>
		<td align="left" width="50">'.$ETDW.'</td>
		<td align="left" width="80"><b>Truck Number:</b></td>
		<td align="left" width="120">'.$TNUM.'</td>
		<td align="left" width="80"><b>PUS Number :</b></td>
		<td align="left" width="90">'.$PUSNO.'</td>
	</tr>
	<tr>
		<td align="left" width="80"><b>Ship To :</b></td>
		<td align="left" width="90">'.$SHPT.'</td>
		<td align="left" width="80"><b>ETA Supplier:</b></td>
		<td align="left" width="50">'.$ETAS.'</td>
		<td align="left" width="80"><b>Truck Type:</b></td>
		<td align="left" width="120">'.$TTYPE.'</td>
		<td align="center" width="170"></td>
	</tr>
	<tr>
		<td align="left" width="80"><b>Pickdup Date:</b></td>
		<td align="left" width="90">'.$PDATE.'</td>
		<td align="left" width="80"><b>ETA W/H:</b></td>
		<td align="left" width="50">'.$ETAW.'</td>
		<td align="left" width="80"><b>Driver Name :</b></td>
		<td align="left" width="120">'.$DNAME.'</td>
		<td align="center" width="170"></td>
	</tr>
	<tr>
		<td align="left" width="80"><b>Forecast Date:</b></td>
		<td align="left" width="90">'.$FDATE.'</td>
		<td align="left" width="80"></td>
		<td align="left" width="50"></td>
		<td align="left" width="80"><b>Phone No :</b></td>
		<td align="left" width="120">'.$DPHONE.'</td>
		<td align="left" width="80"><b>Petition #</b></td>
		<td align="left" width="90">'.$PET.'</td>
	</tr>
	<tr>
		<td align="left" width="80"><b>Remark :</b></td>
		<td align="left" width="400">'.$REMARK.'</td>
		<td align="center" width="190"></td>
	</tr>

</table><br>';
return $headData;
}

function createbox($BPTB,$BCRB,$BPTP,$BSTR)
{
	$boxdata = '<br><br>
	<table style="font-size: 12px;" border="1">
		<tbody>
		<tr style="background-color:#C8C8C8;" >
			<th width="170">Remarks / หมายเหตุ</th>
			<th align="center" width="120">ชื่อผู้จ่ายสินค้า (Supplier)</th>
			<th align="center" width="120">ชื่อผู้รับสินค้า (Driver)</th>
			<th colspan="5" align="center" width="260">&nbsp; Packaging Type Record</th>
		</tr>
		<tr >
			<th rowspan="4"  align="center">&nbsp;&nbsp;&nbsp;&nbsp;</th><th rowspan="4" align="center">
			<p>&nbsp;</p>
			<p>_____________</p>
			<p><br />(____/____/____)</p>
			</th><th rowspan="4" align="center">
			<p>&nbsp;</p>
			<p>&nbsp;_____________</p>
			<p><br />(____/____/____)</p>
			</th>
			<th style="background-color:#C8C8C8;" rowspan="2" align="center">ยอดรับ</th>
			<th style="background-color:#C8C8C8;" align="center"></th>
			<th style="background-color:#C8C8C8;" align="center"></th>
			<th style="background-color:#C8C8C8;" align="center"></th>
			<th style="background-color:#C8C8C8;" align="center"></th>
		</tr>
		<tr style="background-color:#C8C8C8;">
			<th align="center">PTB</th>
			<th align="center">CBM</th>
			<th align="center">PTP</th>
			<th align="center">STD</th>
		</tr>
		<tr>
			<th style="background-color:#C8C8C8;font-size: 10px;" align="center">
				<p>แผนรับ</p>
				<p>&nbsp;</p>
			</th>
			<td><p align="center"><span><br>'.$BPTB.'</span></p></td>
			<td><p align="center"><span><br>'.$BCRB.'</span></p></td>
			<td><p align="center"><span><br>'.$BPTP.'</span></p></td>
			<td><p align="center"><span><br>'.$BSTR.'</span></p></td>
		</tr>
		<tr>
			<th style="background-color:#C8C8C8;font-size: 10px;" align="center">
				<p>รับจริง</p>
				<p>&nbsp;</p>
			</th>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		</tbody>
</table>';
	return $boxdata;
}

function createmastertag($tagno,$pallno,$partno,$partname,$qty,$snp,$spartno,$sdock,$pickloc,$barcodeTAG,$FDATE,$SHPF,$PDATE,$PUSNO,$SHPT)
{
	$mastertag = '<table style="height: 240px;" border="1" width="670">
	<tbody>
		<tr >
			<td >
				<p style="font-size:2px;">&nbsp;</p> 
				<p style="font-size:18px;">
					<span>MASTER TAG</span><br>
					<span align="center">FIFO TAG</span>
				</p>
				<p style="font-size:2px;">&nbsp;</p> 
			</td>
			<td colspan = "2" width="306">
				<p>
				<span style="font-size:10px;">Tag Number</span><br>
				<span style="font-size:22px;">'.$tagno.'</span><br>
				<tcpdf method="write1DBarcode" params="'.$barcodeTAG.'"/></p>
			</td>
			<td colspan = "2" width="230">
				<p style="font-size:10px;" >Pallet No</p>
				<p style="font-size:24px;" align="center">'.$pallno.'</p>
			</td>
		</tr>
		<tr >
			<td colspan = "2" width="230">
				<p>
				<span style="font-size:10px;">Part Number<br><br></span>
				<span style="font-size:20px;">'.$partno.'<br></span>
				</p>
			</td>
			<td width="210">
				<p>
				<span style="font-size:10px;">Part Name<br><br></span>
				<span style="font-size:16px;">'.$partname.'<br></span>
				</p>
			</td>
			<td colspan = "2" width="230">
				<p>
				<span style="font-size:10px;">Qty.<br></span>
				<span style="font-size:30px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$qty.'<br></span>
				<span style="font-size:10px">'.$snp.'</span>
				</p>
			</td>
		</tr>
		<tr >
			<td colspan = "2" width="230">
				<p>
				<span style="font-size:10px;">Supplier Part Number<br><br></span>
				<span style="font-size:20px;">'.$spartno.'<br></span>
				</p>
			</td>
			<td width="210">
				<p>
				<span style="font-size:10px;">Supplier Code<br><br></span>
				<span style="font-size:20px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$SHPF.'<br></span>
				</p>
			</td>
			<td colspan = "2" width="230">
				<p>
				<span style="font-size:10px;">Supplier Dock<br></span>
				<span style="font-size:20px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$sdock.'<br></span>
				</p>
			</td>
		</tr>
		<tr >
			<td>
				<p>
				<span style="font-size:10px;">Forecast Date<br></span>
				<span style="font-size:20px;">&nbsp;&nbsp;&nbsp;&nbsp;'.$FDATE.'<br></span>
				</p>
			</td>
			<td>
				<p>
				<span style="font-size:10px;">Pickup Date<br></span>
				<span style="font-size:20px;">&nbsp;&nbsp;&nbsp;&nbsp;'.$PDATE.'<br></span>
				</p>
			</td>
			<td>
				<p>
				<span style="font-size:10px;">PUS Number<br></span>
				<span style="font-size:20px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$PUSNO.'<br></span>
				</p>
			</td>
			<td width="100">
				<p>
				<span style="font-size:10px;">Ship To<br></span>
				<span style="font-size:18px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$SHPT.'<br></span>
				</p>
			</td>
			<td width="130">
				<p>
				<span style="font-size:10px;">Pick Location<br></span>
				<span style="font-size:24px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$pickloc.'<br></span>
				</p>
			</td>
		</tr>
	</tbody>
</table><br><hr><br>';

	return $mastertag;
}
function truckheadder()
{
	$headder ='<table border="0">
	<tr>
		<td  style="font-size:11px" width="500">
			<p style="font-size:30px;">แบบฟอร์มควบคุมการเดินรถขนส่งสินค้า<br>(Truck Control Form)</p>
		</td>
		<td align="right" style="font-size:11px" width="180"></td>
		<td align="right" width="150"><img src="images/aat-logo.gif" width="140"  height="46"/></td>
		<td align="right" width="150"><img src="images/ttv-logo.gif" width="140"  height="46"/></td>
	</tr>
</table>';
	return $headder;
}
function truckdetail($barcodeTRUCK,$PDATE,$PUSNO,$TNUM,$TTYPE,$DNAME,$DPHONE,$SHPF,$SHPT,$ETDW,$ETAS,$ETAW)
{
	$truckdetail = '<table style="height: 240px;position: absolute; top: 0; bottom: 0; left: 0; right: 0;" border="1" width="980">
	<tbody>
		<tr >
			<td align="center"  width="150">
				<p><span style="font-size:14px;">วันที่รับงาน</span><br>
					<span style="font-size:18px;">'.$PDATE.'</span>
				</p>
			</td>
			
			<td style="font-size:14px;"align="center" width="120">
				<p><span style="font-size:14px;">ทะเบียนรถ</span><br>
					<span style="font-size:18px;">'.$TNUM.'</span>
				</p>
			</td>

			<td style="font-size:14px;"align="center" width="180">
				<p><span style="font-size:14px;">พนักงานขับรถ</span><br>
					<span style="font-size:18px;">'.$DNAME.'</span>
				</p>
			</td>

			<td style="font-size:14px;"align="center" width="150">
				<p><span style="font-size:14px;">เบอร์ติดต่อ</span><br>
					<span style="font-size:18px;">'.$DPHONE.'</span>
				</p>
			</td>
			<td  width="380">
			<p >เลขที่เอกสาร <br><tcpdf method="write1DBarcode" params="'.$barcodeTRUCK.'"/></p>
			</td>
		</tr>
		<tr>
			<td style="font-size:14px;" width="600">
				<p>*ข้อกำหนด : พนักงานขับรถต้องทำการบันทึกข้อมูลในแบบฟอร์มให้ถูกต้อง และ ครบถ้วน*</p>
			</td>
			<td align="center" width="180" rowspan="2">
				<img  style="display:block;" src="images/truck.gif" />
			</td>
			<td align="center" width="200">
				<p>ส่วนของลูกค้า/เจ้าหน้าที่หน้างาน</p>
			</td>
		</tr>
		<tr>
			<td width="240">
				<p>หัวหน้างาน กำหนดการเดินรถ</p>
			</td>
			<td width="310">
				<p>1. พนักงานขับรถ  บันทึกเวลาเดินรถและเลขไมล์จริง</p>
			</td>
			<td width="50"></td>
			<td width="105" align="center">
				<p>สภาพงาน</p>
			</td>
			<td width="95" align="center">
				<p>ลายเซ็น</p>
			</td>
		</tr>
		<tr style="font-size:12px;">
			<td width="50">
				ลำดับ
			</td>
			<td width="100">
				จุดรับ-ส่ง(สินค้า)
			</td>
			<td width="60">
				กำหนดการ
			</td>
			<td width="60">
				เวลาเข้า
			</td>
			<td width="60">
				เวลาเริ่มขึ้นหรือลงของ
			</td>
			<td width="60">
				เวลาขึ้นหรือลงเสร็จ
			</td>
			<td width="50">
				เวลาออก
			</td>
			<td width="50">
				เลขไมล์
			</td>
			<td width="110">
				สาเหตุที่ล่าช้า
			</td>
			<td width="45">
				Seal
			</td>
			<td width="45">
				เลขซีล 1
			</td>
			<td width="45">
				เลขซีล 2
			</td>
			<td width="45">
				เลขซีล 3
			</td>
			<td width="50">
				สมบูรณ์
			</td>
			<td width="55">
				ไม่สมบูรณ์
			</td>
			<td width="95">
				เจ้าหน้าที่/ลูกค้า
			</td>
		</tr>
		<tr style="font-size:12px;">
			<td width="50" rowspan="2">
				1
			</td>
			<td width="100" rowspan="2">
				'.$SHPT.'
			</td>
			<td width="180" rowspan="2">
				ออกก่อนเวลา
			</td>
			<td width="60" rowspan="2">
				'.$ETDW.'
			</td>
			<td width="50" rowspan="2">
				
			</td>
			<td width="50" rowspan="2">
				
			</td>
			<td width="110" rowspan="2">
				
			</td>
			<td width="45" >
				เข้า
			</td>
			<td width="45" >
			</td>
			<td width="45" >
			</td>
			<td width="45" >
			</td>
			<td width="50" align="center" >
				( )
			</td>
			<td width="55" align="center" >
				( )
			</td>
			<td width="95" >
			</td>
		</tr>
		<tr style="font-size:12px;">
			<td width="45" >
				ออก
			</td>
			<td width="45" >
			</td>
			<td width="45" >
			</td>
			<td width="45" >
			</td>
			<td width="50" align="center" >
				( )
			</td>
			<td width="55" align="center" >
				( )
			</td>
			<td width="95" >
			</td>
		</tr>

		<tr style="font-size:12px;">
			<td width="50" rowspan="2">
				2
			</td>
			<td width="100" rowspan="2">
				'.$SHPF.'
			</td>
			<td width="60" rowspan="2">
				'.$ETAS.'
			</td>
			<td width="60" rowspan="2">
				
			</td>
			<td width="60" rowspan="2">
				
			</td>
			<td width="60" rowspan="2">
				
			</td>
			<td width="50" rowspan="2">
				
			</td>
			<td width="50" rowspan="2">
			</td>
			<td width="110" rowspan="2">
			</td>
			<td width="45" >
				เข้า
			</td>
			<td width="45" >
			</td>
			<td width="45" >
			</td>
			<td width="45" >
			</td>
			<td width="50" align="center" >
				( )
			</td>
			<td width="55" align="center" >
				( )
			</td>
			<td width="95" >
			</td>
		</tr>
		<tr style="font-size:12px;">
			<td width="45" >
				ออก
			</td>
			<td width="45" >
			</td>
			<td width="45" >
			</td>
			<td width="45" >
			</td>
			<td width="50" align="center" >
				( )
			</td>
			<td width="55" align="center" >
				( )
			</td>
			<td width="95" >
			</td>
		</tr>

		<tr style="font-size:12px;">
			<td width="50" rowspan="2">
				3
			</td>
			<td width="100" rowspan="2">
				'.$SHPT.'
			</td>
			<td width="60" rowspan="2">
				'.$ETAW.'
			</td>
			<td width="60" rowspan="2">
				
			</td>
			<td width="60" rowspan="2">
				
			</td>
			<td width="60" rowspan="2">
				
			</td>
			<td width="50" rowspan="2">
				
			</td>
			<td width="50" rowspan="2">
			</td>
			<td width="110" rowspan="2">
			</td>
			<td width="45" >
				เข้า
			</td>
			<td width="45" >
			</td>
			<td width="45" >
			</td>
			<td width="45" >
			</td>
			<td width="50" align="center" >
				( )
			</td>
			<td width="55" align="center" >
				( )
			</td>
			<td width="95" >
			</td>
		</tr>
		<tr style="font-size:12px;">
			<td width="45" >
				ออก
			</td>
			<td width="45" >
			</td>
			<td width="45" >
			</td>
			<td width="45" >
			</td>
			<td width="50" align="center" >
				( )
			</td>
			<td width="55" align="center" >
				( )
			</td>
			<td width="95" >
			</td>
		</tr>
	</tbody>
</table><br>
';

	return $truckdetail;
}
function truckfoot()
{
	$truckfoot = '<br>
	<table  style="display: inline-block;" border="1" width="980" style="float: left">
		<tbody>
			<tr style="font-size:12px;">
				<td width="230">
					เบอร์ติดต่อหน้างาน
				</td>
				<td width="50" rowspan="3">
				</td>
				<td width="430">
					เหตุผลที่เกิดการล่าช้าในการ รับ-ส่ง สินค้า (ให้ใส่หมายเลขในช่อง สาเหตุที่ล่าช้า)
				</td>
				<td width="50">
				</td>
				<td width="110">
					ผู้ปล่อยรถ
				</td>
				<td width="110">
					ผู้ตรวจสอบขากลับ
				</td>
			</tr>
			<tr style="font-size:12px;">
				<td width="115">
					Cs Controller
				</td>
				<td width="115">
					นิสารัตน์  091-2394577
				</td>

				<td width="30">
					1.
				</td>
				<td width="200">
					ออกจากลานจอดรถช้า หรือได้รับรถช้า
				</td>
				<td width="30">
					7.
				</td>
				<td width="170">
					ไม่มีข่องจอดรถ
				</td>
				<td width="50">
				</td>
				<td width="110" rowspan="2">
					
				</td>
				<td width="110"  rowspan="2">
					
				</td>
			</tr>

			<tr style="font-size:12px;">
				<td width="115">
					Transport Controller
				</td>
				<td width="115">
					ภัสนี 092-2514170
				</td>
				<td width="30">
					2.
				</td>
				<td width="200">
					ข้ามาจากจุดก่อนหน้า (จุดรับ หรือ ส่งสินค้า)
				</td>
				<td width="30">
					8.
				</td>
				<td width="170">
					รถเจ้าหน้าที่ตรวจรับสินค้า
				</td>
				<td width="50">
				</td>
			</tr>

			<tr style="font-size:12px;">
				<td width="280" rowspan="4">
					
				</td>
				<td width="30">
					3.
				</td>
				<td width="200">
					ฝนตก, รถติด
				</td>
				<td width="30">
					9.
				</td>
				<td width="170">
					ภาชนะเปล่าไม่ได้ถูกจัดเตรียม
				</td>
				<td width="50">
				</td>
				<td width="110">
					หัวหน้างาน
				</td>
				<td width="110">
					หัวหน้างาน
				</td>
			</tr>

			<tr style="font-size:12px;">

				<td width="30">
					4.
				</td>
				<td width="200">
					รอคิวรถ เพื่อเรียกเข้ารับ-ส่งสินค้า
				</td>
				<td width="30">
					10.
				</td>
				<td width="170">
					รอเอกสาร
				</td>
				<td width="50">
				</td>
				<td width="110">
					วันที่ปล่อยรถ
				</td>
				<td width="110">
					วันที่ตรวจสอบขากลับ
				</td>
			</tr>

			<tr style="font-size:12px;">

				<td width="30">
					5.
				</td>
				<td width="200">
					สินค้าไม่พร้อมจัดส่ง หรือ รอขึ้นสินค้า
				</td>
				<td width="30">
					11.
				</td>
				<td width="170">
					รถเสียระหว่างทาง
				</td>
				<td width="50">
				</td>
				<td width="110" rowspan="2">
					
				</td>
				<td width="110" rowspan="2">
					
				</td>
			</tr>

			<tr style="font-size:12px;">

				<td width="30">
					6.
				</td>
				<td width="200">
					ภาชนะบรรจุไม่เพียงพอ
				</td>
				<td width="30">
					12.
				</td>
				<td width="170">
					รถเกิดอุบัติเหตุ
				</td>
				<td width="50">
				</td>
			</tr>
		</tbody>
	</table>
	';
	return $truckfoot;
}
function pettitionn($petNo,$plantName,$petDate,$cuslicen1,$cuslicen2,$wKeep,$wADD,$petReturn,$cOfficer,$pitemnum,$pttitem,$pQTY,$pWeight,$pPrice,$desp)
{
	$pettitionn = '
	<p></p>
	<table border="1">
		<tr>
			<td width="155">
				<p style="font-size:14px;">
				 <span>	สนง.ศุลกากรเขตปลอดอากร	</span><br>
				 <span>	ออโต้อัลลายซ์ ประเทศไทย	</span><br>
				 <span>	เลขรับที่.........................	</span><br>
				</p>
			</td>
		</tr>
	</table>
	<p align="right" style="font-size:10px;">๓๐๘ </p>
	<p align="right" style="font-size:12px;"> แบบแนบท้ายประมวลฯ ข้อ 503 03 04 (ก) </p>
	<p align="center" style="font-size:14px;"> คำขอนำของออกจากเขตปลอดอากร/เขตประกอบการเสรีเป็นการชั่วคราว </p>
	<p align="left" style="font-size:14px;"> เลขที่ '.$petNo.' 
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	เขียนที่ '.$plantName.' </p>
	<p style="text-align: right;font-size:14px;" >วันที่ '.$petDate.'&nbsp;</p>
	<p style="font-size:14px;">เรื่อง&nbsp;ขอนําของออกจากเขตปลอดอากร/เขตประกอบการเสรีเป็นการชั่วคราว</p>
	<p style="font-size:14px;">เรียน หัวหน้าสํานักงานศุลกากรเขตปลอดอากร/เขตประกอบการเสรี '.$plantName.'&nbsp;</p>
	<p style="font-size:14px;">สิ่งที่ส่งมาด้วย รายละเอียดของที่ขอนําออกจากเขตปลอดอากร/เขตประกอบการเสรีเป็นการชั่วคราว</p>
	<table border="0">
		<tbody>
			<tr style="font-size:14px;">
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				ด้วยข้าพเจ้า '.$plantName.' ถือใบรับรองเป็น ผู้ประกอบกิจการ ในเขตปลอดอากร ตาม
				'.$cuslicen1.' ที่ '.$cuslicen2.' มีความประสงค์จะขออนุญาตนำของตามรายการข้างท้ายนี้ออกจากเขต ปลอดอากรเป็นการชั่วคราว เพื่อผ่านขั้นตอนการจัดชิ้นส่วนในการเตรียม
				ความพร้อมเพื่อเข้าสู่สายการผลิต
				และจะไปดําเนินการ ณ สถานประกอบการ '.$wKeep.' ตั้งอยู่เลขที่ '.$wADD.' ถือใบอนุญาตประกอบ กิจการโรงงาน : เลขที่ ชบ.064502, ทะเบียนนิติบคุคล เลขที่ 0205552019111
				</td>
			</tr>
		</tbody>
	</table>
	<br><br>
	<span style="font-size:13px;">(แนบสำเนาใบอนุญาตประกอบการกิจการโรงงานและสำเนาสัญญาจ้าง/ใบสั่งจ้้างทำของ)</span><br>
	<table border="1">
		<tbody>
			<tr style="font-size:14px;">
				<td align="center">
					ลำดับที่
				</td>
				<td align="center">
					จำนวนหีบห่อ
				</td>
				<td align="center">
					ปริมาณ (ชิ้น)
				</td>
				<td align="center">	
					น้ำหนัก (กก.)
				</td>
				<td align="center">
					ราคา (บาท)
				</td>
				<td align="center">
					รายการของ
				</td>
			</tr>

			<tr style="font-size:20px;">
				<td align="center">
					-
				</td>
				<td align="center">
					-
				</td>
				<td align="center">
					'.$pQTY.'
				</td>
				<td align="center">
					'.$pWeight.'
				</td>
				<td align="center">
					'.$pPrice.'
				</td>
				<td align="center">
					<span style="font-size:12px;">รายละเอียด</span><br>
					<span style="font-size:12px;">ตามเอกสารแนบ</span>
				</td>
			</tr>
		</tbody>
	</table><br><br>
	<table border="0">
		<tbody>
			<tr style="font-size:14px;">
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					ข้าพเจ้าขอรับรองว่าข้อมูลและเอกสารแนบคําขอนี้ ตรวจกับความจริง หรือเป็นประมาณการทีใกล้เคียงความจริงที่สุด จะนำของกลับ เข้าในเขตปลอดอากรภายในวันที 
					'.$petReturn.' และยินยอมรับผิดชอบตามที่กรมศลุกากรพิจารณา สั่งการทุกประการ ในกรณีทีข้าพเจ้าสําแดงรายการไม่ถูกต้อง และ/หรือ ไม่ครบถ้วนตามความเป็นจริง 
					รวมทั้งไม่นําของกลับเข้าในเขตปลอดอากรภายใน เวลาที่รับรองไว้ หรือภายในกําหนดที่กรมศลุกากร ได้ขยายเวลาให้ 
				</td>
			</tr>
		</tbody>
	</table><br><br>
	<table border="0">
		<tbody>
			<tr style="font-size:14px;">
				<td width="300">
				</td>
				<td align="center">
					ขอแสดงความนับถือ<br><br>
					 (&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)<br><br>
					 นางสาวอรทัย ปุเรจาริก<br>
					 เจ้าของ/ผู้จัดการ/ประทับตราบริษัท 
				</td>
				<td width="140">
				</td>
			</tr>
		</tbody>
	</table>
	<p align="center" style="font-size:14px;"> บันทึกการตรวจของเจ้าหน้าที่ศลุกากร  </p>
	<table border="1">
		<tbody>
			<tr style="font-size:14px;">
				<td >
					<p>
						<span align="center">นําออกจากเขตปลอดอากร</span><br>
						<span align="center">ตรวจแล้วนับจำนวนถูกต้อง</span><br>
						<span align="center">&nbsp;</span><br>
						<span align="center">&nbsp;</span><br>
						<span align="center">'.$cOfficer.'</span><br>
						<span align="center">นักวิชาการศุลกากรปฏิบัติการ</span><br><br>
						<span align="left">วันที่ </span>
					</p>
				</td>
				<td>
					<p>
						<span align="center">นํากลับเข้าเขตปลอดอากร</span><br>
						<span align="center">ตรวจแล้วนับจำนวนถูกต้อง</span><br>
						<span align="center">&nbsp;</span><br>
						<span align="center">&nbsp;</span><br>
						<span align="center">'.$cOfficer.'</span><br>
						<span align="center">นักวิชาการศุลกากรปฏิบัติการ</span><br><br>
						<span align="left">วันที่ </span>
					</p>
				</td>
			</tr>
		</tbody>
	</table>
';

	return $pettitionn;
}

function pettitionn2()
{
	$pettitionn2 = '
	<p></p>
	<p></p>
	<p align="center" style="font-size:11px;"> รายละเอียดของของที่ขอนําออกจากเขตปลอดอากร/เขตประกอบการเสรีเป็นการชั่วคราว(ตามแบบแนบท้ายประมวลฯ ข้อ 5 03 03 04 (ก) และข้อ 5 04 03 05)  </p>
	<p align="center" style="font-size:14px;"> รายละเอียดการขออนุญาตนําของออกไปจากเขตปลอดอากร/เขตประกอบการเสรีเป็นการชั่วคราวเพื่อผ่านขบวนการผลิต   </p>
	<p align="left" style="font-size:14px;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	 1. ลักษณะของของที่นํากลับเข้าเขตปลอดอากร    </p>

	<table border="1">
		<tbody>
			<tr style="font-size:14px;">
				<td align="center">
					ลำดับที่
				</td>
				<td align="center">
					รายละเอียดของของ
				</td>
				<td align="center">
					ปริมาณ (ชิ้น)
				</td>
				<td align="center">	
					น้ำหนัก (กก.)
				</td>
				<td align="center">
					ราคา (บาท)
				</td>
				<td align="center">
					รายการของ
				</td>
			</tr>
			<tr style="font-size:14px;">
				<td width="666" align="center">
					นํากลับในสภาพเดิม รายละเอียดตามเอกสารแนบและเป็นไปตามอนุมัติหลักการ<br>ของด่านศุลกากรมาบตาพุด ตามจดหมายเลขที่ ก.ค.0502(9) / 1237 ลว. 18 พ.ค.54
				</td>
			</tr>
		</tbody>
	</table><br><br>
	<table border="0">
		<tbody>
			<tr style="font-size:14px;">
				<td>
					(ส่งตัวอย่างหรือภาพถ่ายแบบแปลนเอกสาร ของของที่จะนําออกจากเขตปลอดอากร/เขตประกอบการเสรีและตัวอย่าง ของของทีจะ นํากลับเข้ามาในเขตปลอดอากร/เขตประกอบการเสรีจํานวนอย่างละ 2 ตัวอย่าง )
				</td>
			</tr>
		</tbody>
	</table>
	<p align="left" style="font-size:12px;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	 2. เหตผุลและความจําเป็นทีขอนําของออกไปจากเขตปลอดอากร/เขตประกอบการเสรี     </p>
	<p align="left" style="font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<img  style="display:block;" src="images/sq.gif" width="20"  height="20" /> มีขบวนการผลิตดัง กล่าวภายในเขตปลอดอากร แต่ต้องนําออกไปจากเขตปลอดอากร/เขตประกอบการเสรี<br>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;เป็นการชั่วคราวเนื่องจาก
	...................................................................................................................................................<br><br>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	......................................................................................................................................................................................<br><br>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	......................................................................................................................................................................................<br>
	 </p>
	 <p align="left" style="font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	 <img  style="display:block;" src="images/sqx.gif" width="20"  height="20" /> ไม่มีขบวนการผลิตดังกล่าว 
	 </p>
	 <p align="left" style="font-size:12px;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	 3. แผนงานและกําหนดเวลาทีจะดําเนินการผลิตขั้นตอนดังกล่าวในเขตปลอดอากร/เขตประกอบการเสรี      </p>
	 <p align="left" style="font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<img  style="display:block;" src="images/sqx.gif" width="20"  height="20" /> ไม่มี </p>
	<p align="left" style="font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<img  style="display:block;" src="images/sq.gif" width="20"  height="20" /> มี </p>
	<p style="font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	......................................................................................................................................................................................</p>
	<p style="font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	......................................................................................................................................................................................</p>
	<p style="font-size:12px;";>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	......................................................................................................................................................................................</p>
	<p align="left" style="font-size:12px;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	 4. ข้อมลูทั่วไปของบริษัท/ห้างฯ       </p>
	 <p style="font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	......................................................................................................................................................................................</p>
	<p style="font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	......................................................................................................................................................................................</p>
	<p style="font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	......................................................................................................................................................................................</p>
	<p align="left" style="font-size:12px;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	 5. แผนผังแสดงกรรมวิธีการผลิตโดยย่อ (FLOW PROCESS CHART) ทุกขั้นตอนที่ผลิตภายในเขต ปลอดอากร/เขต ประกอบการเสรี <br>
	 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	 และขั้น ตอนที่ต้องนําออกไปทําการผลิตภายนอกเขตปลอดอากร/เขต ประกอบการเสรีเป็นการ ชั่วคราว       </p>
	';

	return $pettitionn2;
}
