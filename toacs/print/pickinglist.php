<?php
include('../php/connection.php');
require_once('tcpdf/tcpdf.php');
$data = $_REQUEST['data1'];
// $data = '1708656499';
// $printname = $_REQUEST['printname'];
// echo $data;
// exit();
$q1  = "SELECT plh.PIC_NO,plh.SO_NO,DATE_FORMAT(odr.sch_date,'%d-%m-%Y')as sch_date,DATE_FORMAT(plh.Create_date,'%d-%m-%Y') as Create_date,plh.Create_by,ur.user_fName,
concat(odr.Delivery_Int,'-',LPAD(odr.Order_no,6,0)) as order_no ,TIME_FORMAT(odr.Due_time_n, '%H:%i') as Due_time FROM tbl_picklist_header  plh 
LEFT JOIN tbl_order odr ON plh.PIC_NO = odr.Pick_no
LEFT JOIN tbl_user ur ON plh.Create_by = ur.user_id
WHERE plh.SO_NO = '$data' OR plh.PIC_NO = '$data' ";

if ($result = $mysqli->query($q1)) 
{
	if ($result->num_rows == 0)
		{
			echo 'ไม่พบ  ในระบบ';
			$mysqli->close();
			exit();
		}
	while ($srow = $result->fetch_assoc()) 
	{
			$PIC_NO =  $srow["PIC_NO"];
	    	$SO_NO =  $srow["SO_NO"];
	    	$sch_date =  $srow["sch_date"];
	    	$Create_date =  $srow["Create_date"];
	    	$user_fName =  $srow["user_fName"];
	    	$order_no =  $srow["order_no"];
	    	$Due_time =  $srow["Due_time"];
	}
}
// $sono = $data;

$q2 = "SELECT  odr.Customer_item, odr.SO_no, odr.SO_line, odr.Dock,pm.pick_loc, odr.SO_qty,ROUND(odr.SO_qty/pm.snp) as nbox FROM tbl_order odr
LEFT JOIN tbl_partmaster pm ON odr.Customer_item = pm.part_supplier
WHERE odr.SO_no = '$SO_NO'";
if ($result = $mysqli->query($q2)) 
{
	$arpldetail = array();
	while ($srow2 = $result->fetch_assoc()) 
	{
			$crow = $result->num_rows;
			$arpldetail[] = $srow2["Customer_item"].",".$srow2["SO_no"].",".$srow2["SO_line"].",".$srow2["pick_loc"].",".$srow2["Dock"].",".$srow2["SO_qty"].",".$srow2["nbox"];
	}
}

$pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetTitle('PICK');//title

$pdf->SetMargins(10, 5, 10,5);
$pdf->SetAutoPageBreak(TRUE, 0);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}
$pdf->setFontSubsetting(true);
$pdf->SetFont('freeserif', '');
// $pdf->AddPage();
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
//
$thead = '&nbsp;<br><table border="1" cellspacing="0" cellpadding="2">
	<tr style="font-size:12px;background-color:#C8C8C8;" >
		<td align="center" width="30"><b>No. </b></td>
		<td align="center" width="130"><b>Part No</b></td>
		<td align="center" width="130"><b>SO NO</b></td>
		<td align="center" width="80"><b>So Line</b></td>
		<td align="center" width="70"><b>Pick Loc</b></td>
		<td align="center" width="80"><b>Dock</b></td>
		<td align="center" width="70"><b>Qty</b></td>
		<td align="center" width="70"><b>Box</b></td>
	</tr>';
$barcodePL= TCPDF_STATIC::serializeTCPDFtagParameters(array($SO_NO, 'C128', '', '', 0, 16, 0.4, array('position'=>'R', 'border'=>false, 'padding'=>1, 'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>true, 'font'=>'helvetica', 'fontsize'=>5, 'stretchtext'=>4,'cellfitalign'=>'R','stretch'=>true), 'N'));

$rgrn = 1;
$ndetail = count($arpldetail);
// $ndetail = 88;
$d = 39;
$ccpus = 0 ;
$field = 0;
$nn = 1;
$n = 1;
$p = $d*$nn;
$tableData = $thead;
$allPage = ceil($crow/$d);
// $allPage = ceil(88/$d);
$html = createHead('page 1/',$barcodePL,$PIC_NO,$SO_NO,$sch_date,$Create_date,$user_fName,$order_no,$Due_time);
$tableData = $thead;
while ( $ccpus <= $ndetail-1) {
	if ($field == 0) 
			{
				$field = 1;
				$pdf->AddPage();
				$html = createHead('page 1/'.$allPage,$barcodePL,$PIC_NO,$SO_NO,$sch_date,$Create_date,$user_fName,$order_no,$Due_time);
			}
			if($ccpus > $p)
    	      {
    	      	// $ccpus++;
    	      	$n++;
    	      	$nn++;
    	      	$p = $d*$nn;
    	      	$tableData .='</table>';
    	      	$html .= $tableData;
    	      	$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
    	      	$pdf->AddPage();
    	      	$html = createHead('page'.$n.'/'.$allPage,$barcodePL,$PIC_NO,$SO_NO,$sch_date,$Create_date,$user_fName,$order_no,$Due_time);
    	      	$tableData = $thead;
    	      }
    	      $data= explode(",",$arpldetail[$ccpus]);
			  $tableData .= '<tr style="font-size:10px" >';
			  $tableData .= '<td align="center" width="30">'.$rgrn.'</td>';
			  $tableData .= '<td align="center" width="130">'. $data[0].'</td>';
			  $tableData .= '<td align="center" width="130">'. $data[1].'</td>';
			  $tableData .= '<td align="center" width="80">'. $data[2].'</td>';
			  $tableData .= '<td align="center" width="70">'. $data[3].'</td>';
			  $tableData .= '<td align="center" width="80">'. $data[4].'</td>';
			  $tableData .= '<td align="center" width="70">'. $data[5].'</td>';
			  $tableData .= '<td align="center" width="70">'. $data[6].'</td>';
			  $tableData .= '</tr>';
			  $ccpus++;$rgrn++;
}
/*$tableData .= '<tr style="font-size:12px" >';
$tableData .= '<td align="center" width="200"></td>';
$tableData .= '<td align="right" width="200">Total:</td>';
$tableData .= '<td align="center" width="200"></td>';
$tableData .= '<td align="center" width="60"></td>';
$tableData .= '</tr>';*/

$tableData .='</table>';
$html .= $tableData;
$html .= createbox();

$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// $pdf->Output('sda'.'.pdf', 'I');
$randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0,2);
$pdf->Output("D:\\printfile\\".$PIC_NO.$randomString.'-Printer_outbound.pdf', 'F');
echo '{"ch":1,"data":"DONE"}';
function createHead($page,$barcodePL,$PIC_NO,$SO_NO,$sch_date,$Create_date,$user_fName,$order_no,$Due_time)
{
	$headData = str_format('<table border="0">
	<tr>
		<td align="right" style="font-size:11px">{1} </td>
	</tr>
</table>',$page);

	$headData .= '<table border="0">
	<tr>
		<td width="130"><img src="images/abt-logo.gif" width="120"  height="50"/></td>
		<td width="10"></td>
		<td align="left" width="300" style="font-size:10px"><b>ALBATROSS LOGISTICS CO., LTD.</b><br/>
		336/7 MOO 7 BOWIN, SRIRACHA CHONBURI 20230<br/>
		Phone +66 38 058 021, +66 38 058 081-2<br/>
		Fax : +66 38 058 007
		</td>
		<td align="right" width="218"><tcpdf method="write1DBarcode" params="'.$barcodePL.'"/></td>
	</tr>
</table>
<hr>
<table border="0">
	<tr>
		<td align="center"><b style="font-size:18px; margin-left:300px;">PICKING LIST (TOACS)</b></td>
	</tr>
</table>
<hr />
<br>
<table border="0" style="margin-top:10px;" cellspacing="" cellpadding="2" style="font-size:13px">
	<tr>
		<td align="left" width="100"><b>Document No. :</b></td>
		<td align="left" width="120">'.$PIC_NO.'</td>
		<td align="left" width="100"><b>SO No. :</b></td>
		<td align="left" width="120">'.$SO_NO.'</td>
		<td align="left" width="100"><b>Order No :</b></td>
		<td align="left" width="120">'.$order_no.'</td>
	</tr>
	<tr>
		<td align="left" width="100"><b>Order Date :</b></td>
		<td align="left" width="120">'.$sch_date.'</td>
		<td align="left" width="100"><b>Order Time :</b></td>
		<td align="left" width="120">'.$Due_time.'</td>
	</tr>
	<tr>
		<td align="left" width="100"><b>Create By :</b></td>
		<td align="left" width="120">'.$user_fName.'</td>
		<td align="left" width="100"><b>Create  Date :</b></td>
		<td align="left" width="120">'.$Create_date.'</td>
	</tr>
</table>';
return $headData;
}

function createbox()
{
	$foot = '<br><br>
			<table style="font-size: 14px;" border="0" >
					<tbody>
					<tr  >
						<td align="center" width="220">
							<p>
								<span>&nbsp;</span><br>
								<span>-Picker-</span><br>
								<span>&nbsp;</span><br>
								<span>___________________</span><br>
								<span>(______/______/______)</span><br>
							</p>
						</td>
						<td align="center" width="220">
							<p>
								<span>&nbsp;</span><br>
							</p>
						</td>
						<td align="center" width="220">
							<p>
								<span>&nbsp;</span><br>
								<span>-QA-</span><br>
								<span>&nbsp;</span><br>
								<span>___________________</span><br>
								<span>(______/______/______)</span><br>
							</p>
						</td>
					</tr>
					</tbody>
			</table>';

	return $foot;
}

