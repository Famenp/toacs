<?php
include('../php/connection.php');
require_once('tcpdf/tcpdf.php');
$data = $_REQUEST['data'];
$q2  = "CALL SP_Receiving_PrintGRN('$data')";
if ($result2 = $mysqli->query($q2)) 
{
	if ($result2->num_rows == 0)
		{
			echo 'ไม่พบ  ในระบบ';
			$mysqli->close();
			exit();
		}
	while ($srow = $result2->fetch_assoc()) 
	{
			$ReceivedDate =  $srow["Received_Date"];
	    	$DONumber =  $srow["DO_Number"];
	    	$GRNNumber =  $srow["GRN_Number"];
	    	$SupplierCode =  $srow["Supplier_Code"];
	    	$Total_Qty =  $srow["Total_Qty"];
	    	$Total_Boxes =  $srow["Total_Boxes"];

	    	$FDATE =  $srow["Forecast_Date"];
	    	$SHPF =  $srow["Ship_Form"];
	    	$SHPT =  $srow["Ship_To"];
	    	$PUSNO =  $srow["PUS_Number"];
	    	$PDATE =  $srow["Pickup_Date"];
	    	if ($mysqli->next_result())
	    	{
	    		$argrndetail = array();
	    		if ($result2 = $mysqli->store_result())
	    		{
	    			while ($srow = $result2->fetch_assoc()) 
	    			{
	    				$crow = $result2->num_rows;
	    				$argrndetail[] = $srow["Part_Number"].",".$srow["Part_Name"].",".$srow["SNP"].",".$srow["Qty"].",".$srow["Package_Type"]
	    									.",".$srow["Total_Boxes"].",".$srow["Tag_Number"].",".$srow["Suggest_Put_Location"];
	    			}
	    			 if ($mysqli->next_result()) 
	    			 {
	    			 	$arparttag = array();
	    			 	if ($result = $mysqli->store_result()) 
	    			 	{
	    			 		while ($row3 = $result->fetch_assoc())
		                		{
		                			$arparttag[] = $row3["Tag_Number"].",".$row3["Pallet_No"].",".$row3["Part_Number"].",".$row3["Part_Name"].",".$row3["Qty"].",".$row3["SNP"].","
		                			.$row3["Supplier_Part_Number"].",".$row3["Supplier_Dock"].",".$row3["Pick_Location"];
		                		}
	    			 	}
	    			 }
	    		}
	    	}
	}
}

$pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetTitle('GRN');//title

$pdf->SetMargins(10, 5, 10,5);
$pdf->SetAutoPageBreak(TRUE, 0);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}
$pdf->setFontSubsetting(true);
$pdf->SetFont('freeserif', '');
// $pdf->AddPage();
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
//
$thead = '&nbsp;<br><table border="1" cellspacing="0" cellpadding="2">
	<tr style="font-size:10px;background-color:#C8C8C8;" >
		<td align="center" width="30"><b>No. </b></td>
		<td align="center" width="100"><b>Part No</b></td>
		<td align="center" width="150"><b>Part Name</b></td>
		<td align="center" width="40"><b>SNP</b></td>
		<td align="center" width="40"><b>Qty</b></td>
		<td align="center" width="70"><b>Package Type</b></td>
		<td align="center" width="70"><b>Total Package</b></td>
		<td align="center" width="85"><b>Tag FIFO No.</b></td>
		<td align="center" width="85"><b>Suggest Location</b></td>
	</tr>';
$barcodeGRN= TCPDF_STATIC::serializeTCPDFtagParameters(array($GRNNumber, 'C128', '', '', 0, 16, 0.4, array('position'=>'R', 'border'=>false, 'padding'=>1, 'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>true, 'font'=>'helvetica', 'fontsize'=>5, 'stretchtext'=>4,'cellfitalign'=>'R','stretch'=>true), 'N'));
$rgrn = 1;
$ndetail = count($argrndetail);
$d = 39;
$ccpus = 0 ;
$field = 0;
$nn = 1;
$n = 1;
$p = $d*$nn;
$tableData = $thead;
$allPage = ceil($crow/$d);
// $html = createHead('page 1/',$barcodeGRN);
$tableData = $thead;
while ( $ccpus <= $ndetail-1) {
	if ($field == 0) 
			{
				$field = 1;
				$pdf->AddPage();
				$html = createHead('page 1/'.$allPage,$barcodeGRN,$ReceivedDate,$DONumber,$GRNNumber,$SupplierCode);
			}
			if($ccpus > $p)
    	      {
    	      	// $ccpus++;
    	      	$n++;
    	      	$nn++;
    	      	$p = $d*$nn;
    	      	$tableData .='</table>';
    	      	$html .= $tableData;
    	      	$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
    	      	$pdf->AddPage();
    	      	$html = createHead('page'.$n.'/'.$allPage,$barcodeGRN,$ReceivedDate,$DONumber,$GRNNumber,$SupplierCode);
    	      	$tableData = $thead;
    	      }
    	      $data= explode(",",$argrndetail[$ccpus]);
			  $tableData .= '<tr style="font-size:10px" >';
			  $tableData .= '<td align="center" width="30">'.$rgrn.'</td>';
			  $tableData .= '<td align="center" width="100">'.$data[0].'</td>';
			  $tableData .= '<td align="center" width="150">'.$data[1].'</td>';
			  $tableData .= '<td align="center" width="40">'.$data[2].'</td>';
			  $tableData .= '<td align="center" width="40">'.$data[3].'</td>';
			  $tableData .= '<td align="center" width="70">'.$data[4].'</td>';
			  $tableData .= '<td align="center" width="70">'.$data[5].'</td>';
			  $tableData .= '<td align="center" width="85">'.$data[6].'</td>';
			  $tableData .= '<td align="center" width="85">'.$data[7].'</td>';
			  $tableData .= '</tr>';
			  $ccpus++;$rgrn++;
}
$tableData .= '<tr style="font-size:12px" >';
$tableData .= '<td align="center" width="130"></td>';
$tableData .= '<td align="right" width="150">Total:</td>';
$tableData .= '<td align="center" width="40"></td>';
$tableData .= '<td align="center" width="40">'.$Total_Qty.'</td>';
$tableData .= '<td align="center" width="70"></td>';
$tableData .= '<td align="center" width="70">'.$Total_Boxes.'</td>';
$tableData .= '<td align="center" width="85"></td>';
$tableData .= '<td align="center" width="85"></td>';
$tableData .= '</tr>';

$tableData .='</table>';
$html .= $tableData;
$html .= createbox();

// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

if ($arparttag[0][4]!='0') {
	$nparttag = count($arparttag);
	$pagetag = ($nparttag/3);
	$c = 0 ;
	$npart = 0 ;
	// while ( $c < $pagetag) {
		$pdf->AddPage();
		while ( $npart < $nparttag) {
			$data2= explode(",",$arparttag[$npart]);
			$barcodeTAG= $pdf->serializeTCPDFtagParameters(array($data2[0], 'C128', '', '', 80, 10, 0.4, array('position'=>'C', 'border'=>false, 'padding'=>1, 'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>true, 'font'=>'helvetica', 'fontsize'=>5, 'stretchtext'=>4,'cellfitalign'=>'R','stretch'=>true), 'N'));

			$html = createmastertag($data2[0],$data2[1],$data2[2],$data2[3],$data2[4],$data2[5],$data2[6],$data2[7],$data2[8],$barcodeTAG,$FDATE,$SHPF,$PDATE,$PUSNO,$SHPT);
			$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
			$npart++;$c++;
			if ($c%3 == 0 && $c!=$nparttag ) {
				$pdf->AddPage();
			}
		}
}
// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output($GRNNumber.'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+

function createHead($page,$barcodeGRN,$ReceivedDate,$DONumber,$GRNNumber,$SupplierCode)
{
	$headData = str_format('<table border="0">
	<tr>
		<td align="right" style="font-size:11px">{1} </td>
	</tr>
</table>',$page);

	$headData .= '<table border="0">
	<tr>
		<td width="130"><img src="images/ttv-logo.gif" width="120"  height="46"/></td>
		<td align="left" width="320" style="font-size:10px"><b>TITAN-VNS AUTO LOGISTICS CO.,LTD.</b><br/>
		49/66 MOO 5 TUNGSUNKLA SRIRACHA CHONBURI 20230<br/>
		Phone +66(0) 3840 1505-6,3804 1787-8<br/>
		Fax : +66(0) 3849 4300
		</td>
		<td align="right" width="218"><tcpdf method="write1DBarcode" params="'.$barcodeGRN.'"/></td>
	</tr>
</table>
<hr>
<table border="0">
	<tr>
		<td align="center"><b style="font-size:18px; margin-left:300px;">GOOD RECEIPT NOTE (AAT)</b></td>
	</tr>
</table>
<hr />
<br>
<table border="0" style="margin-top:10px;" cellspacing="" cellpadding="2" style="font-size:13px">
	<tr>
		<td align="left" width="120"><b>Receipt Date Time  :</b></td>
		<td align="left" width="120">'.$ReceivedDate.'</td>
		<td align="left" width="100"><b>Document No. :</b></td>
		<td align="left" width="140">'.$GRNNumber.'</td>
		<td align="left" width="100"><b>Supplier Name :</b></td>
		<td align="left" width="100">'.$SupplierCode.'</td>
	</tr>
	<tr>
		<td align="left" width="120"><b>DO No :</b></td>
		<td align="left" width="100">'.$DONumber.'</td>
		<td align="left" width="80"></td>
		<td align="left" width="70"></td>
	</tr>
</table>';
return $headData;
}

function createbox()
{
	$boxdata = '<br><br><br><br>
	<table style="font-size: 16px;" border="0">
		<tbody>
		<tr>
			<th width="330">Data Entry By : ____________________</th>
			<th width="335">Check By : ____________________ Suppervisor</th>
		</tr>
		
		</tbody>
</table>';
	return $boxdata;
}

function createmastertag($tagno,$pallno,$partno,$partname,$qty,$snp,$spartno,$sdock,$pickloc,$barcodeTAG,$FDATE,$SHPF,$PDATE,$PUSNO,$SHPT)
{
	$mastertag = '<table style="height: 240px;" border="1" width="670">
	<tbody>
		<tr >
			<td >
				<p style="font-size:2px;">&nbsp;</p> 
				<p style="font-size:18px;">
					<span>MASTER TAG</span><br>
					<span align="center">FIFO TAG</span>
				</p>
				<p style="font-size:2px;">&nbsp;</p> 
			</td>
			<td colspan = "2" width="306">
				<p>
				<span style="font-size:10px;">Tag Number</span><br>
				<span style="font-size:22px;">'.$tagno.'</span><br>
				<tcpdf method="write1DBarcode" params="'.$barcodeTAG.'"/></p>
			</td>
			<td colspan = "2" width="230">
				<p style="font-size:10px;" >Pallet No</p>
				<p style="font-size:24px;" align="center">'.$pallno.'</p>
			</td>
		</tr>
		<tr >
			<td colspan = "2" width="230">
				<p>
				<span style="font-size:10px;">Part Number<br><br></span>
				<span style="font-size:20px;">'.$partno.'<br></span>
				</p>
			</td>
			<td width="210">
				<p>
				<span style="font-size:10px;">Part Name<br><br></span>
				<span style="font-size:16px;">'.$partname.'<br></span>
				</p>
			</td>
			<td colspan = "2" width="230">
				<p>
				<span style="font-size:10px;">Qty.<br></span>
				<span style="font-size:30px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$qty.'<br></span>
				<span style="font-size:10px">'.$snp.'</span>
				</p>
			</td>
		</tr>
		<tr >
			<td colspan = "2" width="230">
				<p>
				<span style="font-size:10px;">Supplier Part Number<br><br></span>
				<span style="font-size:20px;">'.$spartno.'<br></span>
				</p>
			</td>
			<td width="210">
				<p>
				<span style="font-size:10px;">Supplier Code<br><br></span>
				<span style="font-size:20px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$SHPF.'<br></span>
				</p>
			</td>
			<td colspan = "2" width="230">
				<p>
				<span style="font-size:10px;">Supplier Dock<br></span>
				<span style="font-size:20px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$sdock.'<br></span>
				</p>
			</td>
		</tr>
		<tr >
			<td>
				<p>
				<span style="font-size:10px;">Forecast Date<br></span>
				<span style="font-size:20px;">&nbsp;&nbsp;&nbsp;&nbsp;'.$FDATE.'<br></span>
				</p>
			</td>
			<td>
				<p>
				<span style="font-size:10px;">Pickup Date<br></span>
				<span style="font-size:20px;">&nbsp;&nbsp;&nbsp;&nbsp;'.$PDATE.'<br></span>
				</p>
			</td>
			<td>
				<p>
				<span style="font-size:10px;">PUS Number<br></span>
				<span style="font-size:20px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$PUSNO.'<br></span>
				</p>
			</td>
			<td width="100">
				<p>
				<span style="font-size:10px;">Ship To<br></span>
				<span style="font-size:18px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$SHPT.'<br></span>
				</p>
			</td>
			<td width="130">
				<p>
				<span style="font-size:10px;">Pick Location<br></span>
				<span style="font-size:24px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$pickloc.'<br></span>
				</p>
			</td>
		</tr>
	</tbody>
</table><br><hr><br>';

	return $mastertag;
}
