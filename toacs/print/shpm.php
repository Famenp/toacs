<?php
include('../php/connection.php');
require_once('tcpdf/tcpdf.php');

$data = $_REQUEST['data1'];
// $printname = $_REQUEST['printname'];

$q1  = "SELECT  ship_no, ship_from, ship_to, dDate, working_ship, trip_no, truck_license, driver_Name, phone, 
		DATE_FORMAT(plan_time, '%H:%i') as plan_time ,DATE_FORMAT(ttv_out, '%H:%i') as ttv_out  FROM tbl_shipping_header
		WHERE ship_no = '$data'";

if ($result = $mysqli->query($q1)) 
{
	if ($result->num_rows == 0)
		{
			echo 'ไม่พบ  ในระบบ';
			$mysqli->close();
			exit();
		}
	while ($srow = $result->fetch_assoc()) 
	{
			$ship_no =  $srow["ship_no"];
	    	$ship_from =  $srow["ship_from"];
	    	$ship_to =  $srow["ship_to"];
	    	$dDate =  $srow["dDate"];
	    	$working_ship =  $srow["working_ship"];
	    	$trip_no =  $srow["trip_no"];
	    	$truck_license =  $srow["truck_license"];
	    	$driver_Name =  $srow["driver_Name"];
	    	$phone =  $srow["phone"];
	    	$plan_time =  $srow["plan_time"];
	    	$ttv_out =  $srow["ttv_out"];
	}
}

$q2 = "SELECT shb.gtn,odr.Dock,odr.Customer_item,CONCAT(odr.Delivery_Int,'-',odr.Order_no) as orderno ,odr.Pick_actual FROM tbl_shipping_header shd 
LEFT JOIN tbl_shipping_body shb ON shd.ID = shb.refID
LEFT JOIN tbl_order odr ON shb.gtn = odr.doc_no 
WHERE shd.ship_no = '$data'";

if ($result = $mysqli->query($q2)) 
{
	$arshpdetail = array();
	while ($srow2 = $result->fetch_assoc()) 
	{
			$crow = $result->num_rows;
			$arshpdetail[] = $srow2["Dock"].",".$srow2["Customer_item"].",".$srow2["orderno"].",".$srow2["Pick_actual"];
	}
}

$pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetCreator(PDF_CREATOR);
// $pdf->SetTitle($Doc_no);//title
$pdf->SetTitle('GTN');//title

$pdf->SetMargins(10, 5, 10,5);
$pdf->SetAutoPageBreak(TRUE, 0);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}
$pdf->setFontSubsetting(true);
$pdf->SetFont('freeserif', '');
$pdf->AddPage();
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));


$barcodeSHP= TCPDF_STATIC::serializeTCPDFtagParameters(array($ship_no, 'C128', '', '', 0, 16, 0.4, array('position'=>'R', 'border'=>false, 'padding'=>1, 'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>true, 'font'=>'helvetica', 'fontsize'=>5, 'stretchtext'=>4,'cellfitalign'=>'R','stretch'=>true), 'N'));

$html = createHead('page 1/',$barcodeSHP,$ship_from,$ship_to,$dDate,$working_ship,$trip_no,$truck_license,$driver_Name,$phone);
$html .= createHead1();
$tableData = '';
$rshp = 1;
$ccpus = 0 ;
$ndetail = count($arshpdetail);
$field = 0;
while ( $ccpus <= $ndetail-1)
{
	  $data= explode(",",$arshpdetail[$ccpus]);
	  $tableData .= '<tr style="font-size:12px" >';
	  $tableData .= '<td align="center" width="60">'.$rshp.'</td>';
	  $tableData .= '<td align="center" width="120">'.$data[0].'</td>';
	  $tableData .= '<td align="center" width="120">'.$data[1].'</td>';
	  $tableData .= '<td align="center" width="120">'.$data[2].'</td>';
	  $tableData .= '<td align="center" width="80">'.$data[3].'</td>';
	  $tableData .= '<td align="center" width="80"></td>';
	  $tableData .= '<td align="center" width="80"></td>';
	  $tableData .= '</tr>';
	  $ccpus++;$rshp++;
}

$html .= $tableData;
$html .= '</table>';
$html .= createHead2();
$html .= createHead3($plan_time,$ttv_out);
$html .= createHead4();
$html .= packing1();
$html .= packing2();
$html .= createbox();
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// $pdf->Output('123'.'.pdf', 'I');
$randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0,2);
$pdf->Output("D:\\printfile\\".$ship_no.$randomString.'-Printer_outbound.pdf', 'F');
echo '{"ch":1,"data":"DONE"}';

function createHead($page,$barcodeSHP,$ship_from,$ship_to,$dDate,$working_ship,$trip_no,$truck_license,$driver_Name,$phone)
{
	$headData = str_format('<table border="0">
	<tr>
		<td align="right" style="font-size:12px"></td>
	</tr>
</table>',$page);

	$headData .= '<table border="0">
	<tr>
		<td width="130"><img src="images/ttv-logo.gif" width="150"  height="46"/></td>
		<td width="10"></td>
		<td align="left" width="300" style="font-size:10px"><b>TITAN-VNS AUTO LOGISTICS CO.,LTD.</b><br/>
		49/66 MOO 5 TUNGSUNKLA SRIRACHA CHONBURI 20230<br/>
		Phone +66(0) 3840 1505-6,3804 1787-8<br/>
		Fax : +66(0) 3849 4300
		</td>
		<td align="right" width="218"><tcpdf method="write1DBarcode" params="'.$barcodeSHP.'"/></td>
	</tr>
</table>
<hr>
<table border="0">
	<tr>
		<td align="center"><b style="font-size:18px; margin-left:300px;">SHIPPING MANIFEST SHEET (TOACS)</b></td>
	</tr>
</table>
<hr />
<br>
<table border="0" style="margin-top:10px;" cellspacing="" cellpadding="2" style="font-size:13px">
	<tr>
		<td align="left" width="100"><b>Ship From :</b></td>
		<td align="left" width="140">'.$ship_from.'</td>
		<td align="left" width="100"><b>Ship To :</b></td>
		<td align="left" width="140">'.$ship_to.'</td>
		<td align="left" width="100"><b>Ship Date :</b></td>
		<td align="left" width="140">'.$dDate.'</td>
	</tr>
	<tr>
		<td align="left" width="100"><b>Working Shift :</b></td>
		<td align="left" width="140">'.$working_ship.'</td>
		<td align="left" width="100"><b>Trip No :</b></td>
		<td align="left" width="140">'.$trip_no.'</td>
		<td align="left" width="120"><b>Truck License :</b></td>
		<td align="left" width="110">'.$truck_license.'</td>
	</tr>
	<tr>
		<td align="left" width="100"><b>Driver Name :</b></td>
		<td align="left" width="140">'.$driver_Name.'</td>
		<td align="left" width="100"><b>Phone :</b></td>
		<td align="left" width="140">'.$phone.'</td>
	</tr>
</table>';
return $headData;
}

function createbox()
{
	$foot = '<br><br>
			<table style="font-size: 14px;" border="0" >
					<tbody>
					<tr  >
						<td align="center" width="220">
							<p>
								<span>&nbsp;</span><br>
								<span>TTV In Plant Receiving</span><br>
								<span>&nbsp;</span><br>
								<span>___________________</span><br>
								<span>(______/______/______)</span><br>
							</p>
						</td>
						<td align="center" width="220">
						</td>
						<td align="center" width="220">
							<p>
								<span>&nbsp;</span><br>
								<span>Data Entry</span><br>
								<span>&nbsp;</span><br>
								<span>___________________</span><br>
								<span>(______/______/______)</span><br>
							</p>
						</td>
					</tr>
					</tbody>
			</table>';

	return $foot;
}

function createHead1()
{
	$head1 = '&nbsp;<br>
			<table style="font-size: 12px;" border="1">
				<tr  style="background-color:#C8C8C8;">
					<td align="center" width="60" rowspan="2" ><b>No</b></td>
					<td align="center" width="120" rowspan="2"><b>Dock</b></td>
					<td align="center" width="120" rowspan="2"><b>Part No</b></td>
					<td align="center" width="120" rowspan="2"><b>Order No</b></td>
					<td align="center" width="80" rowspan="2"><b>Qty</b></td>
					<td align="center" width="80" ><b>Loading</b></td>
					<td align="center" width="80" ><b>Unload</b></td>
				</tr>
				<tr style="font-size:10px;background-color:#C8C8C8;">
					<td align="center" width="80"><b>ขึ้นงาน</b></td>
					<td align="center" width="80"><b>ลงงาน</b></td>
				</tr>
			';

	return $head1;
}

function createHead2()
{
	$head2 = '
			<table style="font-size: 12px;" border="1">
				<tr >
					<td align="left" width="300" rowspan="6">หมายเหตุ</td>
					<td align="center" width="360"  style="background-color:#C8C8C8;" ><b>บันทึกการเติมน้ำมันเชื้อเพลิง</b></td>
				</tr>
				<tr>
					<td align="left" width="200">วัน-เวลา ขณะเติม</td>
					<td align="center" width="160"></td>
				</tr>
				<tr>
					<td align="left" width="200">ชนิดเชื้อเพลิง</td>
					<td align="center" width="160">[ ] แก๊ส NGV  [ ] ดีเซล </td>
				</tr>
				<tr>
					<td align="left" width="200">เลขไมล์ขณะเติม</td>
					<td align="center" width="160"></td>
				</tr>
				<tr>
					<td align="left" width="200">ปริมาณ (ลิตร/กิโลเมตร)</td>
					<td align="center" width="160"></td>
				</tr>
				<tr>
					<td align="left" width="200">จำนวนเงินที่เติม (บาท)</td>
					<td align="center" width="160"></td>
				</tr>
			</table>
			';

	return $head2;
}

function createHead3($plan_time,$ttv_out)
{
	$head3 = '
			<table style="font-size: 12px;" border="1">
				<tr  style="background-color:#C8C8C8;">
					<td align="center" width="330"><b>DEPOT </b></td>
					<td align="center" width="330"><b>HONDA </b></td>
				</tr>
				<tr style="font-size:10px;background-color:#C8C8C8;">
					<td align="center" width="110"><b>Planning Time</b></td>
					<td align="center" width="110"><b>เวลาออก</b></td>
					<td align="center" width="110"><b>ลงชื่อผู้ปล่อยรถ</b></td>
					<td align="center" width="110"><b>เวลาเข้า</b></td>
					<td align="center" width="110"><b>เวลาออก</b></td>
					<td align="center" width="110"><b>ผู้รับสิ้นค้า</b></td>
				</tr>
				<tr style="font-size:40px;">
					<td align="center" width="110" style="font-size:26px;">'.$plan_time.'</td>
					<td align="center" width="110" style="font-size:26px;">'.$ttv_out.'</td>
					<td align="center" width="110"></td>
					<td align="center" width="110"></td>
					<td align="center" width="110"></td>
					<td align="center" width="110"></td>
				</tr></table>
			';

	return $head3;
}

function createHead4()
{
	$head4 = '
			<table style="font-size: 12px;" border="1">
				<tr  style="background-color:#C8C8C8;">
					<td align="center" width="330"><b>DEPOT </b></td>
					<td align="center" width="330"><b>เลขไมล์ </b></td>
				</tr>
				<tr style="font-size:10px;background-color:#C8C8C8;">
					<td align="center" width="165"><b>เวลาเข้า</b></td>
					<td align="center" width="165"><b>ลงชื่อผู้รับ EMP</b></td>
					<td align="center" width="165"><b>เลขไมล์เริ่ม</b></td>
					<td align="center" width="165"><b>เลขไมล์สิ้นสุด</b></td>
				</tr>
				<tr style="font-size:40px;">
					<td align="center" width="165"></td>
					<td align="center" width="165"></td>
					<td align="center" width="165"></td>
					<td align="center" width="165"></td>
				</tr></table>
			';

	return $head4;
}

function packing1()
{
	$pack1 = '<br><br>
			<table style="font-size: 12px;" border="1">
				<tr  style="background-color:#C8C8C8;">
					<td align="center" width="660"><b>Delivery Packaging To HONDA. (บรรจุภัณฑ์ที่จัดส่งเข้าฮอนด้า)</b></td>
				</tr>
				<tr style="font-size:10px;background-color:#C8C8C8;">
					<td align="center" width="110" rowspan="2"><b>Vendor</b></td>
					<td align="center" width="110"><b>PTB</b></td>
					<td align="center" width="110"><b>CPB</b></td>
					<td align="center" width="110"><b>STR</b></td>
					<td align="center" width="110"><b>PPL</b></td>
					<td align="center" width="110"><b>WPL</b></td>
				</tr>
				<tr style="font-size:10px;background-color:#C8C8C8;">
					<td align="center" width="110"><b>กล่องพลาสติก</b></td>
					<td align="center" width="110"><b>กล่องลูกฟูก</b></td>
					<td align="center" width="110"><b>แร็คเหล็ก</b></td>
					<td align="center" width="110"><b>พาเลทพลาสติก</b></td>
					<td align="center" width="110"><b>พาเลทไม้</b></td>
				</tr>
				<tr style="font-size:16px;">
					<td align="center" width="110"></td>
					<td align="center" width="110"></td>
					<td align="center" width="110"></td>
					<td align="center" width="110"></td>
					<td align="center" width="110"></td>
					<td align="center" width="110"></td>
				</tr>
				<tr style="font-size:16px;">
					<td align="center" width="110"></td>
					<td align="center" width="110"></td>
					<td align="center" width="110"></td>
					<td align="center" width="110"></td>
					<td align="center" width="110"></td>
					<td align="center" width="110"></td>
				</tr>
				<tr style="font-size:16px;">
					<td align="center" width="110"></td>
					<td align="center" width="110"></td>
					<td align="center" width="110"></td>
					<td align="center" width="110"></td>
					<td align="center" width="110"></td>
					<td align="center" width="110"></td>
				</tr>
			</table>
			';

	return $pack1;
}

function packing2()
{
	$pack2 = '<br><br>
			<table style="font-size: 12px;" border="1">
				<tr  style="background-color:#C8C8C8;">
					<td align="center" width="660"><b>Empty Packaging From HONDA. (บรรจุภัณฑ์ปล่าวที่รับจากเข้าฮอนด้าฮอนด้า)</b></td>
				</tr>
				<tr style="font-size:10px;background-color:#C8C8C8;">
					<td align="center" width="110" rowspan="2"><b>Vendor</b></td>
					<td align="center" width="110"><b>PTB</b></td>
					<td align="center" width="110"><b>CPB</b></td>
					<td align="center" width="110"><b>STR</b></td>
					<td align="center" width="110"><b>PPL</b></td>
					<td align="center" width="110"><b>WPL</b></td>
				</tr>
				<tr style="font-size:10px;background-color:#C8C8C8;">
					<td align="center" width="110"><b>กล่องพลาสติก</b></td>
					<td align="center" width="110"><b>กล่องลูกฟูก</b></td>
					<td align="center" width="110"><b>แร็คเหล็ก</b></td>
					<td align="center" width="110"><b>พาเลทพลาสติก</b></td>
					<td align="center" width="110"><b>พาเลทไม้</b></td>
				</tr>
				<tr style="font-size:16px;">
					<td align="center" width="110"></td>
					<td align="center" width="110"></td>
					<td align="center" width="110"></td>
					<td align="center" width="110"></td>
					<td align="center" width="110"></td>
					<td align="center" width="110"></td>
				</tr>
				<tr style="font-size:16px;">
					<td align="center" width="110"></td>
					<td align="center" width="110"></td>
					<td align="center" width="110"></td>
					<td align="center" width="110"></td>
					<td align="center" width="110"></td>
					<td align="center" width="110"></td>
				</tr>
				<tr style="font-size:16px;">
					<td align="center" width="110"></td>
					<td align="center" width="110"></td>
					<td align="center" width="110"></td>
					<td align="center" width="110"></td>
					<td align="center" width="110"></td>
					<td align="center" width="110"></td>
				</tr>
			</table>
			';

	return $pack2;
}
