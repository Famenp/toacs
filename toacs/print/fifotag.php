<?php


// Include the main TCPDF library (search for installation path).
include('../php/connection.php');
require_once('tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
// set document information
$pdf->SetCreator(PDF_CREATOR);
// $pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('FIFO TAG');//title

$pdf->SetMargins(10, 5, 10,5);

// set auto page breaks
/*$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);*/
$pdf->SetAutoPageBreak(TRUE, 0);
// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('freeserif', '');

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
//

// Set some content to print
$n = 1;
$barcodeFIFO= TCPDF_STATIC::serializeTCPDFtagParameters(array('1703220012', 'C128', '', '', 80, 28, 0.4, array('position'=>'L', 'border'=>false, 'padding'=>1, 'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>true, 'font'=>'helvetica', 'fontsize'=>20, 'stretchtext'=>4,'cellfitalign'=>'C','stretch'=>true), 'N'));
while ( $n < 3) {
	$html = createmastertag($barcodeFIFO);
	$n++;
	$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
}
// Print text using writeHTMLCell()



// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('example_001.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+

function createmastertag($barcodeFIFO)
{
	$mastertag = '<table style="height: 240px;" border="1" width="660">
	<tbody>
		<tr >
			<td colspan="2" align="left" >
				<p style="font-size:54px;"><img src="images/ttv-logo.gif" width="140"  height="30"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TAG FIFO</p>
			</td>
		</tr>
		<tr style="font-size:18px;">
			<td >
				<p> Pickup Sheet No : PUS170300010</p>
			</td>
			<td>
				<p> QTY  : 130</p>
			</td>
		</tr>
		<tr style="font-size:18px;">
			<td>
				<p> Part No.  : EB3T14301EE</p>
			</td>
			<td>
				<p> PART NAME  : CA ASY BAT TO GRD</p>
			</td>
		</tr>
		<tr style="font-size:18px;">
			<td>
				<p> REVISION No.  : 1703010203-1</p>
			</td>
			<td>
				<p> SNP  : 10</p>
			</td>
		</tr>
		<tr style="font-size:18px;">
			<td>
				<p> LOCATION PUT : RB1</p>
			</td>
			<td>
				<p> LOCATION PICK : A01-02-2003</p>
			</td>
		</tr>
		<tr >
			<td colspan="2" align="center">
				<p style="font-size:20px;">
					<span>22-03-2017  13:12:00</span><br>
					<span style="font-size:24px;">FIFO NO.</span><br>
					<tcpdf method="write1DBarcode" params="'.$barcodeFIFO.'"/>
				</p>
			</td>
		</tr>
			
		
		<tr >
			<td colspan="2" align="center">
				<p>
					<span style="font-size:24px;">PART NO.</span><br>
					<span style="font-size:50px;">EB3T14301EE</span>
				</p>
			</td>
		</tr>
	</tbody>
</table><br><br>';

	return $mastertag;
}



