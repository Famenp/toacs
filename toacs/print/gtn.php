<?php
include('../php/connection.php');
require_once('tcpdf/tcpdf.php');
$data = $_REQUEST['data1'];
// $printname = $_REQUEST['printname'];

/*echo $data;
exit();*/

$q1  = "SELECT tran.Doc_No,DATE_FORMAT(tran.Out_Date,'%d-%m-%Y') as outdate,tran.user_id,tran.SO_NO,odr.Pick_no,usr.user_fName FROM tbl_transaction tran 
LEFT JOIN tbl_order odr ON tran.SO_NO = odr.SO_no
LEFT JOIN tbl_user usr ON tran.user_id = usr.user_id
WHERE tran.Doc_No = '$data ' LIMIT 1";

if ($result = $mysqli->query($q1)) 
{
	if ($result->num_rows == 0)
		{
			echo 'ไม่พบ  ในระบบ';
			$mysqli->close();
			exit();
		}
	while ($srow = $result->fetch_assoc()) 
	{
			$Doc_No =  $srow["Doc_No"];
	    	$Out_Date =  $srow["outdate"];
	    	$user_id =  $srow["user_id"];
	    	$SO_NO =  $srow["SO_NO"];
	    	$Pick_no =  $srow["Pick_no"];
	    	$user_fName =  $srow["user_fName"];
	}
}
$q2 = "SELECT pm.part_supplier,pm.part_name,tran.LOT,tran.Box_No,(tran.Qty*-1) as Qty FROM tbl_transaction tran 
LEFT JOIN tbl_partmaster pm ON tran.Part_ID = pm.part_id
WHERE tran.Doc_No = '$data' AND tran.Tran_Type = 'OUT'";

if ($result = $mysqli->query($q2)) 
{
	$argtndetail = array();
	while ($srow2 = $result->fetch_assoc()) 
	{
			$crow = $result->num_rows;
			$argtndetail[] = $srow2["part_supplier"].",".$srow2["part_name"].",".$srow2["LOT"].",".$srow2["Box_No"].",".$srow2["Qty"];
	}
}

$pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetCreator(PDF_CREATOR);
// $pdf->SetTitle($Doc_no);//title
$pdf->SetTitle('GTN');//title

$pdf->SetMargins(10, 5, 10,5);
$pdf->SetAutoPageBreak(TRUE, 0);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}
$pdf->setFontSubsetting(true);
$pdf->SetFont('freeserif', '');
// $pdf->AddPage();
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
//
$thead = '&nbsp;<br><table border="1" cellspacing="0" cellpadding="2">
	<tr style="font-size:12px;background-color:#C8C8C8;" >
		<td align="center" width="30"><b>No. </b></td>
		<td align="center" width="150"><b>Part No</b></td>
		<td align="center" width="200"><b>Part Name</b></td>
		<td align="center" width="150"><b>Lot</b></td>
		<td align="center" width="70"><b>Box No.</b></td>
		<td align="center" width="70"><b>Qty</b></td>
	</tr>';
$barcodeGTN= TCPDF_STATIC::serializeTCPDFtagParameters(array($Doc_No, 'C128', '', '', 0, 16, 0.4, array('position'=>'R', 'border'=>false, 'padding'=>1, 'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>true, 'font'=>'helvetica', 'fontsize'=>5, 'stretchtext'=>4,'cellfitalign'=>'R','stretch'=>true), 'N'));

$rgrn = 1;
$ndetail = count($argtndetail);
// $ndetail = 88;
$d = 39;
$ccpus = 0 ;
$field = 0;
$nn = 1;
$n = 1;
$p = $d*$nn;
$tableData = $thead;
$allPage = ceil($crow/$d);
// $allPage = ceil(88/$d);
// $html = createHead('page 1/',$barcodeGTN,$ReceivedDate,$Doc_no,$user_fName);
$html = createHead('page 1/',$barcodeGTN,$Doc_No,$Pick_no,$SO_NO,$Out_Date,$user_fName);
$tableData = $thead;
while ( $ccpus <= $ndetail-1) {
	if ($field == 0) 
			{
				$field = 1;
				$pdf->AddPage();
				$html = createHead('page 1/'.$allPage,$barcodeGTN,$Doc_No,$Pick_no,$SO_NO,$Out_Date,$user_fName);
			}
			if($ccpus > $p)
    	      {
    	      	// $ccpus++;
    	      	$n++;
    	      	$nn++;
    	      	$p = $d*$nn;
    	      	$tableData .='</table>';
    	      	$html .= $tableData;
    	      	$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
    	      	$pdf->AddPage();
    	      	$html = createHead('page'.$n.'/'.$allPage,$barcodeGTN,$Doc_No,$Pick_no,$SO_NO,$Out_Date,$user_fName);
    	      	$tableData = $thead;
    	      }
    	      $data= explode(",",$argtndetail[$ccpus]);
			  $tableData .= '<tr style="font-size:10px" >';
			  $tableData .= '<td align="center" width="30">'.$rgrn.'</td>';
			  $tableData .= '<td align="center" width="150">'. $data[0].'</td>';
			  $tableData .= '<td align="center" width="200">'. $data[1].'</td>';
			  $tableData .= '<td align="center" width="150">'. $data[2].'</td>';
			  $tableData .= '<td align="center" width="70">'. $data[3].'</td>';
			  $tableData .= '<td align="center" width="70">'. $data[4].'</td>';
			  $tableData .= '</tr>';
			  $ccpus++;$rgrn++;
}
/*$tableData .= '<tr style="font-size:12px" >';
$tableData .= '<td align="center" width="200"></td>';
$tableData .= '<td align="right" width="200">Total:</td>';
$tableData .= '<td align="center" width="200"></td>';
$tableData .= '<td align="center" width="60"></td>';
$tableData .= '</tr>';*/

$tableData .='</table>';
$html .= $tableData;
$html .= createbox();

$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// $pdf->Output('123'.'.pdf', 'I');
$randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0,2);
$pdf->Output("D:\\printfile\\".$Doc_No.$randomString.'-Printer_outbound.pdf', 'F');
echo '{"ch":1,"data":"DONE"}';
function createHead($page,$barcodeGTN,$Doc_No,$Pick_no,$SO_NO,$Out_Date,$user_fName)
{
	$headData = str_format('<table border="0">
	<tr>
		<td align="right" style="font-size:11px">{1} </td>
	</tr>
</table>',$page);

	$headData .= '<table border="0">
	<tr>
		<td width="130"><img src="images/abt-logo.gif" width="120"  height="50"/></td>
		<td width="10"></td>
		<td align="left" width="300" style="font-size:10px"><b>ALBATROSS LOGISTICS CO., LTD.</b><br/>
		336/7 MOO 7 BOWIN, SRIRACHA CHONBURI 20230<br/>
		Phone +66 38 058 021, +66 38 058 081-2<br/>
		Fax : +66 38 058 007
		</td>
		<td align="right" width="218"><tcpdf method="write1DBarcode" params="'.$barcodeGTN.'"/></td>
	</tr>
</table>
<hr>
<table border="0">
	<tr>
		<td align="center"><b style="font-size:18px; margin-left:300px;">GOOD TRANSFER NOTE (TOACS)</b></td>
	</tr>
</table>
<hr />
<br>
<table border="0" style="margin-top:10px;" cellspacing="" cellpadding="2" style="font-size:13px">
	<tr>
		<td align="left" width="100"><b>Document No. :</b></td>
		<td align="left" width="140">'.$Doc_No.'</td>
		<td align="left" width="100"><b>PICK :</b></td>
		<td align="left" width="140">'.$Pick_no.'</td>
		<td align="left" width="100"><b>SO No :</b></td>
		<td align="left" width="140">'.$SO_NO.'</td>
	</tr>
	<tr>
		<td align="left" width="100"><b>Date :</b></td>
		<td align="left" width="140">'.$Out_Date.'</td>
		<td align="left" width="100"><b>Create By :</b></td>
		<td align="left" width="140">'.$user_fName.'</td>
	</tr>
</table>';
return $headData;
}

function createbox()
{
	$foot = '<br><br>
			<table style="font-size: 14px;" border="0" >
					<tbody>
					<tr  >
						<td align="center" width="220">
							<p>
								<span>&nbsp;</span><br>
								<span>Data Entry</span><br>
								<span>&nbsp;</span><br>
								<span>___________________</span><br>
								<span>(______/______/______)</span><br>
							</p>
						</td>
						<td align="center" width="220">
							<p>
								<span>&nbsp;</span><br>
								<span>Delivered By</span><br>
								<span>&nbsp;</span><br>
								<span>___________________</span><br>
								<span>(______/______/______)</span><br>
							</p>
						</td>
						<td align="center" width="220">
							<p>
								<span>&nbsp;</span><br>
								<span>Received By </span><br>
								<span>&nbsp;</span><br>
								<span>___________________</span><br>
								<span>(______/______/______)</span><br>
							</p>
						</td>
					</tr>
					</tbody>
			</table>';

	return $foot;
}

