<?php
include('../start.php');
include('../php/connection.php');
require_once('tcpdf/tcpdf.php');
$data = $_REQUEST['data'];
$q2  = "CALL SP_Shipping_PrintPackingSlip('$data')";
if ($result2 = $mysqli->query($q2)) 
{
	if ($result2->num_rows == 0)
		{
			echo 'ไม่พบ  ในระบบ';
			$mysqli->close();
			exit();
		}
	while ($srow = $result2->fetch_assoc()) 
	{
			$Doc =  $srow["Doc_Type"];
	    	$PSNO =  $srow["Packing_Slip_Number"];
	    	$SNAME =  $srow["Supplier_Name"];
	    	$SADD =  $srow["Supplier_Address"];
	    	$STAX =  $srow["Supplier_Tax_ID"];
	    	$CNAME =  $srow["Customer_Name"];
	    	$CADD1 =  $srow["Customer_Address_Line_1"];
	    	$CADD2 =  $srow["Customer_Address_Line_2"];
	    	$CADD3 =  $srow["Customer_Address_Line_3"];
	    	$CTAX =  $srow["Customer_Tax_ID"];
	    	$IsDATE =  $srow["Issued_Date"];
	    	$CMMS =  $srow["CMMS_Plant_Code"];
	    	$DCODE =  $srow["Dock_Code"];
	    	$DTIMES =  $srow["Delivery_DateTime"];
	    	$SGSDB =  $srow["Supplier_GSDB_Code"];
	    	$RTIRPs =  $srow["Round_Trip"];
	    	$TL =  $srow["Truck_License"];
	    	$DName =  $srow["Driver_Name"];
	    	$sup =  $srow["Supplier_Code"];
	    	if ($mysqli->next_result())
	    	{
	    		if ($result2 = $mysqli->store_result())
	    		{
	    			$arpackpartnum = array();
	    			$arpackpartname = array();
	    			$arpackunit = array();
	    			$arpackqty = array();
	    			$arpackremark = array();
	    			while ($srow2 = $result2->fetch_assoc())
	    			{
	    				$arpackpartnum[] = $srow2["Part_Number"];
	    				$arpackpartname[] = $srow2["Part_Name"];
	    				$arpackunit[] = $srow2["Unit"];
	    				$arpackqty[] = $srow2["Qty"];
	    				$arpackremark[] = $srow2["Remarks"];
	    			}
	    		}
	    	}
	}
}
else
{
	echo "2";
}
$pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetTitle('PACKINGSLIP');//title
$pdf->SetMargins(10, 5, 10,5);
$pdf->SetAutoPageBreak(TRUE, 0);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}
$pdf->setFontSubsetting(true);
$pdf->SetFont('freeserif', '');
// $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));


function packingsleep($Doc,$SNAME,$PSNO,$SADD,$STAX,$CNAME,$CADD1,$CADD2,$CADD3,$CTAX,$IsDATE,$CMMS,$DCODE,$DTIMES,$SGSDB,$RTIRPs,$arpackpartnum,$arpackpartname,$arpackunit,$arpackqty,$arpackremark,$TL,$DName,$flag,$c,$totalpage)
{
	// $pdf->AddPage();
	$barcodePS= TCPDF_STATIC::serializeTCPDFtagParameters(array($PSNO, 'C128', '', '', 0, 10, 0.4, array('position'=>'R', 'border'=>false, 'padding'=>1, 'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>true, 'font'=>'helvetica', 'fontsize'=>8, 'stretchtext'=>4,'cellfitalign'=>'R','stretch'=>true), 'N'));
	$thead2 = '<table border="1" cellspacing="0" cellpadding="2">
		<tr style="font-size:10px;background-color:#C8C8C8;" >
			<td align="center" width="40"><p><span><b>ลำดับ</b></span><br><span>ITEM</span></p></td>
			<td align="center" width="200"><p><span><b>ชื่อสินค้า</b></span><br><span>Part Name</span></p></td>
			<td align="center" width="160"><p><span><b>รหัสสินค้า</b></span><br><span>Part Number</span></p></td>
			<td align="center" width="70"><p><span><b>จำนวน</b></span><br><span>QTY.</span></p></td>
			<td align="center" width="70"><p><span><b>หน่วย</b></span><br><span>UNIT</span></p></td>
			<td align="center" width="125"><p><span><b>หมายเหตุ</b></span><br><span>REMARK</span></p></td>
		</tr>';

	$n1 = $flag;
	$n2 = $flag-1;
	$n3 = $flag-1;
	$n4 = $flag-1;
	$n5 = $flag-1;
	$n6 = $flag-1;
	if ($Doc=="PS") 
	{
		if ($flag<31) {
			$page = 1 ;
		}
		else if ($flag<61)
		{
			$page = 2 ;
		}
		else if($flag<91)
		{
			$page = 3 ;
		}
		else
		{
			$page = 4 ;
		}
		$html = createHeadHirutaSUMITOMO($totalpage,$page,$barcodePS,$Doc,$PSNO,$SNAME,$SADD,$STAX,$CNAME,$CADD1,$CADD2,$CADD3,$CTAX,$IsDATE,$CMMS,$DCODE,$DTIMES,$SGSDB,$RTIRPs);
	}
	else if ($Doc == "GTN") 
	{
		if ($flag<31) {
			$page = 1 ;
		}
		else if ($flag<61)
		{
			$page = 2 ;
		}
		else if($flag<91)
		{
			$page = 3 ;
		}
		else
		{
			$page = 4 ;
		}
		$html = createHeadLEAR($totalpage,$page,$barcodePS,$Doc,$PSNO,$SNAME,$SADD,$STAX,$CNAME,$CADD1,$CADD2,$CADD3,$CTAX,$IsDATE,$CMMS,$DCODE,$DTIMES,$SGSDB,$RTIRPs);
	}

		$tableData = $thead2;
		$tableData .= '<tr style="font-size:13px">';
		$tableData .= '<td align="center" width="40" height="500"><p>';
		while ( $n1 <= $c) {
			# code...
					  $tableData .= '<span>'.$n1.'</span><br>';
					  $n1++;
		}
					  $tableData .= '</p></td>';
		$tableData .= '<td align="center" width="200"><p>';
		while ( $n2 < $c) {
			# code...
					  $tableData .= '<span>'.$arpackpartname[$n2].'</span><br>';
					  $n2++;
		}
					  $tableData .= '</p></td>';
		$tableData .= '<td align="center" width="160"><p>';
		while ( $n3 < $c) {
			# code...
					  $tableData .= '<span>'.$arpackpartnum[$n3].'</span><br>';
					  $n3++;
		}
					  $tableData .= '</p></td>';
		$tableData .= '<td align="center" width="70"><p>';
		while ( $n4 < $c) {
			# code...
					  $tableData .= '<span>'.$arpackqty[$n4].'</span><br>';
					  $n4++;
		}
					  $tableData .= '</p></td>';
		$tableData .= '<td align="center" width="70"><p>';
		while ( $n5 < $c) {
			# code...
					  $tableData .= '<span>'.$arpackunit[$n5].'</span><br>';
					  $n5++;
		}
					  $tableData .= '</p></td>';
		$tableData .= '<td align="center" width="125"><p>';
		while ( $n6 < $c) {
			# code...
					  $tableData .= '<span>'.$arpackremark[$n6].'</span><br>';
					  $n6++;
		}
					  $tableData .= '</p></td>';
		$tableData .= '</tr>';
		$tableData .='</table>';
		$html .= $tableData;
		if ($Doc=="PS")
		{
			$html .= createbox($TL,$DName);
		}
		else if ($Doc == "GTN") {
			$html .= createbox2($TL,$DName);
		}
		return $html;
}
$c = count($arpackpartnum);
// $c = 3;
if ($c<=30) 
{
	$flag = 1;
	$totalpage = 1; 
	$pdf->AddPage();
	$html = packingsleep($Doc,$SNAME,$PSNO,$SADD,$STAX,$CNAME,$CADD1,$CADD2,$CADD3,$CTAX,$IsDATE,$CMMS,$DCODE,$DTIMES,$SGSDB,$RTIRPs,$arpackpartnum,$arpackpartname,$arpackunit,$arpackqty,$arpackremark,$TL,$DName,$flag,$c,$totalpage);
	$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
}
else if($c<=60)
{
	$flag = 1;
	$totalpage = 2; 
	$pdf->AddPage();
	$html = packingsleep($Doc,$SNAME,$PSNO,$SADD,$STAX,$CNAME,$CADD1,$CADD2,$CADD3,$CTAX,$IsDATE,$CMMS,$DCODE,$DTIMES,$SGSDB,$RTIRPs,$arpackpartnum,$arpackpartname,$arpackunit,$arpackqty,$arpackremark,$TL,$DName,$flag,30,$totalpage);
	$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

	$flag = 31;
	$totalpage = 2; 
	$pdf->AddPage();
	$html = packingsleep($Doc,$SNAME,$PSNO,$SADD,$STAX,$CNAME,$CADD1,$CADD2,$CADD3,$CTAX,$IsDATE,$CMMS,$DCODE,$DTIMES,$SGSDB,$RTIRPs,$arpackpartnum,$arpackpartname,$arpackunit,$arpackqty,$arpackremark,$TL,$DName,$flag,$c,$totalpage);
	$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
}
else if ($c<=90)
{
	$flag = 1;
	$totalpage = 3; 
	$pdf->AddPage();
	$html = packingsleep($Doc,$SNAME,$PSNO,$SADD,$STAX,$CNAME,$CADD1,$CADD2,$CADD3,$CTAX,$IsDATE,$CMMS,$DCODE,$DTIMES,$SGSDB,$RTIRPs,$arpackpartnum,$arpackpartname,$arpackunit,$arpackqty,$arpackremark,$TL,$DName,$flag,30,$totalpage);
	$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

	$flag = 31;
	$totalpage = 3; 
	$pdf->AddPage();
	$html = packingsleep($Doc,$SNAME,$PSNO,$SADD,$STAX,$CNAME,$CADD1,$CADD2,$CADD3,$CTAX,$IsDATE,$CMMS,$DCODE,$DTIMES,$SGSDB,$RTIRPs,$arpackpartnum,$arpackpartname,$arpackunit,$arpackqty,$arpackremark,$TL,$DName,$flag,60,$totalpage);
	$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

	$flag = 61;
	$totalpage = 3; 
	$pdf->AddPage();
	$html = packingsleep($Doc,$SNAME,$PSNO,$SADD,$STAX,$CNAME,$CADD1,$CADD2,$CADD3,$CTAX,$IsDATE,$CMMS,$DCODE,$DTIMES,$SGSDB,$RTIRPs,$arpackpartnum,$arpackpartname,$arpackunit,$arpackqty,$arpackremark,$TL,$DName,$flag,$c,$totalpage);
	$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
}
else
{
	$flag = 1;
	$totalpage = 4; 
	$pdf->AddPage();
	$html = packingsleep($Doc,$SNAME,$PSNO,$SADD,$STAX,$CNAME,$CADD1,$CADD2,$CADD3,$CTAX,$IsDATE,$CMMS,$DCODE,$DTIMES,$SGSDB,$RTIRPs,$arpackpartnum,$arpackpartname,$arpackunit,$arpackqty,$arpackremark,$TL,$DName,$flag,30,$totalpage);
	$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

	$flag = 31;
	$totalpage = 4; 
	$pdf->AddPage();
	$html = packingsleep($Doc,$SNAME,$PSNO,$SADD,$STAX,$CNAME,$CADD1,$CADD2,$CADD3,$CTAX,$IsDATE,$CMMS,$DCODE,$DTIMES,$SGSDB,$RTIRPs,$arpackpartnum,$arpackpartname,$arpackunit,$arpackqty,$arpackremark,$TL,$DName,$flag,60,$totalpage);
	$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

	$flag = 61;
	$totalpage = 4; 
	$pdf->AddPage();
	$html = packingsleep($Doc,$SNAME,$PSNO,$SADD,$STAX,$CNAME,$CADD1,$CADD2,$CADD3,$CTAX,$IsDATE,$CMMS,$DCODE,$DTIMES,$SGSDB,$RTIRPs,$arpackpartnum,$arpackpartname,$arpackunit,$arpackqty,$arpackremark,$TL,$DName,$flag,90,$totalpage);
	$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

	$flag = 91;
	$totalpage = 4; 
	$pdf->AddPage();
	$html = packingsleep($Doc,$SNAME,$PSNO,$SADD,$STAX,$CNAME,$CADD1,$CADD2,$CADD3,$CTAX,$IsDATE,$CMMS,$DCODE,$DTIMES,$SGSDB,$RTIRPs,$arpackpartnum,$arpackpartname,$arpackunit,$arpackqty,$arpackremark,$TL,$DName,$flag,$c,$totalpage);
	$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
}
if ($sup=="SEWT") {
	$prifix = "PSS";
}
if ($sup=="HAS") {
	$prifix = "PSH";
}
if ($sup=="LEAR") {
	$prifix = "GTN";
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
$randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0,2);
$pdf->Output("C:\\Report\\".$prifix.$PSNO.$randomString.'.pdf', 'F');
// $pdf->Output($prifix.$PSNO.$randomString.'.pdf', 'I');
// $pdf->Output("C:\\Report\\SUM".$printgroup.$refnum.'.pdf', 'F');
echo '{"ch":1,"data":"DONE"}';
// Close and output PDF document
// This method has several options, check the source code documentation for more information.
// $pdf->Output($PSNO.'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+

function createHeadHirutaSUMITOMO($totalpage,$page,$barcodePS,$Doc,$PSNO,$SNAME,$SADD,$STAX,$CNAME,$CADD1,$CADD2,$CADD3,$CTAX,$IsDATE,$CMMS,$DCODE,$DTIMES,$SGSDB,$RTIRPs)
{
	$headData = str_format('<br><table border="1">
	<tr>
		<td align="center"><b style="font-size:22px;">TEMPORARY INVOICE</b></td>
	</tr>
</table>');

	$headData .= '<table border="0">
	<tr>
		<td width="460" >
			<p>
				<span style="font-size:12px;"><b>'.$SNAME.'</b></span><br>
				<span style="font-size:10px;">'.$SADD.'</span><br>
				<span style="font-size:10px;">'.$STAX.'</span><br>
			</p>
		</td>
		<td width="206" align="right"><br>
			<span>Page '.$page.'/'.$totalpage.'</span><br>
			<span ><tcpdf method="write1DBarcode" params="'.$barcodePS.'"/></span><br>
			<span style="font-size:12px;">Round No : '.$RTIRPs.'</span><br>
			<span style="font-size:12px;">Issue Date : '.$IsDATE.'</span><br>
		</td>
	</tr>
</table>

<table border="0">
	<tr>
		<td align="left" width="60">
			<p>
				<span style="font-size:10px;">Name :</span><br>
				<span style="font-size:10px;">Address :</span>
			</p>
		</td>
		<td align="left" width="320">
			<p>
				<span style="font-size:10px;">'.$CNAME.'</span><br>
				<span style="font-size:10px;">'.$CADD1.'</span><br>
				<span style="font-size:10px;">'.$CADD2.'</span><br>
				<span style="font-size:10px;">'.$CADD3.'</span><br>
				<span style="font-size:10px;">'.$CTAX.'</span><br>
			</p>
		</td>
		<td align="right"  width="156">
			<p>
				<span style="font-size:14px;"><b>Packing Slip Number :</b></span><br>
				<span style="font-size:14px;"><b>CMMS Plant Code :</b></span><br>
				<span style="font-size:14px;"><b>Dock Code :</b></span><br>
				<span style="font-size:14px;"><b>Delivery Time :</b></span><br>
				<span style="font-size:14px;"><b>Supplier Code :</b></span><br>
			</p>
		</td>
		<td align="left"  width="130">
			<p>
				<span style="font-size:14px;"><b> '.$PSNO.'</b></span><br>
				<span style="font-size:14px;"><b> '.$CMMS.'</b></span><br>
				<span style="font-size:14px;"><b> '.$DCODE.'</b></span><br>
				<span style="font-size:14px;"><b> '.$DTIMES.'</b></span><br>
				<span style="font-size:14px;"><b> '.$SGSDB.'</b></span><br>
			</p>
		</td>
	</tr>
</table>';
return $headData;
}

function createHeadLEAR($totalpage,$page,$barcodePS,$Doc,$PSNO,$SNAME,$SADD,$STAX,$CNAME,$CADD1,$CADD2,$CADD3,$CTAX,$IsDATE,$CMMS,$DCODE,$DTIMES,$SGSDB,$RTIRPs)
{
	$headData = str_format('<br><table border="1">
	<tr>
		<td align="center"><b style="font-size:22px;">GOODS TRANSFER NOTE</b></td>
	</tr>
</table>');
	
	$headData .= '<table border="0">
	<tr>
		<td width="460" >
			<p>
				<span style="font-size:12px;"><b>'.$SNAME.'</b></span><br>
				<span style="font-size:10px;">'.$SADD.'</span><br>
				<span style="font-size:10px;">'.$STAX.'</span><br>
			</p>
		</td>
		<td width="206" align="right"><br>
			<span>Page '.$page.'/'.$totalpage.'</span><br>
			<span ><tcpdf method="write1DBarcode" params="'.$barcodePS.'"/></span><br>
			<span style="font-size:12px;">Round No : '.$RTIRPs.'</span><br>
			<span style="font-size:12px;">Issue Date : '.$IsDATE.'</span><br>
		</td>
	</tr>
</table>

<table border="0">
	<tr>
		<td align="left" width="60">
			<p>
				<span style="font-size:10px;">Name :</span><br>
				<span style="font-size:10px;">Address :</span>
			</p>
		</td>
		<td align="left" width="320">
			<p>
				<span style="font-size:10px;">'.$CNAME.'</span><br>
				<span style="font-size:10px;">'.$CADD1.'</span><br>
				<span style="font-size:10px;">'.$CADD2.'</span><br>
				<span style="font-size:10px;">'.$CADD3.'</span><br>
				<span style="font-size:10px;">'.$CTAX.'</span><br>
			</p>
		</td>
		<td align="right"  width="156">
			<p>
				<span style="font-size:14px;"><b>Packing Slip Number :</b></span><br>
				<span style="font-size:14px;"><b>CMMS Plant Code :</b></span><br>
				<span style="font-size:14px;"><b>Dock Code :</b></span><br>
				<span style="font-size:14px;"><b>Delivery Time :</b></span><br>
				<span style="font-size:14px;"><b>Supplier Code :</b></span><br>
			</p>
		</td>
		<td align="left"  width="130">
			<p>
				<span style="font-size:14px;"><b> '.$PSNO.'</b></span><br>
				<span style="font-size:14px;"><b> '.$CMMS.'</b></span><br>
				<span style="font-size:14px;"><b> '.$DCODE.'</b></span><br>
				<span style="font-size:14px;"><b> '.$DTIMES.'</b></span><br>
				<span style="font-size:14px;"><b> '.$SGSDB.'</b></span><br>
			</p>
		</td>
	</tr>
</table>';
return $headData;
}

function createbox($TL,$DName)
{
	$boxdata = '<br><br>
	<table style="font-size: 16px;" border="1">
		<tbody>
		<tr>
			<td>
				<p>
					<span style="font-size:12px;">Truck No.(ทะเบียนรถ)  '.$TL.'</span><br>
					<span style="font-size:12px;">Driver Name (ชื่อ) '.$DName.'</span><br>
					<span style="font-size:12px;">Departure Time (เวลารถออก) ...................</span>
				</p>
			</td>
			<td align="center">
				<p>
					<span style="font-size:12px;">ผู้ออกใบส่งสินค้า (TTV)</span><br>
					<span style="font-size:12px;">Issued by</span><br>
					<span style="font-size:12px;">....................................</span><br>
					<span style="font-size:12px;">วันที่ : ........./........./...........</span>
				</p>
			</td>
			<td align="center">
				<p>
					<span style="font-size:12px;">ผู้รับสินค้า (AAT)</span><br>
					<span style="font-size:12px;">Receive by</span><br>
					<span style="font-size:12px;">....................................</span><br>
					<span style="font-size:12px;">วันที่ : ........./........./...........</span>
				</p>
			</td>
		</tr>
		
		</tbody>
</table>
<table style="font-size: 12px;" border="0">
	<tr>
		<td  width="515">
		</td>
		<td  width="155">
			<p>
				<span align="left">สีขาว ต้นฉบับสำหรับ Supplier</span><br>
				<span align="left">สีเขียว สำเนาสำหรับ TTV</span><br>
				<span align="left">สีชมพู สำเนาสำหรับ AAT</span><br>
			</p>
		</td>
	</tr>
</table>';
	return $boxdata;
}

function createbox2($TL,$DName)
{
	$boxdata = '<br><br>
	<table style="font-size: 16px;" border="1">
		<tbody>
		<tr>
			<td>
				<p>
					<span style="font-size:12px;">Truck No.(ทะเบียนรถ)  '.$TL.'</span><br>
					<span style="font-size:12px;">Driver Name (ชื่อ) '.$DName.'</span><br>
					<span style="font-size:12px;">Departure Time (เวลารถออก) ...................</span>
				</p>
			</td>
			<td align="center">
				<p>
					<span style="font-size:12px;">ผู้ออกใบส่งสินค้า (TTV)</span><br>
					<span style="font-size:12px;">Issued by</span><br>
					<span style="font-size:12px;">....................................</span><br>
					<span style="font-size:12px;">วันที่ : ........./........./...........</span>
				</p>
			</td>
			<td align="center">
				<p>
					<span style="font-size:12px;">ผู้รับสินค้า (AAT)</span><br>
					<span style="font-size:12px;">Receive by</span><br>
					<span style="font-size:12px;">....................................</span><br>
					<span style="font-size:12px;">วันที่ : ........./........./...........</span>
				</p>
			</td>
		</tr>
		
		</tbody>
</table>
';
	return $boxdata;
}
