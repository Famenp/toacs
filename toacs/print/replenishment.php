<?php
include('../php/connection.php');
require_once('tcpdf/tcpdf.php');
date_default_timezone_set("Asia/Bangkok");
$data = $_REQUEST['data'];
$data2 = $_REQUEST['data2'];
$q2  = "CALL SP_Replen_Search('$data2','$data')";
if ($result2 = $mysqli->query($q2)) 
{
	if ($result2->num_rows == 0)
		{
			echo 'ไม่พบ  ในระบบ';
			$mysqli->close();
			exit();
		}
	$arrepdetail = array();
	while ($srow = $result2->fetch_assoc()) 
	{
			$crow = $result2->num_rows;
	    	$arrepdetail[] = $srow["Part_Number"].",".$srow["Part_Name"].",".$srow["Storage_Qty"].",".$srow["Suggest_Storage_Location"].
	    	",".$srow["Suggest_FIFO_Tag"].",".$srow["Pickface_Location"].",".$srow["Pickface_Onhand_Qty"].",".$srow["Last_Replen"];
	}
}
$pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetTitle('REPLENISHMENT');//title
$pdf->SetMargins(10, 5, 10,5);
$pdf->SetAutoPageBreak(TRUE, 0);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}
$pdf->setFontSubsetting(true);
$pdf->SetFont('freeserif', '');
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
//
$thead = '&nbsp;<br><table border="1" cellspacing="0" cellpadding="2">
	<tr style="font-size:10px;background-color:#C8C8C8;" >
		<td align="center" width="30"><b>No. </b></td>
		<td align="center" width="95"><b>Part No</b></td>
		<td align="center" width="160"><b>Part Name</b></td>
		<td align="center" width="50"><b>Storage Qty</b></td>
		<td align="center" width="80"><b>Suggest Storage Location</b></td>
		<td align="center" width="70"><b>Suggest FIFO</b></td>
		<td align="center" width="55"><b>Pickface Location</b></td>
		<td align="center" width="60"><b>Pickface Onhand Qty</b></td>
		<td align="center" width="65"><b>Last Replen</b></td>
	</tr>';
// Set some content to print
$n = 1;
/*$barcodeREP= TCPDF_STATIC::serializeTCPDFtagParameters(array('RPN1703220011', 'C128', '', '', 0, 16, 0.4, array('position'=>'R', 'border'=>false, 'padding'=>1, 'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>true, 'font'=>'helvetica', 'fontsize'=>5, 'stretchtext'=>4,'cellfitalign'=>'R','stretch'=>true), 'N'));*/
$rre = 1;
$date = date("Y-m-d H:i:s");
// $ndetail = 60;
$ndetail = count($arrepdetail);
$field = 0;
$ccre = 0;
$n = 1;
$nn = 1;
$d = 39;
$p = $d*$nn;
$allPage = ceil($crow/$d);
// $html = createHead('page 1/',$barcodeREP);
$tableData = $thead;
while ( $ccre <= $ndetail-1) {
	if ($field == 0) 
	{
		$field = 1;
		$pdf->AddPage();
		$html = createHead('page 1/'.$allPage,$date);
	}
	if($ccre > $p)
    	      {
    	      	// $ccpus++;
    	      	$n++;
    	      	$nn++;
    	      	$p = $d*$nn;
    	      	$tableData .='</table>';
    	      	$html .= $tableData;
    	      	$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
    	      	$pdf->AddPage();
    	      	$html = createHead('page '.$n.'/'.$allPage,$date);
    	      	$tableData = $thead;
    	      }
	# code...
    	      $data= explode(",",$arrepdetail[$ccre]);
			  $tableData .= '<tr style="font-size:10px" >';
			  $tableData .= '<td align="center" width="30">'.$rre.'</td>';
			  $tableData .= '<td align="left" width="95"> '.$data[0].'</td>';
			  $tableData .= '<td align="left" width="160"> '.$data[1].'</td>';
			  $tableData .= '<td align="center" width="50">'.$data[2].'</td>';
			  $tableData .= '<td align="center" width="80">'.$data[3].'</td>';
			  $tableData .= '<td align="left" width="70">'.$data[4].'</td>';
			  $tableData .= '<td align="center" width="55">'.$data[5].'</td>';
			  $tableData .= '<td align="center" width="60">'.$data[6].'</td>';
			  $tableData .= '<td align="center" width="65">'.$data[7].'</td>';
			  $tableData .= '</tr>';
			  // $n++;
			  $ccre++;$rre++;
}


$tableData .='</table>';
$html .= $tableData;
$html .= createbox();

// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);



// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('REPLENISHMENT.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+

function createHead($page,$date)
{
	$headData = str_format('<table border="0">
	<tr>
		<td align="right" style="font-size:11px">{1} </td>
	</tr>
	<tr>
		<td align="right" style="font-size:11px">Print Date Time : '.$date.'</td>
	</tr>
</table>',$page);

	$headData .= '<table border="0">
	<tr>
		<td width="130"><img src="images/ttv-logo.gif" width="190"  height="45"/></td>
		<td width="10"></td>
		<td align="left" width="320" style="font-size:10px"><b>TITAN-VNS AUTO LOGISTICS CO.,LTD.</b><br/>
		49/66 MOO 5 TUNGSUNKLA SRIRACHA CHONBURI 20230<br/>
		Phone +66(0) 3840 1505-6,3804 1787-8 Fax : +66(0) 3849 4300
		</td>
		<td align="right" width="218"></td>
	</tr>
</table>
<hr>
<table border="0">
	<tr>
		<td align="center"><b style="font-size:22px; margin-left:300px;">REPLENISHMENT SHEET</b></td>
	</tr>
</table>
<hr />
<br>';
return $headData;
}

function createbox()
{
	$boxdata = '<br><br>
	<table style="font-size: 16px;" border="0">
		<tbody>
		<tr>
			<th width="75">Print By  :</th>
			<th width="235">_________________ Data Entry</th>
			<th width="140">Picking By  : </th>
			<th width="215"> _________________ Forklift</th>
		</tr>
		<tr>
			<th width="310"></th>
			<th width="140"></th>
			<th width="215"></th>
		</tr>
		<tr>
			<th width="310"></th>
			<th width="140">Replenishment By : </th>
			<th width="215"> _________________ Operation</th>
		</tr>
		
		</tbody>
</table>';
	return $boxdata;
}

