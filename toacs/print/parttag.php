<?php


// Include the main TCPDF library (search for installation path).
include('../php/connection.php');
require_once('tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
// set document information
$pdf->SetCreator(PDF_CREATOR);
// $pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('PARTTAG');//title

$pdf->SetMargins(10, 5, 10,5);

// set auto page breaks
/*$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);*/
$pdf->SetAutoPageBreak(TRUE, 0);
// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('freeserif', '');

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
//

// Set some content to print
$n = 1;
$barcodeLOT= TCPDF_STATIC::serializeTCPDFtagParameters(array('LOT170300010', 'C128', '', '', 0, 12, 0.4, array('position'=>'R', 'border'=>false, 'padding'=>1, 'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>true, 'font'=>'helvetica', 'fontsize'=>5, 'stretchtext'=>4,'cellfitalign'=>'R','stretch'=>true), 'N'));

$barcodePART= TCPDF_STATIC::serializeTCPDFtagParameters(array('AB392104545EF', 'C128', '', '', 80, 14, 0.4, array('position'=>'R', 'border'=>false, 'padding'=>1, 'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>true, 'font'=>'helvetica', 'fontsize'=>8, 'stretchtext'=>4,'cellfitalign'=>'R','stretch'=>true), 'N'));
$n = 0;
$barcodePARTSUP= TCPDF_STATIC::serializeTCPDFtagParameters(array('AB392104545EF', 'C128', '', '', 0, 12, 0.4, array('position'=>'R', 'border'=>false, 'padding'=>1, 'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>true, 'font'=>'helvetica', 'fontsize'=>8, 'stretchtext'=>4,'cellfitalign'=>'R','stretch'=>true), 'N'));
$n = 0;
while ( $n < 1) {
	$html = createmastertag($barcodeLOT,$barcodePART,$barcodePARTSUP);
	$n++;
	$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
}
// Print text using writeHTMLCell()



// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('example_001.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+

function createmastertag()
{
	$mastertag = '<table style="height: 240px;" border="1" width="600">
	<tbody>
		<tr >
			<td >
				<p style="font-size:2px;">&nbsp;</p> 
				<p align="center" style="font-size:14px;">MASTER TAG</p>
				<p style="font-size:2px;">&nbsp;</p> 
			</td>
			<td colspan = "2">
				<p style="font-size:10px;" >Tag Number</p>
				
			</td>
			<td>
				<p style="font-size:10px;" >Pallet No</p>
				<p align="center">16/20</p> 
			</td>
		</tr>
		<tr >
			<td colspan = "2" width="230">
				<p style="font-size:10px;">Part Number</p>
				<p></p>
			</td>
			<td width="220">
				<p style="font-size:10px;">Part Name</p>
				<p></p>
			</td>
			<td>
				<p>
				<span style="font-size:10px;">Qty.<br></span>
				<span style="font-size:30px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10<br></span>
				<span style="font-size:10px">SNP = 10x1</span>
				</p>
			</td>
		</tr>
		<tr >
			<td colspan = "2" width="230">
				<p style="font-size:10px;">Supplier Part Number</p>
				<p></p>
			</td>
			<td width="220">
				<p style="font-size:10px;">Supplier Code</p>
				<p></p>
			</td>
			<td>
				<p style="font-size:10px;">Supplier Dock</p>
				<p></p>
			</td>
		</tr>
		<tr >
			<td>
				<p style="font-size:10px;">Forecast Date</p>
				<p></p>
			</td>
			<td>
				<p style="font-size:10px;">Pickup Date</p>
				<p></p>
			</td>
			<td>
				<p style="font-size:10px;">PUS Number</p>
				<p></p>
			</td>
			<td>
				<p style="font-size:10px;">Ship To</p>
				<p></p>
			</td>
		</tr>
	</tbody>
</table><br><hr><br>';

	return $mastertag;
}



