<?php


// Include the main TCPDF library (search for installation path).
include('../php/connection.php');
require_once('tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
// set document information
$pdf->SetCreator(PDF_CREATOR);
// $pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('TRUCK');//title

$pdf->SetMargins(10, 5, 10,5);

// set auto page breaks
/*$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);*/
$pdf->SetAutoPageBreak(TRUE, 0);
// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('freeserif', '');

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();
$html = truckheadder();
// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
//

// Set some content to print
$n = 1;
$barcodeTRUCK= TCPDF_STATIC::serializeTCPDFtagParameters(array('2017030004567', 'C128', '', '', 60, 13, 0.4, array('position'=>'C', 'border'=>false, 'padding'=>1, 'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>true, 'font'=>'helvetica', 'fontsize'=>8, 'stretchtext'=>4,'cellfitalign'=>'C','stretch'=>false), 'N'));
$n = 0;
while ( $n < 1) {
	$html .= truckdetail($barcodeTRUCK);
	$html .= truckfoot();
	$n++;
	$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
}
// Print text using writeHTMLCell()



// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('example_001.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
function truckheadder()
{
	$headder ='<table border="0">
	<tr>
		<td  style="font-size:11px" width="500">
			<p style="font-size:30px;">แบบฟอร์มควบคุมการเดินรถขนส่งสินค้า<br>(Truck Control Form)</p>
		</td>
		<td align="right" style="font-size:11px" width="180"></td>
		<td align="right" width="150"><img src="images/aat-logo.gif" width="140"  height="46"/></td>
		<td align="right" width="150"><img src="images/ttv-logo.gif" width="140"  height="46"/></td>
	</tr>
</table>';
	return $headder;
}
function truckdetail($barcodeTRUCK)
{
	$truckdetail = '<table style="height: 240px;position: absolute; top: 0; bottom: 0; left: 0; right: 0;" border="1" width="980">
	<tbody>
		<tr >
			<td style="font-size:14px;"align="center"  width="150">
				วันที่รับงาน
			</td>
			
			<td style="font-size:14px;"align="center" width="150">
				<p>ทะเบียนรถ</p>
			</td>

			<td style="font-size:14px;"align="center" width="150">
				<p>พนักงานขับรถ</p>
			</td>

			<td style="font-size:14px;"align="center" width="150">
				<p>เบอร์ติดต่อ</p>
			</td>

			<td  width="380">
			<p >เลขที่เอกสาร <br><tcpdf method="write1DBarcode" params="'.$barcodeTRUCK.'"/></p>
			</td>
		</tr>
		<tr>
			<td style="font-size:14px;" width="600">
				<p>*ข้อกำหนด : พนักงานขับรถต้องทำการบันทึกข้อมูลในแบบฟอร์มให้ถูกต้อง และ ครบถ้วน*</p>
			</td>
			<td align="center" width="180" rowspan="2">
				<img  style="display:block;" src="images/truck.gif" />
			</td>
			<td align="center" width="200">
				<p>ส่วนของลูกค้า/เจ้าหน้าที่หน้างาน</p>
			</td>
		</tr>
		<tr>
			<td width="240">
				<p>หัวหน้างาน กำหนดการเดินรถ</p>
			</td>
			<td width="310">
				<p>1. พนักงานขับรถ  บันทึกเวลาเดินรถและเลขไมล์จริง</p>
			</td>
			<td width="50"></td>
			<td width="105" align="center">
				<p>สภาพงาน</p>
			</td>
			<td width="95" align="center">
				<p>ลายเซ็น</p>
			</td>
		</tr>
		<tr style="font-size:12px;">
			<td width="50">
				ลำดับ
			</td>
			<td width="100">
				จุดรับ-ส่ง(สินค้า)
			</td>
			<td width="60">
				กำหนดการ
			</td>
			<td width="60">
				เวลาเข้า
			</td>
			<td width="60">
				เวลาเริ่มขึ้นหรือลงของ
			</td>
			<td width="60">
				เวลาขึ้นหรือลงเสร็จ
			</td>
			<td width="50">
				เวลาออก
			</td>
			<td width="50">
				เลขไมล์
			</td>
			<td width="110">
				สาเหตุที่ล่าช้า
			</td>
			<td width="45">
				Seal
			</td>
			<td width="45">
				เลขซีล 1
			</td>
			<td width="45">
				เลขซีล 2
			</td>
			<td width="45">
				เลขซีล 3
			</td>
			<td width="50">
				สมบูรณ์
			</td>
			<td width="55">
				ไม่สมบูรณ์
			</td>
			<td width="95">
				เจ้าหน้าที่/ลูกค้า
			</td>
		</tr>
		<tr style="font-size:12px;">
			<td width="50" rowspan="2">
				1
			</td>
			<td width="100" rowspan="2">
				TTV WHA3
			</td>
			<td width="180" rowspan="2">
				ออกก่อนเวลา
			</td>
			<td width="60" rowspan="2">
				23.39
			</td>
			<td width="50" rowspan="2">
				
			</td>
			<td width="50" rowspan="2">
				
			</td>
			<td width="110" rowspan="2">
				
			</td>
			<td width="45" >
				เข้า
			</td>
			<td width="45" >
			</td>
			<td width="45" >
			</td>
			<td width="45" >
			</td>
			<td width="50" align="center" >
				( )
			</td>
			<td width="55" align="center" >
				( )
			</td>
			<td width="95" >
			</td>
		</tr>
		<tr style="font-size:12px;">
			<td width="45" >
				ออก
			</td>
			<td width="45" >
			</td>
			<td width="45" >
			</td>
			<td width="45" >
			</td>
			<td width="50" align="center" >
				( )
			</td>
			<td width="55" align="center" >
				( )
			</td>
			<td width="95" >
			</td>
		</tr>

		<tr style="font-size:12px;">
			<td width="50" rowspan="2">
				2
			</td>
			<td width="100" rowspan="2">
				HIRUTA & SUMMIT
			</td>
			<td width="60" rowspan="2">
				00:10
			</td>
			<td width="60" rowspan="2">
				
			</td>
			<td width="60" rowspan="2">
				
			</td>
			<td width="60" rowspan="2">
				
			</td>
			<td width="50" rowspan="2">
				
			</td>
			<td width="50" rowspan="2">
			</td>
			<td width="110" rowspan="2">
			</td>
			<td width="45" >
				เข้า
			</td>
			<td width="45" >
			</td>
			<td width="45" >
			</td>
			<td width="45" >
			</td>
			<td width="50" align="center" >
				( )
			</td>
			<td width="55" align="center" >
				( )
			</td>
			<td width="95" >
			</td>
		</tr>
		<tr style="font-size:12px;">
			<td width="45" >
				ออก
			</td>
			<td width="45" >
			</td>
			<td width="45" >
			</td>
			<td width="45" >
			</td>
			<td width="50" align="center" >
				( )
			</td>
			<td width="55" align="center" >
				( )
			</td>
			<td width="95" >
			</td>
		</tr>

		<tr style="font-size:12px;">
			<td width="50" rowspan="2">
				2
			</td>
			<td width="100" rowspan="2">
				TTV WHA3
			</td>
			<td width="60" rowspan="2">
				01:25
			</td>
			<td width="60" rowspan="2">
				
			</td>
			<td width="60" rowspan="2">
				
			</td>
			<td width="60" rowspan="2">
				
			</td>
			<td width="50" rowspan="2">
				
			</td>
			<td width="50" rowspan="2">
			</td>
			<td width="110" rowspan="2">
			</td>
			<td width="45" >
				เข้า
			</td>
			<td width="45" >
			</td>
			<td width="45" >
			</td>
			<td width="45" >
			</td>
			<td width="50" align="center" >
				( )
			</td>
			<td width="55" align="center" >
				( )
			</td>
			<td width="95" >
			</td>
		</tr>
		<tr style="font-size:12px;">
			<td width="45" >
				ออก
			</td>
			<td width="45" >
			</td>
			<td width="45" >
			</td>
			<td width="45" >
			</td>
			<td width="50" align="center" >
				( )
			</td>
			<td width="55" align="center" >
				( )
			</td>
			<td width="95" >
			</td>
		</tr>
	</tbody>
</table><br>
';

	return $truckdetail;
}

function truckfoot()
{
	$truckfoot = '<br>
	<table  style="display: inline-block;" border="1" width="980" style="float: left">
		<tbody>
			<tr style="font-size:12px;">
				<td width="230">
					เบอร์ติดต่อหน้างาน
				</td>
				<td width="50" rowspan="3">
				</td>
				<td width="430">
					เหตุผลที่เกิดการล่าช้าในการ รับ-ส่ง สินค้า (ให้ใส่หมายเลขในช่อง สาเหตุที่ล่าช้า)
				</td>
				<td width="50">
				</td>
				<td width="110">
					ผู้ปล่อยรถ
				</td>
				<td width="110">
					ผู้ตรวจสอบขากลับ
				</td>
			</tr>
			<tr style="font-size:12px;">
				<td width="115">
					Cs Controller
				</td>
				<td width="115">
					นิสารัตน์  091-2394577
				</td>

				<td width="30">
					1.
				</td>
				<td width="200">
					ออกจากลานจอดรถช้า หรือได้รับรถช้า
				</td>
				<td width="30">
					7.
				</td>
				<td width="170">
					ไม่มีข่องจอดรถ
				</td>
				<td width="50">
				</td>
				<td width="110" rowspan="2">
					
				</td>
				<td width="110"  rowspan="2">
					
				</td>
			</tr>

			<tr style="font-size:12px;">
				<td width="115">
					Transport Controller
				</td>
				<td width="115">
					ภัสนี 092-2514170
				</td>
				<td width="30">
					2.
				</td>
				<td width="200">
					ข้ามาจากจุดก่อนหน้า (จุดรับ หรือ ส่งสินค้า)
				</td>
				<td width="30">
					8.
				</td>
				<td width="170">
					รถเจ้าหน้าที่ตรวจรับสินค้า
				</td>
				<td width="50">
				</td>
			</tr>

			<tr style="font-size:12px;">
				<td width="280" rowspan="4">
					
				</td>
				<td width="30">
					3.
				</td>
				<td width="200">
					ฝนตก, รถติด
				</td>
				<td width="30">
					9.
				</td>
				<td width="170">
					ภาชนะเปล่าไม่ได้ถูกจัดเตรียม
				</td>
				<td width="50">
				</td>
				<td width="110">
					หัวหน้างาน
				</td>
				<td width="110">
					หัวหน้างาน
				</td>
			</tr>

			<tr style="font-size:12px;">

				<td width="30">
					4.
				</td>
				<td width="200">
					รอคิวรถ เพื่อเรียกเข้ารับ-ส่งสินค้า
				</td>
				<td width="30">
					10.
				</td>
				<td width="170">
					รอเอกสาร
				</td>
				<td width="50">
				</td>
				<td width="110">
					วันที่ปล่อยรถ
				</td>
				<td width="110">
					วันที่ตรวจสอบขากลับ
				</td>
			</tr>

			<tr style="font-size:12px;">

				<td width="30">
					5.
				</td>
				<td width="200">
					สินค้าไม่พร้อมจัดส่ง หรือ รอขึ้นสินค้า
				</td>
				<td width="30">
					11.
				</td>
				<td width="170">
					รถเสียระหว่างทาง
				</td>
				<td width="50">
				</td>
				<td width="110" rowspan="2">
					
				</td>
				<td width="110" rowspan="2">
					
				</td>
			</tr>

			<tr style="font-size:12px;">

				<td width="30">
					6.
				</td>
				<td width="200">
					ภาชนะบรรจุไม่เพียงพอ
				</td>
				<td width="30">
					12.
				</td>
				<td width="170">
					รถเกิดอุบัติเหตุ
				</td>
				<td width="50">
				</td>
			</tr>
		</tbody>
	</table>
	';
	return $truckfoot;
}



