<?php
//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
include('../php/connection.php');
require_once('tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
// set document information
$pdf->SetCreator(PDF_CREATOR);
// $pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('PUS');//title
// $pdf->SetSubject('TCPDF Tutorial');
// $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
// $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
/*$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
$pdf->setFooterData(array(0,64,0), array(0,64,128));*/

// set header and footer fonts
/*$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));*/

// set default monospaced font
// $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
/*$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);*/
$pdf->SetMargins(10, 5, 10,5);

// set auto page breaks
/*$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);*/
$pdf->SetAutoPageBreak(TRUE, 0);
// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('freeserif', '');

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
//
$thead = '<table border="1" cellspacing="0" cellpadding="2">
	<tr style="font-size:10px;background-color:#C8C8C8;" >
		<td align="center" width="20"><b>No. </b></td>
		<td align="center" width="85"><b>Pickup Time</b></td>
		<td align="center" width="50"><b>PGM No.</b></td>
		<td align="center" width="60"><b>GSDB_Code</b></td>
		<td align="center" width="150"><b>GSDB_Name</b></td>
		<td align="center" width="90"><b>Part No.</b></td>
		<td align="center" width="150"><b>Part Name</b></td>
		<td align="center" width="60"><b>Qty</b></td>
		<td align="center" width="40"><b>SNP</b></td>
		<td align="center" width="60"><b>CBM</b></td>
		<td align="center" width="50"><b>Package Type</b></td>
		<td align="center" width="40"><b>Rack</b></td>
		<td align="center" width="40"><b>Pallet</b></td>
		<td align="center" width="40"><b>ได้รับสินค้า</b></td>
		<td align="center" width="40"><b>ไม่ได้รับชิ้นส่วน</b></td>
	</tr>';
// Set some content to print
$n = 1;
$barcodePUS= TCPDF_STATIC::serializeTCPDFtagParameters(array('PUS0009501', 'C128', '', '', 0, 12, 0.4, array('position'=>'R', 'border'=>false, 'padding'=>1, 'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>true, 'font'=>'helvetica', 'fontsize'=>5, 'stretchtext'=>4,'cellfitalign'=>'R','stretch'=>true), 'N'));
$barcodeTRUCK= TCPDF_STATIC::serializeTCPDFtagParameters(array('T170300010', 'C128', '', '', 0, 12, 0.4, array('position'=>'R', 'border'=>false, 'padding'=>1, 'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>true, 'font'=>'helvetica', 'fontsize'=>5, 'stretchtext'=>4,'cellfitalign'=>'R','stretch'=>true), 'N'));
$html = createHead('page 1/',$barcodePUS,$barcodeTRUCK);
$tableData = $thead;
while ( $n <= 5) {
	# code...
$tableData .= '<tr style="font-size:10px" >';
			  $tableData .= '<td align="center" width="20">'.$n.'</td>';
			  $tableData .= '<td align="center" width="85">2017/03/03_11:00</td>';
			  $tableData .= '<td align="center" width="50">841-25</td>';
			  $tableData .= '<td align="center" width="60">FXBYA</td>';
			  $tableData .= '<td align="center" width="150">HIRUTA & SUMMIT CO LTD</td>';
			  $tableData .= '<td align="center" width="90">EB3B4104545AF</td>';
			  $tableData .= '<td align="center" width="150">REINF ASY I/PNL(RHD I5)</td>';
			  $tableData .= '<td align="center" width="60">100</td>';
			  $tableData .= '<td align="center" width="40">10</td>';
			  $tableData .= '<td align="center" width="60">4.700</td>';
			  $tableData .= '<td align="center" width="50"></td>';
			  $tableData .= '<td align="center" width="40">2</td>';
			  $tableData .= '<td align="center" width="40">-</td>';
			  $tableData .= '<td align="center" width="40"></td>';
			  $tableData .= '<td align="center" width="40"></td>';
			  $tableData .= '</tr>';
			  $n++;
}
$tableData .= '<tr style="font-size:12px" >';
$tableData .= '<td align="center" width="455"></td>';
$tableData .= '<td align="right" width="150">Total:</td>';
$tableData .= '<td align="center" width="60">SUMQTY</td>';
$tableData .= '<td align="center" width="40"></td>';
$tableData .= '<td align="center" width="60">SUMCBM</td>';
$tableData .= '<td align="center" width="50"></td>';
$tableData .= '<td align="center" width="40">SRack</td>';
$tableData .= '<td align="center" width="40">-</td>';
$tableData .= '</tr>';

$tableData .='</table>';
$html .= $tableData;
$html .= createbox();

// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

/*$c = 1;
while ( $c <= 5) {
	# code...
	$pdf->AddPage();
	$html = createmastertag();
	$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
	// $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
	$c++;
}*/

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('example_001.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+

function createHead($page,$barcodePUS,$barcodeTRUCK)
{
	$headData = str_format('<table border="0">
	<tr>
		<td align="right" style="font-size:11px">{1} </td>
	</tr>
</table>',$page.'1');

	$headData .= '<table border="0">
	<tr>
		<td align="left" width="300" style="font-size:11px"><b>TITAN-VNS AUTO LOGISTICS CO.,LTD.</b><br/>
		49/66 MOO 5 TUNGSUNKLA SRIRACHA CHONBURI 20230<br/>
		</td>
		<td align="center" width="360"><b style="font-size:24px; margin-left:300px;">PICKUP SHEET</b></td>
		<td width="160"><img src="images/aat-logo.gif" width="160"  height="46"/></td>
		<td width="160"><img src="images/ttv-logo.gif" width="160"  height="46"/></td>
	</tr>
</table>
<hr>
<br>
<table border="0" style="margin-top:10px;" cellspacing="" cellpadding="2" style="font-size:11px">
	<tr>
		<td align="left" width="80"><b>Ship From :<br></b></td>
		<td align="left" width="160">xxxx</td>
		<td align="left" width="120"><b>Time to Supplier:</b></td>
		<td align="left" width="160">xx:xx</td>
		<td align="left" width="80"><b>Truck No:</b></td>
		<td align="left" width="160">xx-xxxx</td>
		<td align="left" width="80"><b>PUS Number :</b></td>
		<td align="left" width="130">PUS0009501</td>
	</tr>
	<tr>
		<td align="left" width="80"><b>Ship To :<br><br>Ship Date :</b></td>
		<td align="left" width="160">xxxxxxxx<br><br>xx-xxx-xxxx</td>
		<td align="left" width="120"><b>Departure time (TTV):<br><br>Pickup time(HAS):</b></td>
		<td align="left" width="160">xx:xx<br><br>xx:xx</td>
		<td align="left" width="80"><b>Truck Type:<br><br>Driver Name :</b></td>
		<td align="left" width="160">6W<br><br>xxxxxxxxxxxxxxxxx</td>
		<td align="center" width="210"><tcpdf method="write1DBarcode" params="'.$barcodePUS.'"/></td>
	</tr>
	<tr>
		<td align="left" width="80"><b>Working Ship :<br></b></td>
		<td align="left" width="160">XXX</td>
		<td align="left" width="120"><b>Arrival (TTV) :</b></td>
		<td align="left" width="160">xx:xx</td>
		<td align="left" width="80"><b>Phone No :</b></td>
		<td align="left" width="160">xxx-xxx-xxxx</td>
		<td align="left" width="130"><b>TRUCK CONTROL NO. :</b></td>
		<td align="left" width="80">T170300010</td>
	</tr>
	<tr>
		<td align="left" width="80"><b>Remark :</b></td>
		<td align="left" width="160">XXX</td>
		<td align="left" width="120"></td>
		<td align="left" width="160"></td>
		<td align="left" width="80"></td>
		<td align="left" width="160"></td>
		<td align="center" width="210"><tcpdf method="write1DBarcode" params="'.$barcodeTRUCK.'"/></td>
	</tr>

</table>';
return $headData;
}

function createbox()
{
	$boxdata = '<br><br>
	<table style="font-size: 13px;" border="1">
		<tbody>
		<tr style="background-color:#C8C8C8;" >
			<th width="270">Remarks / หมายเหตุ</th>
			<th align="center" width="140">ชื่อผู้จ่ายสินค้า (Supplier)</th>
			<th align="center" width="140">ชื่อผู้รับสินค้า (Driver)</th>
			<th colspan="5" align="center" width="430">&nbsp; Packaging Type Record</th>
		</tr>
		<tr >
			<th rowspan="4" align="center">&nbsp;&nbsp;&nbsp;&nbsp;</th><th rowspan="4" align="center">
			<p>&nbsp;</p>
			<p>_______________</p>
			<p><br />(_____/_____/_____)</p>
			<p>&nbsp;</p>
			</th><th rowspan="4" align="center">
			<p>&nbsp;</p>
			<p>&nbsp;_______________</p>
			<p><br />(_____/_____/_____)</p>
			<p>&nbsp;</p>
			</th>
			<th style="background-color:#C8C8C8;" rowspan="2" align="center">ยอดรับ</th>
			<th style="background-color:#C8C8C8;" align="center">กล่องพลาสติก</th>
			<th style="background-color:#C8C8C8;" align="center">กล่องลูกฟูก</th>
			<th style="background-color:#C8C8C8;" align="center">พาเลทพลาสติก</th>
			<th style="background-color:#C8C8C8;" align="center">แร็ค</th>
		</tr>
		<tr style="background-color:#C8C8C8;">
			<th align="center">PTB</th>
			<th align="center">CBM</th>
			<th align="center">PTP</th>
			<th align="center">STD</th>
		</tr>
		<tr>
			<th style="background-color:#C8C8C8;" align="center">
				<p>แผนรับ</p>
				<p>&nbsp;</p>
			</th>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<th style="background-color:#C8C8C8;" align="center">
				<p>รับจริง</p>
				<p>&nbsp;</p>
			</th>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		</tbody>
</table>';
	return $boxdata;
}

function createmastertag()
{
	$mastertag = '<table style="height: 279px;" border="1" width="976">
<tbody>
<tr>
<td rowspan="2">
<p>LOT NO.</p>
<p>LOT170300010</p>
<p>*LOT170300010*</p>
</td>
<td style="text-align: center;" colspan="4">
<p><strong>MASTER TAG</strong></p>
</td>
</tr>
<tr>
<td colspan="4">
<p>PART NUMBER</p>
<p>AB392104545EF *AB392104545EF*</p>
</td>
</tr>
<tr>
<td colspan="2">
<p>PART NUMBER (HAS)&nbsp;</p>
<p>AB392104545EF</p>
<p>*AB392104545EF*</p>
</td>
<td colspan="2">
<p>PART NAME</p>
<p><br />REINF ASY I/PNL</p>
</td>
<td>
<p>QUANITY</p>
<p>10</p>
<p>*10*</p>
</td>
</tr>
<tr>
<td>
<p>PART NUMBER &nbsp;(AAT)</p>
<p>AB39-2104545-EF</p>
</td>
<td>
<p>SUPPLIER CODE</p>
<p>FXBYA</p>
</td>
<td colspan="2">
<p>SUPPLIER NAME</p>
<p><br />HIRUTA &amp; SUMMIT CO LTD</p>
</td>
<td>
<p>PACKAGING TYPE</p>
<p>DOLLY SPECIAL</p>
</td>
</tr>
<tr>
<td>
<p>PICKUP DATE TIME</p>
<p>03-03-2017 &nbsp;11:00:00 AM</p>
</td>
<td>
<p>SHIP TO :&nbsp;</p>
<p>TTV WAREHOUSE&nbsp;AAT WIRING</p>
</td>
<td>
<p>PICKUP SHEET NUMBER</p>
<p>PUS170300010</p>
</td>
<td>
<p>PUT LOC.</p>
<p>TTV WH</p>
</td>
<td>
<p>PICK LOC.</p>
<p>HAS</p>
</td>
</tr>
</tbody>
</table><br><hr><br>';

	return $mastertag;
}