<?php
include('../php/connection.php');
require_once('tcpdf/tcpdf.php');
$pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetTitle('PETTITION');//title
$pdf->SetMargins(10, 5, 10,5);
$pdf->SetAutoPageBreak(TRUE, 0);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}
$pdf->setFontSubsetting(true);
$pdf->SetFont('freeserif', '');
$pdf->AddPage();
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

	$html = pettitionn();
	$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
	$pdf->AddPage();
	$html = pettitionn2();
	$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
$pdf->Output('example_001.pdf', 'I');

function pettitionn()
{
	$pnum = ' AT1704050001';
	$ven = 'บริษัท ออโต้ อัลลายแอนซ์ (ประเทศไทย) จํากัด';
	$date = '17 เมษายน 2560';
	$fdate = '20 เมษายน 2560';
	$kk = 'กศก. 185';
	$kkid = '28/2549 รหัสผู้ประกอบการฯ 28460001';
	$ttv = 'บริษัท ไททัน – วีเอ็นเอส ออโต้ โลจิสติกส์ จํากัด';
	$ttvadd = '64/233 หมู่ 4 ตําบลปลวกแดง อําเภอปลวกแดง จังหวัดระยอง';
	$name = 'นายอนิรุทธิ์ สิทธิมงคล';
	$pettitionn = '
	<p></p>
	<p></p>
	<p></p>
	<p align="right" style="font-size:10px;">๓๐๘ </p>
	<p align="right" style="font-size:12px;"> แบบแนบท้ายประมวลฯ ข้อ ๕ ๐๓ ๐๓ ๐๔ (ก) </p>
	<p align="center" style="font-size:14px;"> คำขอนำของออกจากเขตปลอดอากร/เขตประกอบการเสรีเป็นการชัวคราว </p>
	<p align="left" style="font-size:14px;"> เลขที่ '.$pnum.' 
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	เขียนที่ '.$ven.' </p>
	<p style="text-align: right;font-size:14px;" >วันที '.$date.'&nbsp;</p>
	<p style="font-size:14px;">เรื่อง&nbsp;ขอนําของออกจากเขตปลอดอากร/เขตประกอบการเสรีเป็นการชั่วคราว</p>
	<p style="font-size:14px;">เรียน หัวหน้าสํานักงานศลุกากรเขตปลอดอากร/เขตประกอบการเสรี '.$ven.'&nbsp;</p>
	<p style="font-size:14px;">สิ่งทีส่งมาด้วย รายละเอียดของของที่ขอนําออกจากเขตปลอดอากร/เขตประกอบการเสรีเป็นการชั่วคราว</p>
	<table border="0">
		<tbody>
			<tr style="font-size:14px;">
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				ด้วยข้าพเจ้า '.$ven.' ถือใบรับรองเป็น ผู้ประกอบกิจการ ในเขตปลอดอากร ตาม
				'.$kk.' ทีี '.$kkid.' มีความประสงค์จะขออนุญาต นำของตามรายการข้างท้ายนี้ออกจาก เขตปลอดอากรเป็นการชั่วคราว วันที่ '.$date.' และจะไปดําเนินการ ณ สถานประกอบการ '.$ttv.' ตั้งอยู่เลขที่ '.$ttvadd.' ถือใบอนญุาตประกอบ กิจการโรงงาน เลขที่ ชบ.064502, ทะเบียนนิติบคุคล เลขที่ 0205552019111	    
				</td>
			</tr>
		</tbody>
	</table>
	<br><br>
	<table border="1">
		<tbody>
			<tr style="font-size:14px;">
				<td align="center">
					ลำดับที่
				</td>
				<td align="center">
					จำนวนหีบห่อ
				</td>
				<td align="center">
					ปริมาณ (ชิ้น)
				</td>
				<td align="center">	
					น้ำหนัก (กก.)
				</td>
				<td align="center">
					ราคา (บาท)
				</td>
				<td align="center">
					รายการของ
				</td>
			</tr>

			<tr style="font-size:30px;">
				<td>
				</td>
				<td>
				</td>
				<td>
				</td>
				<td>
				</td>
				<td>
				</td>
				<td>
				</td>
			</tr>
		</tbody>
	</table><br><br>
	<table border="0">
		<tbody>
			<tr style="font-size:14px;">
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					ข้าพเจ้าขอรับรองว่าข้อมลูและเอกสารแนบคําขอนี้ ตรวจกับความจริง หรือเป็นประมาณการทีใกล้เคียงความจริงทีสุด จะนำของกลับ เข้าในเขตปลอดอากร/เขตประกอบการเสรีภายในวันที
					............................. 
					'.$fdate.' และยินยอมรับผิดตามทีกรมศลุกากรพิจารณา สั่งการทุกประการ ในกรณีทีข้าพเจ้าสําแดงรายการไม่ถูกต้อง และ/หรือ ไม่ครบถ้วนตามความเป็นจริง 
					รวมทั้งไม่นําของกลับเข้าในเขต ปลอดอากรภายในเวลาที่รับรองไว้ หรือภายในกําหนดที่กรมศลุกากรได้ขยายเวลาให้ 
				</td>
			</tr>
		</tbody>
	</table><br><br>
	<table border="0">
		<tbody>
			<tr style="font-size:14px;">
				<td width="300">
				</td>
				<td align="center">
					ขอแสดงความนับถือ<br><br>
					 (.......................................)<br><br>
					 เจ้าของ/ผู้จัดการ/ประทับตราบริษัท 
				</td>
				<td width="140">
				</td>
			</tr>
		</tbody>
	</table><br><br>
	<p align="center" style="font-size:14px;"> บันทึกการตรวจของเจ้าหน้าที่ศลุกากร  </p>
	<table border="1">
		<tbody>
			<tr style="font-size:14px;">
				<td align="center">
					นําออกจากเขตปลอดอากร<br>
					ตรวจแล้วนับจำนวนถูกต้อง<br>
					&nbsp;<br>
					&nbsp;<br>
					'.$name.'<br>
					นักวิชาการศุลกากรปฏิบัติการ
				</td>
				<td align="center">
					นํากลับเข้าเขตปลอดอากร<br>
					ตรวจแล้วนับจำนวนถูกต้อง<br>
					&nbsp;<br>
					&nbsp;<br>
					'.$name.'<br>
					นักวิชาการศุลกากรปฏิบัติการ
				</td>
			</tr>
		</tbody>
	</table>
';

	return $pettitionn;
}

function pettitionn2()
{
	$pettitionn2 = '
	<p></p>
	<p></p>
	<p align="center" style="font-size:12px;"> รายละเอียดของของที่ขอนําออกจากเขตปลอดอากร/เขตประกอบการเสรีเป็นการชั่วคราว (ตามแบบแนบท้ายประมวลฯ ข้อ 5 03 0๓04 (ก) และข้อ 5 04 03 05)  </p>
	<p align="center" style="font-size:14px;"> รายละเอียดการขออนุญาตนําของออกไปจากเขตปลอดอากร/เขตประกอบการเสรีเป็นการชั่วคราวเพื่อผ่านขบวนการผลิต   </p>
	<p align="left" style="font-size:14px;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	 1. ลักษณะของของที่นํากลับเข้าเขตปลอดอากร    </p>

	<table border="1">
		<tbody>
			<tr style="font-size:14px;">
				<td align="center">
					ลำดับที่
				</td>
				<td align="center">
					รายละเอียดของของ
				</td>
				<td align="center">
					ปริมาณ (ชิ้น)
				</td>
				<td align="center">	
					น้ำหนัก (กก.)
				</td>
				<td align="center">
					ราคา (บาท)
				</td>
				<td align="center">
					รายการของ
				</td>
			</tr>
			<tr style="font-size:14px;">
				<td width="666" align="center">
					นํากลับในสภาพเดิม รายละเอียดตามเอกสารแนบและเป็นไปตามอนุมัตหิลักการ<br>ของด่านศุลกากรมาบตาพุด ตามจดหมายเลขที่ ________________
				</td>
			</tr>
		</tbody>
	</table><br><br>
	<table border="0">
		<tbody>
			<tr style="font-size:14px;">
				<td>
					(ส่งตัวอย่างหรือภาพถ่ายแบบแปลนเอกสาร ของของที่จะนําออกจากเขตปลอดอากร/เขตประกอบการเสรีและตัวอย่าง ของของทีจะ นํากลับเข้ามาในเขตปลอดอากร/เขตประกอบการเสรีจํานวนอย่างละ 2 ตัวอย่าง 
				</td>
			</tr>
		</tbody>
	</table>
	<p align="left" style="font-size:12px;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	 2. เหตผุลและความจําเป็นทีขอนําของออกไปจากเขตปลอดอากร/เขตประกอบการเสรี     </p>
	<p align="left" style="font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<img  style="display:block;" src="images/sq.gif" width="20"  height="20" /> มีขบวนการผลิตดัง กล่าวภายในเขตปลอดอากร แต่ต้องนําออกไปจากเขตปลอดอากร/เขตประกอบการเสรี<br>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;เป็นการชั่วคราวเนื่องจาก
	...................................................................................................................................................<br><br>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	......................................................................................................................................................................................<br><br>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	......................................................................................................................................................................................<br>
	 </p>
	 <p align="left" style="font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	 <img  style="display:block;" src="images/sqx.gif" width="20"  height="20" /> ไม่มีขบวนการผลิตดังกล่าว 
	 </p>
	 <p align="left" style="font-size:12px;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	 3. แผนงานและกําหนดเวลาทีจะดําเนินการผลิตขั้นตอนดังกล่าวในเขตปลอดอากร/เขตประกอบการเสรี      </p>
	 <p align="left" style="font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<img  style="display:block;" src="images/sqx.gif" width="20"  height="20" /> ไม่มี </p>
	<p align="left" style="font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<img  style="display:block;" src="images/sq.gif" width="20"  height="20" /> มี </p>
	<p style="font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	......................................................................................................................................................................................</p>
	<p style="font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	......................................................................................................................................................................................</p>
	<p style="font-size:12px;";>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	......................................................................................................................................................................................</p>
	<p align="left" style="font-size:12px;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	 4. ข้อมลูทั่วไปของบริษัท/ห้างฯ       </p>
	 <p style="font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	......................................................................................................................................................................................</p>
	<p style="font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	......................................................................................................................................................................................</p>
	<p style="font-size:12px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	......................................................................................................................................................................................</p>
	<p align="left" style="font-size:12px;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	 5. แผนผังแสดงกรรมวิธีการผลิตโดยย่อ (FLOW PROCESS CHART) ทุกขั้นตอนทีผลิตภายในเขต ปลอดอากร/เขต ประกอบการเสรี <br>
	 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	 และขั้น ตอนทีต้องนําออกไปทําการผลิตภายนอกเขตปลอดอากร/เขต ประกอบการเสรีเป็นการ ชั่วคราว       </p>
	';

	return $pettitionn2;
}



