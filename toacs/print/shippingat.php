<?php
include('../start.php');
include('../php/connection.php');
require_once('tcpdf/tcpdf.php');
$data = $_REQUEST['data'];

$query  = "CALL SP_Shipping_PrintDocuments('$data')";
if ($result = $mysqli->query($query)) 
{
	if ($result->num_rows == 0)
	{
		echo 'ไม่พบ'.$data.'ในระบบ';
		$mysqli->close();
		exit();
	}
	while ($row = $result->fetch_assoc()) 
	{
    	$COMP =  $row["Company_Name"];
    	$WHADDS =  $row["Warehouse_Address"];
    	$SHIPNO =  $row["Shipment_Number"];
    	$SHPF =  $row["Ship_From"];
    	$SHPT =  $row["Ship_To"];
    	$SHPD =  $row["Ship_Date"];
    	$DDATE =  $row["Delivery_Date"];
    	$DTIME =  $row["Delivery_Time"];
    	$RTIRP =  $row["Round_Trip"];
    	$TL =  $row["Truck_License"];
    	$TT =  $row["Truck_Type"];
    	$TT =  $row["Truck_Type"];
    	$DNAME =  $row["Driver_Name"];
    	$DPHONE =  $row["Telephone"];
    	if ($mysqli->next_result()) 
    	{
    		$arshipdetail = array();
    		$arpack = array();
    		 if ($result = $mysqli->store_result()) 
    		 {
    		 	while ($row2 = $result->fetch_assoc()) 
    		 	{
    		 		$arpack [] = $row2["Packing_Slip_Number"];
    		 		$arshipdetail[] = $row2["Supplier_Code"].",".$row2["Packing_Slip_Number"].",".$row2["Ducument_Type"].",".$row2["Seal_Number"];
    		 	}
    		 	 if ($mysqli->next_result()) 
    		 	 {
    		 	 	$arshipdetail2 = array();
    		 	 	if ($result = $mysqli->store_result()) 
    		 	 	{
    		 	 		while ($row3 = $result->fetch_assoc())
    		 	 		{
    		 	 			$arshipdetail2[] = $row3["Part_Group"].",".$row3["Description"].",".$row3["Dolly_Number"].",".$row3["Summary_Sheet_Number"]
    		 	 			.",".$row3["Trip_Number"];
    		 	 		}
    		 // 	 		if ($mysqli->next_result()) 
    		 // 	 		{
    		 // 	 			$q2  = "CALL SP_Shipping_PrintPackingSlip('$arpack[1]')";
							// if ($result2 = $mysqli->query($q2)) 
							// {
							// 	if ($result2->num_rows == 0)
							// 		{
							// 			echo 'ไม่พบ  ในระบบ';
							// 			$mysqli->close();
							// 			exit();
							// 		}
							// 	while ($srow = $result2->fetch_assoc()) 
							// 	{
							// 			$Doc =  $srow["Doc_Type"];
							// 	    	$PSNO =  $srow["Packing_Slip_Number"];
							// 	    	$SNAME =  $srow["Supplier_Name"];
							// 	    	$SADD =  $srow["Supplier_Address"];
							// 	    	$STAX =  $srow["Supplier_Tax_ID"];
							// 	    	$CNAME =  $srow["Customer_Name"];
							// 	    	$CADD1 =  $srow["Customer_Address_Line_1"];
							// 	    	$CADD2 =  $srow["Customer_Address_Line_2"];
							// 	    	$CADD3 =  $srow["Customer_Address_Line_3"];
							// 	    	$CTAX =  $srow["Customer_Tax_ID"];
							// 	    	$IsDATE =  $srow["Issued_Date"];
							// 	    	$CMMS =  $srow["CMMS_Plant_Code"];
							// 	    	$DCODE =  $srow["Dock_Code"];
							// 	    	$DTIMES =  $srow["Delivery_DateTime"];
							// 	    	$SGSDB =  $srow["Supplier_GSDB_Code"];
							// 	    	$RTIRPs =  $srow["Round_Trip"];
							// 	    	if ($mysqli->next_result())
							// 	    	{
							// 	    		if ($result2 = $mysqli->store_result())
							// 	    		{
							// 	    			$arpackpartnum = array();
							// 	    			$arpackpartname = array();
							// 	    			$arpackunit = array();
							// 	    			$arpackqty = array();
							// 	    			$arpackremark = array();
							// 	    			while ($srow2 = $result2->fetch_assoc())
							// 	    			{
							// 	    				$arpackpartnum[] = $srow2["Part_Number"];
							// 	    				$arpackpartname[] = $srow2["Part_Name"];
							// 	    				$arpackunit[] = $srow2["Unit"];
							// 	    				$arpackqty[] = $srow2["Qty"];
							// 	    				$arpackremark[] = $srow2["Remarks"];

							// 	    				/*$arpackdetail[] =  $srow2["Part_Number"].",".$srow2["Part_Name"].",".$srow2["Unit"]
							// 	    				.",".$srow2["Qty"].",".$srow2["Remarks"];*/
							// 	    			}
							// 	    		}
							// 	    	}
							// 	}
							// }
							// else
							// {
							// 	echo "2";
							// }
    		 // 	 		}
    		 	 	}
    		 	 }
    		 }
    	}
	}
}
// $npack = count($arpack);
// print_r($arpackpartname);

$pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetTitle('SHIP');
$pdf->SetMargins(10, 5, 10,5);
$pdf->SetAutoPageBreak(TRUE, 0);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}
$pdf->setFontSubsetting(true);
$pdf->SetFont('freeserif', '');
$pdf->AddPage();
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

$ccps = 0 ;
$ndetail = count($arshipdetail);
$ccsm = 0;
$ndetail2 = count($arshipdetail2);
$rps = 1;
$rsm = 1;

$html = createHead('page 1/',$COMP,$WHADDS,$SHIPNO,$SHPF,$SHPT,$SHPD,$DDATE,$DTIME,$RTIRP,$TL,$TT,$DNAME,$DPHONE);
$html .= '<br>';
$tableData = body1();
while ( $ccps <= $ndetail-1)
{
	$data= explode(",",$arshipdetail[$ccps]);
	$tableData .= '<tr style="font-size:10px" >
		<td align="center" width="30">'.$rps.'</td>
		<td align="center" width="100">'.$data[0].'</td>
		<td align="center" width="200">'.$data[1].'</td>
		<td align="center" width="200">'.$data[2].'</td>
		<td align="center" width="130">'.$data[3].'</td>
		</tr>';
	$ccps++;$rps++;
}
$tableData .='</table>';

$tableData .= body2();
while ( $ccsm <= $ndetail2-1)
{
	$data= explode(",",$arshipdetail2[$ccsm]);
	$tableData .= '<tr style="font-size:10px" >
		<td align="center" width="30">'.$rsm.'</td>
		<td align="center" width="100">'.$data[0].'</td>
		<td align="center" width="200">'.$data[1].'</td>
		<td align="center" width="120">'.$data[2].'</td>
		<td align="center" width="130">'.$data[3].'</td>
		<td align="center" width="80">'.$data[4].'</td>
		</tr>';
	$ccsm++;$rsm++;
}
$tableData .='</table>';
$html .= $tableData;

$html .= '<br><br>';
$html .= createbox1($DTIME);
$html .= '<br><br>';
$html .= createbox2();
$html .= '<br><hr style="border:2px dotted "><br>&nbsp;<br>';
$html .= footer($SHIPNO,$DNAME,$TL,$RTIRP,$SHPD);

$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

$randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0,2);
/**/
$pdf->Output("C:\\Report\\SHP".$SHIPNO.$randomString.'.pdf', 'F');
// $pdf->Output("C:\\Report\\SUM".$printgroup.$refnum.'.pdf', 'F');
echo '{"ch":1,"data":"DONE"}';
// echo $html;
//============================================================+
// END OF FILE
//============================================================+

function createHead($page,$COMP,$WHADDS,$SHIPNO,$SHPF,$SHPT,$SHPD,$DDATE,$DTIME,$RTIRP,$TL,$TT,$DNAME,$DPHONE)
{
	$barcodeGRN= TCPDF_STATIC::serializeTCPDFtagParameters(array($SHIPNO, 'C128', '', '', 0, 9, 0.4, array('position'=>'R', 'border'=>false, 'padding'=>1, 'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>true, 'font'=>'helvetica', 'fontsize'=>5, 'stretchtext'=>4,'cellfitalign'=>'R','stretch'=>true), 'N'));
	$headData = str_format('<table border="0">
	<tr>
		<td align="right" style="font-size:11px">{1} </td>
	</tr>
</table>',$page.'1');

	$headData .= '<table border="0">
	<tr>
		<td width="130"><img src="images/ttv-logo.gif" width="120"  height="46"/></td>
		<td align="left" width="320" style="font-size:10px"><b>'.$COMP.'</b><br/>
		'.$WHADDS.'
		</td>
		<td align="right" width="218"></td>
	</tr>
</table>
<hr>
<table border="0">
	<tr>
		<td align="center"><b style="font-size:18px; margin-left:300px;">SHIPPING MANIFEST  (AAT)</b></td>
	</tr>
</table>
<hr />
<br>';

$headData .= '
<table border="0" style="margin-top:10px;" cellspacing="" cellpadding="2" style="font-size:11px">
	<tr>
		<td align="letf" width="80">Ship From :</td>
		<td align="left" width="140">'.$SHPF.'</td>
		<td align="left" width="100">Truck Number:</td>
		<td align="left" width="120">'.$TL.'</td>
		<td align="center" width="220">Shipping Manifest Number</td>
	</tr>
	<tr>
		<td align="left" width="80">Ship To :</td>
		<td align="left" width="140">'.$SHPT.'</td>
		<td align="left" width="100">Truck Type :</td>

		<td align="left" width="120">'.$TT.'</td>
		<td align="center" width="220">'.$SHIPNO.'</td>
	</tr>
	<tr>
		<td align="left" width="80">Ship Date :</td>
		<td align="left" width="140">'.$SHPD.'</td>
		<td align="left" width="100">Driver Name :</td>
		<td align="left" width="120">'.$DNAME.'</td>
		<td align="left" width="220" rowspan="2"><tcpdf method="write1DBarcode" params="'.$barcodeGRN.'"/></td>
		
	</tr>
	<tr>
		<td align="left" width="80">Delivery Date :</td>
		<td align="left" width="140">'.$DDATE.'</td>
		<td align="left" width="100">Phone Number :</td>
		<td align="left" width="120">'.$DPHONE.'</td>
	</tr>
	<tr>
		<td align="left" width="80">Remarks :</td>
		<td align="left" width="360"></td>
		<td align="right" width="120">Round Trip: </td>
		<td align="left" width="100">'.$RTIRP.'</td>
	</tr>
</table><br>';
return $headData;
}

function body1()
{
	/*$tableData = '';*/
	
	$thead = '<b>PACKING SLIP</b><br><br><table border="1" cellspacing="0" cellpadding="2">
	<tr style="font-size:10px;background-color:#C8C8C8;" >
		<td align="center" width="30"><b>No. </b></td>
		<td align="center" width="100"><b>Supplier Code</b></td>
		<td align="center" width="200"><b>Packing Slip Number</b></td>
		<td align="center" width="200"><b>Document Type</b></td>
		<td align="center" width="130"><b>Seal No.</b></td>
	</tr>';

	/*for($i=0,$len=count($data);$i<$len;$i++)
	{
		$row = $data[$i];
		$tableData .= '<tr style="font-size:10px" >
		<td align="center" width="30">'.($i+1).'</td>
		<td align="center" width="100">'.($i+1).'</td>
		<td align="center" width="200">'.($i+1).'</td>
		<td align="center" width="200">'.$row[0].'</td>
		<td align="center" width="130">'.$row[4].'</td>
		</tr>';
	}*/
/*	return '<table border="1" cellspacing="0" cellpadding="2">'.$thead.$tableData.'</table>';*/
	return ''.$thead.'';
}

function body2()
{
	/*$tableData = '';*/
	
	$thead = '<br><br><b>SUMMARY SHEET</b><br><br><table border="1" cellspacing="0" cellpadding="2">
	<tr style="font-size:10px;background-color:#C8C8C8;" >
		<td align="center" width="30"><b>No. </b></td>
		<td align="center" width="100"><b>Part Group</b></td>
		<td align="center" width="200"><b>Description</b></td>
		<td align="center" width="120"><b>Dolly Number</b></td>
		<td align="center" width="130"><b>Summary Sheet Number</b></td>
		<td align="center" width="80"><b>Trip No.</b></td>
	</tr>';

	/*for($i=0,$len=count($data);$i<$len;$i++)
	{
		$row = $data[$i];
		$tableData .= '<tr style="font-size:10px" >
		<td align="center" width="30">'.($i+1).'</td>
		<td align="center" width="100">'.$row[0].'</td>
		<td align="center" width="200">'.$row[1].'</td>
		<td align="center" width="180">'.$row[2].'</td>
		<td align="center" width="150">'.$row[3].'</td>
		</tr>';
	}
	return '<table border="1" cellspacing="0" cellpadding="2">'.$thead.$tableData.'</table>';*/
	return ''.$thead.'';
}

function createbox1($DTIME)
{
	return vsprintf('
	<table style="font-size: 11px;" border="1">
		<tr  style="background-color:#C8C8C8;">
			<td align="center" width="60" rowspan="2"><b>Time To AAT</b></td>
			<td align="center" width="200" colspan="2"><b>TTV Warehouse</b></td>
			<td align="center" width="400" colspan="2"><b>AAT Plant</b></td>
		</tr>
		<tr style="font-size:10px;background-color:#C8C8C8;">
			<td align="center" width="80"><b>เวลาออกจากTTV</b></td>
			<td align="center" width="120"><b>ลงชื่อผู้ปล่อยรถ</b></td>
			<td align="center" width="90"><b>เวลารถถึงจุดลงงาน</b></td>
			<td align="center" width="90"><b>เวลาลงงานเสร็จ</b></td>
			<td align="center" width="100"><b>เวลาออกจากจุดลงงาน</b></td>
			<td align="center" width="120"><b>ผู้รับสินค้า</b></td>
		</tr>
		<tr style="font-size:10px;">
			<td align="center" width="60"  ><span style="font-size: 30px;">&nbsp;</span>%1$s</td>
			<td align="center" width="80" ><span style="font-size: 5px;">&nbsp;</span></td>
			<td align="center" width="120" ><span style="font-size: 30px;">&nbsp;</span>_________________<br><br>&nbsp;&nbsp;(____/____/____)<br></td>
			<td align="center" width="90" ><span style="font-size: 5px;">&nbsp;</span></td>
			<td align="center" width="90" ><span style="font-size: 5px;">&nbsp;</span></td>
			<td align="center" width="100" ><span style="font-size: 5px;">&nbsp;</span></td>
			<td align="center" width="120"   ><span style="font-size: 30px;">&nbsp;</span>_________________<br><br>&nbsp;&nbsp;(____/____/____)<br></td>
		</tr>
	</table>',$DTIME);
}

function createbox2()
{
	$row = '';
	$row .= vsprintf(getTrBox2_1(),array('1','rowspan="5"','rowspan="5"','rowspan="5"','rowspan="5"'));
	for($i=1,$len=5;$i<$len;$i++)
	{
		$row .= vsprintf(getTrBox2_2(),array($i+1,'','','',''));
	}
	return '
	<table style="font-size: 11px;" border="1">
		<tr style="font-weight:bold;background-color:#C8C8C8;" >
			<td align="center" width="500"><b>DOLLY RETRUN CONTROL</b></td>
			<td align="center" width="160"><b>เลขไมล์</b></td>
		</tr>
		<tr style="font-size:11px;font-weight:bold;background-color:#C8C8C8;">
			<td align="center" width="60" rowspan="2"><span style="font-size: 15px;">&nbsp;</span>NO.</td>
			<td align="center" width="160" rowspan="2"><span style="font-size: 15px;">&nbsp;</span>Dolly No.</td>
			<td align="center" width="280" colspan="2">Operation (TTV)</td>
			<td align="center" width="80" rowspan="2"><span style="font-size: 15px;">&nbsp;</span>เลขไมล์เริ่มที่ TTV</td>
			<td align="center" width="80"  rowspan="2"><span style="font-size: 15px;">&nbsp;</span>เลขไมล์สิ้นสุดที่ TTV</td>
		</tr>
		<tr style="font-size:11px;font-weight:bold;background-color:#C8C8C8;">
			<td align="center" width="120" >เวลารถถึง TTV</td>
			<td align="center" width="160" >ผู้รับ Dolly<br>(Warehouse)</td>
		</tr>
		'.$row.'
	</table>';
}

function getTrBox2_1()
{
	return '<tr style="font-size:15px;font-weight:bold;">
			<td align="center" width="60" >%1$s</td>
			<td align="center" width="160" ></td>
			<td align="center" %2$s width="120" ></td>
			<td align="center" %3$s width="160" ></td>
			<td align="center" width="80" ></td>
			<td align="center" width="80" ></td>
		</tr>';
}
function getTrBox2_2()
{
	return '<tr style="font-size:15px;font-weight:bold;">
			<td align="center" width="60" >%1$s</td>
			<td align="center" width="160" ></td>
			<td align="center" width="80" ></td>
			<td align="center" width="80" ></td>
		</tr>';
}
function createboxps()
{
	$boxdata = '<br><br>
	<table style="font-size: 16px;" border="1">
		<tbody>
		<tr>
			<td>
				<p>
					<span style="font-size:12px;">Truck No.(ทะเบียนรถ) ..............................</span><br>
					<span style="font-size:12px;">Driver Name (ชื่อ) ....................................</span><br>
					<span style="font-size:12px;">Departure Time (เวลารถออก) ...................</span>
				</p>
			</td>
			<td align="center">
				<p>
					<span style="font-size:12px;">ผู้ออกใบส่งสินค้า (TTV)</span><br>
					<span style="font-size:12px;">Issued by</span><br>
					<span style="font-size:12px;">....................................</span><br>
					<span style="font-size:12px;">วันที่ : ........./........./...........</span>
				</p>
			</td>
			<td align="center">
				<p>
					<span style="font-size:12px;">ผู้รับสินค้า (AAT)</span><br>
					<span style="font-size:12px;">Receive by</span><br>
					<span style="font-size:12px;">....................................</span><br>
					<span style="font-size:12px;">วันที่ : ........./........./...........</span>
				</p>
			</td>
		</tr>
		
		</tbody>
</table>';
	return $boxdata;
}
function footer($SHIPNO,$DNAME,$TL,$RTIRP,$SHPD)
{
	$date = substr($SHPD,0, 8);
	$barcodeshp= TCPDF_STATIC::serializeTCPDFtagParameters(array($SHIPNO, 'C128', '', '', 70, 14, 0.4, array('position'=>'C', 'border'=>false, 'padding'=>1, 'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>true, 'font'=>'helvetica', 'fontsize'=>8, 'stretchtext'=>4,'cellfitalign'=>'R','stretch'=>true), 'N'));
	$footer = '
	<table  border="1">
		<tr>
			<td align="center" colspan ="2">
				<p>
					<span style="font-size:20px;">ใบผ่านประตู AAT (Gate Pass)</span>
				</p>
			</td>
		</tr>
		<tr>
			<td align="center" width="180">
				<p>
					<span style="font-size:12px;">&nbsp;</span><br>
					<span style="font-size:12px;">Shipping Manifest Number</span><br>
					<span style="font-size:12px;">'.$SHIPNO.'</span>
				</p>
			</td>
			<td align="left" width="0" >
				<span style="font-size:12px;">&nbsp;</span><br>
				<tcpdf method="write1DBarcode" params="'.$barcodeshp.'"/>
			</td>
		</tr>
</table>
<table  border="0">
		<tr>
			<td align="left" colspan ="2" width="0">
				<p>
					<span style="font-size:14px;"><u>Project Wiring and CCB by Sequence</u></span>
				</p>
			</td>
		</tr>
		<tr>
			<td align="left" width="100">
				<p>
					<span style="font-size:12px;"> Driver Name :</span><br>
					<span style="font-size:12px;"> Truck Number :</span><br>
					<span style="font-size:12px;"> Round Trip :</span><br>
				</p>
			</td>
			<td align="left" width="100">
				<p>
					<span style="font-size:12px;">'.$DNAME.'</span><br>
					<span style="font-size:12px;">'.$TL.'</span><br>
					<span style="font-size:12px;">'.$RTIRP.'</span><br>
				</p>
			</td>
			<td align="left" width="0">
				<table  border="1">
					<tr>
						<td align="center" width="100" style="font-weight:bold;background-color:#C8C8C8;">
							<p>
								<span style="font-size:12px;">วันที่</span>
							</p>
						</td>
						<td align="center" width="100" style="font-weight:bold;background-color:#C8C8C8;">
							<p>
								<span style="font-size:12px;">เวลา</span>
							</p>
						</td>
						<td align="center" width="100" rowspan="2" style="font-weight:bold;background-color:#C8C8C8;">
							<p>
								<span style="font-size:12px;">&nbsp;</span><br>
								<span style="font-size:14px;">Total Dolly Return TTV</span><br>
								<span style="font-size:12px;">&nbsp;</span>
							</p>
						</td>
						<td align="center" width="0" rowspan="2">
							<p>
								<span style="font-size:12px;">&nbsp;</span>
							</p>
						</td>
					</tr>
					<tr >
						<td align="center" width="100">
							<p>
								<span style="font-size:14px;">'.$date.'</span><br>
								<span style="font-size:12px;">&nbsp;</span>
							</p>
						</td>
						<td align="center" width="100">
							<p>
								<span style="font-size:12px;">&nbsp;</span><br>
								<span style="font-size:12px;">&nbsp;</span>
							</p>
						</td>
					</tr>
				</table>
			</td>
		</tr>
</table>';
	return $footer;
}

