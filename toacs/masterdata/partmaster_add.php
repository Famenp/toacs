<?php
	if(!ob_start("ob_gzhandler")) ob_start();
	header('Expires: Sun, 01 Jan 2014 00:00:00 GMT');
	header('Cache-Control: no-store, no-cache, must-revalidate');
	header('Cache-Control: post-check=0, pre-check=0', FALSE);
	header('Pragma: no-cache');
	include('../start.php');
	session_start();
	include('../php/connection.php');
	$cBy = $_SESSION['xxxID'];
	$fName = $_SESSION['xxxFName'];
	$obj  = $_POST['obj'];
	$type  = intval($_POST['type']);

	if($type == 1)
	{

		$Supp_PartNo = $mysqli->real_escape_string(trim(strtoupper($obj['SuppPartNo'])));
		$part_Customer = $mysqli->real_escape_string(trim(strtoupper($obj['partCustomer'])));
		$part_Name = $mysqli->real_escape_string(trim(strtoupper($obj['partName'])));
		$Internal_Code = $mysqli->real_escape_string(trim(strtoupper($obj['InternalCode'])));
		$Model = $mysqli->real_escape_string(trim(strtoupper($obj['Model'])));
		$SNP = $mysqli->real_escape_string(trim(strtoupper($obj['SNP'])));
		$Box_Per_Pallet = $mysqli->real_escape_string(trim(strtoupper($obj['BoxPerPallet'])));
		$store_Location = $mysqli->real_escape_string(trim(strtoupper($obj['storeLocation'])));
		$pick_location = $mysqli->real_escape_string(trim(strtoupper($obj['picklocation'])));

		if(!$re1 = $mysqli->query("SELECT part_supplier from tbl_partmaster where part_supplier='$Supp_PartNo' limit 1;"))
			{echo '{ch:2,data:"Error Code 1"}';$mysqli->close();}
		if($re1->num_rows > 0){echo '{ch:2,data:"Part Supplier มีข้อมมูลในระบบแล้ว"}';$mysqli->close();exit();}
		

		try 
			{
				$sql = "INSERT INTO tbl_partmaster(part_supplier,part_customer,part_name,int_code,part_model,snp,box_per_pallet,
									put_loc,pick_loc,user_id,create_date)
									values('$Supp_PartNo','$part_Customer','$part_Name','$Internal_Code','$Model','$SNP','$Box_Per_Pallet',
									'$store_Location','$pick_location','$cBy',NOW())";
				if(!$mysqli->query($sql)) throw new Exception('Error Code 2');			
				$mysqli->commit();

				if($re1 = $mysqli->query("select t1.part_id,t1.part_supplier,t1.part_customer,t1.int_code,
							  			t1.part_name,t1.part_model,t1.snp,t1.box_per_pallet,t1.put_loc,
							  			t1.pick_loc,t1.create_date,t1.modify_date,t2.user_fName from tbl_partmaster t1
							  			LEFT JOIN tbl_user t2 ON t1.user_id = t2.user_id ORDER BY t1.part_id"))
				{
					if($re1->num_rows >0)
						{
							echo '{"ch":1,"data":';
							toArrayStringAddNumberRow($re1,1);
							echo '}';
						}
					else echo '{ch:2,data:"ไม่พบข้อมูลในระบบ"}';
				}
				else echo '{ch:2,data:"โคดผิด"}';
			} 

		catch (Exception $e) 
			{
				$mysqli->rollback();
		  		echo '{ch:2,data:"'.$e->getMessage().'"}';
			}
		
	}
	$mysqli->close();
	exit();	
?>
