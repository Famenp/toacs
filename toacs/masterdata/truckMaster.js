var header_truckMaster = function()
{
	return {
        view: "scrollview",
        scroll: "native-y",
        id:"header_truckMaster",
        body: 
        {
        	id:"truckMaster_id",
        	type:"clean",
    		rows:
    		[
    		    {
                    view:"form",
                    paddingY:20,
                    id:"TruckMaster_form1",
                    elements:
                    [
                        {                            
                            cols:                                    
                                [                                   
                                    {
                                        view:"button", label:"Add Truck Master", type:"form",width:230,
                                        on:
                                            {
                                                onItemClick:function(id, e)
                                                {
                                                    $$('add_truck_master').show();  
                                                }
                                            }                                      
                                    },
                                    { },
                                    { }            
                                ]
                        },
                        {
                            paddingX:2,
                            rows:
                            [
                                {  
                                    view:"datatable",
                                    id:"Truck_Master_dataT1",
                                    navigation:true,
                                    datatype:"jsarray",
                                    css:"my_style",
                                    pager:"Truck_Master_pagerA",
                                    autoheight:true,
                                    resizeColumn:true,
                                    leftSplit:0,
                                    columns:
                                    [                                  
                                        { id:"data21",header:"&nbsp;",width:35,template: "<span style='cursor:pointer;' class='webix_icon fa-pencil'></span>"},
                                        { id:"data22",header:"&nbsp;",width:35,template: "<span style='cursor:pointer;' class='webix_icon fa-trash-o'></span>"},
                                        { id:"data0",header:"No",css:"rank",width:50},
                                        { id:"data2",header:"Truck License Plate",css:"rank",width:200}, 
                                        { id:"data3",header:"Truck Type",css:"rank",width:200},
                                        { id:"data4",header:"Remark",css:"rank",width:200},
                                        { id:"data5",header:"Create Date",css:"rank",width:200},
                                        { id:"data6",header:"Create By",css:"rank",width:200},
                                       
                                                                        
                                    ],
                                    onClick:
                                    {
                                        "fa-pencil":function(e,t)
                                        {
                                            var row = this.getItem(t),obj;
                                            $$('truck_Master_edit').show();
                                            $$('Truck_Master_form1_edit').setValues(
                                            {
                                                TruckLicensePlate_edit:row.data2,
                                                TruckType_edit:row.data3,
                                                Remark_edit:row.data4,
                                                truck_id_edit:row.data1,
                                                                                                    
                                            });                                                                                
                                        },
                                         "fa-trash-o":function(e,t)
                                        {
                                            var r = this.getItem(t),obj;
                                            webix.confirm({
                                                 title: "กรุณายืนยัน",ok:"ใช่", cancel:"ไม่",text:"คุณต้องการ<font color='#ca4635'><br><b>ลบ</b></font><br><font color='#27ae60'><b>ใช่</b></font> หรือ <font color='#3498db'><b>ไม่</b></font>",
                                                 callback:function(res)
                                                 {
                                                    if(res)
                                                    {
                                                        $.post("masterdata/truckMaster_delete.php",{obj:{truck_id:r.data1},type:1})
                                                        .done(function( data ) 
                                                        {
                                                            data = eval('('+data+')');
                                                            if(data.ch==1)
                                                            {
                                                                var dataT1 = $$('Truck_Master_dataT1');
                                                                dataT1.clearAll();
                                                                dataT1.parse(data.data1,"jsarray");
                                                            }
                                                            else if(data.ch == 2){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                                                            else if(data.ch == 9){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                                                            else if(data.ch == 10){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){window.open("login.php","_self");}});}
                                                        });
                                                    }
                                                 }
                                            });
                                        }
                                    }
                                }
                                ,
                                {
                                    type:"wide",
                                        cols:
                                        [
                                            {
                                                view:"pager", id:"Truck_Master_pagerA",
                                                template:function(data, common){
                                                    var start = data.page * data.size
                                                    ,end = start + data.size;
                                                    if(data.count == 0) start = 0;
                                                    else start += 1;
                                                    if(end >= data.count) end = data.count;
                                                    var html = "<b>showing "+(start)+" - "+end+" total "+data.count+" </b>";
                                                    return common.first()+common.prev()+" "+html+" "+common.next()+common.last();
                                                },
                                                size:10,
                                                group:5 
                                            }
                                        ]
                                                        
                                }

                            ]
                        }
                          
                    ]
                    
                }
            ],on:
            {
                onHide:function()
                {
                    
                },
                onShow:function()
                {

                },
                onAddView:function()
                {
                    function loadtable()
                    {
                       
                        $.post("masterdata/truckmaster_data.php", {type:1})
                        .done(function( data ) 
                        {
                          var dataT1 = $$("Truck_Master_dataT1");
                          var data = eval('('+data+')');
                          if(data.ch == 1)
                          { 
                            dataT1.clearAll();
                            dataT1.parse(data.data,"jsarray");
                          }
                          else if(data.ch == 2){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                          else if(data.ch == 9){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                          else if(data.ch == 10){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){window.open("login.php","_self");}});}
                        });
                    }
                    loadtable();

                    webix.ui(
                    {
                        view:"window",id:"add_truck_master",modal:1,move:1,
                        head:{
                                view:"toolbar", margin:-4, cols:[
                                    {view:"label", label: "Add Truck Master"},
                                    { view:"icon", icon:"times-circle", css:"alter",
                                        click:"$$('add_truck_master').hide();"}                                  
                                                    
                                    ]
                            },
                        top:50,position:"center",
                        body:
                        {
                            view:"form", scroll:false,id:"master_form1_addTruck",width:500,
                            elements:
                            [
                                {
                                    cols:
                                    [
                                        {view:"text",label:'Truck License Plate (ทะเบียนรถ)',required:true,name:"TruckLicensePlate",id:"TruckLicensePlate",labelPosition:"top", value:"",width:400},
                                    ]  
                                },                                    
                                {
                                    cols:
                                    [
                                        {view:"combo",label:'Truck Type (ชนิดรถ)',required:true,name:"TruckType",id:"TruckType", labelPosition:"top", options:["6W","10W","Pickup","Trailler"],width:400},
                                    ]  
                                },
                                {
                                    cols:
                                    [
                                       {view:"text",label:'Remark (หมายเหตุ)',name:"Remark",id:"Remark",labelPosition:"top", value:"",width:400},
                                    ]   
                                },                                                               
                                {
                                    cols:
                                    [
                                         {view:"button", value:"OK (ตกลง)",type:"form",id:"master_ok_truck",on:
                                            {
                                                 onItemClick:function(id)
                                                {
                                                    if ($$('master_form1_addTruck').validate()) 
                                                    {
                                                        webix.confirm({
                                                            title: "กรุณายืนยัน",ok:"ใช่", cancel:"ไม่",text:"คุณต้องการ<font color='#ca4635'><br><b>ที่จะบันทึก</b></font><br><font color='#27ae60'><b>ใช่</b></font> หรือ <font color='#3498db'><b>ไม่</b></font>",
                                                            callback:function(res)
                                                            {
                                                                if(res)
                                                                {
                                                                    var btn = $$('master_ok_truck'),
                                                                    obj =  $$('master_form1_addTruck').getValues();
                                                                    btn.disable();
                                                                    $.post("masterdata/truckMaster_add.php",{obj:obj,type:1})
                                                                     .done(function( data ) 
                                                                    {                                                      
                                                                         btn.enable();
                                                                          var data = eval('('+data+')');
                                                                          if(data.ch == 1)
                                                                          {
                                                                              var dataT1 = $$("Truck_Master_dataT1");
                                                                              dataT1.clearAll();
                                                                              dataT1.parse(data.data,"jsarray");
                                                                              $$('masterTruck_cancel').callEvent("onItemClick", []);
                                                                          }
                                                                          else if (data.ch == 2)
                                                                          {
                                                                            webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});
                                                                          }
                                                                    });
                                                                }
                                                            }
                                                        })
                                                    }
                                                }
                                            }
                                        },
                                        {view:"button", value:"Cancel (ยกเลิก)",type:"danger",id:"masterTruck_cancel",on:
                                            {
                                                 onItemClick:function(id)
                                                {
                                                    $$('add_truck_master').hide();
                                                    $$('master_form1_addTruck').clearValidation();
                                                    $$('master_form1_addTruck').clear();
                                                }
                                            }
                                        },{},{}
                                    ]
                                }     
                            ]
                        }
                    });

                     webix.ui(
                    {
                        view:"window",id:"truck_Master_edit",modal:1,move:1,
                        head:{
                                view:"toolbar", margin:-4, cols:[
                                    {view:"label", label: "Edit Truck Master"},
                                    { view:"icon", icon:"times-circle", css:"alter",
                                        click:"$$('truck_Master_edit').hide();"}                                  
                                                    
                                    ]
                            },
                        top:50,position:"center",
                        body:
                        {
                            view:"form", scroll:false,id:"Truck_Master_form1_edit",width:500,
                            elements:
                            [
                                {
                                    cols:
                                    [
                                        {view:"text",label:'Truck License Plate (ทะเบียนรถ)',required:true,name:"TruckLicensePlate_edit",id:"TruckLicensePlate_edit",labelPosition:"top", value:"",width:400},
                                    ]  
                                },                                    
                                {
                                    cols:
                                    [
                                        {view:"combo",label:'Truck Type (ชนิดรถ)',required:true,name:"TruckType_edit",id:"TruckType_edit", labelPosition:"top", options:["6W","10W","Pickup","Trailler"],width:400},
                                    ]  
                                },
                                {
                                    cols:
                                    [
                                       {view:"text",label:'Remark (หมายเหตุ)',name:"Remark_edit",id:"Remark_edit",labelPosition:"top", value:"",width:400},
                                       {view:"text",label:'truck id',name:"truck_id_edit",id:"truck_id_edit",labelPosition:"top", value:"",width:50,hidden:true},
                                    ]   
                                },     
                                {
                                    cols:
                                    [
                                         {view:"button", value:"OK (ตกลง)",type:"form",id:"Edit_Truck",on:
                                            {
                                                 onItemClick:function(id)
                                                {
                                                    if ($$('Truck_Master_form1_edit').validate()) 
                                                    {
                                                        webix.confirm({
                                                            title: "กรุณายืนยัน",ok:"ใช่", cancel:"ไม่",text:"คุณต้องการ<font color='#ca4635'><br><b>ที่จะบันทึก</b></font><br><font color='#27ae60'><b>ใช่</b></font> หรือ <font color='#3498db'><b>ไม่</b></font>",
                                                            callback:function(res)
                                                            {
                                                                if(res)
                                                                {
                                                                    var btn = $$('Edit_Truck'),
                                                                    obj =  $$('Truck_Master_form1_edit').getValues();
                                                                    btn.disable();
                                                                    $.post("masterdata/truckMaster_update.php",{obj:obj,type:1})
                                                                     .done(function( data ) 
                                                                    {                                                      
                                                                         btn.enable();
                                                                          var data = eval('('+data+')');
                                                                          if(data.ch == 1)
                                                                          {
                                                                              var dataT1 = $$("Truck_Master_dataT1");
                                                                              dataT1.clearAll();
                                                                              dataT1.parse(data.data,"jsarray");
                                                                              $$('masterTruck_edit_cancel').callEvent("onItemClick", []);
                                                                          }
                                                                    });
                                                                }
                                                            }
                                                        })
                                                    }
                                                }
                                            }
                                        },
                                        {view:"button", value:"Cancel (ยกเลิก)",type:"danger",id:"masterTruck_edit_cancel",on:
                                            {
                                                 onItemClick:function(id)
                                                {
                                                    $$('truck_Master_edit').hide();
                                                    $$('Truck_Master_form1_edit').clearValidation();
                                                    $$('Truck_Master_form1_edit').clear();
                                                }
                                            }
                                        },{},{}
                                    ]
                                }     
                            ]
                        }
                    });
                }
            }
        }
    };
};