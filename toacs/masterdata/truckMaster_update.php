<?php
	if(!ob_start("ob_gzhandler")) ob_start();
	header('Expires: Sun, 01 Jan 2014 00:00:00 GMT');
	header('Cache-Control: no-store, no-cache, must-revalidate');
	header('Cache-Control: post-check=0, pre-check=0', FALSE);
	header('Pragma: no-cache');
	include('../start.php');
	session_start();
	include('../php/connection.php');
	$cBy = $_SESSION['xxxID'];
	$fName = $_SESSION['xxxFName'];
	$obj  = $_POST['obj'];
	$type  = intval($_POST['type']);

	if($type == 1)
	{

		$truck_license = $mysqli->real_escape_string(trim(strtoupper($obj['TruckLicensePlate_edit'])));
		$truck_type = $mysqli->real_escape_string(trim(strtoupper($obj['TruckType_edit'])));
		$truck_remark = $mysqli->real_escape_string(trim(strtoupper($obj['Remark_edit'])));
		$truck_id = $mysqli->real_escape_string(trim(strtoupper($obj['truck_id_edit'])));

			try 
			{
			
				$sql = "UPDATE tbl_truckmaster SET Truck_Plate_Number ='$truck_license',Truck_Type ='$truck_type',Remarks ='$truck_remark',
						Updated_By = '$cBy' WHERE ID = '$truck_id';";

				if(!$mysqli->query($sql)) throw new Exception('Error Code 2');
				$mysqli->commit();
				$re1 = $mysqli->query("SELECT t1.ID,t1.Truck_Plate_Number,t1.Truck_Type,t1.Remarks,t1.Creation_Date,t2.user_fName 
							FROM tbl_truckmaster t1 LEFT JOIN tbl_user t2 ON t1.Created_By = t2.user_id ORDER BY t1.ID");
				echo '{"ch":1,"data":';
				toArrayStringAddNumberRow($re1,1);
				echo '}';
			} 
			catch (Exception $e) 
			{
				$mysqli->rollback();
		  		echo '{ch:2,data:"'.$e->getMessage().'"}';	
			}		
	}
	
	$mysqli->close();
	exit();
?>