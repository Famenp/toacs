var header_driverMaster = function()
{
	return {
        view: "scrollview",
        scroll: "native-y",
        id:"header_driverMaster",
        body: 
        {
        	id:"driverMaster_id",
        	type:"clean",
    		rows:
    		[
    		{
                    view:"form",
                    paddingY:20,
                    id:"driverMaster_form1",
                    elements:
                    [
                        {                            
                            cols:                                    
                                [                                   
                                    {
                                        view:"button", label:"Add driver Master (เพิ่มคนขับรถ)", type:"form",width:240,
                                        on:
                                            {
                                                onItemClick:function(id, e)
                                                {
                                                    $$('add_driver_master').show();  
                                                }
                                            }                                      
                                    },
                                    { },
                                    { }            
                                ]
                        },
                        {
                            paddingX:2,
                            rows:
                            [
                                {  
                                    view:"datatable",
                                    id:"driverMaster_dataT1",
                                    navigation:true,
                                    datatype:"jsarray",
                                    css:"my_style",
                                    pager:"driverMaster_pagerA",
                                    autoheight:true,
                                    resizeColumn:true,
                                    leftSplit:0,
                                    columns:
                                    [                                  
                                        { id:"data21",header:"&nbsp;",width:35,template: "<span style='cursor:pointer;' class='webix_icon fa-pencil'></span>"},
                                        { id:"data22",header:"&nbsp;",width:35,template: "<span style='cursor:pointer;' class='webix_icon fa-trash-o'></span>"},
                                        { id:"data0",header:"No",css:"rank",width:50},
                                        { id:"data2",header:"Driver Name",css:"rank",width:300},
                                        { id:"data3",header:"Telephone",css:"rank",width:200},
                                        { id:"data4",header:"Remark",css:"rank",width:200},
                                        { id:"data5",header:"Create Date",css:"rank",width:200},
                                        { id:"data6",header:"Create By",css:"rank",width:200},
                                                                        
                                    ],
                                    onClick:
                                    {
                                        "fa-pencil":function(e,t)
                                        {
                                            var row = this.getItem(t),obj;
                                            $$('Driver_Master_edit').show();
                                            $$('DriverMaster_form1_edit').setValues(
                                            {
                                                FirstName_edit:row.data2,
                                                Telephone_edit:row.data3,
                                                Remark_edit:row.data4,
                                                driver_id_edit:row.data1,
                                                                                                    
                                            });                                                                                
                                        },
                                         "fa-trash-o":function(e,t)
                                        {
                                            var r = this.getItem(t),obj;
                                            webix.confirm({
                                                 title: "กรุณายืนยัน",ok:"ใช่", cancel:"ไม่",text:"คุณต้องการ<font color='#ca4635'><br><b>ลบ</b></font><br><font color='#27ae60'><b>ใช่</b></font> หรือ <font color='#3498db'><b>ไม่</b></font>",
                                                 callback:function(res)
                                                 {
                                                    if(res)
                                                    {
                                                        $.post("masterdata/driverMaster_delete.php",{obj:{driver_id:r.data1},type:1})
                                                        .done(function( data ) 
                                                        {
                                                            data = eval('('+data+')');
                                                            if(data.ch==1)
                                                            {
                                                                var dataT1 = $$('driverMaster_dataT1');
                                                                dataT1.clearAll();
                                                                dataT1.parse(data.data1,"jsarray");
                                                            }
                                                            else if(data.ch == 2){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                                                            else if(data.ch == 9){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                                                            else if(data.ch == 10){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){window.open("login.php","_self");}});}
                                                        });
                                                    }
                                                 }
                                            });
                                        }
                                    }
                                }
                                ,
                                {
                                    type:"wide",
                                        cols:
                                        [
                                            {
                                                view:"pager", id:"driverMaster_pagerA",
                                                template:function(data, common){
                                                    var start = data.page * data.size
                                                    ,end = start + data.size;
                                                    if(data.count == 0) start = 0;
                                                    else start += 1;
                                                    if(end >= data.count) end = data.count;
                                                    var html = "<b>showing "+(start)+" - "+end+" total "+data.count+" </b>";
                                                    return common.first()+common.prev()+" "+html+" "+common.next()+common.last();
                                                },
                                                size:10,
                                                group:5 
                                            }
                                        ]
                                                        
                                }

                            ]
                        }
                          
                    ]
                    
                }
            ],on:
            {
                onHide:function()
                {
                    
                },
                onShow:function()
                {

                },
                onAddView:function()
                {
                    function loadtable()
                    {
                       
                        $.post("masterdata/driverMaster_data.php", {type:1})
                        .done(function( data ) 
                        {
                          var dataT1 = $$("driverMaster_dataT1");
                          var data = eval('('+data+')');
                          if(data.ch == 1)
                          { 
                            dataT1.clearAll();
                            dataT1.parse(data.data,"jsarray");
                          }
                          else if(data.ch == 2){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                          else if(data.ch == 9){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                          else if(data.ch == 10){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){window.open("login.php","_self");}});}
                        });
                    }                   
                    loadtable();
                    webix.ui(
                    {
                        view:"window",id:"add_driver_master",modal:1,move:1,
                        head:{
                                view:"toolbar", margin:-4, cols:[
                                    {view:"label", label: "Add Driver Master"},
                                    { view:"icon", icon:"times-circle", css:"alter",
                                        click:"$$('add_driver_master').hide();"}                                  
                                                    
                                    ]
                            },
                        top:50,position:"center",
                        body:
                        {
                            view:"form", scroll:false,id:"master_form1_addDriver",width:680,
                            elements:
                            [
                                {
                                    cols:
                                    [
                                        {view:"text",label:'First Name(ชื่อ)',required:true,name:"FirstName",id:"FirstName",labelPosition:"top", value:"",width:320},
                                        {view:"text",label:'Last Name(นามสกุล)',required:true,name:"LastName",id:"LastName",labelPosition:"top", value:"",width:320},
                                    ]  
                                },                                    
                                {
                                    cols:
                                    [
                                        
                                    ]  
                                },
                                {
                                    cols:
                                    [
                                       {view:"text",label:'Telephone(เบอร์โทร)',required:true,name:"Telephone",id:"Telephone",labelPosition:"top", value:"",width:320},
                                        {view:"text",label:'Remark(หมายเหตุ)',name:"Remark",id:"Remark",labelPosition:"top", value:"",width:320},
                                    ]   
                                },
                                {
                                    cols:
                                    [
                                      
                                    ]   
                                },                                                                
                                {
                                    cols:
                                    [
                                         {view:"button", value:"OK (ตกลง)",type:"form",id:"master_ok_Driver",on:
                                            {
                                                 onItemClick:function(id)
                                                {
                                                    if ($$('master_form1_addDriver').validate()) 
                                                    {
                                                        webix.confirm({
                                                            title: "กรุณายืนยัน",ok:"ใช่", cancel:"ไม่",text:"คุณต้องการ<font color='#ca4635'><br><b>ที่จะบันทึก</b></font><br><font color='#27ae60'><b>ใช่</b></font> หรือ <font color='#3498db'><b>ไม่</b></font>",
                                                            callback:function(res)
                                                            {
                                                                if(res)
                                                                {
                                                                    var btn = $$('master_ok_Driver'),
                                                                    obj =  $$('master_form1_addDriver').getValues();
                                                                    btn.disable();
                                                                    $.post("masterdata/driverMaster_add.php",{obj:obj,type:1})
                                                                     .done(function( data ) 
                                                                    {                                                      
                                                                         btn.enable();
                                                                          var data = eval('('+data+')');
                                                                          if(data.ch == 1)
                                                                          {
                                                                              var dataT1 = $$("driverMaster_dataT1");
                                                                              dataT1.clearAll();
                                                                              dataT1.parse(data.data,"jsarray");
                                                                              $$('masterDriver_cancel').callEvent("onItemClick", []);
                                                                          }
                                                                          else if (data.ch == 2)
                                                                          {
                                                                            webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});
                                                                          }
                                                                    });
                                                                }
                                                            }
                                                        })
                                                    }
                                                }
                                            }
                                        },
                                        {view:"button", value:"Cancel (ยกเลิก)",type:"danger",id:"masterDriver_cancel",on:
                                            {
                                                 onItemClick:function(id)
                                                {
                                                    $$('add_driver_master').hide();
                                                    $$('master_form1_addDriver').clearValidation();
                                                    $$('master_form1_addDriver').clear();
                                                }
                                            }
                                        },{},{}
                                    ]
                                }     
                            ]
                        }
                    });
        
                    webix.ui(
                    {
                        view:"window",id:"Driver_Master_edit",modal:1,move:1,
                        head:{
                                view:"toolbar", margin:-4, cols:[
                                    {view:"label", label: "Edit Driver Master"},
                                    { view:"icon", icon:"times-circle", css:"alter",
                                        click:"$$('Driver_Master_edit').hide();"}                                  
                                                    
                                    ]
                            },
                        top:50,position:"center",
                        body:
                        {
                            view:"form", scroll:false,id:"DriverMaster_form1_edit",width:500,
                            elements:
                            [
                                {
                                    cols:
                                    [
                                        {view:"text",label:'First Name-Last Name(ชื่อ - นามสกุล)',required:true,name:"FirstName_edit",id:"FirstName_edit",labelPosition:"top", value:"",width:320},
                                       
                                    ]  
                                },                                    
                                {
                                    cols:
                                    [
                                        {view:"text",label:'Telephone(เบอร์โทร)',required:true,name:"Telephone_edit",id:"Telephone_edit",labelPosition:"top", value:"",width:320},
                                    ]  
                                },
                                {
                                    cols:
                                    [
                                       
                                        {view:"text",label:'Remark(หมายเหตุ)',name:"Remark_edit",id:"Remark_edit",labelPosition:"top", value:"",width:320},
                                        {view:"text",label:'drivr id',name:"driver_id_edit",id:"driver_id_edit",labelPosition:"top", value:"",width:50,hidden:true},
                                    ]   
                                },                       
                                {
                                    cols:
                                    [
                                         {view:"button", value:"OK (ตกลง)",type:"form",id:"Edit_Driver",on:
                                            {
                                                 onItemClick:function(id)
                                                {
                                                    if ($$('DriverMaster_form1_edit').validate()) 
                                                    {
                                                        webix.confirm({
                                                            title: "กรุณายืนยัน",ok:"ใช่", cancel:"ไม่",text:"คุณต้องการ<font color='#ca4635'><br><b>ที่จะบันทึก</b></font><br><font color='#27ae60'><b>ใช่</b></font> หรือ <font color='#3498db'><b>ไม่</b></font>",
                                                            callback:function(res)
                                                            {
                                                                if(res)
                                                                {
                                                                    var btn = $$('Edit_Driver'),
                                                                    obj =  $$('DriverMaster_form1_edit').getValues();
                                                                    btn.disable();
                                                                    $.post("masterdata/driverMaster_update.php",{obj:obj,type:1})
                                                                     .done(function( data ) 
                                                                    {                                                      
                                                                         btn.enable();
                                                                          var data = eval('('+data+')');
                                                                          if(data.ch == 1)
                                                                          {
                                                                              var dataT1 = $$("driverMaster_dataT1");
                                                                              dataT1.clearAll();
                                                                              dataT1.parse(data.data,"jsarray");
                                                                              $$('masterDriver_edit_cancel').callEvent("onItemClick", []);
                                                                          }
                                                                    });
                                                                }
                                                            }
                                                        })
                                                    }
                                                }
                                            }
                                        },
                                        {view:"button", value:"Cancel (ยกเลิก)",type:"danger",id:"masterDriver_edit_cancel",on:
                                            {
                                                 onItemClick:function(id)
                                                {
                                                    $$('Driver_Master_edit').hide();
                                                    $$('DriverMaster_form1_edit').clearValidation();
                                                    $$('DriverMaster_form1_edit').clear();
                                                }
                                            }
                                        },{},{}
                                    ]
                                }     
                            ]
                        }
                    });

                }
            }
        }
    };
};