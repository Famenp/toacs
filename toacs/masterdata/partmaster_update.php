<?php
	if(!ob_start("ob_gzhandler")) ob_start();
	header('Expires: Sun, 01 Jan 2014 00:00:00 GMT');
	header('Cache-Control: no-store, no-cache, must-revalidate');
	header('Cache-Control: post-check=0, pre-check=0', FALSE);
	header('Pragma: no-cache');
	include('../start.php');
	session_start();
	include('../php/connection.php');
	$cBy = $_SESSION['xxxID'];
	$fName = $_SESSION['xxxFName'];
	$obj  = $_POST['obj'];
	$type  = intval($_POST['type']);

	if($type == 1)
	{

		$Supp_PartNo = $mysqli->real_escape_string(trim(strtoupper($obj['SuppPartNo_edit'])));
		$part_Customer = $mysqli->real_escape_string(trim(strtoupper($obj['partCustomer_edit'])));
		$part_Name = $mysqli->real_escape_string(trim(strtoupper($obj['partName_edit'])));
		$Internal_Code = $mysqli->real_escape_string(trim(strtoupper($obj['InternalCode_edit'])));
		$Model = $mysqli->real_escape_string(trim(strtoupper($obj['Model_edit'])));
		$SNP = $mysqli->real_escape_string(trim(strtoupper($obj['SNP_edit'])));
		$Box_Per_Pallet = $mysqli->real_escape_string(trim(strtoupper($obj['BoxPerPallet_edit'])));
		$store_Location = $mysqli->real_escape_string(trim(strtoupper($obj['storeLocation_edit'])));
		$pick_location = $mysqli->real_escape_string(trim(strtoupper($obj['picklocation_edit'])));
		$idpart = $mysqli->real_escape_string(trim(strtoupper($obj['idpart_edit'])));

		$mysqltime = date ("Y-m-d H:i:s");

			try 
			{
			
				$sql = "UPDATE tbl_partmaster SET part_supplier ='$Supp_PartNo',part_customer ='$part_Customer',part_name = '$part_Name',
						int_code ='$Internal_Code',part_model ='$Model',snp = '$SNP',box_per_pallet ='$Box_Per_Pallet',
						put_loc = '$store_Location',pick_loc = '$pick_location',
						user_id = '$cBy' WHERE part_id = '$idpart';";

				if(!$mysqli->query($sql)) throw new Exception('Error Code 2');
				$mysqli->commit();
				$re1 = $mysqli->query("select t1.part_id,t1.part_supplier,t1.part_customer,t1.int_code,
							  			t1.part_name,t1.part_model,t1.snp,t1.box_per_pallet,t1.put_loc,
							  			t1.pick_loc,t1.create_date,t1.modify_date,t2.user_fName from tbl_partmaster t1
							  			LEFT JOIN tbl_user t2 ON t1.user_id = t2.user_id ORDER BY t1.part_id");
				echo '{"ch":1,"data":';
				toArrayStringAddNumberRow($re1,1);
				echo '}';
			} 
			catch (Exception $e) 
			{
				$mysqli->rollback();
		  		echo '{ch:2,data:"'.$e->getMessage().'"}';	
			}		
	}
	
	$mysqli->close();
	exit();
?>