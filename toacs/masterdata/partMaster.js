var header_partMaster = function()
{
	return {
        view: "scrollview",
        scroll: "native-y",
        id:"header_partMaster",
        body: 
        {
        	id:"partMaster_id",
        	type:"clean",
    		rows:
    		[
    		    {
                    view:"form",
                    paddingY:20,
                    id:"PartMaster_form1",
                    elements:
                    [
                        {                            
                            cols:                                    
                                [                                   
                                    {
                                        view:"button", label:"Add Part Master", type:"form",width:230,
                                        on:
                                            {
                                                onItemClick:function(id, e)
                                                {
                                                    $$('add_part_master').show();  
                                                }
                                            }                                      
                                    },
                                    {
                                        view:"button", value:"Export Excel", type:"form",width:230,id:"PrintReItem_exl",
                                        on:
                                        {
                               
                                            onItemClick:function(id, e)
                                            {
                                                
                                            }
                                        }
                                    },
                                    {
                                         
                                    }            
                                ]
                        },
                        {
                            paddingX:2,
                            rows:
                            [
                                {  
                                    view:"datatable",
                                    id:"partMaster_dataT1",
                                    navigation:true,
                                    datatype:"jsarray",
                                    css:"my_style",
                                    pager:"partMaster_pagerA",
                                    autoheight:true,
                                    resizeColumn:true,
                                    leftSplit:5,
                                    columns:
                                    [
                                   
                                        { id:"data21",header:"&nbsp;",width:35,template: "<span style='cursor:pointer;' class='webix_icon fa-pencil'></span>"},
                                        { id:"data22",header:"&nbsp;",width:35,template: "<span style='cursor:pointer;' class='webix_icon fa-trash-o'></span>"},
                                        { id:"data0",header:"No",css:"rank",width:50},
                                        { id:"data2",header:[{text :"Part Supplier",css:{"text-align":"center"}},{content:"textFilter"}],css:"rank",width:190},
                                        { id:"data3",header:[{text :"Part Customer",css:{"text-align":"center"}},{content:"textFilter"}],css:"rank",width:190},       
                                        { id:"data5",header:"Part Name",css:"rank",width:400},
                                        { id:"data4",header:"Internal Code",css:"rank",width:190},                                    
                                        { id:"data6",header:"Model",css:"rank",width:130},
                                        { id:"data7",header:"SNP",css:"rank",width:80},
                                        { id:"data8",header:"Box Per Pallet",css:"rank",width:120},
                                        { id:"data9",header:"Store Location",css:"rank",width:120},
                                        { id:"data10",header:"Pick Location",css:"rank",width:120},
                                        { id:"data11",header:"Create Date",css:"rank",width:180},
                                        { id:"data12",header:"Modify Date",css:"rank",width:180},
                                        { id:"data13",header:"Create By",css:"rank",width:170},
                                     
                                    ],
                                    onClick:
                                    {
                                        "fa-pencil":function(e,t)
                                        {
                                            var row = this.getItem(t),obj;
                                            $$('Part_Master_edit').show();
                                            $$('PartMaster_form1_edit').setValues(
                                            {
                                                SuppPartNo_edit:row.data2,
                                                partCustomer_edit:row.data3,
                                                partName_edit:row.data5,
                                                InternalCode_edit:row.data4,
                                                Model_edit:row.data6,
                                                SNP_edit:row.data7,
                                                BoxPerPallet_edit:row.data8,
                                                storeLocation_edit:row.data9,
                                                picklocation_edit:row.data10,
                                                idpart_edit:row.data1                                                           
                                            });                                                                                
                                        },
                                         "fa-trash-o":function(e,t)
                                        {
                                            var r = this.getItem(t),obj;
                                            webix.confirm({
                                                 title: "กรุณายืนยัน",ok:"ใช่", cancel:"ไม่",text:"คุณต้องการ<font color='#ca4635'><br><b>ลบ</b></font><br><font color='#27ae60'><b>ใช่</b></font> หรือ <font color='#3498db'><b>ไม่</b></font>",
                                                 callback:function(res)
                                                 {
                                                    if(res)
                                                    {
                                                        $.post("masterdata/partmaster_delete.php",{obj:{ID:r.data1},type:1})
                                                        .done(function( data ) 
                                                        {
                                                            data = eval('('+data+')');
                                                            if(data.ch==1)
                                                            {
                                                                var dataT1 = $$('partMaster_dataT1');
                                                                dataT1.clearAll();
                                                                dataT1.parse(data.data1,"jsarray");
                                                            }
                                                            else if(data.ch == 2){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                                                            else if(data.ch == 9){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                                                            else if(data.ch == 10){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){window.open("login.php","_self");}});}
                                                        });
                                                    }
                                                 }
                                            });
                                        }
                                    }
                                }
                                ,
                                {
                                    type:"wide",
                                        cols:
                                        [
                                            {
                                                view:"pager", id:"partMaster_pagerA",
                                                template:function(data, common){
                                                    var start = data.page * data.size
                                                    ,end = start + data.size;
                                                    if(data.count == 0) start = 0;
                                                    else start += 1;
                                                    if(end >= data.count) end = data.count;
                                                    var html = "<b>showing "+(start)+" - "+end+" total "+data.count+" </b>";
                                                    return common.first()+common.prev()+" "+html+" "+common.next()+common.last();
                                                },
                                                size:10,
                                                group:5 
                                            }
                                        ]
                                                        
                                }

                            ]
                        }
                          
                    ]
                    
                }
            ],on:
            {
                onHide:function()
                {
                    
                },
                onShow:function()
                {

                },
                onAddView:function()
                {
                    function loadtable()
                    {
                       
                        $.post("masterdata/partmaster_data.php", {type:1})
                        .done(function( data ) 
                        {
                          var dataT1 = $$("partMaster_dataT1");
                          var data = eval('('+data+')');
                          if(data.ch == 1)
                          { 
                            dataT1.clearAll();
                            dataT1.parse(data.data,"jsarray");
                          }
                          else if(data.ch == 2){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                          else if(data.ch == 9){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                          else if(data.ch == 10){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){window.open("login.php","_self");}});}
                        });
                    }                    
                    loadtable();

                    webix.ui(
                    {
                        view:"window",id:"add_part_master",modal:1,move:1,
                        head:{
                                view:"toolbar", margin:-4, cols:[
                                    {view:"label", label: "Add Part Master (เพิ่มเบอร์ชิ้นส่วน)"},
                                    { view:"icon", icon:"times-circle", css:"alter",
                                        click:"$$('add_part_master').hide();"}                                  
                                                    
                                    ]
                            },
                        top:50,position:"center",
                        body:
                        {
                            view:"form", scroll:false,id:"mastePart_form1_addPart",width:900,
                            elements:
                            [
                                {
                                    cols:
                                    [
                                        {view:"text",label:'Part Supplier (เบอร์ชิ้นส่วนลูกค้า)',required:true,name:"SuppPartNo",id:"SuppPartNo",labelPosition:"top", value:""},
                                        {view:"text",label:'Part Customer (เบอร์ชิ้นส่วน)',required:true,name:"partCustomer",id:"partCustomer",labelPosition:"top",value:""},
                                        {view:"text",label:'Part Name (ชื่อชิ้นส่วน)',required:true,name:"partName",id:"partName",labelPosition:"top",value:""}
                                    
                                    ]                                  
                                },
                                {
                                    cols:
                                    [
                                        {view:"text",label:'Internal Code',required:true,name:"InternalCode",id:"InternalCode",labelPosition:"top",value:""},
                                        {view:"text",label:'Model',required:true,name:"Model",id:"Model",labelPosition:"top", value:""},
                                        {view:"text",label:'SNP (จำนวน)',required:true,name:"SNP",id:"SNP",labelPosition:"top",value:""}                                     
                                    ]   
                                },
                                {
                                    cols:
                                    [
                                        {view:"text",label:'Box Per Pallet',required:true,name:"BoxPerPallet",id:"BoxPerPallet",labelPosition:"top",value:""},
                                        {view:"text",label:'Store Location',name:"storeLocation",id:"storeLocation",labelPosition:"top", value:""},
                                        {view:"text",label:'Pick Location',name:"picklocation",id:"picklocation",labelPosition:"top",value:""}
                                                                            
                                    ]   
                                },

                                {
                                    cols:
                                    [
                                         {view:"button", value:"OK (ตกลง)",type:"form",id:"master_ok_Part",on:
                                            {
                                                 onItemClick:function(id)
                                                {
                                                    if ($$('mastePart_form1_addPart').validate()) 
                                                    {
                                                        webix.confirm({
                                                            title: "กรุณายืนยัน",ok:"ใช่", cancel:"ไม่",text:"คุณต้องการ<font color='#ca4635'><br><b>ที่จะบันทึก</b></font><br><font color='#27ae60'><b>ใช่</b></font> หรือ <font color='#3498db'><b>ไม่</b></font>",
                                                            callback:function(res)
                                                            {
                                                                if(res)
                                                                {
                                                                    var btn = $$('master_ok_Part'),
                                                                    obj =  $$('mastePart_form1_addPart').getValues();
                                                                    btn.disable();
                                                                    $.post("masterdata/partmaster_add.php",{obj:obj,type:1})
                                                                     .done(function( data ) 
                                                                    {                                                      
                                                                        btn.enable();
                                                                        var data = eval('('+data+')');
                                                                        if(data.ch == 1)
                                                                        {
                                                                            var dataT1 = $$("partMaster_dataT1");
                                                                            dataT1.clearAll();
                                                                            dataT1.parse(data.data,"jsarray");
                                                                            $$('masterPart_cancel').callEvent("onItemClick", []);
                                                                        }
                                                                        else if (data.ch == 2)
                                                                        {
                                                                            webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        })
                                                    }
                                                }
                                            }
                                        },
                                        {view:"button", value:"Cancel (ยกเลิก)",type:"danger",id:"masterPart_cancel",on:
                                            {
                                                 onItemClick:function(id)
                                                {
                                                    $$('add_part_master').hide();
                                                    $$('mastePart_form1_addPart').clearValidation();
                                                    $$('mastePart_form1_addPart').clear();
                                                }
                                            }
                                        },{},{}
                                    ]
                                }     
                            ]
                        }
                    });
                    
                    webix.ui(
                    {
                        view:"window",id:"Part_Master_edit",modal:1,move:1,
                        head:{
                                view:"toolbar", margin:-4, cols:[
                                    {view:"label", label: "Edit Part Master"},
                                    { view:"icon", icon:"times-circle", css:"alter",
                                        click:"$$('Part_Master_edit').hide();"}                                  
                                                    
                                    ]
                            },
                        top:50,position:"center",
                        body:
                        {
                            view:"form", scroll:false,id:"PartMaster_form1_edit",width:900,
                            elements:
                            [
                                {
                                    cols:
                                    [
                                        {view:"text",label:'Part Supplier (เบอร์ชิ้นส่วนลูกค้า)',required:true,name:"SuppPartNo_edit",id:"SuppPartNo_edit",labelPosition:"top", value:""},
                                        {view:"text",label:'Part Customer (เบอร์ชิ้นส่วน)',required:true,name:"partCustomer_edit",id:"partCustomer_edit",labelPosition:"top",value:""},
                                        {view:"text",label:'Part Name (ชื่อชิ้นส่วน)',required:true,name:"partName_edit",id:"partName_edit",labelPosition:"top",value:""}
                                    
                                    ]                                  
                                },
                                {
                                    cols:
                                    [
                                        {view:"text",label:'Internal Code',required:true,name:"InternalCode_edit",id:"InternalCode_edit",labelPosition:"top",value:""},
                                        {view:"text",label:'Model',required:true,name:"Model_edit",id:"Model_edit",labelPosition:"top", value:""},
                                        {view:"text",label:'SNP (จำนวน)',required:true,name:"SNP_edit",id:"SNP_edit",labelPosition:"top",value:""}                                     
                                    ]   
                                },
                                {
                                    cols:
                                    [
                                        {view:"text",label:'Box Per Pallet',required:true,name:"BoxPerPallet_edit",id:"BoxPerPallet_edit",labelPosition:"top",value:""},
                                        {view:"text",label:'Store Location',name:"storeLocation_edit",id:"storeLocation_edit",labelPosition:"top", value:""},
                                        {view:"text",label:'Pick Location',name:"picklocation_edit",id:"picklocation_edit",labelPosition:"top",value:""},
                                        {view:"text",label:'idpart',name:"idpart_edit",id:"idpart_edit",labelPosition:"top",value:"",hidden:true}                                       
                                    ]   
                                },
                                {
                                    cols:
                                    [
                                         {view:"button", value:"OK (ตกลง)",type:"form",id:"Edit_partmaster",on:
                                            {
                                                 onItemClick:function(id)
                                                {
                                                    if ($$('PartMaster_form1_edit').validate()) 
                                                    {
                                                        webix.confirm({
                                                            title: "กรุณายืนยัน",ok:"ใช่", cancel:"ไม่",text:"คุณต้องการ<font color='#ca4635'><br><b>ที่จะบันทึก</b></font><br><font color='#27ae60'><b>ใช่</b></font> หรือ <font color='#3498db'><b>ไม่</b></font>",
                                                            callback:function(res)
                                                            {
                                                                if(res)
                                                                {
                                                                    var btn = $$('Edit_partmaster'),
                                                                    obj =  $$('PartMaster_form1_edit').getValues();
                                                                    btn.disable();
                                                                    $.post("masterdata/partmaster_update.php",{obj:obj,type:1})
                                                                     .done(function( data ) 
                                                                    {                                                      
                                                                         btn.enable();
                                                                          var data = eval('('+data+')');
                                                                          if(data.ch == 1)
                                                                          {
                                                                              var dataT1 = $$("partMaster_dataT1");
                                                                              dataT1.clearAll();
                                                                              dataT1.parse(data.data,"jsarray");
                                                                              $$('partmaster_edit_cancel').callEvent("onItemClick", []);
                                                                          }
                                                                    });
                                                                }
                                                            }
                                                        })
                                                    }
                                                }
                                            }
                                        },
                                        {view:"button", value:"Cancel (ยกเลิก)",type:"danger",id:"partmaster_edit_cancel",on:
                                            {
                                                 onItemClick:function(id)
                                                {
                                                    $$('Part_Master_edit').hide();
                                                    $$('PartMaster_form1_edit').clearValidation();
                                                    $$('PartMaster_form1_edit').clear();
                                                }
                                            }
                                        },{},{}
                                    ]
                                }     
                            ]
                        }
                    });

                }
            }
        }
    };
};