var header_locationMaster = function()
{
	return {
        view: "scrollview",
        scroll: "native-y",
        id:"header_locationMaster",
        body: 
        {
        	id:"locationMaster_id",
        	type:"clean",
    		rows:
    		[
    		    {
                    view:"form",
                    paddingY:20,
                    id:"locationMaster_form1",
                    elements:
                    [
                        {                            
                            cols:                                    
                                [                                   
                                    {
                                        view:"button", label:"Add Location", type:"form",width:230,
                                        on:
                                            {
                                                onItemClick:function(id, e)
                                                {
                                                    $$('add_location_master').show();  
                                                }
                                            }                                      
                                    },
                                    { },
                                    { }            
                                ]
                        },
                        {
                            paddingX:2,
                            rows:
                            [
                                {  
                                    view:"datatable",
                                    id:"locationMaster_dataT1",
                                    navigation:true,
                                    datatype:"jsarray",
                                    css:"my_style",
                                    pager:"locationMaster_pagerA",
                                    autoheight:true,
                                    resizeColumn:true,
                                    leftSplit:0,
                                    columns:
                                    [                                  
                                        { id:"data21",header:"&nbsp;",width:35,template: "<span style='cursor:pointer;' class='webix_icon fa-pencil'></span>"},
                                        { id:"data22",header:"&nbsp;",width:35,template: "<span style='cursor:pointer;' class='webix_icon fa-trash-o'></span>"},
                                        { id:"data0",header:"No",css:"rank",width:50},
                                        { id:"data1",header:"Location name",css:"rank",width:200},
                                        { id:"data2",header:"Location Zone",css:"rank",width:200},
                                        { id:"data3",header:"Location Type",css:"rank",width:200},
                                        { id:"data4",header:"Create Date",css:"rank",width:200},
                                        { id:"data5",header:"Create By",css:"rank",width:200},
                                                                        
                                    ],
                                    onClick:
                                    {
                                        "fa-pencil":function(e,t)
                                        {
                                            var row = this.getItem(t),obj;
                                            $$('Location_Master_edit').show();
                                            $$('LocationMaster_form1_edit').setValues(
                                            {
                                                LocationName_edit:row.data1,
                                                LocationZone_edit:row.data2,
                                                LocationType_edit:row.data3,
                                                                                                    
                                            });                                                                                
                                        },
                                         "fa-trash-o":function(e,t)
                                        {
                                            var r = this.getItem(t),obj;
                                            webix.confirm({
                                                 title: "กรุณายืนยัน",ok:"ใช่", cancel:"ไม่",text:"คุณต้องการ<font color='#ca4635'><br><b>ลบ</b></font><br><font color='#27ae60'><b>ใช่</b></font> หรือ <font color='#3498db'><b>ไม่</b></font>",
                                                 callback:function(res)
                                                 {
                                                    if(res)
                                                    {
                                                        $.post("masterdata/location_delete.php",{obj:{LocationName:r.data1},type:1})
                                                        .done(function( data ) 
                                                        {
                                                            data = eval('('+data+')');
                                                            if(data.ch==1)
                                                            {
                                                                var dataT1 = $$('locationMaster_dataT1');
                                                                dataT1.clearAll();
                                                                dataT1.parse(data.data1,"jsarray");
                                                            }
                                                            else if(data.ch == 2){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                                                            else if(data.ch == 9){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                                                            else if(data.ch == 10){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){window.open("login.php","_self");}});}
                                                        });
                                                    }
                                                 }
                                            });
                                        }
                                    }
                                }
                                ,
                                {
                                    type:"wide",
                                        cols:
                                        [
                                            {
                                                view:"pager", id:"locationMaster_pagerA",
                                                template:function(data, common){
                                                    var start = data.page * data.size
                                                    ,end = start + data.size;
                                                    if(data.count == 0) start = 0;
                                                    else start += 1;
                                                    if(end >= data.count) end = data.count;
                                                    var html = "<b>showing "+(start)+" - "+end+" total "+data.count+" </b>";
                                                    return common.first()+common.prev()+" "+html+" "+common.next()+common.last();
                                                },
                                                size:10,
                                                group:5 
                                            }
                                        ]
                                                        
                                }

                            ]
                        }
                          
                    ]
                    
                }
            ],on:
            {
                onHide:function()
                {
                    
                },
                onShow:function()
                {

                },
                onAddView:function()
                {
                    function loadtable()
                    {
                       
                        $.post("masterdata/location_data.php", {type:1})
                        .done(function( data ) 
                        {
                          var dataT1 = $$("locationMaster_dataT1");
                          var data = eval('('+data+')');
                          if(data.ch == 1)
                          { 
                            dataT1.clearAll();
                            dataT1.parse(data.data,"jsarray");
                          }
                          else if(data.ch == 2){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                          else if(data.ch == 9){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                          else if(data.ch == 10){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){window.open("login.php","_self");}});}
                        });
                    }                   
                    loadtable();

                    webix.ui(
                    {
                        view:"window",id:"add_location_master",modal:1,move:1,
                        head:{
                                view:"toolbar", margin:-4, cols:[
                                    {view:"label", label: "Add Location Master"},
                                    { view:"icon", icon:"times-circle", css:"alter",
                                        click:"$$('add_location_master').hide();"}                                  
                                                    
                                    ]
                            },
                        top:50,position:"center",
                        body:
                        {
                            view:"form", scroll:false,id:"master_form1_addLocation",width:500,
                            elements:
                            [
                                {
                                    cols:
                                    [
                                        {view:"text",label:'Location name',required:true,name:"LocationName",id:"LocationName",labelPosition:"top", value:"",width:400},
                                    ]  
                                },                                    
                                {
                                    cols:
                                    [
                                        {view:"text",label:'Location Zone',required:true,name:"LocationZone",id:"LocationZone",labelPosition:"top", value:"",width:400},
                                    ]  
                                },
                                {
                                    cols:
                                    [
                                       {view:"text",label:'Location Type',required:true,name:"LocationType",id:"LocationType",labelPosition:"top", value:"",width:400},
                                    ]   
                                },                                                               
                                {
                                    cols:
                                    [
                                         {view:"button", value:"OK (ตกลง)",type:"form",id:"master_ok_location",on:
                                            {
                                                 onItemClick:function(id)
                                                {
                                                    if ($$('master_form1_addLocation').validate()) 
                                                    {
                                                        webix.confirm({
                                                            title: "กรุณายืนยัน",ok:"ใช่", cancel:"ไม่",text:"คุณต้องการ<font color='#ca4635'><br><b>ที่จะบันทึก</b></font><br><font color='#27ae60'><b>ใช่</b></font> หรือ <font color='#3498db'><b>ไม่</b></font>",
                                                            callback:function(res)
                                                            {
                                                                if(res)
                                                                {
                                                                    var btn = $$('master_ok_location'),
                                                                    obj =  $$('master_form1_addLocation').getValues();
                                                                    btn.disable();
                                                                    $.post("masterdata/location_add.php",{obj:obj,type:1})
                                                                     .done(function( data ) 
                                                                    {                                                      
                                                                         btn.enable();
                                                                          var data = eval('('+data+')');
                                                                          if(data.ch == 1)
                                                                          {
                                                                              var dataT1 = $$("locationMaster_dataT1");
                                                                              dataT1.clearAll();
                                                                              dataT1.parse(data.data,"jsarray");
                                                                              $$('masterLocation_cancel').callEvent("onItemClick", []);
                                                                          }
                                                                          else if (data.ch == 2)
                                                                          {
                                                                            webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});
                                                                          }
                                                                    });
                                                                }
                                                            }
                                                        })
                                                    }
                                                }
                                            }
                                        },
                                        {view:"button", value:"Cancel (ยกเลิก)",type:"danger",id:"masterLocation_cancel",on:
                                            {
                                                 onItemClick:function(id)
                                                {
                                                    $$('add_location_master').hide();
                                                    $$('master_form1_addLocation').clearValidation();
                                                    $$('master_form1_addLocation').clear();
                                                }
                                            }
                                        },{},{}
                                    ]
                                }     
                            ]
                        }
                    });

                    webix.ui(
                    {
                        view:"window",id:"Location_Master_edit",modal:1,move:1,
                        head:{
                                view:"toolbar", margin:-4, cols:[
                                    {view:"label", label: "Edit Location Master"},
                                    { view:"icon", icon:"times-circle", css:"alter",
                                        click:"$$('Location_Master_edit').hide();"}                                  
                                                    
                                    ]
                            },
                        top:50,position:"center",
                        body:
                        {
                            view:"form", scroll:false,id:"LocationMaster_form1_edit",width:500,
                            elements:
                            [
                                {
                                    cols:
                                    [
                                        {view:"text",label:'Location name',required:true, name:"LocationName_edit",id:"LocationName_edit",labelPosition:"top", value:"",width:400,disabled:1},
                                    ]                                  
                                },
                                {
                                    cols:
                                    [
                                        {view:"text",label:'Location zone',required:true,name:"LocationZone_edit",id:"LocationZone_edit",labelPosition:"top", value:"",width:400},                                     
                                    ]   
                                },
                                {
                                    cols:
                                    [                                    
                                        {view:"text",label:'Location type',required:true,name:"LocationType_edit",id:"LocationType_edit",labelPosition:"top",value:"",width:400}
                                    ]   
                                },
                                {
                                    cols:
                                    [
                                         {view:"button", value:"OK (ตกลง)",type:"form",id:"Edit_Location",on:
                                            {
                                                 onItemClick:function(id)
                                                {
                                                    if ($$('LocationMaster_form1_edit').validate()) 
                                                    {
                                                        webix.confirm({
                                                            title: "กรุณายืนยัน",ok:"ใช่", cancel:"ไม่",text:"คุณต้องการ<font color='#ca4635'><br><b>ที่จะบันทึก</b></font><br><font color='#27ae60'><b>ใช่</b></font> หรือ <font color='#3498db'><b>ไม่</b></font>",
                                                            callback:function(res)
                                                            {
                                                                if(res)
                                                                {
                                                                    var btn = $$('Edit_Location'),
                                                                    obj =  $$('LocationMaster_form1_edit').getValues();
                                                                    btn.disable();
                                                                    $.post("masterdata/location_update.php",{obj:obj,type:1})
                                                                     .done(function( data ) 
                                                                    {                                                      
                                                                         btn.enable();
                                                                          var data = eval('('+data+')');
                                                                          if(data.ch == 1)
                                                                          {
                                                                              var dataT1 = $$("locationMaster_dataT1");
                                                                              dataT1.clearAll();
                                                                              dataT1.parse(data.data,"jsarray");
                                                                              $$('masterLocation_edit_cancel').callEvent("onItemClick", []);
                                                                          }
                                                                    });
                                                                }
                                                            }
                                                        })
                                                    }
                                                }
                                            }
                                        },
                                        {view:"button", value:"Cancel (ยกเลิก)",type:"danger",id:"masterLocation_edit_cancel",on:
                                            {
                                                 onItemClick:function(id)
                                                {
                                                    $$('Location_Master_edit').hide();
                                                    $$('LocationMaster_form1_edit').clearValidation();
                                                    $$('LocationMaster_form1_edit').clear();
                                                }
                                            }
                                        },{},{}
                                    ]
                                }     
                            ]
                        }
                    });

                }
            }
        }
    };
};