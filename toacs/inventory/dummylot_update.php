<?php
	if(!ob_start("ob_gzhandler")) ob_start();
	header('Expires: Sun, 01 Jan 2014 00:00:00 GMT');
	header('Cache-Control: no-store, no-cache, must-revalidate');
	header('Cache-Control: post-check=0, pre-check=0', FALSE);
	header('Pragma: no-cache');
	include('../start.php');
	session_start();
	include('../php/connection.php');
	$cBy = $_SESSION['xxxID'];
	$fName = $_SESSION['xxxFName'];
	$obj  = $_POST['obj'];
	$type  = intval($_POST['type']);

	if($type == 1)
	{

		
		$lot_dummy = $mysqli->real_escape_string(trim(strtoupper($obj['dummylot_lot'])));

		$lot_array_2  = $lot_dummy;
		$lot_cut_2 = explode("|", $lot_array_2);
		if(sizeof($lot_cut_2)!=3){echo '{ch:2,data:"ข้อมูล Lot Dummy ไม่ถูกต้อง"}';exit();}
		if ($lot_cut_2[0]!="L") {echo '{ch:2,data:"ข้อมูล Lot Dummy ไม่ถูกต้อง"}';exit();}
		$lot_dum = $lot_cut_2[1];
		$box_dum = $lot_cut_2[2];

		echo '{ch:1,data:""}';

		
				
	}
	else if ($type == 2) 
	{
		$lot_dummy = $mysqli->real_escape_string(trim(strtoupper($obj['dummylot_lot'])));
		$qty = $mysqli->real_escape_string(trim(strtoupper($obj['dummylot_qty'])));
		$lot_no = $mysqli->real_escape_string(trim(strtoupper($obj['dummylot_no'])));

		$lot_array_2  = $lot_dummy;
		$lot_cut_2 = explode("|", $lot_array_2);
		if(sizeof($lot_cut_2)!=3){echo '{ch:2,data:"ข้อมูล Lot Dummy ไม่ถูกต้อง"}';exit();}
		if ($lot_cut_2[0]!="L") {echo '{ch:2,data:"ข้อมูล Lot Dummy ไม่ถูกต้อง"}';exit();}
		$lot_dum = $lot_cut_2[1];
		$box_dum = $lot_cut_2[2];
		
		$lot_array  = $lot_no;
		$lot_cut = explode("|", $lot_array);
		if(sizeof($lot_cut)!=3){echo '{ch:2,data:"ข้อมูล Lot ไม่ถูกต้อง"}';exit();}
		if ($lot_cut[0]!="L") {echo '{ch:2,data:"ข้อมูล Lot ไม่ถูกต้อง"}';exit();}
		$lot = $lot_cut[1];
		$box = $lot_cut[2];
		
		$mysqli->autocommit(FALSE);
		try 
		{

			if(!$re1 = $mysqli->query("SELECT part_id,lot_box,pick_loc,Doc_no,DCD_no,area FROM tbl_inventory where lot ='$lot' and box = '$box' limit 1;"))
			{echo '{ch:2,data:"Error Code 1"}';$mysqli->close();}
			if($re1->num_rows == 0){echo '{ch:2,data:"เลข '.$lot.' ไม่พบข้อมูลในระบบ"}';$mysqli->close();exit();}
			$row = $re1->fetch_object();

			if(!$re1 = $mysqli->query("SELECT part_id,qty,lot_box,pick_loc,Doc_no,DCD_no,area FROM tbl_inventory where area ='OVERFLOW' and lot ='$lot' and box = '$box' limit 1;"))
			{echo '{ch:2,data:"Error Code 1"}';$mysqli->close();}
			if($re1->num_rows == 0){echo '{ch:2,data:"เลข '.$lot.' นี้ สถานะอยูที่ PICK"}';$mysqli->close();exit();}
			$row = $re1->fetch_object();
			$part_id = $row->part_id;
			$qty_1 = $row->qty;			
			$pick_loc = $row->pick_loc;
			$Doc_no = $row->Doc_no;
			$DCD_no = $row->DCD_no;
			$area = $row->area;

			if ($qty > $qty_1) 
			{
				echo '{ch:2,data:"จำนวนไม่ถูกต้อง"}';
				$mysqli->close();
				exit();

			}
			else if ($qty < $qty_1) 
			{
				$sql = "UPDATE tbl_inventory SET qty = $qty_1 - $qty where area ='OVERFLOW' and lot ='$lot' and box = '$box' limit 1;";						
				if(!$mysqli->query($sql)) throw new Exception('Error Code 2');


				if(!$mysqli->query("INSERT INTO tbl_inventory ( Part_ID,Lot,Box,Lot_Box,reflot,refBox,Qty,area,put_loc,pick_loc,Doc_no,
				DCD_No,Create_date, user_id,Lot_status) values ('$part_id','$lot_dum','$box_dum',concat('$lot_dum','$box_dum'),
				'$lot','$box','$qty','$area','','$pick_loc','$Doc_no','$DCD_no',NOW(),'$cBy','DUMMYLOT')")) 
				throw new Exception('ไม่ทึกไม่สำเร็จ อาจเกิด Lot ซ้ำ');
				$tranInsertRow = $mysqli->affected_rows;
				if($tranInsertRow == 0) throw new Exception('Error Code 4');

				if(!$mysqli->query("INSERT INTO tbl_transaction (Part_ID,LOT,Box_No,reflot,refbox,Qty,Rec_Date,DCD_No,Doc_No,
				Tran_Status,Tran_Type,area,tarea,loc,toloc,create_date,user_id) 
				values ('$part_id','$lot_dum','$box_dum','$lot','$box','$qty',NOW(),'$DCD_no',
				(SELECT Doc_no FROM tbl_receive_header WHERE ID = '$Doc_no'),'YES','IN','STORAGE',
				'OVERFLOW','NOLOC','$pick_loc',NOW(),'$cBy')"))
				{throw new Exception('Error Code 5');}
				if($mysqli->affected_rows != $tranInsertRow) throw new Exception('Error Code 6');

				echo '{ch:1,data:"บันทึกสำเร็จ"}';
				$mysqli->commit();
			}
			else if ($qty = $qty_1)
			{

				if(!$mysqli->query("DELETE FROM tbl_inventory where area ='OVERFLOW' and lot ='$lot' and box = '$box' limit 1;"))
				{
					throw new Exception('Error Code 8');
				}


				if(!$mysqli->query("INSERT INTO tbl_inventory ( Part_ID,Lot,Box,Lot_Box,reflot,refBox,Qty,area,put_loc,pick_loc,Doc_no,
				DCD_No,Create_date, user_id,Lot_status) values ('$part_id','$lot_dum','$box_dum',concat('$lot_dum','$box_dum'),
				'$lot','$box','$qty','$area','','$pick_loc','$Doc_no','$DCD_no',NOW(),'$cBy','DUMMYLOT')")) 
				throw new Exception('ไม่ทึกไม่สำเร็จ อาจเกิด Lot ซ้ำ');
				$tranInsertRow = $mysqli->affected_rows;
				if($tranInsertRow == 0) throw new Exception('Error Code 4');

				if(!$mysqli->query("INSERT INTO tbl_transaction (Part_ID,LOT,Box_No,reflot,refbox,Qty,Rec_Date,DCD_No,Doc_No,
				Tran_Status,Tran_Type,area,tarea,loc,toloc,create_date,user_id) 
				values ('$part_id','$lot_dum','$box_dum','$lot','$box','$qty',NOW(),'$DCD_no',
				(SELECT Doc_no FROM tbl_receive_header WHERE ID = '$Doc_no'),'YES','IN','STORAGE',
				'OVERFLOW','NOLOC','$pick_loc',NOW(),'$cBy')"))
				{throw new Exception('Error Code 5');}
				if($mysqli->affected_rows != $tranInsertRow) throw new Exception('Error Code 6');

				echo '{ch:1,data:"บันทึกสำเร็จ"}';
				$mysqli->commit();
			}

		} 
		catch (Exception $e) 
		{
			$mysqli->rollback();
		  	echo '{ch:2,data:"'.$e->getMessage().'"}';	
		}		
	}
	
	
	$mysqli->close();
	exit();
?>