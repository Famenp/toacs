<?php
	if(!ob_start("ob_gzhandler")) ob_start();
	header('Expires: Sun, 01 Jan 2014 00:00:00 GMT');
	header('Cache-Control: no-store, no-cache, must-revalidate');
	header('Cache-Control: post-check=0, pre-check=0', FALSE);
	header('Pragma: no-cache');
	include('../start.php');
	session_start();
	include('../php/connection.php');
	$cBy = $_SESSION['xxxID'];
	$fName = $_SESSION['xxxFName'];
	$obj  = $_POST['obj'];
	$type  = intval($_POST['type']);

	if($type == 1)
	{

		$location = $mysqli->real_escape_string(trim(strtoupper($obj['move_loc_name'])));
		$lot = $mysqli->real_escape_string(trim(strtoupper($obj['move_lot_no'])));
		
			$mysqli->autocommit(FALSE);
			try 
			{

				$lot_array  = $lot;
				$lot_cut = explode("|", $lot_array);
				if(sizeof($lot_cut)!=3){echo '{ch:2,data:"ข้อมูลไม่ถูกต้อง"}';	exit();}
				if ($lot_cut[0]!="L") {echo '{ch:2,data:"ข้อมูลไม่ถูกต้อง"}';exit();}
				$lot_no = $lot_cut[1];
				$box = $lot_cut[2];

				if(!$re2 = $mysqli->query("SELECT part_id,qty,lot_box,pick_loc,Doc_no,DCD_no,area FROM tbl_inventory 
					where lot = '$lot_no' and box = '$box' limit 1;"))
				{echo '{ch:2,data:"Error Code 1"}';$mysqli->close();}
				if($re2->num_rows == 0){echo '{ch:2,data:"ไม่พบเลข LOT ในระบบ"}';$mysqli->close();exit();}
				$row = $re2->fetch_object();
				$part_id = $row->part_id;
				$qty_1 = $row->qty;			
				$pick_loc = $row->pick_loc;
				$Doc_no = $row->Doc_no;
				$DCD_no = $row->DCD_no;
				$area = $row->area;

				
				$sql = "UPDATE tbl_inventory SET area ='$location' WHERE lot ='$lot_no' and box = '$box';";						
				if(!$mysqli->query($sql)) throw new Exception('Error Code 2');

				if(!$mysqli->query("INSERT INTO tbl_transaction (Part_ID,LOT,Box_No,Qty,Rec_Date,DCD_No,
					Doc_No,Tran_Status,Tran_Type,area,tarea,loc,toloc,create_date,user_id) 
					values ('$part_id','$lot_no','$box','$qty_1',NOW(),'$DCD_no',
					(SELECT Doc_no FROM tbl_receive_header WHERE ID = '$Doc_no'),'YES','MOVE','$area',
					'$location','NOLOC','$pick_loc',NOW(),'$cBy')"))
					{throw new Exception('Error Code 5');}

				$mysqli->commit();

				echo '{ch:1,data:"บักทึกสำเร็จ"}';
				
			} 
			catch (Exception $e) 
			{
				$mysqli->rollback();
		  		echo '{ch:2,data:"'.$e->getMessage().'"}';	
			}		
	}
	
	$mysqli->close();
	exit();
?>