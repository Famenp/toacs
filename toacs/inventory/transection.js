var header_transection = function()
{
	return {
        view: "scrollview",
        scroll: "native-y",
        id:"header_transection",
        body: 
        {
        	id:"transection_id",
        	type:"clean",
    		rows:
    		[
    		   {
                    view:"form",
                    paddingY:20,
                    id:"transection_form1",
                    elements:
                    [
                        {                            
                            cols:                                    
                                [    
                                    {  
                                        view:"datepicker", label:"Start date",value:new Date(), name:"start" ,labelPosition:"top", stringResult:true,format:webix.Date.dateToStr("%Y-%m-%d"),width:250
                                    },
                                    {
                                        view:"datepicker", label:"End date",value:new Date(), name:"end",labelPosition:"top", stringResult:true,format:webix.Date.dateToStr("%Y-%m-%d"),width:250
                                    },
                                    /*{  
                                        view:"text",label:'Part Supplier',name:"part_sup",id:"part_sup",labelPosition:"top", value:"",width:230
                                    },
                                    {  
                                        view:"text",label:'Part Supplier',name:"part_sup",id:"part_sup",labelPosition:"top", value:"",width:230
                                    },
                                    {  
                                        view:"text",label:'Part Supplier',name:"part_sup",id:"part_sup",labelPosition:"top", value:"",width:230
                                    },           */                       
                                    {
                                         
                                    }                               
                                ]
                        },
                        {
                            cols:
                            [
                                {                                             
                                    view:"button", label:"Find (ค้นหา)", type:"form",width:230,id:"tran_find",
                                    on:
                                    {                                                    
                                        onItemClick:function(id, e)
                                        {
                                            if ($$('transection_form1').validate())
                                            {
                                                var btn = $$('tran_find'),
                                                obj =  $$('transection_form1').getValues();
                                                btn.disable();                                                     
                                                $.post("inventory/transection_data.php",{obj:obj,type:1})
                                                .done(function( data )
                                                {
                                                    btn.enable();
                                                    data = eval('('+data+')');
                                                    if(data.ch == 1)
                                                    {
                                                        var dataT1 = $$('transection_dataT1');
                                                        dataT1.clearAll();
                                                        dataT1.parse(data.data,"jsarray");
                                                        dataT1.getPager().render();
                                                    }
                                                    else if (data.ch == 2)
                                                    {
                                                        webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});
                                                    }                                                                    
                                                });
                                            }
                                            else{webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:"กรุณากรอกข้อมูล",callback:function(){}});}
                                        }                                                    
                                    }                                                   
                                },
                                {
                                    view:"button", label:"EDI Export", type:"form",width:230,id:"tran_edi",on:
                                    {
                                        onItemClick:function(id, e)
                                        {
                                            $$('EDI_window').show();
                                            /*if ($$('transection_form1').validate())
                                            {
                                                var btn = $$('tran_find'),
                                                obj =  $$('transection_form1').getValues();
                                                btn.disable();                                                     
                                                $.post("inventory/transection_data.php",{obj:obj,type:1})
                                                .done(function( data )
                                                {
                                                    btn.enable();
                                                    data = eval('('+data+')');
                                                    if(data.ch == 1)
                                                    {
                                                        var dataT1 = $$('transection_dataT1');
                                                        dataT1.clearAll();
                                                        dataT1.parse(data.data,"jsarray");
                                                        dataT1.getPager().render();
                                                        var blob = new Blob([data.data], {type: "text/plain;charset=utf-8"});
                                                        saveAs(blob, "testfile1.txt");
                                                    }
                                                    else if (data.ch == 2)
                                                    {
                                                        webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});
                                                    }                                                                    
                                                });
                                            }
                                            else{webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:"กรุณากรอกข้อมูล",callback:function(){}});}*/
                                        }
                                    }
                                },
                                {
                                    view:"button", value:"Export Excel", type:"form",width:230,id:"transec_exl",
                                    on:
                                    {
                   
                                        onItemClick:function(id, e)
                                        {
                                            webix.confirm(
                                                    {
                                                        title:"<b>ข้อความจากระบบ</b>",ok:'ใช่',cancel:"ไม่",text:"คุณต้องการ<font color='#ca4635'><br><b>export ข้อมูล</b></font><br><font color='#27ae60'><b>ใช่</b></font> หรือ <font color='#3498db'><b>ไม่</b></font>",callback:function(result)
                                                        {
                                                        if(result)
                                                            {

                                                                var dataT1 = $$("transection_dataT1"),data = [],btn=$$(this);
                                                                if(dataT1.count()>0)
                                                                {
                                                                    
                                                                    var f = Object.keys(dataT1.getItem(dataT1.getFirstId())),fName=[];
                                                                    f = ['data0','data1','data2','data3','data4','data5','data6','data7','data8','data9','data10','data11','data12','data13','data14','data15'];
                                                                 
                                                                    data[0] = ['No','DOC No.','Type','Part No.','LOT','Box No.','Qty','DCD No.','From Area','To Area','From Loc','To Loc','Qty Before','Qty After','Create Date','Create By'];

                                                                    dataT1.eachRow( function (row)
                                                                    {
                                                                        var r = dataT1.getItem(row),rr=[];
                                                                        for(var i=-1,len=f.length;++i<len;)
                                                                        {
                                                                            rr[rr.length] = r[f[i]];
                                                                        }
                                                                        data[data.length] = rr;
                                                                    });
                                                                    var worker = new Worker('js/workerToExcel.js?v=1');
                                                                    worker.addEventListener('message', function(e) 
                                                                    {
                                                                        saveAs(e.data, 'TRANSECTION'+new Date()+".xlsx");
                                                                        
                                                                        webix.message({expire:7000, text:"Export สำเร็จ" });
                                                                    }, false);  
                                                                        worker.postMessage({'cmd': 'start', 'msg':data});  
                                                                }
                                                            }
                                                        }
                                                    });        
                                        }
                                    }
                                },{}
                            ]

                        },
                        {
                            paddingX:2,
                            rows:
                            [
                                {  
                                    view:"datatable",
                                    id:"transection_dataT1",
                                    navigation:true,
                                    datatype:"jsarray",
                                    css:"my_style",
                                    pager:"transection_pagerA",
                                    autoheight:true,
                                    resizeColumn:true,
                                    leftSplit:0,
                                    footer:true,
                                    columns:
                                    [                                  
                                        { id:"data0",header:"No",css:"rank",width:50},
                                        { id:"data1",header:[{text :"DOC No.",css:{"text-align":"center"}},{content:"textFilter"}],css:"rank",width:190},              
                                        { id:"data2",header:[{text :"Type",css:{"text-align":"center"}},{content:"textFilter"}],css:"rank",width:80},
                                        { id:"data3",header:[{text :"Part No.",css:{"text-align":"center"}},{content:"textFilter"}],css:"rank",width:200},
                                        { id:"data4",header:[{text :"LOT",css:{"text-align":"center"}},{content:"textFilter"}],css:"rank",width:300},
                                        { id:"data5",header:[{text :"Box No.",css:{"text-align":"center"}},{content:"textFilter"}],css:"rank",width:120,footer:{text:"<b>Total:</b>",height:25,css:{"text-align":"right"}}},
                                        { id:"data6",header:[{text :"Qty",css:{"text-align":"center"}},{content:"textFilter"}],css:"rank",width:80,footer:{content:"summColumn",colspan:1}},
                                        { id:"data7",header:[{text :"DCD No.",css:{"text-align":"center"}},{content:"textFilter"}],css:"rank",width:140},
                                        { id:"data8",header:[{text :"From Area",css:{"text-align":"center"}},{content:"textFilter"}],css:"rank",width:120},
                                        { id:"data9",header:[{text :"To Area",css:{"text-align":"center"}},{content:"textFilter"}],css:"rank",width:100},
                                        { id:"data10",header:"From Loc",css:"rank",width:100},
                                        { id:"data11",header:"To Loc",css:"rank",width:100}, 
                                        { id:"data12",header:"Qty Before",css:"rank",width:100}, 
                                        { id:"data13",header:"Qty After",css:"rank",width:100}, 
                                        { id:"data14",header:"Create Date",css:"rank",width:150}, 
                                        { id:"data15",header:"Create By",css:"rank",width:150}, 

                                    ],
                                }
                                ,
                                {
                                    type:"wide",
                                        cols:
                                        [
                                            {
                                                view:"pager", id:"transection_pagerA",
                                                template:function(data, common){
                                                    var start = data.page * data.size
                                                    ,end = start + data.size;
                                                    if(data.count == 0) start = 0;
                                                    else start += 1;
                                                    if(end >= data.count) end = data.count;
                                                    var html = "<b>showing "+(start)+" - "+end+" total "+data.count+" </b>";
                                                    return common.first()+common.prev()+" "+html+" "+common.next()+common.last();
                                                },
                                                size:10,
                                                group:5 
                                            }
                                        ]
                                                        
                                }

                            ]
                        }
                    ]
                    
                }
            ],on:
            {
                onHide:function()
                {
                    
                },
                onShow:function()
                {

                },
                onAddView:function()
                {
                    $.post("inventory/transection_data.php",{type:2})
                    .done(function( data )
                    {
                        data = eval('('+data+')');
                        if(data.ch == 1)
                        {
                            var dataT1 = $$('transection_dataT1');
                            dataT1.clearAll();
                            dataT1.parse(data.data,"jsarray");
                            dataT1.getPager().render();
                        }
                        else if (data.ch == 2)
                        {
                            webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});
                        }                                                                    
                    });

                     webix.ui(
                     {
                        view:"window",id:"EDI_window",modal:1,move:1,
                        head:{
                                view:"toolbar", margin:-4,
                                cols:[
                                        {view:"label", label: "EDI Export"},
                                        {view:"icon", icon:"times-circle", css:"alter",click:"$$('EDI_window').hide();"}           
                                    ]
                            },
                        top:50,position:"center",
                        body:
                        {
                            view:"form", scroll:false,id:"EDI_window_form1",width:400,
                            elements:
                            [
                                {
                                    cols:
                                    [
                                        {view:"datepicker", label:"Select Date",value:new Date(), name:"dateedi" ,labelPosition:"top", stringResult:true,format:webix.Date.dateToStr("%Y-%m-%d"),width:250}
                                    ]                                  
                                },
                                {
                                    cols:
                                    [
                                        {
                                            view:"button", label:"Export", type:"form",width:230,id:"ex_edi",on:
                                            {
                                                onItemClick:function(id, e)
                                                {
                                                    if ($$('EDI_window_form1').validate())
                                                    {
                                                        var btn = $$('ex_edi'),
                                                        obj =  $$('EDI_window_form1').getValues();
                                                        btn.disable();

                                                        $.post("inventory/transection_exportedi.php",{obj:obj,type:1})
                                                        .done(function( data )
                                                        {
                                                            btn.enable();
                                                            data = eval('('+data+')');
                                                            if(data.ch == 1)
                                                            {
                                                                console.log(data.data);
                                                                var text = data.data.replace(/<nl>/gi, "\r\n");
                                                                console.log(text);
                                                                var blob = new Blob([text], {type: "text/plain;charset=utf-8"});
                                                                saveAs(blob, "EDI"+new Date().getTime()+".txt");
                                                            }
                                                            else if (data.ch == 2)
                                                            {
                                                                webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});
                                                            }                                                                    
                                                        });
                                                    }
                                                    else{webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:"กรุณากรอกข้อมูล",callback:function(){}});}
                                                }
                                            }
                                        }
                                    ]
                                }
                            ]
                        }
                     });
                }
            }
        }
    };
};