<?php
if(!ob_start("ob_gzhandler")) ob_start();
header('Expires: Sun, 01 Jan 2014 00:00:00 GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Pragma: no-cache');
include('../start.php');
session_start();
include('../php/connection.php');
$type  = intval($_REQUEST['type']);
$cBy = $_SESSION['xxxID'];

if($type == 1)
{
  $obj  = $_POST['obj'];
  $dstart = $mysqli->real_escape_string(trim(strtoupper($obj['start'])));
  $dend = $mysqli->real_escape_string(trim(strtoupper($obj['end'])));
  $dend_cut = explode(" ", $dend);
  $dend = $dend_cut[0]." 23:59:59";

  if($re1 = $mysqli->query("SELECT tran.Doc_No,tran.Tran_Type, pm.part_supplier ,tran.LOT,tran.Box_No,tran.Qty,tran.DCD_No,tran.area,tran.tarea,tran.Loc,tran.toLoc,tran.Qty_before,tran.Qty_after,tran.create_date,us.user_fName  
    FROM tbl_transaction tran
    LEFT JOIN tbl_partmaster pm ON tran.Part_ID = pm.part_id
    LEFT JOIN tbl_user us ON tran.user_id = us.user_id 
    WHERE tran.create_date BETWEEN '$dstart' AND '$dend' AND tran.Tran_Status = 'YES'  ORDER BY tran.create_date DESC limit 10000"))
    {
      if($re1->num_rows >0)
      {
        echo '{"ch":1,"data":';
        toArrayStringAddNumberRow($re1,1);
        echo '}';
      }
      else echo '{ch:2,data:"ไม่พบข้อมูลในระบบ"}';
    }
    else echo '{ch:2,data:"โคดผิด"}'; 
}
else if ($type == 2) 
{
   if($re1 = $mysqli->query("SELECT tran.Doc_No,tran.Tran_Type, pm.part_supplier ,tran.LOT,tran.Box_No,tran.Qty,tran.DCD_No,tran.area,tran.tarea,tran.Loc,tran.toLoc,tran.Qty_before,tran.Qty_after,tran.create_date,us.user_fName  
    FROM tbl_transaction tran
    LEFT JOIN tbl_partmaster pm ON tran.Part_ID = pm.part_id
    LEFT JOIN tbl_user us ON tran.user_id = us.user_id WHERE  tran.Tran_Status = 'YES'
    ORDER BY tran.create_date DESC limit 10000"))
    {
      if($re1->num_rows >0)
      {
        echo '{"ch":1,"data":';
        toArrayStringAddNumberRow($re1,1);
        echo '}';
      }
      else echo '{ch:2,data:"ไม่พบข้อมูลในระบบ"}';
    }
    else echo '{ch:2,data:"โคดผิด"}';
}


function getDatesFromRange($startDate, $endDate)
{
    $return = array($startDate);
    $start = $startDate;
    $i=1;
    if (strtotime($startDate) < strtotime($endDate))
    {
       while (strtotime($start) < strtotime($endDate))
        {
            $start = date('Y-m-d', strtotime($startDate.'+'.$i.' days'));
            $return[] = $start;
            $i++;
        }
    }

    return $return;
}


$mysqli->close();
exit(); 
?>