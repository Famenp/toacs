var header_dummylot = function()
{
	return {
        view: "scrollview",
        scroll: "native-y",
        id:"header_dummylot",
        body: 
        {
        	id:"dummylot_id",
        	type:"clean",
    		rows:
    		[
    		    {
                    view:"form",
                    paddingY:20,
                    id:"dummylot_form1",
                    elements:
                    [
                        {                            
                            cols:                                    
                                [                           
                                    /*{
                                        view:"text",label:'Part Supplier.',required:true,name:"dummylot_partsupp",id:"dummylot_partsupp",labelPosition:"top", value:"",width:230
                                    },*/
                                    {
                                        view:"text",label:'Lot Dummy.(Lot ใหม่)',required:true,name:"dummylot_lot",id:"dummylot_lot",labelPosition:"top", value:"",width:230
                                    },
                                    {
                                        rows:
                                        [
                                            {},
                                            {
                                                view:"button", label:"Scan Lot Dummy",width:230,id:"Scan_lotdummy",
                                                on:
                                                {                                                   
                                                    onItemClick:function(id, e)
                                                    {
                                                        if ($$('dummylot_form1').validate())
                                                            {
                                                                
                                                                obj =  $$('dummylot_form1').getValues();                                                           
                                                                $.post("inventory/dummylot_update.php",{obj:obj,type:1})
                                                                .done(function( data )
                                                                {
                                                                    data = eval('('+data+')');
                                                                    if(data.ch == 1)
                                                                    {
                                                                        dummylot_FN2();                                                 
                                                                    }
                                                                    else if (data.ch == 2) 
                                                                    {
                                                                        webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function()
                                                                        {
                                                                            $$('dummylot_lot').focus();
                                                                            $$('dummylot_lot').setValue('');   
                                                                        }});
                                                                    }          
                                                                });
                                                            }                               
                                                    }
                                                }   
                                            }
                                        ]
                                    },
                                    {}                          
                                ]
                        },
                        {
                            id:"dummylot_1",
                            cols:
                            [
                                {
                                    view:"text",label:'Qty (จำนวนที่ต้องการเบิก)',required:true,name:"dummylot_qty",id:"dummylot_qty",labelPosition:"top", value:"",width:230
                                },
                                {
                                    view:"text",label:'Lot (lot เก่า)',required:true,name:"dummylot_no",id:"dummylot_no",labelPosition:"top", value:"",width:230
                                },
                                {
                                        rows:
                                        [
                                            {},
                                            {
                                                view:"button", label:"Scan Lot",width:230,id:"dummy_Scan_lotdu",
                                                on:
                                                {                                                   
                                                    onItemClick:function(id, e)
                                                    {
                                                        if ($$('dummylot_form1').validate())
                                                            {
                                                                var btn = $$('dummy_Scan_lotdu'),
                                                                obj =  $$('dummylot_form1').getValues();
                                                                btn.disable();
                                                     
                                                                $.post("inventory/dummylot_update.php",{obj:obj,type:2})
                                                                .done(function( data )
                                                                {
                                                                    btn.enable();
                                                                    data = eval('('+data+')');
                                                                    if(data.ch == 1)
                                                                    {
                                                                        webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function()
                                                                        {
                                                                            $$('dummy_clear').callEvent("onItemClick", []);
                                                                        }});                                                  
                                                                    }
                                                                    else if (data.ch == 2) 
                                                                    {
                                                                        webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function()
                                                                        {
                                                                            $$('dummylot_qty').focus();
                                                                            $$('dummylot_qty').setValue('');
                                                                            $$('dummylot_no').setValue('');   
                                                                        }});
                                                                    }          
                                                                });
                                                            }                               
                                                    }
                                                }   
                                            }
                                        ]
                                },
                                {
                                        rows:
                                        [
                                            {},
                                            {
                                                view:"button", type:"danger",label:"Clear (ล้างหน้าจอ)",width:230,id:"dummy_clear",
                                                on:
                                                {                                                   
                                                    onItemClick:function(id, e)
                                                    {
                                                        /*$$('dummylot_partsupp').enable(); */
                                                        $$('dummylot_lot').enable();
                                                        $$('Scan_lotdummy').show();
                                                        /*$$('dummylot_partsupp').setValue(''); */
                                                        $$('dummylot_lot').setValue('');
                                                        $$('dummylot_qty').setValue(''); 
                                                        $$('dummylot_no').setValue('');
                                                        dummylot_FN1();
                                                    }
                                                }   
                                            }
                                        ]
                                },
                                {}
                            ]
                        }
                       
                                                                     
                    ],
                    on:
                            {
                                "onSubmit":function(view,e)
                                {
                                    if(view.config.name =='dummylot_lot')
                                    {
                                        view.blur();
                                        $$('Scan_lotdummy').callEvent("onItemClick", []);
                                    }
                                    else if(view.config.name =='dummylot_no')
                                    {
                                        view.blur();
                                        $$('dummy_Scan_lotdu').callEvent("onItemClick", []);
                                    }                              
                                    else if(webix.UIManager.getNext(view).config.type == 'line')
                                    {
                                        webix.UIManager.setFocus(webix.UIManager.getNext(webix.UIManager.getNext(view)));
                                    }
                                    else
                                    {
                                        webix.UIManager.setFocus(webix.UIManager.getNext(view));
                                    }
                                }
                            }
                    
            }
            ],on:
            {
                onHide:function()
                {
                    
                },
                onShow:function()
                {

                },
                onAddView:function()
                {
                    window['dummylot_FN1'] = function()  
                    {
                        $$('dummylot_1').hide();                       
                    };

                    window['dummylot_FN2'] = function()  
                    {
                       /* $$('dummylot_partsupp').disable(); */
                        $$('dummylot_lot').disable();
                        $$('Scan_lotdummy').hide();
                        $$('dummylot_1').show();
                        $$('dummylot_qty').focus();                        
                    };

                    window.dummylot_FN1();
                }
            }
        }
    };
};