var header_sumlot = function()
{
	return {
        view: "scrollview",
        scroll: "native-y",
        id:"header_sumlot",
        body: 
        {
        	id:"sumlot_id",
        	type:"clean",
    		rows:
    		[
    		    {
                    view:"form",
                    paddingY:20,
                    id:"sumlot_form1",
                    elements:
                    [
                        {                            
                            cols:                                    
                                [                           
                                    /*{
                                        view:"text",label:'Part Supplier.',required:true,name:"dummylot_partsupp",id:"dummylot_partsupp",labelPosition:"top", value:"",width:230
                                    },*/
                                    {
                                        view:"text",label:'Lot Dummy.(Lot ใหม่)',required:true,name:"sumlot_dummylot_lot",id:"sumlot_dummylot_lot",labelPosition:"top", value:"",width:230
                                    },
                                    {
                                        rows:
                                        [
                                            {},
                                            {
                                                view:"button", label:"Scan Lot Dummy",width:230,id:"sumlot_Scan_lotdummy",
                                                on:
                                                {                                                   
                                                    onItemClick:function(id, e)
                                                    {
                                                        if ($$('sumlot_form1').validate())
                                                            {
                                                                
                                                                obj =  $$('sumlot_form1').getValues();                                                           
                                                                $.post("inventory/sumlot_save.php",{obj:obj,type:1})
                                                                .done(function( data )
                                                                {
                                                                    data = eval('('+data+')');
                                                                    if(data.ch == 1)
                                                                    {
                                                                        dummylot_FN2();
                                                                        var dataT1 = $$('sumlot_dataT1');
                                                                        dataT1.clearAll();
                                                                        dataT1.parse(data.data,"jsarray");                                                 
                                                                    }
                                                                    else if (data.ch == 2) 
                                                                    {
                                                                        webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function()
                                                                        {
                                                                            $$('sumlot_dummylot_lot').focus();
                                                                            $$('sumlot_dummylot_lot').setValue('');   
                                                                        }});
                                                                    }
                                                                    else if (data.ch == 3) 
                                                                    {
                                                                        dummylot_FN2();
                                                                    }          
                                                                });
                                                            }                               
                                                    }
                                                }   
                                            }
                                        ]
                                    },
                                    {}                          
                                ]
                        },
                        {
                            id:"sumlot_lot_1",
                            cols:
                            [
                                {
                                    view:"text",label:'Lot (lot เก่า)',required:true,name:"sumlot_1",id:"sumlot_1",labelPosition:"top", value:"",width:230
                                },
                                {
                                        rows:
                                        [
                                            {},
                                            {
                                                view:"button", label:"Scan Lot",width:230,id:"sumlot_save",
                                                on:
                                                {                                                   
                                                    onItemClick:function(id, e)
                                                    {
                                                        if ($$('sumlot_form1').validate())
                                                            {
                                                                var btn = $$('sumlot_save'),
                                                                obj =  $$('sumlot_form1').getValues();
                                                                btn.disable();
                                                     
                                                                $.post("inventory/sumlot_save.php",{obj:obj,type:2})
                                                                .done(function( data )
                                                                {
                                                                    btn.enable();
                                                                    data = eval('('+data+')');
                                                                    if(data.ch == 1)
                                                                    {
                                                                        var dataT1 = $$('sumlot_dataT1');
                                                                        dataT1.clearAll();
                                                                        dataT1.parse(data.data,"jsarray"); 

                                                                        $$('sumlot_1').focus();
                                                                        $$('sumlot_1').setValue('');

                                                                    }
                                                                    else if (data.ch == 2) 
                                                                    {
                                                                        webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function()
                                                                        {
                                                                            $$('sumlot_1').focus();
                                                                            $$('sumlot_1').setValue('');
                                              
                                                                        }});
                                                                    }          
                                                                });
                                                            }                               
                                                    }
                                                }   
                                            }
                                        ]
                                },
                                {
                                        rows:
                                        [
                                            {},
                                            {
                                                view:"button", label:"Confirm Sum",width:230,id:"sumlot_confirm",type:"form",
                                                on:
                                                {                                                   
                                                    onItemClick:function(id, e)
                                                    {
                                                        var dataT1 = $$("sumlot_dataT1");
                                                        if(dataT1.count() >0)
                                                            {
                                                                webix.confirm(
                                                                {
                                                                    title:"<b>ข้อความจากระบบ</b>",ok:'ใช่',cancel:"ไม่",text:"คุณต้องการ<font color='#ca4635'><br><b>บันทึกข้อมูล</b></font><br><font color='#27ae60'><b>ใช่</b></font> หรือ <font color='#3498db'><b>ไม่</b></font>",callback:function(result)
                                                                    {
                                                                    if(result)
                                                                        {
                                                                           
                                                                            $.post( "inventory/sumlot_save.php", {obj:obj,type:3})
                                                                            .done(function( data ) 
                                                                            {
                                                                              
                                                                                var data = eval('('+data+')');
                                                                                if(data.ch == 1)
                                                                                { 
                                                                                    webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});
                                                                                   
                                                                                    $$('sumlot_dummy_clear').callEvent("onItemClick", []);
                                                                        
                                                                                }
                                                                                else if(data.ch == 2){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                                                                               
                                                                            });
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                        else{webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:'ไม่พบข้อมูลในตาราง',callback:function(){}});}

                                                                   
                                                    }
                                                }   
                                            }
                                        ]
                                },
                                {
                                        rows:
                                        [
                                            {},
                                            {
                                                view:"button", type:"danger",label:"Clear (ล้างหน้าจอ)",width:230,id:"sumlot_dummy_clear",
                                                on:
                                                {                                                   
                                                    onItemClick:function(id, e)
                                                    {
                                                       
                                                        $$('sumlot_dummylot_lot').enable();
                                                        $$('sumlot_Scan_lotdummy').show();
                                                        $$("sumlot_dataT1").clearAll();
                                                        $$('sumlot_dummylot_lot').setValue('');
                                                        $$('sumlot_1').setValue(''); 
                                                        dummylot_FN1();
                                                    }
                                                }   
                                            }
                                        ]
                                },
                                {}
                            ]
                        }
                                                                                           
                    ],
                    on:
                            {
                                "onSubmit":function(view,e)
                                {
                                    if(view.config.name =='sumlot_dummylot_lot')
                                    {
                                        view.blur();
                                        $$('sumlot_Scan_lotdummy').callEvent("onItemClick", []);
                                    }
                                    else if(view.config.name =='sumlot_1')
                                    {
                                        view.blur();
                                        $$('sumlot_save').callEvent("onItemClick", []);
                                    }                              
                                    else if(webix.UIManager.getNext(view).config.type == 'line')
                                    {
                                        webix.UIManager.setFocus(webix.UIManager.getNext(webix.UIManager.getNext(view)));
                                    }
                                    else
                                    {
                                        webix.UIManager.setFocus(webix.UIManager.getNext(view));
                                    }
                                }
                            }
                    
            },
            {
                    cols:
                    [
                        {
                            paddingX:20,
                            paddingY:20,
                            id:"sumlot_tabel",
                            rows:
                            [
                                {
                                    view:"datatable",
                                    id:"sumlot_dataT1",
                                    navigation:true,
                                    resizeColumn:true,
                                    autoheight:true,
                                    css:"my_style",
                                    datatype:"jsarray",
                                    headerRowHeight:50,
                                    width:1250,
                                    
                                    columns:
                                    [
                                        { id:"data23",header:"&nbsp;",width:35,
                                            template: "<span style='cursor:pointer;' class='webix_icon fa-trash-o'></span>"
                                        },
                                        { id:"data0",header:"No",css:"rank",width:50},
                                        { id:"data1",header:"New Lot (ใหม่)",width:200},
                                        { id:"data2",header:"New box (ใหม่)",width:200},
                                        { id:"data3",header:"Old Lot (เก่า)",width:200},
                                        { id:"data4",header:"Old Box (เก่า)",width:200},
                                        { id:"data5",header:"Qty",width:100},
                                        { id:"data6",header:"Part Number",width:225},
                                    ],
                                    on:
                                    {
                
                                    },
                                    onClick:
                                    {
                                        "fa-trash-o":function(e,t)
                                        {
                                            var row = this.getItem(t),obj;
                                            webix.confirm(
                                            {
                                                title: "กรุณายืนยัน",ok:"ใช่", cancel:"ไม่",text:"คุณต้องการ<font color='#ca4635'><br><b>ลบชิ้นส่วน</b></font><br><font color='#27ae60'><b>ใช่</b></font> หรือ <font color='#3498db'><b>ไม่</b></font>",
                                                callback:function(res)
                                                {
                                                    if(res)
                                                    {
                                                        $.post("inventory/sumlot_save.php",{obj:{lot:row.data3,box:row.data4,lot_dummy:$$('sumlot_dummylot_lot').getValue()},type:4})
                                                        .done(function( data ) 
                                                        {
                                                            data = eval('('+data+')');
                                                            if(data.ch==1)
                                                            {
                                                                var dataT1 = $$('sumlot_dataT1');
                                                                dataT1.clearAll();
                                                                dataT1.parse(data.data,"jsarray");
                                                                            
                                                                
                                                            }
                                                            else if(data.ch == 2){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                                                            else if(data.ch == 9){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                                                            else if(data.ch == 10){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){window.open("login.php","_self");}});}
                                                        });
                                                    }
                                                }
                                            });
                                        }
                                    }
                                }
                               
                            ]
                        },{}
                                    
                    ],
            }
            ],on:
            {
                onHide:function()
                {
                    
                },
                onShow:function()
                {

                },
                onAddView:function()
                {
                    window['dummylot_FN1'] = function()  
                    {
                        $$('sumlot_lot_1').hide();
                        $$('sumlot_tabel').hide();                       
                    };

                    window['dummylot_FN2'] = function()  
                    {
                       /* $$('dummylot_partsupp').disable(); */
                        $$('sumlot_dummylot_lot').disable();
                        $$('sumlot_Scan_lotdummy').hide();
                        $$('sumlot_lot_1').show();
                         $$('sumlot_tabel').show();
                        $$('sumlot_1').focus();                        
                    };

                    window.dummylot_FN1();
                }
            }
        }
    };
};