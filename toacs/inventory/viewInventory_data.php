<?php
if(!ob_start("ob_gzhandler")) ob_start();
header('Expires: Sun, 01 Jan 2014 00:00:00 GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Pragma: no-cache');
include('../start.php');
session_start();
include('../php/connection.php');
$type  = intval($_REQUEST['type']);
$cBy = $_SESSION['xxxID'];

if($type == 1)
{
  if($re1 = $mysqli->query("select t2.part_supplier,t2.Part_name,t1.lot,t1.qty,t1.box,t3.doc_no,t1.area,DATE_FORMAT(t1.Rec_Date,'%d-%m-%Y'),t4.user_fName,t1.lot_status,t1.Create_date 
    from tbl_inventory t1
    LEFT JOIN tbl_partmaster t2 ON t1.part_id = t2.part_id
    LEFT JOIN tbl_receive_header t3 ON t1.doc_no = t3.id
    LEFT JOIN tbl_user t4 ON t1.user_id = t4.user_id"))
  {
    if($re1->num_rows >0)
    {
      echo '{"ch":1,"data":';
      toArrayStringAddNumberRow($re1,1);
      echo '}';
    }
    else echo '{ch:2,data:"ไม่พบข้อมูลในระบบ"}';
  }
  else echo '{ch:2,data:"โคดผิด"}'; 
}
else if ($type == 2) 
{
    $obj  = $_POST['obj'];
    $SuppPartNo = $mysqli->real_escape_string(trim(strtoupper($obj['inv_part_sup'])));
    $grn = $mysqli->real_escape_string(trim(strtoupper($obj['inv_doc_grn'])));
   

    if ($SuppPartNo == "" && $grn == "") 
    {
      echo '{ch:2,data:"กรุณากรอกข้อมูล"}'; 
      exit();
    }
    
  if($re1 = $mysqli->query("select t2.part_supplier,t2.Part_name,t1.lot,t1.qty,t1.box,t3.doc_no,t1.area,DATE_FORMAT(t1.Rec_Date,'%d-%m-%Y'),t4.user_fName,t1.lot_status,t1.Create_date 
                              from tbl_inventory t1
                              LEFT JOIN tbl_partmaster t2 ON t1.part_id = t2.part_id
                              LEFT JOIN tbl_receive_header t3 ON t1.doc_no = t3.id
                              LEFT JOIN tbl_user t4 ON t1.user_id = t4.user_id 
                              where t2.part_supplier = '$SuppPartNo' or t3.doc_no = '$grn'"))
      {
        if($re1->num_rows >0)
        {
          echo '{"ch":1,"data":';
          toArrayStringAddNumberRow($re1,1);
          echo '}';
        }
        else echo '{ch:2,data:"ไม่พบข้อมูลในระบบ"}';
      }
        
    else echo '{ch:2,data:"โคดผิด"}';
}
$mysqli->close();
exit(); 
?>