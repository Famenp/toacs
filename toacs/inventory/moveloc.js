var header_moveloc = function()
{
	return {
        view: "scrollview",
        scroll: "native-y",
        id:"header_moveloc",
        body: 
        {
        	id:"moveloc_id",
        	type:"clean",
    		rows:
    		[
    		    {
                    view:"form",
                    paddingY:20,
                    id:"moveloc_form1",
                    elements:
                    [
                        {                            
                            cols:                                    
                                [    
                                    {
                                        view:"combo",yCount:"10",label:'Location',id:"move_loc_name",required:true,name:"move_loc_name",labelPosition:"top",width:230 ,value:"",
                                        options:[ "REC","STORAGE","PICK","OVERFLOW","HOLD"],
                                        on:
                                            {
                                                onChange: function(value)
                                                {
                                                    $$('move_lot_no').focus();
                                                }
                                            }                                       
                                    },
                                    {
                                        view:"text",label:'Lot No.',required:true,name:"move_lot_no",id:"move_lot_no",labelPosition:"top", value:"",width:230
                                    },
                                    {
                                        rows:
                                        [
                                            {},
                                            {
                                                view:"button", label:"Scan Lot No.",width:230,id:"move_scan_shipno",
                                                on:
                                                {                                                   
                                                    onItemClick:function(id, e)
                                                    {
                                                        if ($$('moveloc_form1').validate())
                                                            {
                                                                var btn = $$('move_scan_shipno'),
                                                                obj =  $$('moveloc_form1').getValues();
                                                                btn.disable();
                                                     
                                                                $.post("inventory/moveloc_update.php",{obj:obj,type:1})
                                                                .done(function( data )
                                                                {
                                                                    btn.enable();
                                                                    data = eval('('+data+')');
                                                                    if(data.ch == 1)
                                                                    {
                                                                        webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function()
                                                                        {
                                                                            $$('move_lot_no').focus();
                                                                            $$('move_lot_no').setValue('');
                                                                        }});                                                  
                                                                    }
                                                                    else if (data.ch == 2) 
                                                                    {
                                                                        webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function()
                                                                        {
                                                                            $$('move_lot_no').focus();
                                                                            $$('move_lot_no').setValue('');   
                                                                        }});
                                                                    }          
                                                                });
                                                            }                               
                                                    }
                                                }   
                                            }
                                        ]
                                    },{}                          
                                ]
                        },
                                                                     
                    ],
                    on:
                            {
                                "onSubmit":function(view,e)
                                {
                                    if(view.config.name =='move_lot_no')
                                    {
                                        view.blur();
                                        $$('move_scan_shipno').callEvent("onItemClick", []);
                                    }                              
                                    else if(webix.UIManager.getNext(view).config.type == 'line')
                                    {
                                        webix.UIManager.setFocus(webix.UIManager.getNext(webix.UIManager.getNext(view)));
                                    }
                                    else
                                    {
                                        webix.UIManager.setFocus(webix.UIManager.getNext(view));
                                    }
                                }
                            }
                    
                }
            ],on:
            {
                onHide:function()
                {
                    
                },
                onShow:function()
                {

                },
                onAddView:function()
                {
                   
                }
            }
        }
    };
};