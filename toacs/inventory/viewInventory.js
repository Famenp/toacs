var header_viewInventory = function()
{
	return {
        view: "scrollview",
        scroll: "native-y",
        id:"header_viewInventory",
        body: 
        {
        	id:"viewInventory_id",
        	type:"clean",
    		rows:
    		[
    		   {
                    view:"form",
                    paddingY:20,
                    id:"viewInventory_form1",
                    elements:
                    [
                        {                            
                            cols:                                    
                                [    
                                    {  
                                        view:"text",label:'Part Supplier',name:"inv_part_sup",id:"inv_part_sup",labelPosition:"top", value:"",width:230
                                    },
                                    {
                                        view:"text",label:'GRN (เลขที่เอกสารขาเข้า)',name:"inv_doc_grn",id:"inv_doc_grn",labelPosition:"top", value:"",width:230
                                    },
                                    {
                                        rows:
                                        [
                                            {
                                                                               
                                            },
                                            {
                                             
                                                view:"button", label:"Find (ค้นหา)",width:230,id:"Find",
                                                on:
                                                {
                                                    
                                                        onItemClick:function(id, e)
                                                        {
                                                           
                                                                var btn = $$('Find'),
                                                                obj =  $$('viewInventory_form1').getValues();
                                                                btn.disable();
                                                     
                                                                $.post("inventory/viewInventory_data.php",{obj:obj,type:2})
                                                                 .done(function( data )
                                                                {
                                                                    btn.enable();
                                                                    data = eval('('+data+')');
                                                                    if(data.ch == 1)
                                                                    {
                                                                        var dataT1 = $$('viewInventory_dataT1');
                                                                        dataT1.clearAll();
                                                                        dataT1.parse(data.data,"jsarray");
                                                                        dataT1.getPager().render();
                                                                    }
                                                                    else if (data.ch == 2)
                                                                    {
                                                                        webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function()
                                                                        {
                                                                            $.post("Inventory/viewInventory_data.php", {type:1})
                                                                            .done(function( data ) 
                                                                            {
                                                                              var dataT1 = $$("viewInventory_dataT1");
                                                                              var data = eval('('+data+')');
                                                                              if(data.ch == 1)
                                                                              { 
                                                                                dataT1.clearAll();
                                                                                dataT1.parse(data.data,"jsarray");
                                                                              }
                                                                              else if(data.ch == 2){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                                                                              else if(data.ch == 9){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                                                                              else if(data.ch == 10){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){window.open("login.php","_self");}});}
                                                                            });

                                                                        }});
                                                                    }
                                                                    
                                                                });
                                                            
                                                        }
                                                    
                                                }       
                                            
                                            }
                                            
                                        ]   
                                    },
                                    {
                                        rows:
                                        [
                                            {

                                            },
                                            {
                                                view:"button", value:"Export Excel", type:"form",width:230,id:"inv_exl",
                                                on:
                                                {
                               
                                                    onItemClick:function(id, e)
                                                    {
                                                        webix.confirm(
                                                                {
                                                                    title:"<b>ข้อความจากระบบ</b>",ok:'ใช่',cancel:"ไม่",text:"คุณต้องการ<font color='#ca4635'><br><b>export ข้อมูล</b></font><br><font color='#27ae60'><b>ใช่</b></font> หรือ <font color='#3498db'><b>ไม่</b></font>",callback:function(result)
                                                                    {
                                                                    if(result)
                                                                        {

                                                                            var dataT1 = $$("viewInventory_dataT1"),data = [],btn=$$(this);
                                                                            if(dataT1.count()>0)
                                                                            {
                                                                                
                                                                                var f = Object.keys(dataT1.getItem(dataT1.getFirstId())),fName=[];
                                                                                f = ['data0','data1','data2','data3','data4','data5','data6','data7','data8'];
                                                                             
                                                                                data[0] = ['No','Part Supplier','Part Name','LOT','Qty','Box No.','Doc GRN','Area','Create Date'];

                                                                                dataT1.eachRow( function (row)
                                                                                {
                                                                                    var r = dataT1.getItem(row),rr=[];
                                                                                    for(var i=-1,len=f.length;++i<len;)
                                                                                    {
                                                                                        rr[rr.length] = r[f[i]];
                                                                                    }
                                                                                    data[data.length] = rr;
                                                                                });
                                                                                var worker = new Worker('js/workerToExcel.js?v=1');
                                                                                worker.addEventListener('message', function(e) 
                                                                                {
                                                                                    saveAs(e.data, 'INVENTORY'+new Date()+".xlsx");
                                                                                    
                                                                                    webix.message({expire:7000, text:"Export สำเร็จ" });
                                                                                }, false);  
                                                                                    worker.postMessage({'cmd': 'start', 'msg':data});  
                                                                            }
                                                                        }
                                                                    }
                                                                });        
                                                    }
                                                }
                                            },
                                        ]   
                                    },
                                    {
                                         
                                    }                               
                                ]
                        },
                        {
                            paddingX:2,
                            rows:
                            [
                                {  
                                    view:"datatable",
                                    id:"viewInventory_dataT1",
                                    navigation:true,
                                    datatype:"jsarray",
                                    css:"my_style",
                                    pager:"viewInventory_pagerA",
                                    autoheight:true,
                                    resizeColumn:true,
                                    leftSplit:0,
                                    footer:true,
                                    columns:
                                    [                                  
                                        { id:"data0",header:"No",css:"rank",width:50},
                                        { id:"data1",header:[{text :"Part Supplier",css:{"text-align":"center"}},{content:"textFilter"}],css:"rank",width:190},              
                                        { id:"data2",header:"Part Name",css:"rank",width:370},
                                        { id:"data3",header:[{text :"LOT",css:{"text-align":"center"}},{content:"textFilter"}],css:"rank",width:190},
                                        { id:"data5",header:[{text :"Box No.",css:{"text-align":"center"}},{content:"textFilter"}],css:"rank",width:120,footer:{text:"<b>Total:</b>",height:25,css:{"text-align":"right"}}},
                                        { id:"data4",header:"Qty",css:"rank",width:120,footer:{content:"summColumn",colspan:1}},                                  
                                        { id:"data6",header:[{text :"Doc GRN",css:{"text-align":"center"}},{content:"textFilter"}],css:"rank",width:190},
                                        { id:"data7",header:[{text :"Area",css:{"text-align":"center"}},{content:"textFilter"}],css:"rank",width:120},
                                        { id:"data10",header:[{text :"Lot Status",css:{"text-align":"center"}},{content:"textFilter"}],css:"rank",width:120}, 
                                        { id:"data8",header:"Receive Date",css:"rank",width:160},
                                        { id:"data9",header:"Create By",css:"rank",width:150},
                                        { id:"data11",header:"Create Date",css:"rank",width:150}, 
                                        

                                    ],
                                }
                                ,
                                {
                                    type:"wide",
                                        cols:
                                        [
                                            {
                                                view:"pager", id:"viewInventory_pagerA",
                                                template:function(data, common){
                                                    var start = data.page * data.size
                                                    ,end = start + data.size;
                                                    if(data.count == 0) start = 0;
                                                    else start += 1;
                                                    if(end >= data.count) end = data.count;
                                                    var html = "<b>showing "+(start)+" - "+end+" total "+data.count+" </b>";
                                                    return common.first()+common.prev()+" "+html+" "+common.next()+common.last();
                                                },
                                                size:10,
                                                group:5 
                                            }
                                        ]
                                                        
                                }

                            ]
                        }
                          
                    ]
                    
                }
            ],on:
            {
                onHide:function()
                {
                    
                },
                onShow:function()
                {

                },
                onAddView:function()
                {

                    function loadtable()
                    {
                       
                        $.post("Inventory/viewInventory_data.php", {type:1})
                        .done(function( data ) 
                        {
                          var dataT1 = $$("viewInventory_dataT1");
                          var data = eval('('+data+')');
                          if(data.ch == 1)
                          { 
                            dataT1.clearAll();
                            dataT1.parse(data.data,"jsarray");
                          }
                          else if(data.ch == 2){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                          else if(data.ch == 9){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                          else if(data.ch == 10){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){window.open("login.php","_self");}});}
                        });
                    }                   
                    loadtable();
                }
            }
        }
    };
};