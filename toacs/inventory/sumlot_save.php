<?php
	if(!ob_start("ob_gzhandler")) ob_start();
	header('Expires: Sun, 01 Jan 2014 00:00:00 GMT');
	header('Cache-Control: no-store, no-cache, must-revalidate');
	header('Cache-Control: post-check=0, pre-check=0', FALSE);
	header('Pragma: no-cache');
	include('../start.php');
	session_start();
	include('../php/connection.php');
	$cBy = $_SESSION['xxxID'];
	$fName = $_SESSION['xxxFName'];
	$obj  = $_POST['obj'];
	$type  = intval($_POST['type']);

	if($type == 1)
	{

		
		$lot_dummy = $mysqli->real_escape_string(trim(strtoupper($obj['sumlot_dummylot_lot'])));

		$lot_array_2  = $lot_dummy;
		$lot_cut_2 = explode("|", $lot_array_2);
		if(sizeof($lot_cut_2)!=3){echo '{ch:2,data:"ข้อมูล Lot Dummy ไม่ถูกต้อง"}';exit();}
		if ($lot_cut_2[0]!="L") {echo '{ch:2,data:"ข้อมูล Lot Dummy ไม่ถูกต้อง"}';exit();}
		$lot_dum = $lot_cut_2[1];
		$box_dum = $lot_cut_2[2];

		if($re1 = $mysqli->query("SELECT t1.New_Lot_Dum,t1.New_Box_Dum,t1.Old_Lot_Dum,t1.Old_Box_Dum,
			t1.Qty,t2.part_supplier 
     		from tbl_com_lot t1
        	LEFT JOIN tbl_partmaster t2 ON t1.part_id = t2.part_id
        	where t1.New_Lot_Dum = '$lot_dum' and t1.New_Box_Dum = '$box_dum' and t1.status = 'NO' "))
      	{
        if($re1->num_rows >0)
        {
          echo '{"ch":1,"data":';
          toArrayStringAddNumberRow($re1,1);
          echo '}';
        }
        else echo '{ch:3,data:""}';
      }
        
    else echo '{ch:2,data:"โคดผิด"}';

		
				
	}
	else if ($type == 2) 
	{
		$lot_dummy = $mysqli->real_escape_string(trim(strtoupper($obj['sumlot_dummylot_lot'])));
		$lot_no = $mysqli->real_escape_string(trim(strtoupper($obj['sumlot_1'])));
		
		$lot_array_2  = $lot_dummy;
		$lot_cut_2 = explode("|", $lot_array_2);
		if(sizeof($lot_cut_2)!=3){echo '{ch:2,data:"ข้อมูล Lot Dummy ไม่ถูกต้อง"}';exit();}
		if ($lot_cut_2[0]!="L") {echo '{ch:2,data:"ข้อมูล Lot Dummy ไม่ถูกต้อง"}';exit();}
		$lot_dum = $lot_cut_2[1];
		$box_dum = $lot_cut_2[2];
		
		$lot_array  = $lot_no;
		$lot_cut = explode("|", $lot_array);
		if(sizeof($lot_cut)!=3){echo '{ch:2,data:"ข้อมูล Lot ไม่ถูกต้อง"}';exit();}
		if ($lot_cut[0]!="L") {echo '{ch:2,data:"ข้อมูล Lot ไม่ถูกต้อง"}';exit();}
		$lot = $lot_cut[1];
		$box = $lot_cut[2];

		$mysqli->autocommit(FALSE);
		
		try 
		{

			if(!$re1 = $mysqli->query("SELECT part_id,lot_box,pick_loc,Doc_no,DCD_no,area FROM tbl_inventory where lot ='$lot' and box = '$box' limit 1;"))
			{echo '{ch:2,data:"Error Code 1"}';$mysqli->close();}
			if($re1->num_rows == 0){echo '{ch:2,data:"เลข '.$lot.' ไม่พบข้อมูลในระบบ"}';$mysqli->close();exit();}
			$row = $re1->fetch_object();

			if(!$re1 = $mysqli->query("SELECT part_id,qty,lot,box,pick_loc,Doc_no,DCD_no,area,refLot,refBox FROM tbl_inventory where area ='OVERFLOW' and lot ='$lot' and box = '$box' limit 1;"))
			{echo '{ch:2,data:"Error Code 1"}';$mysqli->close();}
			if($re1->num_rows == 0){echo '{ch:2,data:"เลข '.$lot.' นี้ สถานะอยูที่ PICK"}';$mysqli->close();exit();}
			$row = $re1->fetch_object();
			$part_id = $row->part_id;
			$qty_1 = $row->qty;			
			$pick_loc = $row->pick_loc;
			$Doc_no = $row->Doc_no;
			$DCD_no = $row->DCD_no;
			$area = $row->area;

			if (!$row->refLot) 
			{
				$Master_Lot = $row->lot;
				$Master_Box = $row->box;
			}
			else
			{
				$Master_Lot = $row->refLot;
				$Master_Box = $row->refBox;
			}
			

			if(!$mysqli->query("INSERT INTO tbl_com_lot (New_Lot_Dum,New_Box_Dum,New_LotBox_Dum,Old_Lot_Dum,Old_Box_Dum,
				Master_Lot,Master_Box,Qty,Part_ID,Create_By,Create_Date) values ('$lot_dum','$box_dum',
				concat('$lot_dum','$box_dum'),'$lot','$box','$Master_Lot','$Master_Box','$qty_1','$part_id','$cBy',NOW())")) 
				throw new Exception('ไม่ทึกไม่สำเร็จ อาจเกิด Lot ซ้ำ');
				$tranInsertRow = $mysqli->affected_rows;
				if($tranInsertRow == 0) throw new Exception('Error Code 4');

				if($re1 = $mysqli->query("SELECT t1.New_Lot_Dum,t1.New_Box_Dum,t1.Old_Lot_Dum,t1.Old_Box_Dum,
				t1.Qty,t2.part_supplier 
	     		from tbl_com_lot t1
	        	LEFT JOIN tbl_partmaster t2 ON t1.part_id = t2.part_id
	        	where t1.New_Lot_Dum = '$lot_dum' and t1.New_Box_Dum = '$box_dum' and t1.status = 'NO' "))
		      	{
		        if($re1->num_rows >0)
		        {
		          echo '{"ch":1,"data":';
		          toArrayStringAddNumberRow($re1,1);
		          echo '}';
		        }
		        else echo '{ch:3,data:""}';
		      }
		        
		    else echo '{ch:2,data:"โคดผิด"}';

		    $mysqli->commit();	
		} 
		catch (Exception $e) 
		{
			$mysqli->rollback();
		  	echo '{ch:2,data:"'.$e->getMessage().'"}';	
		}		
	}
	else if ($type == 3) 
	{

		$mysqli->autocommit(FALSE);
		try 
		{

			$lot_dummy = $mysqli->real_escape_string(trim(strtoupper($obj['sumlot_dummylot_lot'])));

		
			$lot_array_2  = $lot_dummy;
			$lot_cut_2 = explode("|", $lot_array_2);
			if(sizeof($lot_cut_2)!=3){echo '{ch:2,data:"ข้อมูล Lot Dummy ไม่ถูกต้อง"}';exit();}
			if ($lot_cut_2[0]!="L") {echo '{ch:2,data:"ข้อมูล Lot Dummy ไม่ถูกต้อง"}';exit();}
			$lot_dum = $lot_cut_2[1];
			$box_dum = $lot_cut_2[2];

			$lot_box = $lot_dum.$box_dum;

			if(!$re1 = $mysqli->query("SELECT * FROM tbl_com_lot WHERE New_LotBox_Dum = '$lot_box' 
				group by New_LotBox_Dum,part_ID"))
			{echo '{ch:2,data:"Error Code 1"}';$mysqli->close();}
			if($re1->num_rows > 1){echo '{ch:2,data:"part ไม่ตรงกัน รวม Lot ไม่ได้"}';$mysqli->close();exit();}


			if(!$re1 = $mysqli->query("SELECT Old_Lot_Dum, Old_Box_Dum FROM tbl_com_lot WHERE New_LotBox_Dum = '$lot_box'"))
			{echo '{ch:2,data:"Error Code 1"}';$mysqli->close();}
			if($re1->num_rows == 0){echo '{ch:2,data:"เลข '.$lot.' ไม่พบข้อมูลในระบบ"}';$mysqli->close();exit();}
			$lotarr = array();
			$boxarr = array();

			while ($row = $re1->fetch_assoc()) 
			{

				$lotarr[] = $row['Old_Lot_Dum'];
				$boxarr[] = $row['Old_Box_Dum'];
			}

			for ($i=0; $i < sizeof($lotarr); $i++) 
			{ 

				if(!$re1 = $mysqli->query("SELECT * FROM tbl_com_lot where Old_Lot_Dum ='$lotarr[$i]' 
					and Old_Box_Dum ='$boxarr[$i]' limit 1;"))
					{echo '{ch:2,data:"Error Code 1"}';$mysqli->close();}
					if($re1->num_rows == 0){echo '{ch:2,data:"ไม่พบข้อมูล"}';$mysqli->close();exit();}
					$row = $re1->fetch_object();
					$New_Lot_Dum = $row->New_Lot_Dum;
					$New_Box_Dum = $row->New_Box_Dum;			
					$New_LotBox_Dum = $row->New_LotBox_Dum;
					$Master_Lot = $row->Master_Lot;
					$Master_Box = $row->Master_Box;
				

				$sql = "UPDATE tbl_inventory SET Lot = '$New_Lot_Dum',Box = '$New_Box_Dum',Lot_Box = '$New_LotBox_Dum',refLot = '$Master_Lot',refBox = '$Master_Box'
				where Lot ='$lotarr[$i]' and Box ='$boxarr[$i]' limit 1;";						
				if(!$mysqli->query($sql)) throw new Exception('Error Code 2');


			}

			$sql = "UPDATE tbl_com_lot SET Status = 'YES' where New_LotBox_Dum = '$lot_box';";
			if(!$mysqli->query($sql)) throw new Exception('Error Code 2');

			else echo '{ch:1,data:"บันทึกสำเร็จ"}';
			$mysqli->commit();
		} 
		catch (Exception $e) 
		{
			$mysqli->rollback();
		  	echo '{ch:2,data:"'.$e->getMessage().'"}';
		}
	}
	else if ($type == 4)
	{
		$lot = $mysqli->real_escape_string(trim(strtoupper($obj['lot'])));
		$box = $mysqli->real_escape_string(trim(strtoupper($obj['box'])));
		$lot_dummy = $mysqli->real_escape_string(trim(strtoupper($obj['lot_dummy'])));

		$lot_array_2  = $lot_dummy;
		$lot_cut_2 = explode("|", $lot_array_2);
		if(sizeof($lot_cut_2)!=3){echo '{ch:2,data:"ข้อมูล Lot Dummy ไม่ถูกต้อง"}';exit();}
		if ($lot_cut_2[0]!="L") {echo '{ch:2,data:"ข้อมูล Lot Dummy ไม่ถูกต้อง"}';exit();}
		$lot_dum = $lot_cut_2[1];
		$box_dum = $lot_cut_2[2];

		if(!$mysqli->query("DELETE FROM tbl_com_lot where Old_Lot_Dum ='$lot' and Old_Box_Dum = '$box'"))
		{
			throw new Exception('Error Code 8');
		}

		if($re1 = $mysqli->query("SELECT t1.New_Lot_Dum,t1.New_Box_Dum,t1.Old_Lot_Dum,t1.Old_Box_Dum,
			t1.Qty,t2.part_supplier 
     		from tbl_com_lot t1
        	LEFT JOIN tbl_partmaster t2 ON t1.part_id = t2.part_id
        	where t1.New_Lot_Dum = '$lot_dum' and t1.New_Box_Dum = '$box_dum' and t1.status = 'NO' "))
      	{
        if($re1->num_rows >0)
        {
          echo '{"ch":1,"data":';
          toArrayStringAddNumberRow($re1,1);
          echo '}';
        }
        else echo '{ch:3,data:""}';
      }
        
    else echo '{ch:2,data:"โคดผิด"}';

	}
	
	
	$mysqli->close();
	exit();
?>