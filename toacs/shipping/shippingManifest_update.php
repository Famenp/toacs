<?php
	if(!ob_start("ob_gzhandler")) ob_start();
	header('Expires: Sun, 01 Jan 2014 00:00:00 GMT');
	header('Cache-Control: no-store, no-cache, must-revalidate');
	header('Cache-Control: post-check=0, pre-check=0', FALSE);
	header('Pragma: no-cache');
	include('../start.php');
	session_start();
	include('../php/connection.php');
	$cBy = $_SESSION['xxxID'];
	$fName = $_SESSION['xxxFName'];
	$obj  = $_POST['obj'];
	$type  = intval($_POST['type']);

	if($type == 1)
	{

		$shp_date = $mysqli->real_escape_string(trim(strtoupper($obj['edit_date'])));
		$shp_work = $mysqli->real_escape_string(trim(strtoupper($obj['edit_work'])));
		$shp_shipto = $mysqli->real_escape_string(trim(strtoupper($obj['edit_shipTo'])));
		$shp_trip = $mysqli->real_escape_string(trim(strtoupper($obj['edit_trip'])));
		$shp_route = $mysqli->real_escape_string(trim(strtoupper($obj['edit_route'])));
		$shp_tLicense = $mysqli->real_escape_string(trim(strtoupper($obj['edit_tLicense'])));
		$shp_driver = $mysqli->real_escape_string(trim(strtoupper($obj['edit_driver'])));
		$shp_plantime = $mysqli->real_escape_string(trim(strtoupper($obj['edit_planTime'])));
		$shp_remark = $mysqli->real_escape_string(trim(strtoupper($obj['edit_remark'])));
		$shp_shipNo = $mysqli->real_escape_string(trim(strtoupper($obj['edit_ship_no'])));

		$mysqli->autocommit(FALSE);
		try 
		{
			if(!$re = $mysqli->query("SELECT truck_type FROM tbl_truckmaster where Truck_Plate_Number = '$shp_tLicense' limit 1;"))
				{echo '{ch:2,data:"Error Code 1"}';$mysqli->close();}
				if($re->num_rows == 0){echo '{ch:2,data:"ไม่มีข้อมูลในระบบ"}';$mysqli->close();exit();}
				$row = $re->fetch_object();
				$truck_type = $row->truck_type;

				if(!$re = $mysqli->query("SELECT telephone FROM tbl_drivermaster where driver_name = '$shp_driver' limit 1;"))
				{echo '{ch:2,data:"Error Code 1"}';$mysqli->close();}
				if($re->num_rows == 0){echo '{ch:2,data:"ไม่มีข้อมูลในระบบ"}';$mysqli->close();exit();}
				$row = $re->fetch_object();
				$Tel = $row->telephone;

				if(!$re = $mysqli->query("SELECT zone FROM tbl_route where code = '$shp_route' limit 1;"))
				{echo '{ch:2,data:"Error Code 1"}';$mysqli->close();}
				if($re->num_rows == 0){echo '{ch:2,data:"ไม่มีข้อมูลในระบบ"}';$mysqli->close();exit();}
				$row = $re->fetch_object();
				$zone = $row->zone;


				
				$sql = "UPDATE tbl_shipping_header SET ship_to ='$shp_shipto',working_ship ='$shp_work',trip_no = '$shp_trip',
				truck_license ='$shp_tLicense',truck_type ='$truck_type',driver_Name = '$shp_driver',phone = '$Tel', 
				routeCode ='$shp_route',routeZone ='$zone',remark = '$shp_remark',plan_time = '$shp_plantime',dDate = '$shp_date' 
				WHERE ship_no = '$shp_shipNo';";					
				if(!$mysqli->query($sql)) throw new Exception('Error Code 2');
				$mysqli->commit();

				if($re3 = $mysqli->query("SELECT ship_no,ship_to,working_ship,trip_no,truck_license,driver_name,routeCode,remark,plan_time,dDate from tbl_shipping_header where ship_no ='$shp_shipNo'"))
				{
					if($re3->num_rows >0)
						{
							echo '{"ch":1,"data":';
							toArrayStringAddNumberRow($re3,1);
							echo '}';
						}
					else echo '{ch:2,data:"ไม่พบข้อมูลในระบบ"}';
				}
				else echo '{ch:2,data:"โคดผิด"}';
			} 
			catch (Exception $e) 
			{
				$mysqli->rollback();
		  		echo '{ch:2,data:"'.$e->getMessage().'"}';	
			}		
	}
	
	$mysqli->close();
	exit();
?>