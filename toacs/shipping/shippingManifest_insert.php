<?php
	if(!ob_start("ob_gzhandler")) ob_start();
	header('Expires: Sun, 01 Jan 2014 00:00:00 GMT');
	header('Cache-Control: no-store, no-cache, must-revalidate');
	header('Cache-Control: post-check=0, pre-check=0', FALSE);
	header('Pragma: no-cache');
	include('../start.php');
	session_start();
	include('../php/connection.php');
	$cBy = $_SESSION['xxxID'];
	$fName = $_SESSION['xxxFName'];
	$obj  = $_POST['obj'];
	$type  = intval($_POST['type']);

	if($type == 1)
	{
		$mysqli->autocommit(FALSE);
 		try 
			{
				

				$shp_date = $mysqli->real_escape_string(trim(strtoupper($obj['date'])));
				$shp_work = $mysqli->real_escape_string(trim(strtoupper($obj['work'])));
				$shp_to = $mysqli->real_escape_string(trim(strtoupper($obj['shipTo'])));
				$shp_trip = $mysqli->real_escape_string(trim(strtoupper($obj['trip'])));
				$shp_route = $mysqli->real_escape_string(trim(strtoupper($obj['route'])));
				$shp_truck = $mysqli->real_escape_string(trim(($obj['tLicense'])));
				$shp_driver = $mysqli->real_escape_string(trim(($obj['driver'])));
				$shp_plantime = $mysqli->real_escape_string(trim(strtoupper($obj['planTime'])));
				$shp_remark = $mysqli->real_escape_string(trim(strtoupper($obj['remark'])));
			

				if(!$re = $mysqli->query("SELECT truck_type FROM tbl_truckmaster where Truck_Plate_Number = '$shp_truck' limit 1;"))
				{echo '{ch:2,data:"Error Code 1"}';$mysqli->close();}
				if($re->num_rows == 0){echo '{ch:2,data:"ไม่มีข้อมูลในระบบ"}';$mysqli->close();exit();}
				$row = $re->fetch_object();
				$truck_type = $row->truck_type;

				if(!$re = $mysqli->query("SELECT telephone FROM tbl_drivermaster where driver_name = '$shp_driver' limit 1;"))
				{echo '{ch:2,data:"Error Code 1"}';$mysqli->close();}
				if($re->num_rows == 0){echo '{ch:2,data:"ไม่มีข้อมูลในระบบ"}';$mysqli->close();exit();}
				$row = $re->fetch_object();
				$Tel = $row->telephone;

				if(!$re = $mysqli->query("SELECT zone FROM tbl_route where code = '$shp_route' limit 1;"))
				{echo '{ch:2,data:"Error Code 1"}';$mysqli->close();}
				if($re->num_rows == 0){echo '{ch:2,data:"ไม่มีข้อมูลในระบบ"}';$mysqli->close();exit();}
				$row = $re->fetch_object();
				$zone = $row->zone;

				if(!$re = $mysqli->query("SELECT func_GenRuningNumber('shp',0) t")) throw new Exception('Error Code 1'); 
				if($re->num_rows == 0) throw new Exception('Error Code 2'); 
				$autoID = $re->fetch_object()->t;


				$sql = "INSERT INTO tbl_shipping_header(ship_no,ship_from,ship_to,working_ship,trip_no,
					truck_license,truck_type,driver_Name,phone,routeCode,routeZone,remark,plan_time,cBy,dDate,dTime,
					status,tstatus)
				values('$autoID', 'ABT', '$shp_to','$shp_work','$shp_trip','$shp_truck','$truck_type','$shp_driver',
					'$Tel','$shp_route','$zone','$shp_remark','$shp_plantime','$cBy','$shp_date','$shp_plantime','PENDING','HOLD')";
				if(!$mysqli->query($sql)) throw new Exception('Error Code 2');

				echo '{ch:1,data:"'.$autoID.'"}';	
			
				$mysqli->commit();	
			} 

		catch (Exception $e) 
			{
				$mysqli->rollback();
		  		echo '{ch:2,data:"'.$e->getMessage().'"}';
			}		
	}
	elseif ($type == 2) 
	{
		$SHP_no = $mysqli->real_escape_string(trim(strtoupper($obj['doctype'])));
		$GTN_no = $mysqli->real_escape_string(trim(strtoupper($obj['gtn'])));

		$mysqli->autocommit(FALSE);

		try 
			{
			if(!$re = $mysqli->query("SELECT Doc_no FROM tbl_transaction where Doc_no = '$GTN_no' and tran_type = 'OUT' limit 1;"))
					{echo '{ch:2,data:"Error Code 1"}';$mysqli->close();}
					if($re->num_rows == 0){echo '{ch:2,data:"ไม่มีข้อมูลในระบบ"}';$mysqli->close();exit();}


					if(!$re = $mysqli->query("SELECT ID FROM tbl_shipping_header where ship_no = '$SHP_no' limit 1;"))
					{echo '{ch:2,data:"Error Code 1"}';$mysqli->close();}
					if($re->num_rows == 0){echo '{ch:2,data:"ไม่มีข้อมูลในระบบ"}';$mysqli->close();exit();}
					$row = $re->fetch_object();
					$id_shp = $row->ID;

					$sql = "INSERT INTO tbl_shipping_body(refID,gtn) values ('$id_shp', '$GTN_no')";
					if(!$mysqli->query($sql)) throw new Exception('เลข GTN สแกนรับแล้ว');

					if($re3 = $mysqli->query("SELECT refID,gtn from tbl_shipping_body where refID ='$id_shp'"))
					{
						if($re3->num_rows >0)
							{
								echo '{"ch":1,"data":';
								toArrayStringAddNumberRow($re3,1);
								echo '}';
							}
						else echo '{ch:2,data:"ไม่พบข้อมูลในระบบ"}';
					}
					else echo '{ch:2,data:"โคดผิด"}';

					$mysqli->commit();	

			} 
		catch (Exception $e) 
			{
				$mysqli->rollback();
		  		echo '{ch:2,data:"'.$e->getMessage().'"}';
			}

		

	}

	$mysqli->close();
	exit();	
?>
