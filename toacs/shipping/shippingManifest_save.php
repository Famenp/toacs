<?php
	if(!ob_start("ob_gzhandler")) ob_start();
	header('Expires: Sun, 01 Jan 2014 00:00:00 GMT');
	header('Cache-Control: no-store, no-cache, must-revalidate');
	header('Cache-Control: post-check=0, pre-check=0', FALSE);
	header('Pragma: no-cache');
	include('../start.php');
	session_start();
	include('../php/connection.php');
	$cBy = $_SESSION['xxxID'];
	$fName = $_SESSION['xxxFName'];
	$obj  = $_POST['obj'];
	$type  = intval($_POST['type']);

	if($type == 1)
	{

		
		$shp_shipNo = $mysqli->real_escape_string(trim(strtoupper($obj['doctype'])));

		$_Time = date('H:i');
		
		$mysqli->autocommit(FALSE);
		try 
		{
							
				$sql = "UPDATE tbl_shipping_header SET ttv_out ='$_Time',status ='CLOSED',tstatus = 'YES'
				WHERE ship_no = '$shp_shipNo';";					
				if(!$mysqli->query($sql)) throw new Exception('Error Code 2');
				$mysqli->commit();

				echo '{ch:1,data:"'.$shp_shipNo.'"}';

		} 
		catch (Exception $e) 
		{
			$mysqli->rollback();
		  	echo '{ch:2,data:"'.$e->getMessage().'"}';	
		}		
	}
	
	$mysqli->close();
	exit();
?>