var header_shippingManifest = function()
{
	return {
        view: "scrollview",
        scroll: "native-y",
        id:"header_shippingManifest",
        body: 
        {
        	id:"shippingManifest_id",
        	type:"clean",
    		rows:
    		[
    		    {
                    view:"form",
                    paddingY:0,
                    id:"shippingManifest_form1",
                    elements:
                    [
                        {
                            cols:
                            [
                                {
                                    view:"combo",yCount:"10",suggest:"shipping/shippingManifest_data.php?type=1",label:'Document No (งานค้าง)',id:"shippingManifest_doctype",name:"doctype",value:"",labelPosition:"top",on:
                                    {
                                        onChange: function(value)
                                        {
                                            if(value.length>0) this.disable();else this.enable();
                                            $.post("shipping/shippingManifest_data.php",{obj:value,type:5})
                                            .done(function( data ) 
                                            {
                                                data = eval('('+data+')');
                                                if(data.data1.length)
                                                {
                                                    var ar = data.data1[0];
                                                    $$('shippingManifest_date').setValue(ar[10]);
                                                    $$('shippingManifest_trip').setValue(ar[4]);
                                                    $$('shippingManifest_remark').setValue(ar[8]);
                                                    $$('shippingManifest_work').setValue(ar[3]);
                                                    $$('shippingManifest_shipTo').setValue(ar[2]);
                                                    $$('shippingManifest_planTime').setValue(ar[9]);
                                                    
                                                    comboSet('shippingManifest_route',ar[7]);
                                                    comboSet('shippingManifest_tLicense',ar[5]);
                                                    comboSet('shippingManifest_driver',ar[6]);
                                                }

                                                var dataT1 = $$('shippingManifest_dataT1');
                                                dataT1.clearAll();
                                                dataT1.parse(data.data2,"jsarray");

                                                if($$('shippingManifest_form1').validate())

                                                window.shippingManifest_FN2();
                                                shippingManifest_FN3();
                                                $$('shippingManifest_gtn').focus();
                                            });
                                        }
                                    }
                                },
                                {
                                  view:"datepicker",align:"left",label:'Date[y-m-d] (วันที่[ป/ด/ว])',labelPosition:"top",value:new Date(),id:"shippingManifest_date",name:"date",stringResult:1,format:webix.Date.dateToStr("%Y-%m-%d")
                                },
                                {view:"combo",required:true,label:'Working Shift',id:"shippingManifest_work",name:"work",labelPosition:"top",value:'DAY',options:['DAY','NIGTH']},
                                {view:"text",required:true,label:'Ship To(ส่งถึง)',id:"shippingManifest_shipTo",name:"shipTo",labelPosition:"top"},
                                {view:"text",required:true,label:'Trip',id:"shippingManifest_trip",name:"trip",labelPosition:"top"},

                            ]
                        },
                        {
                            cols:
                            [
                                {view:"combo",required:true,label:'Route',id:"shippingManifest_route",name:"route",labelPosition:"top",suggest:"shipping/shippingManifest_data.php?type=2",hidden:0,
                                    on:
                                    {
                                      onKeyPress:function(code, e)
                                      {
                                        if(code==13)
                                        {
                                            var list = $$(this.config.suggest).getBody();
                                            if(list.getSelectedItem() == undefined && list.getFirstId() !== undefined)
                                            {
                                              list.select(list.getFirstId());
                                              this.setValue(list.getFirstId());
                                            }
                                        }
                                      },
                                    }
                                },
                                {view:"combo",required:true,label:'Truck License',id:"shippingManifest_tLicense",name:"tLicense",labelPosition:"top",suggest:"shipping/shippingManifest_data.php?type=3",hidden:0,
                                    on:
                                    {
                                      onKeyPress:function(code, e)
                                      {
                                        if(code==13)
                                        {
                                            var list = $$(this.config.suggest).getBody();
                                            if(list.getSelectedItem() == undefined && list.getFirstId() !== undefined)
                                            {
                                              list.select(list.getFirstId());
                                              this.setValue(list.getFirstId());
                                            }
                                        }
                                      },
                                    }
                                },
                                {view:"combo",required:true,label:'Driver Name',id:"shippingManifest_driver",name:"driver",labelPosition:"top",suggest:"shipping/shippingManifest_data.php?type=4",hidden:0,
                                    on:
                                    {
                                      onKeyPress:function(code, e)
                                      {
                                        if(code==13)
                                        {
                                            var list = $$(this.config.suggest).getBody();

                                            if(list.getSelectedItem() == undefined && list.getFirstId() !== undefined)
                                            {
                                              list.select(list.getFirstId());
                                              this.setValue(list.getFirstId());
                                            }
                                        }
                                      },
                                    }
                                },
                                {view:"text",required:true,label:'Planing Time',id:"shippingManifest_planTime",name:"planTime",labelPosition:"top",type:"time"},
                                {view:"text",label:'Remark (หมายเหตุ)',id:"shippingManifest_remark",name:"remark",labelPosition:"top"},                            
                            ]
                        },
                        {
                            rows:
                            [
                                {
                                    id:"shippingManifest_btnGroup1",
                                    cols:
                                    [                                   
                                        {
                                          view:"button",icon:"users",value:"Add Shipping Manifest",id:"shippingManifest_add",width:250,on:
                                          {
                                            onItemClick:function(id, e)
                                            {

                                                if($$('shippingManifest_form1').validate())
                                                {
                                                    var btn = this;
                                                    btn.disable();
                                                    $.post("shipping/shippingManifest_insert.php",{obj:$$('shippingManifest_form1').getValues(),type:1})
                                                    .done(function( data ) 
                                                    {
                                                        btn.enable();
                                                        var data = eval('('+data+')'),dataT1 = $$("shippingManifest_dataT1");
                                                        if(data.ch == 1)
                                                        {
                                                            var doctype = $$('shippingManifest_doctype'),list = doctype.getPopup().getList();
                                                            doctype.blockEvent();
                                                            list.clearAll();
                                                            list.parse([data.data]);
                                                            doctype.setValue(list.getFirstId());
                                                            doctype.unblockEvent();
                                                           
                                                            window.shippingManifest_FN2();
                                                            shippingManifest_FN3();
                                                            $$('shippingManifest_gtn').focus();
                                                        }
                                                        else if(data.ch == 2){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                                                        else if(data.ch == 9){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                                                        else if(data.ch == 10){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){window.open("login.php","_self");}});}
                                                    });
                                                }
                                                else
                                                {
                                                    webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:'กรุณาป้อนข้อมูล<font color="#ce5545"><b>ในช่องสีแดง</b></font>ให้ถูกต้อง',callback:function(){}});
                                                }
                                            }
                                          }
                                        },
                                        {}
                                    ]
                                }
                            ]
                        },
                        {
                            id:"shippingManifest_btnGroup2",
                            rows:
                            [
                                {
                                    cols:
                                    [
                                        {
                                            view:"button",type:"danger",value:"Cancel (ยกเลิกเอกสาร)",hidden:0,id:"shippingManifest_calcelGRN",width:250,on:
                                            {
                                                onItemClick:function(id, e)
                                                {
                                                    webix.confirm(
                                                    {
                                                        title: "กรุณายืนยัน",ok:"ใช่", cancel:"ไม่",text:"คุณต้องการ<font color='#ca4635'><br><b>ยกเลิกเอกสาร</b></font><br><font color='#27ae60'><b>ใช่</b></font> หรือ <font color='#3498db'><b>ไม่</b></font>",
                                                        callback:function(res)
                                                        {
                                                            if(res)
                                                            {
                                                                var btn = $$('shippingManifest_calcelGRN'),obj = {doctype:$$('shippingManifest_doctype').getValue().trim().toUpperCase()};
                                                                btn.disable();
                                                                $.post( "shipping/shippingManifest_delete.php", {obj:obj,type:1})
                                                                .done(function( data ) 
                                                                {
                                                                    btn.enable();
                                                                    var data = eval('('+data+')');
                                                                    if(data.ch == 1)
                                                                    { 
                                                                        webix.message({ type:"default",expire:7000, text:data.data});
                                                                        $$('shippingManifest_clear').callEvent("onItemClick", []);
                                                                    }
                                                                    else if(data.ch == 2){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                                                                    else if(data.ch == 9){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                                                                    else if(data.ch == 10){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){window.open("login.php","_self");}});}
                                                                });
                                                            }
                                                        }
                                                    });
                                                }
                                            }
                                        },
                                        {
                                            view:"button", value:"Clear (ล้างหน้าจอ)",hidden:0,id:"shippingManifest_clear",width:250,on:
                                            {
                                                onItemClick:function(id, e)
                                                {
                                                    window.shippingManifest_FN1();
                                                    shippingManifest_init();
                                                    shippingManifest_FN4();
                                                    $$('shippingManifest_date').setValue(new Date());
                                                    $$('shippingManifest_work').setValue('');
                                                    $$('shippingManifest_shipTo').setValue('');
                                                    $$('shippingManifest_trip').setValue('');
                                                    $$('shippingManifest_route').setValue('');
                                                    $$('shippingManifest_tLicense').setValue('');
                                                    $$('shippingManifest_driver').setValue('');
                                                    $$('shippingManifest_planTime').setValue('');
                                                    $$('shippingManifest_remark').setValue('');
                                                    $$('shippingManifest_gtn').setValue('');
        
                                                    $$("shippingManifest_dataT1").clearAll();
                                                   
                                                }
                                            }
                                        },
                                        {
                                            view:"button",hidden:false, type:"danger",value:"Edit (แก้ไข)",id:"shippingManifest_edit",width:250,on:
                                            {
                                                onItemClick:function(id, e)
                                                {
                                                    $$('shippingManifest_editHead_win').show();

                                                    $$('edit_shippingManifest_date').setValue($$('shippingManifest_date').getValue());
                                                    $$('edit_shippingManifest_work').setValue($$('shippingManifest_work').getValue());
                                                    $$('edit_shippingManifest_shipTo').setValue($$('shippingManifest_shipTo').getValue());
                                                    $$('edit_shippingManifest_trip').setValue($$('shippingManifest_trip').getValue());
                                                    comboSet('edit_shippingManifest_route',$$('shippingManifest_route').getValue());
                                                    comboSet('edit_shippingManifest_tLicense',$$('shippingManifest_tLicense').getValue());
                                                    comboSet('edit_shippingManifest_driver',$$('shippingManifest_driver').getValue());

                                                    $$('edit_shippingManifest_planTime').setValue($$('shippingManifest_planTime').getValue());
                                                    $$('edit_shippingManifest_remark').setValue($$('shippingManifest_remark').getValue());
                                                    $$('edit_shippingManifest_ship_no').setValue($$('shippingManifest_doctype').getValue()); 
                                                            
                                                    
                                                }
                                            }
                                        },{}                                                                                                                                             
                                    ]
                                }
                            ]
                        },
                        {
                            id:"shippingManifest_btnGroup3",
                            rows:
                            [
                                {
                                    cols:
                                    [
                                        {view:"text",label:'GTN No.',id:"shippingManifest_gtn",name:"gtn_no",labelPosition:"top",width:250},
                                        {
                                            rows:
                                            [
                                                {

                                                },
                                                {
                                                    view:"button",icon:"users",value:"Scan GTN No.",id:"shippingManifest_addBox",width:250,on:
                                                    {
                                                        onItemClick:function(id, e)
                                                        {                                  
                                                                var btn = this;
                                                                btn.disable();
                                                                var obj = {};
                                                                obj.doctype = $$('shippingManifest_doctype').getValue();
                                                                obj.gtn = $$('shippingManifest_gtn').getValue();
                                                                $.post("shipping/shippingManifest_insert.php",{obj:obj,type:2})
                                                                .done(function( data ) 
                                                                {
                                                                    btn.enable();
                                                                    var data = eval('('+data+')');
                                                                    if(data.ch == 1)
                                                                    {
                                                                        var dataT1 = $$('shippingManifest_dataT1');
                                                                        dataT1.clearAll();
                                                                        dataT1.parse(data.data,"jsarray");

                                                                        $$('shippingManifest_gtn').setValue('');
                                                                        $$('shippingManifest_gtn').focus();
                                                                    }
                                                                    else if(data.ch == 2){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){$$('shippingManifest_gtn').setValue('');$$('shippingManifest_gtn').focus();}});}
                                                                    else if(data.ch == 9){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                                                                    else if(data.ch == 10){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){window.open("login.php","_self");}});}
                                                                });
                                                            
                                                        }
                                                    }
                                                }, 
                                            ]
                                        },
                                        {
                                            rows:
                                            [
                                                {

                                                },
                                                {
                                                    view:"button", value:"Save (บันทึก)",id:"shippingManifest_save",type:"form",width:250,
                                                    click:function(e)
                                                    {
                                                        var dataT1 = $$("shippingManifest_dataT1");
                                                        if(dataT1.count() >0)
                                                        {
                                                            webix.confirm(
                                                            {
                                                                title:"<b>ข้อความจากระบบ</b>",ok:'ใช่',cancel:"ไม่",text:"คุณต้องการ<font color='#ca4635'><br><b>บันทึกข้อมูล</b></font><br><font color='#27ae60'><b>ใช่</b></font> หรือ <font color='#3498db'><b>ไม่</b></font>",callback:function(result)
                                                                {
                                                                    if(result)
                                                                    {
                                                                        var btn = $$('shippingManifest_save'),
                                                                        obj = {doctype:$$('shippingManifest_doctype').getValue().trim().toUpperCase()};
                                                                        btn.disable();
                                                                        $.post( "shipping/shippingManifest_save.php", {obj:obj,type:1})
                                                                        .done(function( data ) 
                                                                        {
                                                                            btn.enable();
                                                                            var data = eval('('+data+')');
                                                                            if(data.ch == 1)
                                                                            { 
                                                                                webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});
                                                                                $.post( "print/shpm.php", {data1:data.data})
                                                                                .done(function( data ) 
                                                                                {
                                                                                    var data = eval('('+data+')');
                                                                                    if(data.ch == 1)
                                                                                    {
                                                                                      webix.message({ type:"default",expire:7000, text:'ปริ้น SHIP สำเร็จ'});
                                                                                    }
                                                                                    else webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});
                                                                                });
                                                                                $$('shippingManifest_clear').callEvent("onItemClick", []);
                                                                                
                                                                            }
                                                                            else if(data.ch == 2){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                                                                            else if(data.ch == 9){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                                                                            else if(data.ch == 10){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){window.open("login.php","_self");}});}
                                                                        });
                                                                    }
                                                                }
                                                            });
                                                        }
                                                            else{webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:'ไม่พบข้อมูลในตาราง',callback:function(){}});}
                                                    }
                                                },
                                            ]
                                        },
                                        {},
                                    ]
                                }
                            ]
                        }
                    ],
                    rules:
                    {
                      
                    },on:
                    {
                        "onSubmit":function(view,e)
                        {
                            if(view.config.name=='gtn_no')
                            {
                                view.blur();
                                $$('shippingManifest_addBox').callEvent("onItemClick", []);
                            }
                            else if(webix.UIManager.getNext(view).config.type == 'line')
                            {
                                webix.UIManager.setFocus(webix.UIManager.getNext(webix.UIManager.getNext(view)));
                            }
                            else
                            {
                                webix.UIManager.setFocus(webix.UIManager.getNext(view));
                            }
                        }
                    }
                },
                {
                    cols:
                    [
                        {
                            paddingX:20,
                            paddingY:20,
                            id:"shippingManifest_tabel",
                            rows:
                            [
                                {
                                    view:"datatable",
                                    id:"shippingManifest_dataT1",
                                    navigation:true,
                                    resizeColumn:true,
                                    autoheight:true,
                                    css:"my_style",
                                    datatype:"jsarray",
                                    headerRowHeight:50,
                                    width:740,
                                    columns:
                                    [
                                        { id:"data23",header:"&nbsp;",width:35,
                                            template: "<span style='cursor:pointer;' class='webix_icon fa-trash-o'></span>"
                                        },
                                        { id:"data0",header:"No",css:"rank",width:50},
                                        { id:"data2",header:"GTN",width:225},
                                    ],
                                    on:
                                    {
                
                                    },
                                    onClick:
                                    {
                                        "fa-trash-o":function(e,t)
                                        {
                                            var row = this.getItem(t),obj;
                                            webix.confirm(
                                            {
                                                title: "กรุณายืนยัน",ok:"ใช่", cancel:"ไม่",text:"คุณต้องการ<font color='#ca4635'><br><b>ลบชิ้นส่วน</b></font><br><font color='#27ae60'><b>ใช่</b></font> หรือ <font color='#3498db'><b>ไม่</b></font>",
                                                callback:function(res)
                                                {
                                                    if(res)
                                                    {
                                                        $.post("shipping/shippingManifest_delete.php",{obj:{id:row.data1,doc:row.data2},type:2})
                                                        .done(function( data ) 
                                                        {
                                                            data = eval('('+data+')');
                                                            if(data.ch==1)
                                                            {
                                                                var dataT1 = $$('shippingManifest_dataT1');
                                                                dataT1.clearAll();
                                                                dataT1.parse(data.data,"jsarray");
                                                                            
                                                                $$('shippingManifest_gtn').focus();
                                                            }
                                                            else if(data.ch == 2){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                                                            else if(data.ch == 9){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                                                            else if(data.ch == 10){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){window.open("login.php","_self");}});}
                                                        });
                                                    }
                                                }
                                            });
                                        }
                                    }
                                },
                                {
                                  type:"wide",
                                  cols:
                                  [
                                    {
                                      view:"pager", id:"shippingManifest_pagerA",
                                      template:function(data, common){
                                      var start = data.page * data.size
                                      ,end = start + data.size;
                                      if(data.count == 0) start = 0;
                                      else start += 1;
                                      if(end >= data.count) end = data.count;
                                      var html = "<b>showing "+(start)+" - "+end+" total "+data.count+" </b>";
                                      return common.first()+common.prev()+" "+html+" "+common.next()+common.last();
                                      },
                                      size:10,
                                      group:5 
                                    }
                                  ]
                                }
                            ]
                        },
                        {}
                    ],
                }
            ],on:
            {
                onHide:function()
                {
                    
                },
                onShow:function()
                {

                },
                onAddView:function()
                {
                     webix.ui(
                    {
                        view:"window",id:"shippingManifest_editHead_win",modal:1,move:1,
                        head:{
                                view:"toolbar", margin:-4, cols:[
                                    {view:"label", label: "Edat Shipping Manifest"},
                                    { view:"icon", icon:"times-circle", css:"alter",
                                        click:"$$('shippingManifest_editHead_win').hide();"}                                  
                                                    
                                    ]
                            },
                        top:50,position:"center",
                        body:
                        {
                            view:"form", scroll:false,id:"shippingManifest_editHead_win_form",width:800,
                            elements:
                            [
                                {
                                    cols:
                                    [
                                                    
                                                                                                                                                               
                                        {view:"datepicker",align:"left",label:'Date[y-m-d] (วันที่[ป/ด/ว])',labelPosition:"top",value:new Date(),id:"edit_shippingManifest_date",name:"edit_date",stringResult:1,format:webix.Date.dateToStr("%Y-%m-%d")},
                                        {view:"combo",required:true,label:'Working Shift',id:"edit_shippingManifest_work",name:"edit_work",labelPosition:"top",value:'DAY',options:['DAY','NIGTH']},
                                        {view:"text",required:true,label:'Ship To(ส่งถึง)',id:"edit_shippingManifest_shipTo",name:"edit_shipTo",labelPosition:"top"},
                                        
                                    ]  
                                },                                    
                                {
                                    cols:
                                    [
                                        
                                    ]  
                                },
                                {
                                    cols:
                                    [
                                        {view:"text",required:true,label:'Trip',id:"edit_shippingManifest_trip",name:"edit_trip",labelPosition:"top"},
                                        {view:"combo",required:true,label:'Route',id:"edit_shippingManifest_route",name:"edit_route",labelPosition:"top",suggest:"shipping/shippingManifest_data.php?type=2",hidden:0},
                                        {view:"combo",required:true,label:'Truck License',id:"edit_shippingManifest_tLicense",name:"edit_tLicense",labelPosition:"top",suggest:"shipping/shippingManifest_data.php?type=3",hidden:0},
                                        
                                    ]   
                                },
                                {
                                    cols:
                                    [
                                      
                                    ]   
                                },
                                {
                                    cols:
                                    [ 
                                        {view:"combo",required:true,label:'Driver Name',id:"edit_shippingManifest_driver",name:"edit_driver",labelPosition:"top",suggest:"shipping/shippingManifest_data.php?type=4",hidden:0},
                                        {view:"text",required:true,label:'Planing Time',id:"edit_shippingManifest_planTime",name:"edit_planTime",labelPosition:"top",type:"time"},
                                        {view:"text",label:'Remark (หมายเหตุ)',id:"edit_shippingManifest_remark",name:"edit_remark",labelPosition:"top"}, 
                                        {view:"text",label:'ship no',id:"edit_shippingManifest_ship_no",name:"edit_ship_no",labelPosition:"top",hidden:true},  
                                    ]   
                                },
                                {
                                    cols:
                                    [
                                      
                                    ]   
                                },                                                                
                                {
                                    cols:
                                    [
                                         {view:"button", value:"OK (ตกลง)",type:"form",id:"edit_shipping_update",width:255,on:
                                            {
                                                 onItemClick:function(id)
                                                {
                                                    if ($$('shippingManifest_editHead_win_form').validate()) 
                                                    {
                                                        webix.confirm({
                                                            title: "กรุณายืนยัน",ok:"ใช่", cancel:"ไม่",text:"คุณต้องการ<font color='#ca4635'><br><b>ที่จะบันทึก</b></font><br><font color='#27ae60'><b>ใช่</b></font> หรือ <font color='#3498db'><b>ไม่</b></font>",
                                                            callback:function(res)
                                                            {
                                                                if(res)
                                                                {
                                                                    var btn = $$('edit_shipping_update'),
                                                                    obj =  $$('shippingManifest_editHead_win_form').getValues();
                                                                    btn.disable();
                                                                    $.post("shipping/shippingManifest_update.php",{obj:obj,type:1})
                                                                     .done(function( data ) 
                                                                    {                                                      
                                                                        data = eval('('+data+')');
                                                                        if(data.data.length)
                                                                        {
                                                                            btn.enable();
                                                                            var ar = data.data[0];
                                                                            $$('shippingManifest_date').setValue(ar[10]);
                                                                            $$('shippingManifest_trip').setValue(ar[4]);
                                                                            $$('shippingManifest_remark').setValue(ar[8]);
                                                                            $$('shippingManifest_work').setValue(ar[3]);
                                                                            $$('shippingManifest_shipTo').setValue(ar[2]);
                                                                            $$('shippingManifest_planTime').setValue(ar[9]);
                                                                            
                                                                            comboSet('shippingManifest_route',ar[7]);
                                                                            comboSet('shippingManifest_tLicense',ar[5]);
                                                                            comboSet('shippingManifest_driver',ar[6]);

                                                                            $$('edit_cancel').callEvent("onItemClick", []);
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        })
                                                    }
                                                }
                                            }
                                        },
                                        {view:"button", value:"Cancel (ยกเลิก)",type:"danger",id:"edit_cancel",width:255,on:
                                            {
                                                 onItemClick:function(id)
                                                {
                                                    $$('shippingManifest_editHead_win').hide();
                                                    $$('shippingManifest_editHead_win_form').clearValidation();
                                                    $$('shippingManifest_editHead_win_form').clear();
                                                }
                                            }
                                        },{},{}
                                    ]
                                }     
                            ]
                        }
                    });

                    window['shippingManifest_FN1'] = function()  
                    {               
                        $$('shippingManifest_btnGroup1').show();
                        $$('shippingManifest_btnGroup2').hide();
                        $$('shippingManifest_btnGroup3').hide();
                        $$('shippingManifest_tabel').hide();                                        
                    };

                    window['shippingManifest_FN2'] = function()  
                    {               
                        $$('shippingManifest_btnGroup1').hide();
                        $$('shippingManifest_btnGroup2').show();
                        $$('shippingManifest_btnGroup3').show();
                        $$('shippingManifest_tabel').show();                                        
                    };

                    window['shippingManifest_FN3'] = function()  
                    {                              
                        $$('shippingManifest_date').disable();
                        $$('shippingManifest_work').disable();
                        $$('shippingManifest_shipTo').disable();
                        $$('shippingManifest_trip').disable();
                        $$('shippingManifest_route').disable();
                        $$('shippingManifest_tLicense').disable();
                        $$('shippingManifest_driver').disable();
                        $$('shippingManifest_planTime').disable();
                        $$('shippingManifest_remark').disable();                                 
                    };

                     window['shippingManifest_FN4'] = function()  
                    {                              
                        $$('shippingManifest_date').enable();
                        $$('shippingManifest_work').enable();
                        $$('shippingManifest_shipTo').enable();
                        $$('shippingManifest_trip').enable();
                        $$('shippingManifest_route').enable();
                        $$('shippingManifest_tLicense').enable();
                        $$('shippingManifest_driver').enable();
                        $$('shippingManifest_planTime').enable();
                        $$('shippingManifest_remark').enable();                                  
                    };

                    window['shippingManifest_init'] = function()
                    {
                        var doctype = $$('shippingManifest_doctype'),list = doctype.getPopup().getList();
                        doctype.blockEvent();
                        list.clearAll();
                        doctype.setValue('');
                        webix.ajax().post("shipping/shippingManifest_data.php", {type:1}, function(text, data)
                        {
                            var data=eval('('+text+')'),htmlNode=$(doctype.$view);
                            list.parse(data);
                            doctype.unblockEvent();
                            if(data.length>0)
                            {
                                doctype.enable();
                                if(!htmlNode.hasClass('webix_invalid')) htmlNode.addClass('webix_invalid');
                                webix.message({ type:"error",expire:7000,text:'คุณมีงานค้างโปรดตรวจสอบ'});
                            }else
                            {
                                doctype.disable();
                                if(htmlNode.hasClass('webix_invalid')) htmlNode.removeClass('webix_invalid');
                            }
                        });
                    };

                    shippingManifest_init();
                    window.shippingManifest_FN1();
                }
            }
        }
    };
};