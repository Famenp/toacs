var header_viewShipping = function()
{
	return {
        view: "scrollview",
        scroll: "native-y",
        id:"header_viewShipping",
        body: 
        {
        	id:"viewShipping_id",
        	type:"clean",
    		rows:
    		[
    		   {
                    view:"form",
                    paddingY:20,
                    id:"viewShipping_form1",
                    elements:
                    [
                        {                            
                            cols:                                    
                                [    
                                    {  
                                        view:"datepicker", label:"Start date",value:new Date(), name:"viewShipping_datestart" ,labelPosition:"top", stringResult:true,format:webix.Date.dateToStr("%Y-%m-%d"),width:250
                                    },
                                    {
                                        view:"datepicker", label:"End date",value:new Date(), name:"viewShipping_dateend",labelPosition:"top", stringResult:true,format:webix.Date.dateToStr("%Y-%m-%d"),width:250
                                    },
                                    /*{  
                                        view:"text",label:'Part Supplier',name:"part_sup",id:"part_sup",labelPosition:"top", value:"",width:230
                                    },
                                    {  
                                        view:"text",label:'Part Supplier',name:"part_sup",id:"part_sup",labelPosition:"top", value:"",width:230
                                    },
                                    {  
                                        view:"text",label:'Part Supplier',name:"part_sup",id:"part_sup",labelPosition:"top", value:"",width:230
                                    },           */                       
                                    {
                                         
                                    }                               
                                ]
                        },
                        {
                            cols:
                            [
                                {                                             
                                    view:"button", label:"Find (ค้นหา)", type:"form",width:230,id:"viewShipping_find",
                                    on:
                                    {                                                    
                                        onItemClick:function(id, e)
                                        {
                                            if ($$('viewShipping_form1').validate())
                                            {
                                                var btn = $$('viewShipping_find'),
                                                obj =  $$('viewShipping_form1').getValues();
                                                btn.disable();                                                     
                                                $.post("shipping/viewShipping_data.php",{obj:obj,type:1})
                                                .done(function( data )
                                                {
                                                    btn.enable();
                                                    data = eval('('+data+')');
                                                    if(data.ch == 1)
                                                    {
                                                        var dataT1 = $$('viewShipping_dataT1');
                                                        dataT1.clearAll();
                                                        dataT1.parse(data.data,"jsarray");
                                                        dataT1.getPager().render();
                                                    }
                                                    else if (data.ch == 2)
                                                    {
                                                        webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});
                                                    }                                                                    
                                                });
                                            }
                                            else{webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:"กรุณากรอกข้อมูล",callback:function(){}});}
                                        }                                                    
                                    }                                                   
                                },
                                {}
                            ]

                        },
                        {
                            paddingX:2,
                            rows:
                            [
                                {  
                                    view:"datatable",
                                    id:"viewShipping_dataT1",
                                    navigation:true,
                                    datatype:"jsarray",
                                    css:"my_style",
                                    pager:"viewShipping_pagerA",
                                    autoheight:true,
                                    resizeColumn:true,
                                    leftSplit:0,
                                  
                                    columns:
                                    [                                  
                                        { id:"data0",header:"No",css:"rank",width:50},
                                        { id:"data1",header:[{text :"Ship No.",css:{"text-align":"center"}},{content:"textFilter"}],css:"rank",width:200},              
                                        { id:"data2",header:[{text :"DOC GTN",css:{"text-align":"center"}},{content:"textFilter"}],css:"rank",width:200},
                                        { id:"data3",header:"Ship From",css:"rank",width:150},
                                        { id:"data4",header:"Ship To",css:"rank",width:150},                              
                                        { id:"data5",header:"Working Ship",css:"rank",width:150},
                                        { id:"data6",header:"Trip No.",css:"rank",width:90},
                                        { id:"data7",header:"Truck License",css:"rank",width:190},
                                        { id:"data8",header:"Truck Type",css:"rank",width:150},
                                        { id:"data9",header:"Driver Name",css:"rank",width:200},
                                        { id:"data10",header:"Route Zone",css:"rank",width:100},
                                        { id:"data11",header:"Plan Time",css:"rank",width:150},
                                        { id:"data12",header:"ABT IN",css:"rank",width:150}, 
                                        { id:"data13",header:"ABT OUT",css:"rank",width:150}, 
                                        { id:"data14",header:"HONDA IN",css:"rank",width:150}, 
                                        { id:"data15",header:"HONDA OUT",css:"rank",width:150},
                                        { id:"data16",header:"Status",css:"rank",width:130}, 
                                        { id:"data17",header:"Create Date",css:"rank",width:200}, 
                                       
                                    ],
                                }
                                ,
                                {
                                    type:"wide",
                                        cols:
                                        [
                                            {
                                                view:"pager", id:"viewShipping_pagerA",
                                                template:function(data, common){
                                                    var start = data.page * data.size
                                                    ,end = start + data.size;
                                                    if(data.count == 0) start = 0;
                                                    else start += 1;
                                                    if(end >= data.count) end = data.count;
                                                    var html = "<b>showing "+(start)+" - "+end+" total "+data.count+" </b>";
                                                    return common.first()+common.prev()+" "+html+" "+common.next()+common.last();
                                                },
                                               
                                            }
                                        ]
                                                        
                                }

                            ]
                        }
                    ]
                    
                }
            ],on:
            {
                onHide:function()
                {
                    
                },
                onShow:function()
                {

                },
                onAddView:function()
                {

                }
            }
        }
    };
};