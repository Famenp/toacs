<?php
// include('project.php');
$path = $_SERVER['PHP_SELF'];
      $getRootUrl = "http://".$_SERVER['HTTP_HOST'].substr($path, 0,strpos($path, '/', 1));
      echo 'var rootUrl ="'.$getRootUrl.'/"
      ,mainMenu = 
      [
          { view:"button", css:"logo1",align:"left",width:200, value:"",click:"window.open(rootUrl,'.'\'_self\''.');"},
      { view:"menu",
                data:[
          { id:"1",value:"1.Inbound",config:{width:220}, submenu:["1.1 ASN Receipt",{value:"1.2 Manual Receipt", submenu:["1.2.1 Gen Pack","1.2.2 Add Part"]},"1.3 Fullcase Transfer","1.4 Cancel Receipt"]},
          { id:"2",value:"2.Unpack",config:{width:220}, submenu:["2.1 Unpack","2.2 Repack By Robbing","2.4 Picking Ticket Transfer","2.5 Min Max","2.6 Box In Location","2.7 Repack Picking Ticket","2.8 Check FIFO"]},
          { id:"3",value:"3.Outbound",config:{width:280},
            submenu:["3.1 D-Note Issued(Fullcase)","3.2 D-Note Issued(Picking Ticket)","3.3 Manual Issued(Fullcase)","3.4 Manual Issued(Picking Ticket)","3.5 Confirm Issued","3.6 Cancel Issued(Picking Ticket)","3.7 Cancel Issued(Fullcase)"],width:280},
          { id:"4",value:"4.Inventory",config:{width:200},submenu:["4.1 Onhand","4.2 Lock Item","4.3 Unlock Item","4.4 Lock Picking Ticket"]},
          { id:"5",value:"5.View Data",submenu:["5.1 ASN","5.2 D-Note","5.3 Transaction","5.4 File Interface","5.5 Item Log"]},
          { id:"6",value:"6.Reprint",submenu:["6.1 GRN","6.2 D-Van","6.3 Packing Sheet","6.4 Label Pack","6.5 Unpack","6.6 GTN"]},
          { id:"7",value:"7.Master Data",submenu:["7.1 Warehouse","7.2 Locaion","7.3 Part Master"]},
          { id:"8",value:"8.Operation",submenu:["8.1 Add User","8.2 Edit User","8.3 Close D-Note"]},
          { id:"9",value:"9.Profile",config:{width:200},submenu:["9.1 Change Password","9.2 Logout"]}
        ],
        on:{
          onMenuItemClick:function(id){
            var _id = this.getMenuItem(id).value;
            if(_id == "1.1 ASN Receipt") window.open(rootUrl+"inbound/asn_receip.php","_self");
            else if(_id == "1.3 Fullcase Transfer") window.open(rootUrl+"inbound/fullcaseTransfer.php","_self");
            else if(_id == "1.4 Cancel Receipt") window.open(rootUrl+"inbound/cancel_grn.php","_self");
            else if(_id == "1.2.1 Gen Pack") window.open(rootUrl+"inbound/manual_receip.php","_self");
            else if(_id == "1.2.2 Add Part") window.open(rootUrl+"inbound/manual_receip_addpart.php","_self");
            else if(_id == "2.1 Unpack") window.open(rootUrl+"unpack/unpack.php","_self");
            else if(_id == "2.2 Repack By Robbing") window.open(rootUrl+"unpack/unpack_Robbing.php","_self");
            else if(_id == "2.4 Picking Ticket Transfer") window.open(rootUrl+"unpack/ptTransfer.php","_self");
            else if(_id == "2.5 Min Max") window.open(rootUrl+"unpack/minmax.php","_self");
            else if(_id == "2.6 Box In Location") window.open(rootUrl+"unpack/box_in_location.php","_self");
            else if(_id == "2.7 Repack Picking Ticket") window.open(rootUrl+"unpack/unpack_Robbing_reuse.php","_self");
            else if(_id == "2.8 Check FIFO") window.open(rootUrl+"unpack/checkFIFO.php","_self");
            else if(_id == "3.1 D-Note Issued(Fullcase)") window.open(rootUrl+"outbound/dNoteIss_full.php","_self");
            else if(_id == "3.2 D-Note Issued(Picking Ticket)") window.open(rootUrl+"outbound/dNoteIss_pick.php","_self");
            else if(_id == "3.3 Manual Issued(Fullcase)") window.open(rootUrl+"outbound/manaulIss_full.php","_self");
            else if(_id == "3.4 Manual Issued(Picking Ticket)") window.open(rootUrl+"outbound/manaulIss_pick.php","_self");
            else if(_id == "3.6 Cancel Issued(Picking Ticket)") window.open(rootUrl+"outbound/cancel_gtn_pick.php","_self");
            else if(_id == "3.7 Cancel Issued(Fullcase)") window.open(rootUrl+"outbound/cancel_gtn_full.php","_self");
            else if(_id == "4.1 Onhand") window.open(rootUrl+"inventory/onhandNormal.php","_self");
            else if(_id == "4.2 Lock Item") window.open(rootUrl+"inventory/lockItem.php","_self");
            else if(_id == "4.3 Unlock Item") window.open(rootUrl+"inventory/unlockItem.php","_self");
            else if(_id == "4.4 Lock Picking Ticket") window.open(rootUrl+"inventory/lockPick.php","_self");
            else if(_id == "5.1 ASN") window.open(rootUrl+"view/asnData.php","_self");
            else if(_id == "5.2 D-Note") window.open(rootUrl+"view/dNoteData.php","_self");
            else if(_id == "5.3 Transaction") window.open(rootUrl+"view/tranasac.php","_self");
            else if(_id == "5.4 File Interface") window.open(rootUrl+"view/fileinterface.php","_self");
            else if(_id == "5.5 Item Log") window.open(rootUrl+"view/itemLog.php","_self");
            else if(_id == "6.1 GRN") window.open(rootUrl+"reprint/reprint_grn2.php","_self");
            else if(_id == "6.2 D-Van") window.open(rootUrl+"reprint/reprint_dVan.php","_self");
            else if(_id == "6.3 Packing Sheet") window.open(rootUrl+"reprint/reprint_caseTag.php","_self");
            else if(_id == "6.4 Label Pack") window.open(rootUrl+"reprint/reprint_labelTag.php","_self");
            else if(_id == "6.5 Unpack") window.open(rootUrl+"reprint/reprint_unpack.php","_self");
            else if(_id == "6.6 GTN") window.open(rootUrl+"reprint/reprint_gtn2.php","_self");
            else if(_id == "7.3 Part Master") window.open(rootUrl+"masterData/partMaster.php","_self");
            else if(_id == "8.3 Close D-Note") window.open(rootUrl+"operation/close_dNoteData.php","_self");
            else if(_id == "9.1 Change Password") window.open(rootUrl+"profile/changePass.php","_self");
            else if(_id == "9.2 Logout") window.open(rootUrl+"logOut.php","_self");
          }
        },
                type:{
                    subsign:true
                }
            }
    ];';
?>
