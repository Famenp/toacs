var header_goodsReceipt = function()
{
    return {
        view: "scrollview",
        scroll: "native-y",
        id:"header_goodsReceipt",
        body: 
        {
            id:"goodsReceipt_id",
            type:"clean",
            rows:
            [
                {
                    view:"form",
                    paddingY:20,
                    id:"goodsReceipt_form1",
                    elements:
                    [
                        {
                            rows:
                            [
                                {
                                    id:"gen_grnReceipt_1",
                                    cols:
                                    [
                                        {
                                            view:"combo",yCount:"10",suggest:"inbound/goodsReceipt_data.php?type=1",label:'GRN No. (งานค้าง)',labelPosition:"top",labelWidth:125,id:"goodsReceipt_doc_grn",name:"goodsReceipt_doc_grn",width:250,value:"",
                                            on:
                                            {
                                                onChange: function(value)
                                                {

                                                    if(value.length>0) this.disable();else this.enable();
                                                    if ($$('goodsReceipt_form1').validate())
                                                    {
                                                        obj =  $$('goodsReceipt_form1').getValues();
                                                        $.post("inbound/goodsReceipt_data_2.php", {obj:obj,type:1})
                                                        .done(function( data ) 
                                                        {
                                                            var dataT1 = $$("receive_dataT1");
                                                            var data = eval('('+data+')');
                                                            if(data.ch == 1)
                                                            { 
                                                                dataT1.clearAll();
                                                                dataT1.parse(data.data,"jsarray");
                                                                receive_2();
                                                            }
                                                            else if (data.ch == 2) 
                                                            {
                                                                receive_3();
                                                            }                                                           
                                                        });

                                                    }

                                                }
                                            }
                                        },{width:10},                                      
                                        {view:"text",label:'DCD No.',id:"goodsReceipt_dcd_no",name:"goodsReceipt_dcd_no",labelPosition:"top",width:350,labelWidth:75,required:true},
                                        {view:"text",label:'Total Qty.',id:"goodsReceipt_total_qty",name:"goodsReceipt_total_qty",labelPosition:"top",width:120,labelWidth:75,required:true},
                                        {
                                            rows:
                                            [
                                                {

                                                },
                                                {                                          
                                                    view:"button",icon:"users",value:"Scan DCD",id:"scan_dcd",css:"bt_blue",width:180,on:
                                                    {
                                                        onItemClick:function(id, e)
                                                        {    
                                                            if ($$('goodsReceipt_form1').validate())
                                                            {
                                                                receive_4();
                                                            }
                                                            else ($$('goodsReceipt_dcd_no').focus())
                                                        }                             
                                                    }                     
                                                },
                                            ]
                                        },           
                                                                              
                                                                                                                                                                                          
                                        {

                                        }
                                    ]
                                },
                                {
                                    id:"create_grn",
                                    cols:
                                    [
                                        {                  
                                            view:"button",icon:"users",value:"Create GRN",id:"createGRN",css:"bt_blue",width:250,on:
                                            {
                                                onItemClick:function(id, e)
                                                {    
                                                    if ($$('goodsReceipt_form1').validate())
                                                    {
                                                        var btn = $$('createGRN'),
                                                        obj =  $$('goodsReceipt_form1').getValues();
                                                        btn.disable();
                                                     
                                                        $.post("inbound/goodsReceipt_insert.php",{obj:obj,type:1})
                                                        .done(function( data )
                                                        {
                                                            btn.enable();
                                                            data = eval('('+data+')');
                                                            if(data.ch == 1)
                                                            {
                                                                comboSet('goodsReceipt_doc_grn',data.data)                                                                       
                                                            }          
                                                        });
                                                    }                                                                                   
                                                }
                                            }
                                        },                                  
                                        {}              
                                    ]
                                },                                
                                {
                                    id:"gen_Receipt_2",
                                    cols:
                                    [
                                                                                                                                                                                                          {                  
                                            view:"button",icon:"users",type:"danger",value:"Cancel (ยกเลิกเอกสาร)",id:"cancel_grn",css:"bt_blue",width:250,
                                            on:
                                            {
                                                onItemClick:function(id, e)
                                                {                                                                                                         
                                                    webix.confirm({
                                                    title: "กรุณายืนยัน",ok:"ใช่", cancel:"ไม่",text:"คุณต้องการ<font color='#ca4635'><br><b>ที่จะยกเลิกเอกสาร</b></font><br><font color='#27ae60'><b>ใช่</b></font> หรือ <font color='#3498db'><b>ไม่</b></font>",
                                                    callback:function(res)
                                                    {
                                                        if(res)
                                                        {
                                                            var btn = $$('cancel_grn'),
                                                            obj =  $$('goodsReceipt_form1').getValues();
                                                            btn.disable();
                                                            $.post("inbound/goodsReceipt_delete.php",{obj:obj,type:1})
                                                            .done(function( data ) 
                                                            {                                                      
                                                                btn.enable();
                                                                $$('goodsReceipt_part_no').setValue('');
                                                                $$('goodsReceipt_lot_no').setValue('');
                                                                $$('goodsReceipt_qty').setValue('');
                                                                $$('goodsReceipt_dcd_no').setValue('');
                                                                $$('goodsReceipt_total_qty').setValue('');
                                                                                                        
                                                                $$("receive_dataT1").clearAll();
                                                                        $$("receive_dataT1").getPager().render();
                                                                        Receipt_init();   
                                                                        receive_1();                                                                     

                                                                    });
                                                                }

                                                            }
                                                        })                            
                                                        
                                                }
                                            }
                                        },{width:10},
                                        {                  
                                            view:"button",icon:"users",type:"danger",value:"Clear (ล้างหน้าจอ)",id:"clear_all",css:"bt_blue",width:170,on:
                                            {
                                                onItemClick:function(id, e)

                                                {    
                                                                                                                                               
                                                    $$('goodsReceipt_part_no').setValue('');
                                                    $$('goodsReceipt_lot_no').setValue('');
                                                    $$('goodsReceipt_qty').setValue('');
                                                    $$('goodsReceipt_dcd_no').setValue('');
                                                    $$('goodsReceipt_total_qty').setValue('');
                                                    $$('createGRN').disable();
                                                                                                        
                                                    $$("receive_dataT1").clearAll();
                                                    $$("receive_dataT1").getPager().render();
                                                    
                                                    Receipt_init();
                                                    receive_1();
                                                    
                                                }
                                            }
                                        },
                                        {                  
                                            view:"button",icon:"users",type:"form",value:"Confirm DCD",id:"confirm_dcd",css:"bt_blue",width:180,on:
                                            {
                                                onItemClick:function(id, e)

                                                {       
                                                    var btn = $$('confirm_dcd'),
                                                        obj =  $$('goodsReceipt_form1').getValues();
                                                        btn.disable();
                                                     
                                                        $.post("inbound/goodsReceipt_update.php",{obj:obj,type:2})
                                                        .done(function( data )
                                                        {
                                                            btn.enable();
                                                            data = eval('('+data+')');
                                                            if(data.ch == 1)
                                                            {

                                                                $$('goodsReceipt_dcd_no').setValue('');
                                                                $$('goodsReceipt_part_no').setValue('');
                                                                $$('goodsReceipt_lot_no').setValue('');
                                                                $$('goodsReceipt_qty').setValue('');
                                                                $$('goodsReceipt_total_qty').setValue('');
                                                                receive_5();                                                                       
                                                            }
                                                            else if (data.ch == 2) 
                                                            {
                                                                webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function()
                                                                        {
                                                                            $$('goodsReceipt_part_no').setValue('');
                                                                            $$('goodsReceipt_lot_no').setValue('');
                                                                            $$('goodsReceipt_qty').setValue('');
                                                                            $$('goodsReceipt_part_no').focus();
                                                                        }});
                                                            }         
                                                        });
                                                        
                                                        

                                                        
                                                }
                                            }
                                        },                                                                                                                           
                                        {
                                        
                                        }
                                    ]
                                },                   
                                {
                                    id:"dcd_receive",
                                    cols:
                                    [
                                        {view:"text",label:'Part No.',id:"goodsReceipt_part_no",name:"goodsReceipt_part_no",labelPosition:"top",width:250},
                                        {view:"text",label:'Lot No.',id:"goodsReceipt_lot_no",name:"goodsReceipt_lot_no",labelPosition:"top",width:250},
                                        {view:"text",label:'Qty',id:"goodsReceipt_qty",name:"goodsReceipt_qty",labelPosition:"top",width:170},  
                                        {
                                            rows:
                                            [
                                                {

                                                },
                                                {
                                                    view:"button",value:"Add Part",id:"goodsReceipt_add_part",css:"bt_red",width:210,on:
                                                    {
                                                        onItemClick:function(id, e)
                                                        {
                                                            
                                                           if ($$('goodsReceipt_form1').validate())
                                                            {
                                                                var btn = $$('goodsReceipt_add_part'),
                                                                obj =  $$('goodsReceipt_form1').getValues();
                                                                btn.disable();
                                                     
                                                                $.post("inbound/goodsReceipt_insert.php",{obj:obj,type:2})
                                                                 .done(function( data )
                                                                {
                                                                    
                                                                    btn.enable();
                                                                    data = eval('('+data+')');
                                                                    if(data.ch == 1)
                                                                    {
                                                                        $$('goodsReceipt_part_no').setValue('');
                                                                        $$('goodsReceipt_lot_no').setValue('');
                                                                        $$('goodsReceipt_qty').setValue('');
                                                                        $$('goodsReceipt_part_no').focus();
                                                                        var dataT1 = $$('receive_dataT1');
                                                                        dataT1.clearAll();
                                                                        dataT1.parse(data.data,"jsarray");
                                                                        dataT1.getPager().render();
                                                                    }
                                                                    else if (data.ch == 2)
                                                                    {
                                                                        webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function()
                                                                        {
                                                                            $$('goodsReceipt_part_no').setValue('');
                                                                            $$('goodsReceipt_lot_no').setValue('');
                                                                            $$('goodsReceipt_qty').setValue('');
                                                                            $$('goodsReceipt_part_no').focus();
                                                                        }});

                                                                    }
                                                                    
                                                                });
                                                            }
                                                            
                                                      
                                                        }
                                                    }                                         
                                                }
                                            ]
                                        },
                                        {
                                            rows:
                                            [
                                                {

                                                },
                                                {
                                                    view:"button",type:"form",value:"Save (บันทึก)",id:"save_receive",css:"bt_red",width:230,on:
                                                    {
                                                        onItemClick:function(id, e)
                                                        {
                                                            var dataT1 = $$("receive_dataT1");
                                                            if(dataT1.count() >0)
                                                            {
                                                                webix.confirm(
                                                                {
                                                                    title:"<b>ข้อความจากระบบ</b>",ok:'ใช่',cancel:"ไม่",text:"คุณต้องการ<font color='#ca4635'><br><b>บันทึกข้อมูล</b></font><br><font color='#27ae60'><b>ใช่</b></font> หรือ <font color='#3498db'><b>ไม่</b></font>",callback:function(result)
                                                                    {
                                                                    if(result)
                                                                        {
                                                                            var btn = $$('save_receive'),
                                                                            obj = {doctype:$$('goodsReceipt_doc_grn').getValue().trim().toUpperCase()};
                                                                            btn.disable();
                                                                            $.post( "inbound/goodsReceipt_insert.php", {obj:obj,type:3})
                                                                            .done(function( data ) 
                                                                            {
                                                                                btn.enable();
                                                                                var data = eval('('+data+')');
                                                                                if(data.ch == 1)
                                                                                { 
                                                                                    webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});
                                                                                    $.post( "print/grn.php", {data1:data.data})
                                                                                    .done(function( data ) 
                                                                                    {
                                                                                        var data = eval('('+data+')');
                                                                                        if(data.ch == 1)
                                                                                        {
                                                                                          webix.message({ type:"default",expire:7000, text:'ปริ้น GRN สำเร็จ'});
                                                                                        }
                                                                                        else webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});
                                                                                    });
                                                                                    $$('clear_all').callEvent("onItemClick", []);
                                                                        
                                                                                }
                                                                                else if(data.ch == 2){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                                                                                else if(data.ch == 9){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                                                                                else if(data.ch == 10){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){window.open("login.php","_self");}});}                    
                                                                            });
                                                                        }
                                                                    }
                                                                });


                                                            }
                                                            else{webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:'ไม่พบข้อมูลในตาราง',callback:function(){}});}
                                                        }
                                                    }                                         
                                                }
                                            ]
                                        },
                                        {}              
                                    ]
                                }
                            ]
                        },
                        
                    ],
                    on:
                            {
                                "onSubmit":function(view,e)
                                {
                                    if(view.config.name =='goodsReceipt_total_qty')
                                    {
                                        view.blur();
                                        $$('scan_dcd').callEvent("onItemClick", []);
                                    }
                                    else if (view.config.name == 'goodsReceipt_qty') 
                                    {
                                        view.blur();
                                        $$('goodsReceipt_add_part').callEvent("onItemClick", []);
                                    }
                                    else if(webix.UIManager.getNext(view).config.type == 'line')
                                    {
                                        webix.UIManager.setFocus(webix.UIManager.getNext(webix.UIManager.getNext(view)));
                                    }
                                    else
                                    {
                                        webix.UIManager.setFocus(webix.UIManager.getNext(view));
                                    }
                                }
                            }
                    
                },
                {           
                            paddingX:2,
                            id:"tabel_id",
                            rows:
                            [
                                {  
                                    view:"datatable",
                                    id:"receive_dataT1",
                                    navigation:true,
                                    datatype:"jsarray",
                                    css:"my_style",
                                    pager:"receive_pagerA",
                                    autoheight:true,
                                    resizeColumn:true,
                                    leftSplit:0,
                                    columns:
                                    [                                  
                                        /*{ id:"data21",header:"&nbsp;",width:35,template: "<span style='cursor:pointer;' class='webix_icon fa-pencil'></span>"},*/
                                        { id:"data22",header:"&nbsp;",width:35,template: "<span style='cursor:pointer;' class='webix_icon fa-trash-o'></span>"},
                                        { id:"data0",header:"No",css:"rank",width:50},
                                        { id:"data2",header:"Doc GRN",css:"rank",width:200},
                                        { id:"data3",header:"DCD No.",css:"rank",width:200},
                                        { id:"data4",header:"Part Supplier",css:"rank",width:240},
                                        { id:"data5",header:"LOT",css:"rank",width:250},
                                        { id:"data6",header:"Box No.",css:"rank",width:120},
                                        { id:"data7",header:"Qty",css:"rank",width:120},
                                                                        
                                    ],
                                    onClick:
                                    {
                                        "fa-pencil":function(e,t)
                                        {
                                            var row = this.getItem(t),obj;
                                            $$('receive_edit').show();
                                            $$('receive_form1_edit').setValues(
                                            {
                                                dcdNo_edit:row.data3,
                                                lot_edit:row.data5,
                                                box_edit:row.data6,
                                                qty_edit:row.data7,
                                                docID_edit:row.data1,
                                                grn_edit:row.data2,
                                                                                                    
                                            });                                                                                
                                        },
                                         "fa-trash-o":function(e,t)
                                        {
                                            var r = this.getItem(t),obj;
                                            webix.confirm({
                                                 title: "กรุณายืนยัน",ok:"ใช่", cancel:"ไม่",text:"คุณต้องการ<font color='#ca4635'><br><b>ลบ</b></font><br><font color='#27ae60'><b>ใช่</b></font> หรือ <font color='#3498db'><b>ไม่</b></font>",
                                                 callback:function(res)
                                                 {
                                                    if(res)
                                                    {
                                                        $.post("inbound/goodsReceipt_delete.php",{obj:{goodsReceipt_doc_id:r.data1,goodsReceipt_doc_grn:r.data2},type:2})
                                                        .done(function( data ) 
                                                        {
                                                            data = eval('('+data+')');
                                                            if(data.ch==1)
                                                            {
                                                                var dataT1 = $$('receive_dataT1');
                                                                dataT1.clearAll();
                                                                dataT1.parse(data.data1,"jsarray");
                                                            }
                                                            else if(data.ch == 2){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                                                            else if(data.ch == 9){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                                                            else if(data.ch == 10){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){window.open("login.php","_self");}});}
                                                        });
                                                    }
                                                 }
                                            });
                                        }
                                    }
                                }
                                ,
                                {
                                    type:"wide",
                                        cols:
                                        [
                                            {
                                                view:"pager", id:"receive_pagerA",
                                                template:function(data, common){
                                                    var start = data.page * data.size
                                                    ,end = start + data.size;
                                                    if(data.count == 0) start = 0;
                                                    else start += 1;
                                                    if(end >= data.count) end = data.count;
                                                    var html = "<b>showing "+(start)+" - "+end+" total "+data.count+" </b>";
                                                    return common.first()+common.prev()+" "+html+" "+common.next()+common.last();
                                                },
                                                size:10,
                                                group:5 
                                            }
                                        ]
                                                        
                                }

                            ]
                        }
            ],on:
            {
                onHide:function()
                {
                    
                },
                onShow:function()
                {

                },
                onAddView:function()
                {    
                    webix.ui(
                    {
                        view:"window",id:"receive_edit",modal:1,move:1,
                        head:{
                                view:"toolbar", margin:-4, cols:[
                                    {view:"label", label: "Edit (แก้ไขข้อมูล)"},
                                    { view:"icon", icon:"times-circle", css:"alter",
                                        click:"$$('receive_edit').hide();"}                                  
                                                    
                                    ]
                            },
                        top:50,position:"center",
                        body:
                        {
                            view:"form", scroll:false,id:"receive_form1_edit",width:700,
                            elements:
                            [
                                {
                                    cols:
                                    [
                                        {view:"text",label:'DCD No.',required:true,name:"dcdNo_edit",id:"dcdNo_edit",labelPosition:"top", value:"",width:300},
                                        {view:"text",label:'LOT',required:true,name:"lot_edit",id:"lot_edit",labelPosition:"top", value:"",width:300},
                                       
                                    ]  
                                },                                    
                                {
                                    cols:
                                    [
                                        
                                    ]  
                                },
                                {
                                    cols:
                                    [
                                       
                                        {view:"text",label:'BOX',required:true,name:"box_edit",id:"box_edit",labelPosition:"top", value:"",width:300},
                                        {view:"text",label:'Qty',required:true,name:"qty_edit",id:"qty_edit",labelPosition:"top", value:"",width:300},
                                        {view:"text",label:'docid',name:"docID_edit",id:"docID_edit",labelPosition:"top", value:"",width:50,hidden:true},
                                        {view:"text",label:'grnid',name:"grn_edit",id:"grn_edit",labelPosition:"top", value:"",width:50,hidden:true},
                                       
                                    ]   
                                },
                                                  
                                {
                                    cols:
                                    [
                                         {view:"button", value:"OK (ตกลง)",type:"form",id:"Edit_receive",on:
                                            {
                                                 onItemClick:function(id)
                                                {
                                                    if ($$('receive_form1_edit').validate()) 
                                                    {
                                                        webix.confirm({
                                                            title: "กรุณายืนยัน",ok:"ใช่", cancel:"ไม่",text:"คุณต้องการ<font color='#ca4635'><br><b>ที่จะบันทึก</b></font><br><font color='#27ae60'><b>ใช่</b></font> หรือ <font color='#3498db'><b>ไม่</b></font>",
                                                            callback:function(res)
                                                            {
                                                                if(res)
                                                                {
                                                                    var btn = $$('Edit_receive'),
                                                                    obj =  $$('receive_form1_edit').getValues();
                                                                    btn.disable();
                                                                    $.post("inbound/goodsReceipt_update.php",{obj:obj,type:1})
                                                                     .done(function( data ) 
                                                                    {                                                      
                                                                         btn.enable();
                                                                          var data = eval('('+data+')');
                                                                          if(data.ch == 1)
                                                                          {
                                                                              var dataT1 = $$("receive_dataT1");
                                                                              dataT1.clearAll();
                                                                              dataT1.parse(data.data,"jsarray");
                                                                              $$('receive_edit_cancel').callEvent("onItemClick", []);
                                                                          }
                                                                    });
                                                                }
                                                            }
                                                        })
                                                    }
                                                }
                                            }
                                        },
                                        {view:"button", value:"Cancel (ยกเลิก)",type:"danger",id:"receive_edit_cancel",on:
                                            {
                                                 onItemClick:function(id)
                                                {
                                                    $$('receive_edit').hide();
                                                    $$('receive_form1_edit').clear();
                                                }
                                            }
                                        },{},{}
                                    ]
                                }     
                            ]
                        }
                    });

                    window['receive_1'] = function()  
                    {
                        $$('goodsReceipt_doc_grn').setValue('');
                        $$('dcd_receive').hide();
                        $$('goodsReceipt_dcd_no').enable();
                        $$('goodsReceipt_dcd_no').hide();
                        $$('scan_dcd').hide();
                        $$('create_grn').show();
                        $$('cancel_grn').hide();
                        $$('tabel_id').hide();
                        $$('clear_all').hide();
                        $$('confirm_dcd').hide();
                        $$('goodsReceipt_total_qty').hide();
                        $$('goodsReceipt_total_qty').enable();
                        
                    };

                    window['receive_2'] = function()  
                    {
                        $$('dcd_receive').show();
                        $$('goodsReceipt_dcd_no').show();
                        $$('scan_dcd').show();
                        $$('create_grn').hide();
                        $$('cancel_grn').show();
                        $$('tabel_id').show();
                        $$('clear_all').show();
                        $$('confirm_dcd').hide();
                        $$('goodsReceipt_dcd_no').focus();
                        $$('goodsReceipt_total_qty').show();                       
                    };

                    window['receive_3'] = function()  
                    {
                        $$('dcd_receive').hide();
                        $$('goodsReceipt_dcd_no').show();
                        $$('scan_dcd').show();
                        $$('create_grn').hide();
                        $$('cancel_grn').show();
                        $$('tabel_id').hide();
                        $$('clear_all').show();
                        $$('confirm_dcd').hide();
                        $$('goodsReceipt_dcd_no').focus();
                        $$('goodsReceipt_total_qty').show();
                        $$('goodsReceipt_total_qty').enable();                       
                    };

                    window['receive_4'] = function()  
                    {
                        $$('dcd_receive').show();
                        $$('goodsReceipt_dcd_no').show();
                        $$('goodsReceipt_dcd_no').disable();
                        $$('scan_dcd').hide();
                        $$('create_grn').hide();
                        $$('cancel_grn').show();
                        $$('tabel_id').show();
                        $$('clear_all').show();
                        $$('confirm_dcd').show();
                        $$('goodsReceipt_part_no').focus(); 
                        $$('goodsReceipt_total_qty').show();
                        $$('goodsReceipt_total_qty').disable();                      
                    };

                    window['receive_5'] = function()  
                    {
                        $$('dcd_receive').show();
                        $$('goodsReceipt_dcd_no').show();
                        $$('goodsReceipt_dcd_no').enable();
                        $$('scan_dcd').show();
                        $$('create_grn').hide();
                        $$('cancel_grn').show();
                        $$('tabel_id').show();
                        $$('clear_all').show();
                        $$('confirm_dcd').hide();
                        $$('goodsReceipt_dcd_no').focus(); 
                        $$('goodsReceipt_total_qty').show();
                        $$('goodsReceipt_total_qty').enable();                      
                    };
            
                    window['Receipt_init'] = function()
                    {
                        var doctype = $$('goodsReceipt_doc_grn'),list = doctype.getPopup().getList();
                        doctype.blockEvent();
                        list.clearAll();
                        doctype.setValue('');
                        webix.ajax().post("inbound/goodsReceipt_data.php", {type:1}, function(text, data)
                        {
                            var data=eval('('+text+')'),htmlNode=$(doctype.$view);
                            list.parse(data);
                            doctype.unblockEvent();
                            if(data.length>0)
                            {
                                doctype.enable();
                                $$('createGRN').disable();
                                if(!htmlNode.hasClass('webix_invalid')) htmlNode.addClass('webix_invalid');
                                webix.message({ type:"error",expire:7000,text:'คุณมีงานค้างโปรดตรวจสอบ'});

                            }else
                            {
                                $$('createGRN').enable();
                                doctype.disable();
                                
                                if(htmlNode.hasClass('webix_invalid')) htmlNode.removeClass('webix_invalid');
                            }
                        });
                    };

                    Receipt_init();                   
                    /*gen_grn();*/
                    /*genPO_loadPO();*/
                    window.receive_1();
                }
            }
        }
    };
};