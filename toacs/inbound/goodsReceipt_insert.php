<?php
	if(!ob_start("ob_gzhandler")) ob_start();
	header('Expires: Sun, 01 Jan 2014 00:00:00 GMT');
	header('Cache-Control: no-store, no-cache, must-revalidate');
	header('Cache-Control: post-check=0, pre-check=0', FALSE);
	header('Pragma: no-cache');
	include('../start.php');
	session_start();
	include('../php/connection.php');
	$cBy = $_SESSION['xxxID'];
	$fName = $_SESSION['xxxFName'];
	$obj  = $_POST['obj'];
	$type  = intval($_POST['type']);
	$user_ip = $_SERVER['REMOTE_ADDR'];
	if($type == 1)
	{
		$mysqli->autocommit(FALSE);
 		try 
			{
				if(!$re = $mysqli->query("SELECT func_GenRuningNumber('grn',0) t")) throw new Exception('Error Code 1'); 
				if($re->num_rows == 0) throw new Exception('Error Code 2'); 
				$autoID = $re->fetch_object()->t;

				$sql = "INSERT INTO tbl_receive_header(Doc_No,Create_by) values('$autoID','$cBy')";
				if(!$mysqli->query($sql)) throw new Exception('Error Code 3'); 
				if($mysqli->affected_rows == 0) throw new Exception('Error Code 4');

				$logdesc = "Create ".$autoID." Complete";
				$sqllog = logpost($user_ip,$cBy,$logdesc);
				if(!$mysqli->query($sqllog)) throw new Exception('Error log post 001'); 
				if($mysqli->affected_rows == 0) throw new Exception('Error log post 002');
				 
				$mysqli->commit();
				echo '{ch:1,data:"'.$autoID.'"}';
			} 

		catch (Exception $e) 
			{
				$mysqli->rollback();
		  		echo '{ch:2,data:"'.$e->getMessage().'"}';
			}		
	}
	else if($type == 2)
	{
		$doc_grn = $mysqli->real_escape_string(trim(strtoupper($obj['goodsReceipt_doc_grn'])));
		$doc_dcd = $mysqli->real_escape_string(trim(strtoupper($obj['goodsReceipt_dcd_no'])));
		$part_no = $mysqli->real_escape_string(trim(strtoupper($obj['goodsReceipt_part_no'])));
		$lot_no = $mysqli->real_escape_string(trim(strtoupper($obj['goodsReceipt_lot_no'])));
		$qty = $mysqli->real_escape_string(trim(strtoupper($obj['goodsReceipt_qty'])));

		if ($part_no == "" OR $lot_no == "" OR $qty =="") 
		{
			echo '{ch:2,data:"กรอกข้อมูลไม่ครบ"}';	
			exit();
		}
	
			$part_array  = $part_no;
			$part_cut = explode("|", $part_array);
			if(sizeof($part_cut)!=4){echo '{ch:2,data:"ข้อมูลไม่ถูกต้อง"}';	exit();}
			if ($part_cut[0]!="M") {echo '{ch:2,data:"ข้อมูลไม่ถูกต้อง"}';exit();}
			$part_sup_no  = $part_cut[1];
				
			$lot_array  = $lot_no;
			$lot_cut = explode("|", $lot_array);
			if(sizeof($lot_cut)!=3){echo '{ch:2,data:"ข้อมูลไม่ถูกต้อง"}';	exit();}
			if ($lot_cut[0]!="L") {echo '{ch:2,data:"ข้อมูลไม่ถูกต้อง"}';exit();}
			$lot = $lot_cut[1];
			$box = $lot_cut[2];

			$qty_array  = $qty;
			$qty_cut = explode("|", $qty_array);
			if(sizeof($qty_cut)!=2){echo '{ch:2,data:"ข้อมูลไม่ถูกต้อง"}';	exit();}
			if ($qty_cut[0]!="Q") {echo '{ch:2,data:"ข้อมูลไม่ถูกต้อง"}';exit();}
			$qty_save = $qty_cut[1];

			$mysqli->autocommit(FALSE);
		try 
			{

				if(!$re1 = $mysqli->query("SELECT part_id,put_loc,pick_loc FROM tbl_partmaster 
											where part_supplier ='$part_sup_no' limit 1;"))
				{echo '{ch:2,data:"Error Code 1"}';$mysqli->close();}
				if($re1->num_rows == 0){echo '{ch:2,data:"part No. ไม่มีข้อมูลในระบบ"}';$mysqli->close();exit();}
				$row = $re1->fetch_object();
				$pari_id = $row->part_id;
				$put_loc = $row->put_loc;
				$pick_loc = $row->pick_loc;

				if(!$re1 = $mysqli->query("SELECT lot,box FROM tbl_receive 
											where lot ='$lot' and box = '$box' limit 1;"))
				{echo '{ch:2,data:"Error Code 1"}';$mysqli->close();}
				if($re1->num_rows > 0){echo '{ch:2,data:"เลข lot นี้สแกนรับเข้ามาในระบบแล้ว"}';$mysqli->close();exit();}
			


				if(!$re2 = $mysqli->query("SELECT ID FROM tbl_receive_header where Doc_no = '$doc_grn' limit 1;"))
				{echo '{ch:2,data:"Error Code 1"}';$mysqli->close();}
				if($re2->num_rows == 0){echo '{ch:2,data:"part No. ไม่มีข้อมูลในระบบ"}';$mysqli->close();exit();}
				$row = $re2->fetch_object();
				$docGrn_id = $row->ID;

				if(!$re2 = $mysqli->query("SELECT qty_after FROM tbl_receive WHERE Part_ID = '$pari_id' ORDER BY ID DESC limit 1"))
				{echo '{ch:2,data:"Error Code 1"}';$mysqli->close();}
				if($re2->num_rows == 0)
					{
						if(!$re1 = $mysqli->query("SELECT qty_onhand FROM tbl_partonhand where part_id ='$pari_id' limit 1;"))
							{echo '{ch:2,data:"Error Code 1"}';$mysqli->close();}
							if($re1->num_rows == 0){echo '{ch:2,data:"part No. ไม่มีข้อมูลในระบบ"}';$mysqli->close();exit();}
							$row = $re1->fetch_object();
							$qty_before = $row->qty_onhand;		

					}
				else
				{
					$row = $re2->fetch_object();
					$qty_before = $row->qty_after;
				}
							
				$qty_after = $qty_before + $qty_save;

				$sql = "INSERT INTO tbl_receive (Doc_ID,DCD_NO,Part_ID,LOT,box,Rec_QTY,Put_loc,Pick_loc,qty_before,qty_after,
				Create_date,user_id) VALUES ('$docGrn_id','$doc_dcd','$pari_id','$lot','$box','$qty_save','$put_loc',
				'$pick_loc','$qty_before','$qty_after',NOW(),'$cBy')";
				if(!$mysqli->query($sql)) throw new Exception('Error Code 2');			
				$mysqli->commit();


				if($re3 = $mysqli->query("SELECT t1.id,t2.doc_no,dcd_no,t3.part_supplier,LOT,box,rec_qty FROM tbl_receive t1
										  LEFT JOIN tbl_receive_header t2 ON t1.doc_id = t2.id
										  LEFT JOIN tbl_partmaster t3 ON t1.part_id = t3.part_id
										  WHERE t1.Doc_ID = '$docGrn_id'"))
				{
					if($re3->num_rows >0)
						{
							echo '{"ch":1,"data":';
							toArrayStringAddNumberRow($re3,1);
							echo '}';
						}
					else echo '{ch:2,data:"ไม่พบข้อมูลในระบบ"}';
				}
				else echo '{ch:2,data:"โคดผิด"}';
			} 

		catch (Exception $e) 
			{
				$mysqli->rollback();
		  		echo '{ch:2,data:"'.$e->getMessage().'"}';
			}
	}
	else if ($type ==  3) 
	{
		$doc_grn = $mysqli->real_escape_string(trim(strtoupper($obj['doctype'])));

		if(!$re2 = $mysqli->query("SELECT ID FROM tbl_receive_header where Doc_no = '$doc_grn' limit 1;"))
		{echo '{ch:2,data:"Error Code 1"}';$mysqli->close();}
		if($re2->num_rows == 0){echo '{ch:2,data:"part No. ไม่มีข้อมูลในระบบ"}';$mysqli->close();exit();}
		$row = $re2->fetch_object();
		$docGrn_id = $row->ID;

	if(!$re2 = $mysqli->query("SELECT Doc_ID,DCD_status FROM tbl_receive WHERE Doc_ID ='$docGrn_id' GROUP BY Doc_ID,DCD_status"))
		{echo '{ch:2,data:"Error Code 1"}';$mysqli->close();}
		if($re2->num_rows == 1)
			{				
				$row = $re2->fetch_object();
				$status_check = $row->DCD_status;
				if ($status_check != "YES") 
				{
					echo '{ch:2,data:"กรุณา confirm DCD"}';$mysqli->close();exit();
				}
				else
				{
					$mysqli->autocommit(FALSE);
					try
					{
						
						if(!$mysqli->query("INSERT INTO tbl_inventory ( Part_ID,Lot,
							Box,Lot_Box,Qty,area,put_loc,pick_loc,Doc_no,DCD_No,Create_date,Rec_Date,user_id) SELECT Part_ID, LOT, Box,
							concat(LOT,Box), Rec_QTY,'PICK',put_loc,pick_loc,Doc_ID,DCD_NO,Create_date,NOW(),user_id 
							FROM tbl_receive WHERE Doc_ID = '$docGrn_id'")) throw new Exception('ไม่ทึกไม่สำเร็จ อาจเกิด Lot ซ้ำ');
						$tranInsertRow = $mysqli->affected_rows;
						if($tranInsertRow == 0) throw new Exception('Error Code 4');

						if(!$mysqli->query("INSERT INTO tbl_transaction (Part_ID,LOT,Box_No,Qty,Rec_Date,DCD_No,Doc_No,
							Tran_Status,Tran_Type,area,tarea,loc,toloc,create_date,user_id,qty_before,qty_operate,qty_after) 
							SELECT Part_ID, LOT, Box,Rec_QTY, Create_date, DCD_NO, '$doc_grn',
							'YES','IN','STORAGE','PICK','NOLOC',pick_loc,NOW(),'$cBy',qty_before,rec_qty,qty_after  
							FROM tbl_receive WHERE Doc_ID = '$docGrn_id'")){throw new Exception('Error Code 5');}
						if($mysqli->affected_rows != $tranInsertRow) throw new Exception('Error Code 6');

						if(!$mysqli->query("UPDATE tbl_partonhand t1,(SELECT SUM(Rec_Qty)as qty, Part_ID FROM tbl_receive 
							WHERE Doc_ID = '$docGrn_id' 
							GROUP BY Part_ID ORDER BY tbl_receive.Part_ID) as t2 
							SET t1.Qty_onhand = t1.Qty_onhand + t2.qty WHERE t1.Part_ID = t2.Part_ID")) 
							throw new Exception('Error Code 2');
						if($mysqli->affected_rows == 0) throw new Exception('Error Code 7');

						if(!$mysqli->query("UPDATE tbl_receive_header SET status = '1' WHERE Doc_no = '$doc_grn'")) 
							throw new Exception('Error Code 2');
						if($mysqli->affected_rows == 0) throw new Exception('Error Code 8');

						if(!$mysqli->query("DELETE FROM tbl_receive WHERE Doc_ID = '$docGrn_id'")) throw new Exception('Error Code 1');
						if($mysqli->affected_rows == 0) throw new Exception('Error Code 10');

						$mysqli->commit();
						echo '{ch:1,data:"'.$doc_grn.'"}';
					}
					catch( Exception $e )
					{
				  		$mysqli->rollback();
				  		echo '{ch:2,data:"'.$e->getMessage().'"}';
					}

				}

				
			}
		else echo '{ch:2,data:"กรุณา confirm DCD"}';$mysqli->close();exit();	
	}
	
	$mysqli->close();
	exit();	
?>
