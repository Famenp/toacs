<?php
	if(!ob_start("ob_gzhandler")) ob_start();
	header('Expires: Sun, 01 Jan 2014 00:00:00 GMT');
	header('Cache-Control: no-store, no-cache, must-revalidate');
	header('Cache-Control: post-check=0, pre-check=0', FALSE);
	header('Pragma: no-cache');
	include('../start.php');
	session_start();
	include('../php/connection.php');
	$cBy = $_SESSION['xxxID'];
	$fName = $_SESSION['xxxFName'];
	$obj  = $_POST['obj'];
	$type  = intval($_POST['type']);

	if($type == 1)
	{

		$dcdNO = $mysqli->real_escape_string(trim(strtoupper($obj['dcdNo_edit'])));
		$lot = $mysqli->real_escape_string(trim(strtoupper($obj['lot_edit'])));
		$box = $mysqli->real_escape_string(trim(strtoupper($obj['box_edit'])));
		$qty = $mysqli->real_escape_string(trim(strtoupper($obj['qty_edit'])));
		$docID = $mysqli->real_escape_string(trim(strtoupper($obj['docID_edit'])));
		$doc_grn = $mysqli->real_escape_string(trim(strtoupper($obj['grn_edit'])));
		
			$mysqli->autocommit(FALSE);
			try 
			{

				if(!$re2 = $mysqli->query("SELECT ID FROM tbl_receive_header where Doc_no = '$doc_grn' limit 1;"))
				{echo '{ch:2,data:"Error Code 1"}';$mysqli->close();}
				if($re2->num_rows == 0){echo '{ch:2,data:"part No. ไม่มีข้อมูลในระบบ"}';$mysqli->close();exit();}
				$row = $re2->fetch_object();
				$docGrn_id = $row->ID;
				
				$sql = "UPDATE tbl_receive SET dcd_no ='$dcdNO',lot ='$lot',box ='$box',rec_qty ='$qty' WHERE ID ='$docID';";
						
				if(!$mysqli->query($sql)) throw new Exception('Error Code 2');
				$mysqli->commit();

				if($re3 = $mysqli->query("SELECT t1.id,t2.doc_no,dcd_no,t3.part_supplier,LOT,box,rec_qty FROM tbl_receive t1
										  LEFT JOIN tbl_receive_header t2 ON t1.doc_id = t2.id
										  LEFT JOIN tbl_partmaster t3 ON t1.part_id = t3.part_id
										  WHERE t1.Doc_ID = '$docGrn_id'"))
				{
					if($re3->num_rows >0)
						{
							echo '{"ch":1,"data":';
							toArrayStringAddNumberRow($re3,1);
							echo '}';
						}
					else echo '{ch:2,data:"ไม่พบข้อมูลในระบบ"}';
				}
				else echo '{ch:2,data:"โคดผิด"}';
			} 
			catch (Exception $e) 
			{
				$mysqli->rollback();
		  		echo '{ch:2,data:"'.$e->getMessage().'"}';	
			}		
	}
	elseif ($type == 2) 
	{
		$qty = $mysqli->real_escape_string(trim(strtoupper($obj['goodsReceipt_total_qty'])));
		$dcdNO = $mysqli->real_escape_string(trim(strtoupper($obj['goodsReceipt_dcd_no'])));

		if(!$re1 = $mysqli->query("SELECT  sum(rec_qty) as totalqty from tbl_receive where DCD_NO = '$dcdNO' limit 1;"))
				{echo '{ch:2,data:"Error Code 1"}';$mysqli->close();}
				if($re1->num_rows == 0){echo '{ch:2,data:"part No. ไม่มีข้อมูลในระบบ"}';$mysqli->close();exit();}
				$row = $re1->fetch_object();
				$qty_dcd = $row->totalqty;

		if ($qty != $qty_dcd) 
		{
			echo '{ch:2,data:"จำนวนไม่ถูกต้อง DCD No."}';	
			exit();
		}

		$sql = "UPDATE tbl_receive SET DCD_status ='YES' WHERE DCD_NO ='$dcdNO';";
						
		if(!$mysqli->query($sql)) throw new Exception('Error Code 2');
		$mysqli->commit();
		echo '{ch:1,data:"บันทึก DCD สำเร็จ"}';


	}
	
	$mysqli->close();
	exit();
?>