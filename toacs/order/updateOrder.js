var header_updateOrder = function()
{
	return {
        view: "scrollview",
        scroll: "native-y",
        id:"header_updateOrder",
        body: 
        {
        	id:"updateOrder_id",
        	type:"clean",
    		rows:
    		[
    		     {
                    view:"form",
                    paddingY:20,
                    id:"updateOrder_form1",
                    elements:
                    [
                        {
                            rows:
                            [
                                {
                                    id:"scan_ship_id",
                                    cols:
                                    [                                       
                                        {view:"text",label:'Ship Instruction No',id:"ship_no",name:"ship_no",width:400,labelWidth:145,required:true},           
                                        {                                          
                                            view:"button",icon:"users",value:"Scan Ship No",id:"scan_ship_no",css:"bt_blue",width:180,on:
                                            {
                                                onItemClick:function(id, e)
                                                {  
                                                    if ($$('updateOrder_form1').validate())
                                                            {
                                                                var btn = $$('scan_ship_no'),
                                                                obj =  $$('updateOrder_form1').getValues();
                                                                btn.disable();
                                                     
                                                                $.post("order/updateOrder_data.php",{obj:obj,type:1})
                                                                 .done(function( data )
                                                                {
                                                                    btn.enable();
                                                                    data = eval('('+data+')');
                                                                    if(data.ch == 1)
                                                                    {
                                                                        var dataT1 = $$('order_dataT1');
                                                                        dataT1.clearAll();
                                                                        dataT1.parse(data.data,"jsarray");
                                                                        dataT1.getPager().render();
                                                                        update_2();
                                                                    }
                                                                    else if (data.ch == 2)
                                                                    {
                                                                        webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){$$('ship_no').focus();}});
                                                                    }
                                                                    
                                                                });
                                                            }
                                                            else
                                                            {
                                                                webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:"กรุณากรอกข้อมูล Ship No",callback:function(){$$('ship_no').focus();}});
                                                                
                                                            }
                                                }                             
                                            }                     
                                        },                                       
                                        {                  
                                            view:"button",icon:"users",type:"danger",value:"Clear (ล้างหน้าจอ)",id:"clear_all",css:"bt_blue",width:180,on:
                                            {
                                                onItemClick:function(id, e)

                                                {                                                                                                                                                                                               
                                                    update_3();
                                                    
                                                }
                                            }
                                        },
                                        
                                        
                                         

                                    {}
                                    ]
                                },                      
                                {
                                    id:"time_dock_order",
                                    cols:
                                    [
                                        {view:"text",label:'Time (เวลา)',id:"time_txt",name:"time_txt",labelPosition:"top",width:180,required:true},
                                        {view:"text",label:'Dock No.',id:"dock_no",name:"dock_no",labelPosition:"top",width:250,required:true},                      
                                        {
                                            rows:
                                            [
                                                {

                                                },
                                                {
                                                    view:"button",value:"Save (บันทึก)",id:"save_time_dock",type:"form",css:"bt_red",width:230,on:
                                                    {
                                                        onItemClick:function(id, e)
                                                        {
                                                            if ($$('updateOrder_form1').validate())
                                                            {
                                                            webix.confirm(
                                                                {
                                                                    title:"<b>ข้อความจากระบบ</b>",ok:'ใช่',cancel:"ไม่",text:"คุณต้องการ<font color='#ca4635'><br><b>บันทึกข้อมูล</b></font><br><font color='#27ae60'><b>ใช่</b></font> หรือ <font color='#3498db'><b>ไม่</b></font>",callback:function(result)
                                                                    {
                                                                    if(result)
                                                                        {
                                                                            var btn = $$('save_time_dock'),
                                                                            obj =  $$('updateOrder_form1').getValues();
                                                                            btn.disable();
                                                                            $.post("order/updateOrder_save.php",{obj:obj,type:1})
                                                                            .done(function( data ) 
                                                                            {
                                                                                btn.enable();
                                                                                data = eval('('+data+')');
                                                                                if(data.ch == 1)
                                                                                {
                                                                                    var dataT1 = $$('order_dataT1');
                                                                                    dataT1.clearAll();
                                                                                    dataT1.parse(data.data,"jsarray");
                                                                                    dataT1.getPager().render();
                                                                                }
                                                                                else if (data.ch == 2)
                                                                                {
                                                                                    webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){$$('order_no').focus();}});

                                                                                }
                                                                            });
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                            else
                                                            {
                                                                 webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:"กรุณากรอกข้อมูลให้ครบ",callback:function(){$$('order_no').focus();}});
                                                            }
                                                          
                                                        }
                                                    }                                         
                                                }
                                            ]
                                        },
                                        {}              
                                    ]
                                }
                            ]
                        },
                        
                    ],
                    on:
                            {
                                "onSubmit":function(view,e)
                                {
                                    if(view.config.name =='ship_no')
                                    {
                                        view.blur();
                                        $$('scan_ship_no').callEvent("onItemClick", []);
                                    }                      
                                    else if(webix.UIManager.getNext(view).config.type == 'line')
                                    {
                                        webix.UIManager.setFocus(webix.UIManager.getNext(webix.UIManager.getNext(view)));
                                    }
                                    else
                                    {
                                        webix.UIManager.setFocus(webix.UIManager.getNext(view));
                                    }
                                }
                            }
                    
                },
                {           
                            paddingX:2,
                            id:"tabel_id",
                            rows:
                            [
                                {  
                                    view:"datatable",
                                    id:"order_dataT1",
                                    navigation:true,
                                    datatype:"jsarray",
                                    css:"my_style",
                                    pager:"order_pagerA",
                                    autoheight:true,
                                    resizeColumn:true,
                                    leftSplit:0,
                                    columns:
                                    [                                  
                                        { id:"data21",header:"&nbsp;",width:35,template: "<span style='cursor:pointer;' class='webix_icon fa-pencil'></span>"},
                                        { id:"data22",header:"&nbsp;",width:35,template: "<span style='cursor:pointer;' class='webix_icon fa-trash-o'></span>"},
                                        { id:"data0",header:"No",css:"rank",width:50},
                                        { id:"data1",header:"Time",css:"rank",width:150},
                                        { id:"data2",header:"Dock",css:"rank",width:200},
                                        { id:"data3",header:"Delivery Date",css:"rank",width:200},
                                        { id:"data4",header:"Ship No",css:"rank",width:180},
                                        { id:"data5",header:"Order No",css:"rank",width:180},
                                        { id:"data6",header:"customer_item",css:"rank",width:200},
                                                                        
                                    ],
                                    onClick:
                                    {
                                        "fa-pencil":function(e,t)
                                        {
                                            var row = this.getItem(t),obj;
                                            $$('receive_edit').show();
                                            $$('receive_form1_edit').setValues(
                                            {
                                                dcdNo_edit:row.data3,
                                                lot_edit:row.data5,
                                                box_edit:row.data6,
                                                qty_edit:row.data7,
                                                docID_edit:row.data1,
                                                grn_edit:row.data2,
                                                                                                    
                                            });                                                                                
                                        },
                                         "fa-trash-o":function(e,t)
                                        {
                                            var r = this.getItem(t),obj;
                                            webix.confirm({
                                                 title: "กรุณายืนยัน",ok:"ใช่", cancel:"ไม่",text:"คุณต้องการ<font color='#ca4635'><br><b>ลบ</b></font><br><font color='#27ae60'><b>ใช่</b></font> หรือ <font color='#3498db'><b>ไม่</b></font>",
                                                 callback:function(res)
                                                 {
                                                    if(res)
                                                    {
                                                        $.post("inbound/goodsReceipt_delete.php",{obj:{doc_id:r.data1,doc_grn:r.data2},type:2})
                                                        .done(function( data ) 
                                                        {
                                                            data = eval('('+data+')');
                                                            if(data.ch==1)
                                                            {
                                                                var dataT1 = $$('receive_dataT1');
                                                                dataT1.clearAll();
                                                                dataT1.parse(data.data1,"jsarray");
                                                            }
                                                            else if(data.ch == 2){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                                                            else if(data.ch == 9){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                                                            else if(data.ch == 10){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){window.open("login.php","_self");}});}
                                                        });
                                                    }
                                                 }
                                            });
                                        }
                                    }
                                }
                                ,
                                {
                                    type:"wide",
                                        cols:
                                        [
                                            {
                                                view:"pager", id:"order_pagerA",
                                                template:function(data, common){
                                                    var start = data.page * data.size
                                                    ,end = start + data.size;
                                                    if(data.count == 0) start = 0;
                                                    else start += 1;
                                                    if(end >= data.count) end = data.count;
                                                    var html = "<b>showing "+(start)+" - "+end+" total "+data.count+" </b>";
                                                    return common.first()+common.prev()+" "+html+" "+common.next()+common.last();
                                                },
                                                size:10,
                                                group:5 
                                            }
                                        ]
                                                        
                                }

                            ]
                        }
            ],on:
            {
                onHide:function()
                {
                    
                },
                onShow:function()
                {

                },
                onAddView:function()
                {
                    window['update_1'] = function()  
                    {
                        $$('time_dock_order').hide();
                        $$('clear_all').hide();
                        $$('tabel_id').hide();                       
                    };

                    window['update_2'] = function()  
                    {
                        $$('ship_no').disable();
                        $$('clear_all').show();
                        $$('tabel_id').show();
                        $$('time_dock_order').show(); 
                        $$('scan_ship_no').hide(); 
                        $$('time_txt').focus(); 
                                      
                    };

                    window['update_3'] = function()  
                    {
                        
                        $$('clear_all').hide();
                        $$('tabel_id').hide();
                        $$('time_dock_order').hide(); 
                        $$('scan_ship_no').show();
                        $$('time_txt').focus(); 
                        $$('time_txt').setValue('');
                        $$('dock_no').setValue('');
                        $$('ship_no').setValue('');
                        $$('ship_no').enable();
                        $$('ship_no').focus();
                       

                                      
                    };
                   window.update_1();
                }
            }
        }
    };
};