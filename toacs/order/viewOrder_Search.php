<?php
	if(!ob_start("ob_gzhandler")) ob_start();
	header('Expires: Sun, 01 Jan 2014 00:00:00 GMT');
	header('Cache-Control: no-store, no-cache, must-revalidate');
	header('Cache-Control: post-check=0, pre-check=0', FALSE);
	header('Pragma: no-cache');
	include('../start.php');
	session_start();
	include('../php/connection.php');
	$obj  = $_POST['obj'];
	$type  = intval($_POST['type']);

	if($type == 1)
	{

		$start_date = $mysqli->real_escape_string(trim(strtoupper($obj['start'])));
		$end_date = $mysqli->real_escape_string(trim(strtoupper($obj['end'])));

		if ($start_date == "" OR $end_date == "") 
		{
			echo '{ch:2,data:"กรุณาเลือกวันที่"}';	
			exit();
		}
		if($re1 = $mysqli->query("SELECT t1.ship_no,t1.customer_item,DATE_FORMAT(t1.sch_date,'%Y-%m-%d'),t1.so_no,t1.so_line,t1.so_qty,t1.so_balance,t1.qty_box,
		t1.customer_name,t1.toacs_item,t1.order_no,t1.due_time,t1.dock,t1.pick_no,t1.pick_actual,t1.create_date,t2.user_fName
		from tbl_order t1 LEFT JOIN tbl_user t2 ON t1.user_id = t2.user_id where t1.sch_date BETWEEN '$start_date' AND '$end_date' 
		ORDER BY t1.ID"))
		{
			if($re1->num_rows >0)
			{
				echo '{"ch":1,"data":';
				toArrayStringAddNumberRow($re1,1);
				echo '}';
			}
			else echo '{ch:2,data:"ไม่พบข้อมูลในระบบ"}';
		}

		else echo '{ch:2,data:"โคดผิด"}';
	}

	$mysqli->close();
	exit();	
	
?>