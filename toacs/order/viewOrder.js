var header_viewOrder = function()
{
	return {
        view: "scrollview",
        scroll: "native-y",
        id:"header_viewOrder",
        body: 
        {
        	id:"viewOrder_id",
        	type:"clean",
    		rows:
    		[
    		    {
                    view:"form",
                    paddingY:20,
                    id:"viewOrder_form1",
                    elements:
                    [
                        {                            
                            cols:                                    
                                [    
                                    {  
                                        view:"datepicker", label:"Start date",value:new Date(), name:"start" ,labelPosition:"top", stringResult:true,format:webix.Date.dateToStr("%Y-%m-%d"),width:300
                                    },
                                    {
                                        view:"datepicker", label:"End date",value:new Date(), name:"end",labelPosition:"top", stringResult:true,format:webix.Date.dateToStr("%Y-%m-%d"),width:300
                                    },
                                    {
                                        rows:
                                        [
                                            {
                                                                               
                                            },
                                            {
                                             
                                                view:"button", label:"Find (ค้นหา)", type:"form",width:230,id:"Find_order",
                                                on:
                                                {
                                                    
                                                        onItemClick:function(id, e)
                                                        {
                                                            if ($$('viewOrder_form1').validate())
                                                            {
                                                                var btn = $$('Find_order'),
                                                                obj =  $$('viewOrder_form1').getValues();
                                                                btn.disable();
                                                     
                                                                $.post("order/viewOrder_Search.php",{obj:obj,type:1})
                                                                 .done(function( data )
                                                                {
                                                                    btn.enable();
                                                                    data = eval('('+data+')');
                                                                    if(data.ch == 1)
                                                                    {
                                                                        var dataT1 = $$('viewOrder_dataT1');
                                                                        dataT1.clearAll();
                                                                        dataT1.parse(data.data,"jsarray");
                                                                        dataT1.getPager().render();
                                                                    }
                                                                    else if (data.ch == 2)
                                                                    {
                                                                        webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});
                                                                    }
                                                                    
                                                                });
                                                            }
                                                            else{webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:"กรุณากรอกข้อมูล",callback:function(){}});}
                                                        }
                                                    
                                                }       
                                            
                                            }
                                            
                                        ]   
                                    },
                                    {
                                         
                                    }                               
                                ]
                        },
                        {
                            paddingX:2,
                            rows:
                            [
                                {  
                                    view:"datatable",
                                    id:"viewOrder_dataT1",
                                    navigation:true,
                                    datatype:"jsarray",
                                    css:"my_style",
                                    pager:"viewOrder_pagerA",
                                    autoheight:true,
                                    resizeColumn:true,
                                    leftSplit:3,
                                    columns:
                                    [                                  
                                        { id:"data0",header:"No",css:"rank",width:50},
                                        { id:"data1",header:[{text :"Ship Number",css:{"text-align":"center"}},{content:"textFilter"}],css:"rank",width:190},
                                        { id:"data2",header:[{text :"Customer Item",css:{"text-align":"center"}},{content:"textFilter"}],css:"rank",width:190},       
                                        { id:"data3",header:"Delivery Schedule",css:"rank",width:200},
                                        { id:"data12",header:"Time",css:"rank",width:170},
                                        { id:"data13",header:"Dock",css:"rank",width:170},
                                        { id:"data4",header:[{text :"So Number",css:{"text-align":"center"}},{content:"textFilter"}],css:"rank",width:190},                                    
                                        { id:"data5",header:"SO Line",css:"rank",width:130},
                                        { id:"data6",header:"Qty",css:"rank",width:80},
                                        { id:"data7",header:"Balance",css:"rank",width:120},
                                        { id:"data8",header:"Qty Per Box",css:"rank",width:150},
                                        { id:"data9",header:"Customer Name",css:"rank",width:120},
                                        { id:"data10",header:"Toacs Item",css:"rank",width:180},
                                        { id:"data11",header:"Order Number",css:"rank",width:180},
                                        { id:"data14",header:"Pick No.",css:"rank",width:170},
                                        { id:"data15",header:"Pick Actual",css:"rank",width:170},
                                        { id:"data16",header:"Create Date",css:"rank",width:170},
                                        { id:"data17",header:"Create By",css:"rank",width:170},              
                                    ],
                                }
                                ,
                                {
                                    type:"wide",
                                        cols:
                                        [
                                            {
                                                view:"pager", id:"viewOrder_pagerA",
                                                template:function(data, common){
                                                    var start = data.page * data.size
                                                    ,end = start + data.size;
                                                    if(data.count == 0) start = 0;
                                                    else start += 1;
                                                    if(end >= data.count) end = data.count;
                                                    var html = "<b>showing "+(start)+" - "+end+" total "+data.count+" </b>";
                                                    return common.first()+common.prev()+" "+html+" "+common.next()+common.last();
                                                },
                                                size:10,
                                                group:5 
                                            }
                                        ]
                                                        
                                }

                            ]
                        }
                          
                    ]
                    
                }
            ],on:
            {
                onHide:function()
                {
                    
                },
                onShow:function()
                {

                },
                onAddView:function()
                {

                }
            }
        }
    };
};