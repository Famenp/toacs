<?php
if(!ob_start("ob_gzhandler")) ob_start();
header('Expires: Sun, 01 Jan 2014 00:00:00 GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Pragma: no-cache');
include('../start.php');
session_start();
if(empty($_SESSION['xxxID']))
{
    echo "{ch:10,data:'เวลาการเชื่อมต่อหมด<br>คุณจำเป็นต้อง login ใหม่'}";
    exit();
}
$cBy = $_SESSION['xxxID'];
include('../php/connection.php');
$fName = $_SESSION['xxxFName'];
if(move_uploaded_file($_FILES["upload"]["tmp_name"],"../order_file/".$_FILES["upload"]["name"]))
{
    $extension = explode('.',$_FILES["upload"]["name"]);
    $fileUpload =$_FILES["upload"]["name"];
    $len = count($extension);
    $extension = $extension[$len-1];
    $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0,5);
    $fileName = $randomString.strtotime(date('Y-m-d H:i:s')).'.'.$extension; 
    rename("../order_file/".$_FILES["upload"]["name"],"../order_file/".$fileName);
    //ย้ายไฟล์เข้าโฟรเดอแปลงชื่อไฟลเติมrandsrring5ตัวข้างหน้า
    $strFileName = "../order_file/".$fileName;
    $objFopen = fopen($strFileName, 'r');
    $fileAr = Array();
    $fileArEX = Array();
    $fileB = 0;
    if ($objFopen) 
    {
        // fgets($objFopen, 4096);
        while (!feof($objFopen)) 
        {
            $file = fgets($objFopen, 4096);
            $file = explode('|', $file);
            if (!$file[0]) {
                break;
            }
            $file[11] = preg_replace('/\s+/', '',$file[11]);
            $sc_date = date("Y-m-d H:i:s", strtotime("$file[0]"));
            $fileArEX[] = "('$file[0]','$sc_date','$file[1]','$file[2]','$file[3]','$file[4]','$file[5]','$file[6]','$file[7]','$file[8]','$file[9]','$file[10]','$file[11]',NOW(),'$fileName','$cBy')";
        }
        if(count($fileArEX)>0)
        {
            fclose($objFopen);
            $sql =  'INSERT IGNORE INTO tbl_order
            (Delivery_schedule,sch_date,Ship_no,Inv_no,SO_no,SO_line,Customer_item,SO_qty,SO_balance,Qty_box,Customer_name,Toacs_item,Order_no,Create_date,Filename,user_id) 
            values'.join(',',$fileArEX);

            $mysqli->autocommit(FALSE);
            try
            {
                if(!$mysqli->query($sql)) throw new Exception('Error Code 1');   
                $fileCount = $mysqli->affected_rows;
                $mysqli->commit();    
                echo '{"status":"server","mms":"Upload สำเร็จ '.$fileCount.' รายการ","data":[]';
                echo '}';
            }
            catch( Exception $e )
            {
                $mysqli->rollback();
                echo '{"status":"server","mms":"'.$e->getMessage().'","sname":[]}';
            }
        }
        else echo '{"status":"server","mms":"ข้อมูลในไฟล์ไม่ถูกต้อง","sname":[]}';
    }
}

// function _checkDate($d)
// {
//     $dateArray1 = explode('/',$d);
//     $dateArray2 = explode('-',$d);
//     if(count($dateArray1)== 3)
//     {
//         if(strlen($dateArray1[0]) == 4)return str_replace('/','-',$d);
//         else return "$dateArray1[2]-$dateArray1[1]-$dateArray1[0]";
//     }
//     else if(count($dateArray2)== 3)
//     {
//         if(strlen($dateArray2[0]) == 4)return $d;
//         else return "$dateArray2[2]-$dateArray2[1]-$dateArray2[0]";
//     }
//     else return '0';
// }

$mysqli->close();
exit();
?>