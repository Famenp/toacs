<?php
if(!ob_start("ob_gzhandler")) ob_start();
header('Expires: Sun, 01 Jan 2014 00:00:00 GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Pragma: no-cache');
include('../start.php');
require '../php/PHPExcel/Classes/PHPExcel.php';
require_once '../php/PHPExcel/Classes/PHPExcel/IOFactory.php';
session_start();
if(empty($_SESSION['xxxID']))
{
    echo "{ch:10,data:'เวลาการเชื่อมต่อหมด<br>คุณจำเป็นต้อง login ใหม่'}";
    exit();
}
$fName = $_SESSION['xxxFName'];
include('../php/connection.php');
if(move_uploaded_file($_FILES["upload"]["tmp_name"],"../order_file/".$_FILES["upload"]["name"]))
{
	$extension = explode('.',$_FILES["upload"]["name"]);
    $fileUpload =$_FILES["upload"]["name"];
    $len = count($extension);
    $extension = $extension[$len-1];
    $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0,5);
    $fileName = $randomString.strtotime(date('Y-m-d H:i:s')).'.'.$extension; 
    rename("../order_file/".$_FILES["upload"]["name"],"../order_file/".$fileName);
    $strFileName = "../order_file/".$fileName;

	function _checkDate($d)
	{
	    $dateArray1 = explode('/',$d);
	    $dateArray2 = explode('-',$d);
	    if(count($dateArray1)== 3)
	    {
	        if(strlen($dateArray1[0]) == 4)return str_replace('/','-',$d);
	        else return "$dateArray1[2]-$dateArray1[1]-$dateArray1[0]";
	    }
	    else if(count($dateArray2)== 3)
	    {
	        if(strlen($dateArray2[0]) == 4)return $d;
	        else return "$dateArray2[2]-$dateArray2[1]-$dateArray2[0]";
	    }
	    else return '0';
	}
	function unixdate($d)
	{
		$UNIX_DATE = ($d - 25569) * 86400;
		return gmdate("d-m-Y H:i:s", $UNIX_DATE);
	}

	function _checkDatestr($d)
	{
	    $dateArray1 = explode('-',$d);
	    //04-09-2017
	    if(count($dateArray1)== 3)
	    {
	        if(strlen($dateArray1[1]) == 2)
	        {

	        	return "$dateArray1[0]$dateArray1[1]$dateArray1[2]";
	        }
	        else
	        {
	        	$dateArray1[1] = '0'.$dateArray1[1];
	        	return "$dateArray1[0]$dateArray1[1]$dateArray1[2]";
	        } 
	    }
	    else return '0';
	}

	function convtime($t)
	{
		$len = strlen($t);
		if ($len == 3) 
		{
			$t = '0'.$t;
		}
		$a = ":";
		$hr = substr($t,0,2);
		$mm = substr($t,2);
		if ($hr > 24) {
			$hr = $hr - 24;
		}
		$time = $hr.$a.$mm;
		return $time;	
	}
	
	function nonconvtime($t)
	{
		$len = strlen($t);
		if ($len == 3) 
		{
			$t = '0'.$t;
		}
		$a = ":";
		$hr = substr($t,0,2);
		$mm = substr($t,2);
		$time = $hr.$a.$mm;
		return $time;	
	}

	function nextdate($d,$t)
	{
		$hr = substr($t, 0, 2);
		$dateArray1 = explode('-',$d);
		if ($hr > 24) {
			$dateArray1[2] = $dateArray1[2]+1;
		}
		return "$dateArray1[0]-$dateArray1[1]-$dateArray1[2]";	
	}

	$objPHPExcel = PHPExcel_IOFactory::load($strFileName);
	foreach ($objPHPExcel->getWorksheetIterator() as $worksheet)
	{
		$worksheetTitle     = $worksheet->getTitle();
		$highestRow         = $worksheet->getHighestRow(); // e.g. 10
		$highestColumn      = $worksheet->getHighestColumn(''); // e.g 'F'
		$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
		$nrColumns = ord($highestColumn) - 64;

		$fileArEX = Array();
		for ($row = 2; $row <= $highestRow; ++ $row)
		{
			$val=array();
			for ($col = 0; $col < $highestColumnIndex; ++ $col)
			{
				$cell = $worksheet->getCellByColumnAndRow($col,$row);
				$val[] = $cell->getValue();
				/*if ($col == 10) 
				{
					$val[10] =  PHPExcel_Style_NumberFormat::toFormattedString($cell->getCalculatedValue(), 'hh:mm');
				}*/
			}
			$stime = convtime($val[10]);
			$snontime = nonconvtime($val[10]);
			$date_ = _checkDate($val[6]);

			// $sdate= nextdate($date_,$stime);
			$schdate_ = _checkDatestr($date_);


			$val[4] = preg_replace('/\s+/', '',$val[4]);
			$fileArEX[] = "('$val[0]','$val[1]','$val[2]','$val[3]','$val[4]','$val[5]','$val[5]','$schdate_','$date_','$val[7]','$val[8]','$val[9]','$stime','$val[11]'
			,'$val[12]','$val[13]','$fileName','4','$snontime')";

			// print_r($fileArEX);
			// echo $date_."<br>";
			// echo $schdate_."<br>";
		}

		if(count($fileArEX)>0)
		{
			$sql =  'INSERT IGNORE INTO tbl_order
				(SO_no,SO_line,Order_no,Customer_name,Dock,Customer_item,Toacs_item,Delivery_schedule,sch_date,SO_qty,model,trip_no,Due_time,Delivery_Int,Plant,Ship_no,
				Filename,user_id,Due_time_n) 
	            VALUES'.join(',',$fileArEX);
	        // echo $sql;

	         $mysqli->autocommit(FALSE);
	            try
	            {
	                if(!$mysqli->query($sql)) throw new Exception('query failed');   
	                $fileCount = $mysqli->affected_rows;
	                $mysqli->commit();
	                echo '{"status":"server","mms":"Upload สำเร็จ '.$fileCount.' รายการ","data":[]';
	                echo '}'; 
	            }
	            catch( Exception $e )
	            {
	                $mysqli->rollback();
	                echo '{"status":"server","mms":"'.$e->getMessage().'","sname":[]}';
	            }	
		} else echo '{"status":"server","mms":"ข้อมูลในไฟล์ไม่ถูกต้อง","sname":[]}';
	}
}

// $fileName = "SaleOrder2.xlsx";
// $strFileName = "../order_file/".$fileName;
// 	function _checkDate($d)
// 	{
// 	    $dateArray1 = explode('/',$d);
// 	    $dateArray2 = explode('-',$d);
// 	    if(count($dateArray1)== 3)
// 	    {
// 	        if(strlen($dateArray1[0]) == 4)return str_replace('/','-',$d);
// 	        else return "$dateArray1[2]-$dateArray1[1]-$dateArray1[0]";
// 	    }
// 	    else if(count($dateArray2)== 3)
// 	    {
// 	        if(strlen($dateArray2[0]) == 4)return $d;
// 	        else return "$dateArray2[2]-$dateArray2[1]-$dateArray2[0]";
// 	    }
// 	    else return '0';
// 	}
// 	function _checkDatestr($d)
// 	{
// 	    $dateArray1 = explode('-',$d);
// 	    //28/8/2017
// 	    if(count($dateArray1)== 3)
// 	    {
// 	        if(strlen($dateArray1[1]) == 2)
// 	        {

// 	        	return "$dateArray1[0]$dateArray1[1]$dateArray1[2]";
// 	        }
// 	        else
// 	        {
// 	        	$dateArray1[1] = '0'.$dateArray1[1];
// 	        	return "$dateArray1[0]$dateArray1[1]$dateArray1[2]";
// 	        } 
// 	    }
// 	    else return '0';
// 	}

// 	function convtime($t)
// 	{
// 		$a = ":";
// 		$hr = substr($t, 0, 2);
// 		$mm = substr($t, 2);
// 		if ($hr > 24) {
// 			$hr = $hr - 24;
// 		}
// 		$time = $hr.$a.$mm;
// 		return $time;	
// 	}

// 	function nextdate($d,$t)
// 	{
// 		$hr = substr($t, 0, 2);
// 		$dateArray1 = explode('-',$d);
// 		if ($hr > 24) {
// 			$dateArray1[2] = $dateArray1[2]+1;
// 		}
// 		return "$dateArray1[0]-$dateArray1[1]-$dateArray1[2]";	
// 	}

// 	$objPHPExcel = PHPExcel_IOFactory::load($strFileName);
// 	foreach ($objPHPExcel->getWorksheetIterator() as $worksheet)
// 	{
// 		$worksheetTitle     = $worksheet->getTitle();
// 		$highestRow         = $worksheet->getHighestRow(); // e.g. 10
// 		$highestColumn      = $worksheet->getHighestColumn(''); // e.g 'F'
// 		$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
// 		$nrColumns = ord($highestColumn) - 64;

// 		$fileArEX = Array();
// 		for ($row = 3; $row <= $highestRow; ++ $row)
// 		{
// 			$val=array();
// 			for ($col = 0; $col < $highestColumnIndex; ++ $col)
// 			{
// 				$cell = $worksheet->getCellByColumnAndRow($col,$row);
// 				$val[] = $cell->getValue();
// 				/*if ($col == 10) 
// 				{
// 					$val[10] =  PHPExcel_Style_NumberFormat::toFormattedString($cell->getCalculatedValue(), 'hh:mm');
// 				}*/
// 			}
// 			$stime = convtime($val[10]);
// 			$date_ = _checkDate($val[6]);

// 			$sdate= nextdate($date_,$val[10]);
// 			$schdate_ = _checkDatestr($sdate);

// 			$val[4] = preg_replace('/\s+/', '',$val[4]);
// 			$fileArEX[] = "('$val[0]','$val[1]','$val[2]','$val[3]','$val[4]','$val[5]','$val[5]','$schdate_','$sdate','$val[7]','$val[8]','$val[9]','$stime','$val[11]'
// 			,'$val[12]','$val[13]','$fileName','4')";

// 			// echo $sdate."<br>";
// 			// echo $schdate_."<br>";
// 		}
// 		if(count($fileArEX)>0)
// 		{
// 			$sql =  'INSERT IGNORE INTO tbl_order
// 				(SO_no,SO_line,Order_no,Customer_name,Dock,Customer_item,Toacs_item,Delivery_schedule,sch_date,SO_qty,model,trip_no,Due_time,Delivery_Int,Plant,Ship_no,
// 				Filename,user_id) 
// 	            VALUES'.join(',',$fileArEX);
// 	        // echo $sql."<br>";

// 	         $mysqli->autocommit(FALSE);
// 	            try
// 	            {
// 	                if(!$mysqli->query($sql)) throw new Exception('query failed');   
// 	                $fileCount = $mysqli->affected_rows;
// 	                $mysqli->commit();
// 	                echo '{"status":"server","mms":"Upload สำเร็จ '.$fileCount.' รายการ","data":[]';
// 	                echo '}'; 
// 	            }
// 	            catch( Exception $e )
// 	            {
// 	                $mysqli->rollback();
// 	                echo '{"status":"server","mms":"'.$e->getMessage().'","sname":[]}';
// 	            }	
// 		} else echo '{"status":"server","mms":"ข้อมูลในไฟล์ไม่ถูกต้อง","sname":[]}';
// 	}



$mysqli->close();
exit();
?>