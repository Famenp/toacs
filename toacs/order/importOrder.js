var header_importOrder = function()
{
	return {
        view: "scrollview",
        scroll: "native-y",
        id:"header_importOrder",
        body: 
        {
        	id:"importOrder_id",
        	type:"clean",
    		rows:
    		[
                {
                    view:"form",
                    paddingY:20,
                    id:"viewOrder_form1",
                    elements:
                    [
                        {                            
                            cols:                                    
                                [    
                                    {view:"button",value:"Upload (อัพโหลด)",width:200,id:"uploadOrder_pop_upload",on:
                                        {
                                            onItemClick:function(id, e)
                                                {
                                                    $$('uploadOrder_win_upload').show();
                                                }
                                        }
                                    },{}                          
                                ]
                        }
                    
                          
                    ]
                    
                }
    		  
            ],on:
            {
                onHide:function()
                {
                    
                },
                onShow:function()
                {

                },
                onAddView:function()
                {
                    webix.ui(
                    {
                        view:"window", move:true,modal:true,id:"uploadOrder_win_upload",
                        top:100,position:"center",head:
                        {
                            view:"toolbar", margin:-4,
                            cols:
                            [
                                {view:"label", label: "Upload File" },
                                {},
                                { view:"icon", icon:"times-circle",click:"$$('uploadOrder_win_upload').hide();$$('uploadOrder_upload').files.clearAll();"}
                            ],
                        },
                        body:
                        {
                            view:"form", scroll:false,id:"uploadOrder_win_form1",width:300,
                            elements:
                            [
                                {
                                    cols:
                                    [
                                        {},
                                        {
                                            rows:
                                            [
                                                { 
                                                    view: "uploader", value: 'Select File(กดเพื่อเลือกไฟล์)', 
                                                    multiple:false, autosend:false,
                                                    name:"uploader",width:250,id:"uploadOrder_upload",
                                                    link:"uploadOrder_list",upload:"order/importexl.php",
                                                    on:{
                                                        onBeforeFileAdd:function(item){
                                                            var type = item.type.toLowerCase();
                                                            if (type != "csv"){
                                                                webix.message("Only xlsx are supported");
                                                                return false;
                                                            }
                                                        },
                                                        onFileUpload:function(item){
                                                        },
                                                        onFileUploadError:function(item){
                                                             webix.alert("Error during file upload");
                                                        },
                                                        onUploadComplete:function(data)
                                                        {
                                                            $$("uploadOrder_upload").files.clearAll();
                                                            /*$$('uploadOrder_dataT1').clearAll();
                                                            $$('uploadOrder_dataT1').parse(data.data,"jsarray");*/
                                                            $$('uploadOrder_win_upload').hide();
                                                        }
                                                    },
                                                },
                                                {
                                                    view:"list",id:"uploadOrder_list", type:"uploader",
                                                    autoheight:true, borderless:true,on:
                                                    {
                                                        onAfterRender:function()
                                                        {
                                                            if(this.count() >0) $$('uploadOrder_save').show();
                                                            else $$('uploadOrder_save').hide();
                                                        }
                                                    }
                                                },
                                                { view:"button",label:"Upload (อัพโหลด)",id:"uploadOrder_save",type:'form',hidden:true, click: function() 
                                                    {
                                                        $$("uploadOrder_upload").send(function(response)
                                                        {
                                                            if(response)
                                                                webix.message(response.mms);
                                                        });
                                                    }
                                                }
                                            ]
                                        },{}
                                    ]
                                }
                            ]
                        }
                    });
                }
            }
        }
    };
};