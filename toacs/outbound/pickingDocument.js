var header_pickingDocument = function()
{
	return {
        view: "scrollview",
        scroll: "native-y",
        id:"header_pickingDocument",
        body: 
        {
        	id:"pickingDocument_id",
        	type:"clean",
    		rows:
    		[
    		   {
                    view:"form",
                    paddingY:20,
                    id:"pickingDocument_form1",
                    elements:
                    [
                        {                            
                            cols:                                    
                                [    
                                    {  
                                        view:"datepicker", label:"date[Y-M-D] วัน(ปี-เดือน-วัน)",value:new Date(), name:"date_order",id:"date_order",required:true,labelPosition:"top", stringResult:true,format:webix.Date.dateToStr("%Y-%m-%d"),width:250
                                    },
                                    {
                                        view:"text",label:'Time [--:--] (เวลา)',required:true,name:"time_order",id:"time_order",labelPosition:"top", value:"",width:200
                                    },
                                    {
                                        rows:
                                        [
                                            {},
                                            {
                                                view:"button", label:"Find (ค้นหา)",width:230,id:"find_order",
                                                on:
                                                {                                                   
                                                    onItemClick:function(id, e)
                                                    {
                                                        if ($$('pickingDocument_form1').validate())
                                                            {
                                                                var btn = $$('find_order'),
                                                                obj =  $$('pickingDocument_form1').getValues();
                                                                btn.disable();
                                                     
                                                                $.post("outbound/picking_data.php",{obj:obj,type:1})
                                                                .done(function( data )
                                                                {
                                                                   btn.enable();
                                                                    data = eval('('+data+')');
                                                                    if(data.ch == 1)
                                                                    {
                                                                        var dataT1 = $$('picking_dataT1');
                                                                        dataT1.clearAll();
                                                                        dataT1.parse(data.data,"jsarray");
                                                                        dataT1.getPager().render();
                                                                    }
                                                                    else if (data.ch == 2)
                                                                    {
                                                                        webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});
                                                                    }     
                                                                });
                                                            }                               
                                                    }
                                                }   
                                            }
                                        ]
                                    },
                                    {
                                        rows:
                                        [
                                            {},
                                            {
                                                view:"button", label:"Generate pickingDocument",width:240,id:"gen_pick",type:"form",
                                                on:
                                                {                                                   
                                                    onItemClick:function(id, e)
                                                    {

                                                        var dataT1 = $$("picking_dataT1"),data = [],btn=$$(this);
                                                        if(dataT1.count()>0)
                                                        {             
                                                            var f = Object.keys(dataT1.getItem(dataT1.getFirstId())),fName=[];
                                                            f = ['ch1','data4'];
                                                            data[0] = ['status','No'];
                                                            dataT1.eachRow( function (row)
                                                            {
                                                                var r = dataT1.getItem(row),rr=[];
                                                                for(var i=-1,len=f.length;++i<len;)
                                                                {
                                                                  rr[rr.length] = r[f[i]];
                                                                }
                                                                if (rr[0]==1) 
                                                                {
                                                                    data[data.length] = rr;
                                                                }
                                                            });

                                                            $.post("outbound/picking_update.php",{obj:data,date:$$('date_order').getValue(),type:1})
                                                            .done(function( data ) 
                                                            {
                                                                data = eval('('+data+')');
                                                                if(data.ch==1)
                                                                {
                                                                    console.log(data.sono);
                                                                   webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){$$('find_order').callEvent("onItemClick", []);}});

                                                                   for (var i = 0; i < data.sono.length; i++) 
                                                                   {
                                                                       $.post( "print/pickinglist.php", {data1:data.sono[i]})
                                                                        .done(function( data ) 
                                                                        {
                                                                            var data = eval('('+data+')');
                                                                            if(data.ch == 1)
                                                                            {
                                                                              webix.message({ type:"default",expire:7000, text:'ปริ้น PICKING LIST สำเร็จ'});
                                                                            }
                                                                            else webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});
                                                                        });

                                                                        /*window.open("print/pickinglist.php?data="+data.sono[i], '_blank');*/
                                                                   }           
                                                                }
                                                                else if(data.ch == 2){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}        
                                                            });
                                                        }
                                                        else {webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:"ไม่พบข้อมูลในตาราง",callback:function(){}});}
                                                    }   
                                                }   
                                            }
                                        ]
                                    },
                                    {
                                        rows:
                                        [
                                            {},
                                            {
                                                view:"button", label:"Clear (ล้างหน้าจอ)",width:240,id:"clear",type:"danger",
                                                on:
                                                {                                                   
                                                    onItemClick:function(id, e)
                                                    {
                                                        $$("time_order").setValue('');
                                                        $$("picking_dataT1").clearAll();
                                                        $$("picking_pagerA").getPager().render();

                                                    }
                                                }   
                                            }
                                        ]
                                    },
                                    {}                          
                                ]
                        },
                        {           
                            paddingX:2,
                            id:"tabel_id",
                            rows:
                            [
                                {  
                                    view:"datatable",
                                    id:"picking_dataT1",
                                    navigation:true,
                                    datatype:"jsarray",
                                    css:"my_style",
                                    pager:"picking_pagerA",
                                    autoheight:true,
                                    resizeColumn:true,
                                    leftSplit:0,
                                    columns:
                                    [                                  
                                        { id:"ch1", header:{ content:"masterCheckbox" }, template:"{common.checkbox()}",width:50},
                                        { id:"data0",header:"No",css:"rank",width:50},
                                        { id:"data1",header:"Delivery Schedule",css:"rank",width:200},
                                        { id:"data2",header:"Time",css:"rank",width:150},
                                        { id:"data3",header:"Part No",css:"rank",width:240},
                                        { id:"data4",header:"SO No.",css:"rank",width:200},
                                        { id:"data5",header:"SO Line",css:"rank",width:100},
                                        { id:"data6",header:"Dock",css:"rank",width:150},
                                        { id:"data7",header:"Qty",css:"rank",width:100},
                                                                        
                                    ],
                
                                }
                                ,
                                {
                                    type:"wide",
                                        cols:
                                        [
                                            {
                                                view:"pager", id:"picking_pagerA",
                                                template:function(data, common){
                                                    var start = data.page * data.size
                                                    ,end = start + data.size;
                                                    if(data.count == 0) start = 0;
                                                    else start += 1;
                                                    if(end >= data.count) end = data.count;
                                                    var html = "<b>showing "+(start)+" - "+end+" total "+data.count+" </b>";
                                                    return common.first()+common.prev()+" "+html+" "+common.next()+common.last();
                                                },
                                                size:10,
                                                group:5 
                                            }
                                        ]
                                                        
                                }

                            ]
                        }
                                                                     
                    ]
                   
                }
            ],on:
            {
                onHide:function()
                {
                    
                },
                onShow:function()
                {

                },
                onAddView:function()
                {

                }
            }
        }
    };
};