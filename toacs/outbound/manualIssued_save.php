<?php
	if(!ob_start("ob_gzhandler")) ob_start();
	header('Expires: Sun, 01 Jan 2014 00:00:00 GMT');
	header('Cache-Control: no-store, no-cache, must-revalidate');
	header('Cache-Control: post-check=0, pre-check=0', FALSE);
	header('Pragma: no-cache');
	include('../start.php');
	session_start();
	include('../php/connection.php');
	$cBy = $_SESSION['xxxID'];
	$fName = $_SESSION['xxxFName'];
	$obj  = $_POST['obj'];
	$type  = intval($_POST['type']);

	if($type == 1)
	{
		$mysqli->autocommit(FALSE);
 		try 
			{
				$gtn_no = $mysqli->real_escape_string(trim(strtoupper($obj['manualIssued_doc_gtn'])));
				$invoice = $mysqli->real_escape_string(trim(strtoupper($obj['manualIssued_Invoice'])));


				if(!$re = $mysqli->query("SELECT ID FROM tbl_out_header where Doc_no = '$gtn_no' limit 1;"))
				{echo '{ch:2,data:"Error Code 1"}';$mysqli->close();}
				if($re->num_rows == 0){echo '{ch:2,data:"ไม่มีข้อมูลในระบบ"}';$mysqli->close();exit();}
				$row = $re->fetch_object();
				$docGtn_id = $row->ID;

				if(!$re = $mysqli->query("SELECT doc_id,Part_ID,sum(qty) as pick_qty from tbl_out_body 
											where doc_id = '$docGtn_id' group by Doc_id,part_id"))
				{echo '{ch:2,data:"Error Code 1"}';$mysqli->close();}
				if($re->num_rows == 0){echo '{ch:2,data:"ไม่มีข้อมูลในระบบ"}';$mysqli->close();exit();}
				$row = $re->fetch_object();
				$part_id = $row->Part_ID;
				$pick_qty = $row->pick_qty;

				if(!$re1 = $mysqli->query("SELECT qty_onhand FROM tbl_partonhand where part_id ='$part_id' limit 1;"))
				{echo '{ch:2,data:"Error Code 1"}';$mysqli->close();}
				if($re1->num_rows == 0){echo '{ch:2,data:"error 5"}';$mysqli->close();exit();}
				$row = $re1->fetch_object();
				$qty_before = $row->qty_onhand;

				if(!$re1 = $mysqli->query("SELECT pick_loc from tbl_inventory where part_id = '$part_id' limit 1;"))
				{echo '{ch:2,data:"Error Code 1"}';$mysqli->close();}
				if($re1->num_rows == 0){echo '{ch:2,data:"error 3"}';$mysqli->close();exit();}
				$row = $re1->fetch_object();
				$pick_loc = $row->pick_loc;


				$sql = "UPDATE tbl_partonhand SET qty_onhand = qty_onhand - $pick_qty WHERE Part_ID ='$part_id'";			
				if(!$mysqli->query($sql)) throw new Exception('Error Code 2'); 


				if(!$mysqli->query("INSERT INTO tbl_transaction (Part_ID,LOT,Box_No,Qty,Out_Date,DCD_No,Doc_No,
					So_No,Tran_Status,Tran_Type,area,tarea,loc,toloc,create_date,user_id,qty_before,qty_operate,
					qty_after) SELECT Part_ID, LOT, Box,qty*-1, Create_date,'','$gtn_no','$invoice','YES','OUT','PICK','TRUCKSIM',
					'NOLOC','$pick_loc',NOW(),'$cBy',qty_before,qty,qty_after FROM tbl_out_body WHERE Doc_ID = '$docGtn_id'"))
					{throw new Exception('Error Code 5');}

				if(!$mysqli->query("UPDATE tbl_out_header SET status = '1' WHERE Doc_no = '$gtn_no'")) 
							throw new Exception('Error Code 6');

				if(!$mysqli->query("DELETE FROM tbl_out_body WHERE Doc_ID ='$docGtn_id'")) throw new Exception('Error Code 10');

				if(!$mysqli->query("DELETE FROM tbl_inventory WHERE pickdoc_id ='$docGtn_id'")) throw new Exception('Error Code 11');
									
				$mysqli->commit();

				echo '{ch:1,data:"'.$gtn_no.'"}';				
			} 

		catch (Exception $e) 
			{
				$mysqli->rollback();
		  		echo '{ch:2,data:"'.$e->getMessage().'"}';
			}		
	}

	$mysqli->close();
	exit();	
?>
