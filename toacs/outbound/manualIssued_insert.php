<?php
	if(!ob_start("ob_gzhandler")) ob_start();
	header('Expires: Sun, 01 Jan 2014 00:00:00 GMT');
	header('Cache-Control: no-store, no-cache, must-revalidate');
	header('Cache-Control: post-check=0, pre-check=0', FALSE);
	header('Pragma: no-cache');
	include('../start.php');
	session_start();
	include('../php/connection.php');
	$cBy = $_SESSION['xxxID'];
	$fName = $_SESSION['xxxFName'];
	$obj  = $_POST['obj'];
	$type  = intval($_POST['type']);

	if($type == 1)
	{
		$mysqli->autocommit(FALSE);
 		try 
			{

				$invoice = $mysqli->real_escape_string(trim(strtoupper($obj['manualIssued_Invoice'])));

				if(!$re1 = $mysqli->query("SELECT so_no from tbl_out_header where so_no ='$invoice' AND Status = '0' and Doc_type = 'MANUAL' limit 1"))
				{echo '{ch:2,data:"Error Code 1"}';$mysqli->close();}
				if($re1->num_rows > 0){echo '{ch:2,data:"เลข '.$invoice.' กำลังถูกทำงานอยู่"}';$mysqli->close();exit();}


				if(!$re = $mysqli->query("SELECT func_GenRuningNumber('gtn',0) t")) throw new Exception('Error Code 1'); 
				if($re->num_rows == 0) throw new Exception('Error Code 2'); 
				$autoID = $re->fetch_object()->t;


				$sql = "INSERT INTO tbl_out_header(Doc_No,So_NO,Create_date,Create_by,Doc_Type) values('$autoID','$invoice',NOW(),'$cBy','MANUAL')";
				if(!$mysqli->query($sql)) throw new Exception('Error Code 3'); 
				if($mysqli->affected_rows == 0) throw new Exception('Error Code 4');

				echo '{ch:1,data:';
				echo "'$autoID'";
				echo '}';

				$mysqli->commit();				
			} 

		catch (Exception $e) 
			{
				$mysqli->rollback();
		  		echo '{ch:2,data:"'.$e->getMessage().'"}';
			}		
	}
	else if ($type == 2) 
	{
		$gtn_no = $mysqli->real_escape_string(trim(strtoupper($obj['manualIssued_doc_gtn'])));
		$lot_no = $mysqli->real_escape_string(trim(strtoupper($obj['manualIssued_lot_no'])));
		$so_no = $mysqli->real_escape_string(trim(strtoupper($obj['manualIssued_Invoice'])));

		$lot_array  = $lot_no;
		$lot_cut = explode("|", $lot_array);
		if(sizeof($lot_cut)!=3){echo '{ch:2,data:"ข้อมูลไม่ถูกต้อง"}';exit();}
		if ($lot_cut[0]!="L") {echo '{ch:2,data:"ข้อมูลไม่ถูกต้อง"}';exit();}
		$lot = $lot_cut[1];
		$box = $lot_cut[2];
		$mysqli->autocommit(FALSE);
		try 
			{

				if(!$re = $mysqli->query("SELECT lot,box FROM tbl_out_body where lot = '$lot' and box = '$box' and Lot_type = 'MASTER' limit 1;"))
			    {echo '{ch:2,data:"Error Code 1"}';$mysqli->close();}
			    if($re->num_rows > 0){echo '{ch:2,data:"เลข Lot '.$lot.' ซ้ำ"}';$mysqli->close();exit();}


				if(!$re = $mysqli->query("SELECT ID FROM tbl_out_header where Doc_no = '$gtn_no' limit 1;"))
				{echo '{ch:2,data:"Error Code 1"}';$mysqli->close();}
				if($re->num_rows == 0){echo '{ch:2,data:"ไม่มีข้อมูลในระบบ"}';$mysqli->close();exit();}
				$row = $re->fetch_object();
				$docGtn_id = $row->ID;

				if(!$re1 = $mysqli->query("SELECT lot,Pickdoc_id FROM tbl_inventory WHERE lot = '$lot' and box ='$box' and (area ='PICK' OR area = 'OVERFLOW') limit 1;"))
				{echo '{ch:2,data:"Error Code 1"}';$mysqli->close();}
				if($re1->num_rows == 0){echo '{ch:2,data:"เลข Lot '.$lot.' ไม่มีข้อมูลในระบบ"}';$mysqli->close();exit();}
				$row = $re1->fetch_object();
				$Pickdoc_id = $row->Pickdoc_id;
				if ($Pickdoc_id != "0") 
				{
					echo '{ch:2,data:"เลข Lot '.$lot.' นี้กำลังถูกใช้อยู่"}';exit();
				}
			

				if(!$re2 = $mysqli->query("SELECT t1.part_id,t2.part_supplier,t1.qty,t1.lot,t1.Pick_loc FROM tbl_inventory t1 
				LEFT JOIN tbl_partmaster t2 ON t1.part_id = t2.part_id 
				WHERE t1.lot = '$lot' and t1.box ='$box' and  (t1.area ='PICK' OR t1.area = 'OVERFLOW')"))
				{
					echo '{ch:2,data:"Error Code 1"}';$mysqli->close();}
					if($re2->num_rows == 0){echo '{ch:2,data:"Part Number นี้ไม่ตรงกับ Order"}';$mysqli->close();exit();
				}


				if($re2->num_rows != 1)
				{
					while ($row = $re2->fetch_assoc()) 
					{

					 	$part_id = $row['part_id'];
						$qty = $row['qty'];
						$pick_loc = $row['Pick_loc'];

						
			
						if(!$re4 = $mysqli->query("SELECT qty_after FROM tbl_out_body WHERE Part_ID = '$part_id' ORDER BY ID DESC limit 1"))
						{echo '{ch:2,data:"Error Code 1"}';$mysqli->close();}
						if($re4->num_rows == 0)
						{
							if(!$re1 = $mysqli->query("SELECT qty_onhand FROM tbl_partonhand where part_id ='$part_id' limit 1;"))
								{echo '{ch:2,data:"Error Code 2"}';$mysqli->close();}
								if($re1->num_rows == 0){echo '{ch:2,data:"part No. ไม่มีข้อมูลในระบบ"}';$mysqli->close();exit();}
								$row = $re1->fetch_object();
								$qty_before = $row->qty_onhand;		
						}
						else
						{
							$row3 = $re4->fetch_object();
							$qty_before = $row3->qty_after;
						}
									
						$qty_after = $qty_before - $qty;

						
						$sql = "UPDATE tbl_inventory SET Pickdoc_id ='$docGtn_id',Order_id ='1' 
								WHERE lot ='$lot' and box = '$box';";			
						if(!$mysqli->query($sql)) throw new Exception('Error update inventory');

						$sql = "INSERT INTO tbl_out_body(Doc_id,Part_Id,Qty,Lot,Box,Loc,Order_No,So_no,Create_by,
						Create_date,dDate,dTime,orderID,Qty_before,Qty_after,Lot_type) values('$docGtn_id','$part_id',
						'$qty','$lot','$box','$pick_loc','0','$so_no','$cBy',NOW(),NOW(),'',
						'0','$qty_before','$qty_after','COMLOT')";

						if(!$mysqli->query($sql)) throw new Exception('บันทึกไม่สำเร็จ');
						$mysqli->commit();
					}
				}
				else
				{
					$row = $re2->fetch_object();
					$part_id = $row->part_id;
					$qty = $row->qty;
					$pick_loc = $row->Pick_loc;

					if(!$re5 = $mysqli->query("SELECT qty_after FROM tbl_out_body WHERE Part_ID = '$part_id' ORDER BY ID DESC limit 1"))
					{echo '{ch:2,data:"Error Code 1"}';$mysqli->close();}
					if($re5->num_rows == 0)
					{
						if(!$re1 = $mysqli->query("SELECT qty_onhand FROM tbl_partonhand where part_id ='$part_id' limit 1;"))
							{echo '{ch:2,data:"Error Code 1"}';$mysqli->close();}
							if($re1->num_rows == 0){echo '{ch:2,data:"part No. ไม่มีข้อมูลในระบบ"}';$mysqli->close();exit();}
							$row = $re1->fetch_object();
							$qty_before = $row->qty_onhand;		
					}
					else
					{
						$row = $re5->fetch_object();
						$qty_before = $row->qty_after;
					}
								
					$qty_after = $qty_before - $qty;

					$sql = "UPDATE tbl_inventory SET Pickdoc_id ='$docGtn_id',Order_id ='1' 
						WHERE lot ='$lot' and box = '$box';";			
					if(!$mysqli->query($sql)) throw new Exception('Error update inventory');

					$sql = "INSERT INTO tbl_out_body(Doc_id,Part_Id,Qty,Lot,Box,Loc,Order_No,So_no,Create_by,
						Create_date,dDate,dTime,orderID,Qty_before,Qty_after,Lot_type) values('$docGtn_id','$part_id',
						'$qty','$lot','$box','$pick_loc','0','$so_no','$cBy',NOW(),NOW(),'',
						'0','$qty_before','$qty_after','MASTER')";
						if(!$mysqli->query($sql)) throw new Exception('บันทึกไม่สำเร็จ');
						$mysqli->commit();

				}

				if($re4 = $mysqli->query("SELECT t1.ID,t2.part_supplier,t1.lot,t1.Box,t1.qty FROM tbl_out_body t1 
								LEFT JOIN tbl_partmaster t2 ON t1.part_ID = t2.Part_ID WHERE t1.doc_id = '$docGtn_id'"))
				{
					if($re4->num_rows >0)
						{
							
							echo '{ch:1,data1:';
							echo toArrayStringAddNumberRow($re4,1);
							echo '}';
							
						}
					
				}
				else echo '{ch:2,data:"โคดผิด"}';
				
			}

		catch (Exception $e) 
			{
				$mysqli->rollback();
		  		echo '{ch:2,data:"'.$e->getMessage().'"}';
			}
		
	}
		
	$mysqli->close();
	exit();	
?>
