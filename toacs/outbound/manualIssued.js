var header_manualIssued = function()
{
	return {
        view: "scrollview",
        scroll: "native-y",
        id:"header_manualIssued",
        body: 
        {
        	id:"manualIssued_id",
        	type:"clean",
    		rows:
    		[
    		   {
                    view:"form",
                    paddingY:20,
                    id:"manualIssued_form1",
                    elements:
                    [
                        {
                            rows:
                            [
                                {                            
                                    cols:
                                    [
                                        {
                                            view:"combo",yCount:"10",suggest:"outbound/manualIssued_data.php?type=1",label:'GTN No. (งานค้าง)',labelPosition:"top",labelWidth:125,id:"manualIssued_doc_gtn",name:"manualIssued_doc_gtn",width:230,value:"",
                                            on:
                                            {
                                                onChange: function(value)
                                                 {

                                                    if(value.length>0) this.disable();else this.enable();
                                                    {
                                                        obj =  $$('manualIssued_form1').getValues();
                                                        $.post("outbound/manualIssued_data.php", {obj:obj,type:2})
                                                        .done(function( data ) 
                                                        {
                                                            data = eval('('+data+')');
                                                            if(data.data1.length)
                                                            {
                                                                var ar = data.data1[0];
                                                                $$('manualIssued_Invoice').setValue(ar[2]);                                                              
                                                            }
                                                            manualIssued_FN_2();

                                                            var dataT1 = $$("manualIssued_dataT1");                                                          
                                                            if(data.ch == 1)
                                                            { 
                                                                dataT1.clearAll();
                                                                dataT1.parse(data.data2,"jsarray");
                                                                
                                                            }                   
                                                       });

                                                    }

                                                }
                                            }
                                        },{width:10},                                      
                                        {view:"text",label:'Invoice (เลขที่เอกสาร)',id:"manualIssued_Invoice",name:"manualIssued_Invoice",labelPosition:"top",width:350,labelWidth:75,required:true},                               
                                        {
                                            rows:
                                            [
                                                {

                                                },
                                                {                                          
                                                    view:"button",icon:"users",value:"Add (เพิ่ม)",id:"manualIssued_Add",css:"bt_blue",width:210,on:
                                                    {
                                                        onItemClick:function(id, e)
                                                        {    
                                                            if ($$('manualIssued_form1').validate())
                                                            {
                                                                var btn = this;
                                                                btn.disable();
                                                                $.post("outbound/manualIssued_insert.php",{obj:$$('manualIssued_form1').getValues(),type:1})
                                                                .done(function( data ) 
                                                                {
                                                                    btn.enable();
                                                                    var data = eval('('+data+')');                                     
                                                                    if(data.ch == 1)
                                                                    {
                                                                        var doctype = $$('manualIssued_doc_gtn'),list = doctype.getPopup().getList();
                                                                        doctype.blockEvent();
                                                                        list.clearAll();
                                                                        list.parse([data.data]);
                                                                        doctype.setValue(list.getFirstId());
                                                                        doctype.unblockEvent();
                                                                        doctype.disable();
                                                                       
                                                                        manualIssued_FN_2();
                                                                    }
                                                                    else if(data.ch == 2){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){$$('manualIssued_Invoice').focus();$$('manualIssued_Invoice').setValue('');}});}
                                                                    else if(data.ch == 9){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                                                                    else if(data.ch == 10){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){window.open("login.php","_self");}});}
                                                                });
                                                            }
                                                            else
                                                            {
                                                                webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:'กรุณาป้อนข้อมูล<font color="#ce5545"><b>ในช่องสีแดง</b></font>ให้ถูกต้อง',callback:function(){$$('manualIssued_Invoice').focus();}});
                                                            }
                                                        }                             
                                                    }                     
                                                },
                                            ]
                                        },                                                                                                                                                                                                                                                                                  
                                        {
                                        }
                                    ]
                                },                             
                                {
                                    id:"manualIssued_row_2",
                                    cols:
                                    [
                                        {
                                            rows:
                                            [
                                                {

                                                },
                                                {
                                                    view:"button",icon:"users",type:"danger",value:"Cancel (ยกเลิกเอกสาร)",id:"manualIssued_cancel_all",css:"bt_blue",width:230,on:
                                                    {
                                                        onItemClick:function(id, e)
                                                        {                                                                                                         
                                                            webix.confirm({
                                                            title: "กรุณายืนยัน",ok:"ใช่", cancel:"ไม่",text:"คุณต้องการ<font color='#ca4635'><br><b>ที่จะยกเลิกเอกสาร</b></font><br><font color='#27ae60'><b>ใช่</b></font> หรือ <font color='#3498db'><b>ไม่</b></font>",
                                                            callback:function(res)
                                                            {
                                                                if(res)
                                                                {
                                                                    var btn = $$('manualIssued_cancel_all'),
                                                                    obj =  $$('manualIssued_form1').getValues();
                                                                    btn.disable();
                                                                    $.post("outbound/manualIssued_delete.php",{obj:obj,type:1})
                                                                    .done(function( data ) 
                                                                    {   
                                                                        btn.enable();                                                   
                                                                        $$("manualIssued_Invoice").enable();
                                                                        $$("manualIssued_Add").show();
                                                                        $$("manualIssued_Invoice").setValue('');
                                                                        $$("manualIssued_lot_no").setValue('');

                                                                        $$("manualIssued_dataT1").clearAll();                                                       
                                                                                                                                                                                        
                                                                        manualIssued_init();
                                                                        window.manualIssued_FN_1();                                                         

                                                                    });
                                                                }

                                                            }
                                                            })                            
                                                                
                                                        }
                                                    } 
                                                }

                                            ]                  
                                            
                                        },{width:10},
                                        {view:"text",label:'Lot No.',id:"manualIssued_lot_no",name:"manualIssued_lot_no",labelPosition:"top",width:350,required:true},
                                        {
                                            rows:
                                            [
                                                {

                                                },
                                                {
                                                    view:"button",value:"Scan Lot",id:"manualIssued_scan_lot",css:"bt_red",width:210,on:
                                                    {
                                                        onItemClick:function(id, e)
                                                        {
                                                            
                                                           if ($$('manualIssued_form1').validate())
                                                            {
                                                                var btn = $$('manualIssued_scan_lot'),
                                                                obj =  $$('manualIssued_form1').getValues();
                                                                btn.disable();
                                                     
                                                                $.post("outbound/manualIssued_insert.php",{obj:obj,type:2})
                                                                 .done(function( data )
                                                                {
                                                                    
                                                                    btn.enable();
                                                                    data = eval('('+data+')');
                                                                    if(data.ch == 1)
                                                                    {
                                                                        var dataT1 = $$('manualIssued_dataT1');
                                                                        dataT1.clearAll();
                                                                        dataT1.parse(data.data1,"jsarray");

                                                                        $$('manualIssued_lot_no').setValue('');
                                                                        $$('manualIssued_lot_no').focus();                                                              
                                                                       
                                                                    }
                                                                    else if (data.ch == 2)
                                                                    {
                                                                        webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function()
                                                                        {
                                                                            $$('manualIssued_lot_no').setValue('');
                                                                            $$('manualIssued_lot_no').focus();
                                                                        }});

                                                                    }
                                                                    
                                                                });
                                                            }
                                                            else
                                                            {
                                                                webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:'กรุณาป้อนข้อมูล<font color="#ce5545"><b>ในช่องสีแดง</b></font>ให้ถูกต้อง',callback:function(){$$('manualIssued_lot_no').focus();}});
                                                            }
                                                      
                                                        }
                                                    }                                         
                                                }
                                            ]
                                        },
                                        {
                                            rows:
                                            [
                                                {

                                                },
                                                {
                                                    view:"button",icon:"users",type:"danger",value:"Clear (ล้างหน้าจอ)",id:"manualIssued_clear_all",css:"bt_blue",width:200,on:
                                                    {
                                                        onItemClick:function(id, e)
                                                        {

                                                            $$("manualIssued_Invoice").enable();
                                                            $$("manualIssued_Add").show();
                                                            $$("manualIssued_Invoice").setValue('');
                                                            $$("manualIssued_lot_no").setValue('');

                                                            $$("manualIssued_dataT1").clearAll();                                                       
                                                                                                                                                                            
                                                            manualIssued_init();
                                                            window.manualIssued_FN_1(); 
                                                        }
                                                    } 
                                                }

                                            ]                  
                                            
                                        },
                                        {
                                            rows:
                                            [
                                                {

                                                },
                                                {
                                                    view:"button",type:"form",value:"Confirm Issued",id:"manualIssued_Issued_save",css:"bt_red",width:230,on:
                                                    {
                                                        onItemClick:function(id, e)
                                                        {
                                                            var dataT1 = $$("manualIssued_dataT1");
                                                            if(dataT1.count() >0)
                                                            {
                                                                webix.confirm(
                                                                {
                                                                    title:"<b>ข้อความจากระบบ</b>",ok:'ใช่',cancel:"ไม่",text:"คุณต้องการ<font color='#ca4635'><br><b>บันทึกข้อมูล</b></font><br><font color='#27ae60'><b>ใช่</b></font> หรือ <font color='#3498db'><b>ไม่</b></font>",callback:function(result)
                                                                    {
                                                                    if(result)
                                                                        {
                                                                            var btn = $$('manualIssued_Issued_save'),
                                                                            obj =  $$('manualIssued_form1').getValues();
                                                                            btn.disable();
                                                                            $.post( "outbound/manualIssued_save.php", {obj:obj,type:1})
                                                                            .done(function( data ) 
                                                                            {
                                                                                btn.enable();
                                                                                var data = eval('('+data+')');
                                                                                if(data.ch == 1)
                                                                                { 
                                                                                    webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});
                                                                                    $.post( "print/gtn.php", {data1:data.data})
                                                                                    .done(function( data ) 
                                                                                    {
                                                                                        var data = eval('('+data+')');
                                                                                        if(data.ch == 1)
                                                                                        {
                                                                                          webix.message({ type:"default",expire:7000, text:'ปริ้น GTN สำเร็จ'});
                                                                                        }
                                                                                        else webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});
                                                                                    });
                                                                                    $$('manualIssued_clear_all').callEvent("onItemClick", []);
                                                                        
                                                                                }
                                                                                else if(data.ch == 2){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                                                                                else if(data.ch == 9){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                                                                                else if(data.ch == 10){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){window.open("login.php","_self");}});}                    
                                                                            });
                                                                        }
                                                                    }
                                                                });


                                                            }
                                                            else{webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:'ไม่พบข้อมูลในตาราง',callback:function(){}});}
                                                        }
                                                    }                                         
                                                }
                                            ]
                                        },
                                        {

                                        }              
                                    ]
                                }
                              
                            ]
                        },
                    ],
                    on:
                            {
                                "onSubmit":function(view,e)
                                {
                                    if(view.config.name =='manualIssued_Invoice')
                                    {
                                        view.blur();
                                        $$('manualIssued_Add').callEvent("onItemClick", []);
                                    }
                                    else if (view.config.name == 'manualIssued_lot_no') 
                                    {
                                        view.blur();
                                        $$('manualIssued_scan_lot').callEvent("onItemClick", []);
                                    }
                                    else if(webix.UIManager.getNext(view).config.type == 'line')
                                    {
                                        webix.UIManager.setFocus(webix.UIManager.getNext(webix.UIManager.getNext(view)));
                                    }
                                    else
                                    {
                                        webix.UIManager.setFocus(webix.UIManager.getNext(view));
                                    }
                                }
                            }
                    
                },
                {
                    paddingX:20,
                    paddingY:5,
                    cols:
                    [
                        {
                            id:"manualIssued_tabel_1",
                            rows:
                            [
                                {
                                    view:"datatable",
                                    id:"manualIssued_dataT1",
                                    navigation:true,
                                    resizeColumn:true,
                                    autoheight:true,
                                    css:"my_style",
                                    datatype:"jsarray",
                                    headerRowHeight:55,
                                    width:860,        
                                    columns:
                                    [   
                                        { id:"data22",header:"&nbsp;",width:35,template: "<span style='cursor:pointer;' class='webix_icon fa-trash-o'></span>"},                                        
                                        { id:"data0",header:"No.",css:"rank",width:40},
                                        { id:"data2",header:{text:"Part No", css:{"text-align":"center"}},css:"rank",width:270,css:{'text-align':'center'}},
                                        { id:"data3",header:{text:"Lot", css:{"text-align":"center"}},css:"rank",width:250,css:{'text-align':'center'}},
                                        { id:"data4",header:{text:"Box No.", css:{"text-align":"center"}},css:"rank",width:130,css:{'text-align':'center'}},
                                        { id:"data5",header:{text:"Qty", css:{"text-align":"center"}},css:"rank",width:130,css:{'text-align':'center'}},
                                    ],
                                    onClick:
                                    {                 
                                        "fa-trash-o":function(e,t)
                                        {
                                            var r = this.getItem(t),obj;
                                            webix.confirm({
                                                title: "กรุณายืนยัน",ok:"ใช่", cancel:"ไม่",text:"คุณต้องการ<font color='#ca4635'><br><b>ลบ</b></font><br><font color='#27ae60'><b>ใช่</b></font> หรือ <font color='#3498db'><b>ไม่</b></font>",
                                                callback:function(res)
                                                {
                                                    if(res)
                                                    {
                                                        $.post("outbound/manualIssued_delete.php",{obj:{out_body_id:r.data1},type:2})
                                                        .done(function( data ) 
                                                        {
                                                            data = eval('('+data+')');
                                                            if(data.ch==1)
                                                            {
                                                                var dataT1 = $$('manualIssued_dataT1');
                                                                dataT1.clearAll();
                                                                dataT1.parse(data.data1,"jsarray");
                                                            }

                                                            else if(data.ch == 2)
                                                            {
                                                                var dataT1 = $$('manualIssued_dataT1');
                                                                dataT1.clearAll();
                                                            }
                                                                               
                                                        });
                                                    }
                                                }
                                            });
                                        }
                                    }                                     
                                }
                            ]
                        },
                        {}
                    ]
                }
            ],on:
            {
                onHide:function()
                {
                    
                },
                onShow:function()
                {

                },
                onAddView:function()
                {

                    window['manualIssued_FN_1'] = function()  
                    {               
                        setTimeout(function(){$$('manualIssued_Invoice').focus();},500);
                        $$('manualIssued_row_2').hide();
                        $$('manualIssued_tabel_1').hide();
                                 
                    };

                    window['manualIssued_FN_2'] = function()  
                    {               
                        $$('manualIssued_row_2').show();
                        $$('manualIssued_tabel_1').show();
                        $$('manualIssued_Invoice').disable();
                        $$('manualIssued_Add').hide();
                        $$('manualIssued_lot_no').focus();  
                                 
                    };

                    window['manualIssued_init'] = function()
                    {
                        var doctype = $$('manualIssued_doc_gtn'),list = doctype.getPopup().getList();
                        doctype.blockEvent();
                        list.clearAll();
                        doctype.setValue('');
                        webix.ajax().post("outbound/manualIssued_data.php", {type:1}, function(text, data)
                        {
                            var data=eval('('+text+')'),htmlNode=$(doctype.$view);
                            list.parse(data);
                            doctype.unblockEvent();
                            if(data.length>0)
                            {
                                doctype.enable();
                                if(!htmlNode.hasClass('webix_invalid')) htmlNode.addClass('webix_invalid');
                                webix.message({ type:"error",expire:7000,text:'คุณมีงานค้างโปรดตรวจสอบ'});
                            }else
                            {
                                doctype.disable();
                                if(htmlNode.hasClass('webix_invalid')) htmlNode.removeClass('webix_invalid');
                            }
                        });
                    };



                    manualIssued_init();
                    window.manualIssued_FN_1();  

                }
            }
        }
    };
};