var header_confirmPicking = function()
{
	return {
        view: "scrollview",
        scroll: "native-y",
        id:"header_confirmPicking",
        body: 
        {
        	id:"confirmPicking_id",
        	type:"clean",
    		rows:
    		[
    		    {
                    view:"form",
                    paddingY:20,
                    id:"confirmPicking_form1",
                    elements:
                    [
                        {
                            rows:
                            [
                                {                            
                                    cols:
                                    [
                                        {
                                            view:"combo",yCount:"10",suggest:"outbound/confirmPicking_data.php?type=1",label:'GTN No. (งานค้าง)',labelPosition:"top",labelWidth:125,id:"confirm_doc_gtn",name:"confirm_doc_gtn",width:250,value:"",
                                            on:
                                            {
                                                onChange: function(value)
                                                 {

                                                    if(value.length>0) this.disable();else this.enable();
                                                    {
                                                        obj =  $$('confirmPicking_form1').getValues();
                                                        $.post("outbound/confirmPicking_data.php", {obj:obj,type:2})
                                                        .done(function( data ) 
                                                        {
                                                            data = eval('('+data+')');
                                                            if(data.data1.length)
                                                            {
                                                                var ar = data.data1[0];
                                                                $$('confirm_so_no').setValue(ar[5]);                                                              
                                                            }
                                                            confirm_FN_1();

                                                            var dataT1 = $$("confirmPicking_dataT1");                                                          
                                                            if(data.ch == 1)
                                                            { 
                                                                dataT1.clearAll();
                                                                dataT1.parse(data.data1,"jsarray");
                                                                
                                                            }

                                                            var dataT1 = $$("confirmPicking_dataT2");                                                          
                                                            if(data.ch == 1)
                                                            { 
                                                                dataT1.clearAll();
                                                                dataT1.parse(data.data2,"jsarray");
                                                                
                                                            }
                                                                                 
                                                       });

                                                    }

                                                }
                                            }
                                        },{width:10},                                      
                                        {view:"text",label:'SO No.',id:"confirm_so_no",name:"confirm_so_no",labelPosition:"top",width:350,labelWidth:75,required:true},                               
                                        {
                                            rows:
                                            [
                                                {

                                                },
                                                {                                          
                                                    view:"button",icon:"users",value:"Scan SO No.",id:"confirm_scan_soNO",css:"bt_blue",width:180,on:
                                                    {
                                                        onItemClick:function(id, e)
                                                        {    
                                                            if ($$('confirmPicking_form1').validate())
                                                            {
                                                                var btn = this;
                                                                btn.disable();
                                                                $.post("outbound/confirmPicking_insert.php",{obj:$$('confirmPicking_form1').getValues(),type:1})
                                                                .done(function( data ) 
                                                                {
                                                                    btn.enable();
                                                                    var data = eval('('+data+')');
                                                                    dataT1 = $$("confirmPicking_dataT1");
                                                                    if(data.ch == 1)
                                                                    {
                                                                        var doctype = $$('confirm_doc_gtn'),list = doctype.getPopup().getList();
                                                                        doctype.blockEvent();
                                                                        list.clearAll();
                                                                        list.parse([data.data]);
                                                                        doctype.setValue(list.getFirstId());
                                                                        doctype.unblockEvent();
                                                                        doctype.disable();
                                                                       
                                                                        dataT1.clearAll();
                                                                        dataT1.parse(data.value,"jsarray");
                                                                    
                                                                        confirm_FN_1();
                                                                    }
                                                                    else if(data.ch == 2){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){$$('confirm_so_no').focus();$$('confirm_so_no').setValue('');}});}
                                                                    else if(data.ch == 9){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                                                                    else if(data.ch == 10){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){window.open("login.php","_self");}});}
                                                                });
                                                            }
                                                            else
                                                            {
                                                                webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:'กรุณาป้อนข้อมูล<font color="#ce5545"><b>ในช่องสีแดง</b></font>ให้ถูกต้อง',callback:function(){$$('so_no').focus();}});
                                                            }
                                                        }                             
                                                    }                     
                                                },
                                            ]
                                        },                                                                                                                                                                                                                                                                                  
                                        {
                                        }
                                    ]
                                },                             
                                {
                                    id:"row_2",
                                    cols:
                                    [
                                        {
                                            rows:
                                            [
                                                {

                                                },
                                                {
                                                    view:"button",icon:"users",type:"danger",value:"Cancel (ยกเลิกเอกสาร)",id:"confirm_cancel_all",css:"bt_blue",width:170,on:
                                                    {
                                                        onItemClick:function(id, e)
                                                        {                                                                                                         
                                                            webix.confirm({
                                                            title: "กรุณายืนยัน",ok:"ใช่", cancel:"ไม่",text:"คุณต้องการ<font color='#ca4635'><br><b>ที่จะยกเลิกเอกสาร</b></font><br><font color='#27ae60'><b>ใช่</b></font> หรือ <font color='#3498db'><b>ไม่</b></font>",
                                                            callback:function(res)
                                                            {
                                                                if(res)
                                                                {
                                                                    var btn = $$('confirm_cancel_all'),
                                                                    obj =  $$('confirmPicking_form1').getValues();
                                                                    btn.disable();
                                                                    $.post("outbound/confirmPicking_delete.php",{obj:obj,type:1})
                                                                    .done(function( data ) 
                                                                    {   
                                                                        btn.enable();                                                   
                                                                        $$("confirm_so_no").enable();
                                                                        $$("confirm_scan_soNO").show();
                                                                        $$("confirm_so_no").setValue('');
                                                                        $$("confirm_lot_no").setValue('');

                                                                        $$("confirmPicking_dataT1").clearAll();                                                                       

                                                                        $$("confirmPicking_dataT2").clearAll();
                                                                                                                                                                                                                                   
                                                                        confirm_FN();
                                                                        confirm_init();                                                            

                                                                    });
                                                                }

                                                            }
                                                            })                            
                                                                
                                                        }
                                                    } 
                                                }

                                            ]                  
                                            
                                        },
                                        {
                                            rows:
                                            [
                                                {

                                                },
                                                {
                                                    view:"button",icon:"users",type:"danger",value:"Clear (ล้างหน้าจอ)",id:"confirm_clear_all",css:"bt_blue",width:170,on:
                                                    {
                                                        onItemClick:function(id, e)
                                                        {

                                                            $$("confirm_so_no").enable();
                                                            $$("confirm_scan_soNO").show();
                                                            $$("confirm_so_no").setValue('');
                                                            $$("confirm_lot_no").setValue('');


                                                            $$("confirmPicking_dataT1").clearAll();
                                                            /*$$("confirmPicking_dataT1").getPager().render();*/

                                                            $$("confirmPicking_dataT2").clearAll();
                                                           /* $$("confirmPicking_dataT2").getPager().render(); */                                                                                                                                                              
                                                            confirm_FN();
                                                            confirm_init();
                                                        }
                                                    } 
                                                }

                                            ]                  
                                            
                                        },
                                        {view:"text",label:'Lot No.',id:"confirm_lot_no",name:"confirm_lot_no",labelPosition:"top",width:400,required:true},
                                        {
                                            rows:
                                            [
                                                {

                                                },
                                                {
                                                    view:"button",value:"Scan Lot",id:"confirm_scan_lot",css:"bt_red",width:210,on:
                                                    {
                                                        onItemClick:function(id, e)
                                                        {
                                                            
                                                           if ($$('confirmPicking_form1').validate())
                                                            {
                                                                var btn = $$('confirm_scan_lot'),
                                                                obj =  $$('confirmPicking_form1').getValues();
                                                                btn.disable();
                                                     
                                                                $.post("outbound/confirmPicking_insert.php",{obj:obj,type:2})
                                                                 .done(function( data )
                                                                {
                                                                    
                                                                    btn.enable();
                                                                    data = eval('('+data+')');
                                                                    if(data.ch == 1)
                                                                    {
                                                                        var dataT1 = $$('confirmPicking_dataT1');
                                                                        dataT1.clearAll();
                                                                        dataT1.parse(data.data1,"jsarray");

                                                                        $$('confirm_lot_no').setValue('');
                                                                        $$('confirm_lot_no').focus();
                                                                        var dataT1 = $$('confirmPicking_dataT2');
                                                                        dataT1.clearAll();
                                                                        dataT1.parse(data.data2,"jsarray");

                                                                       
                                                                    }
                                                                    else if (data.ch == 2)
                                                                    {
                                                                        webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function()
                                                                        {
                                                                            $$('confirm_lot_no').setValue('');
                                                                            $$('confirm_lot_no').focus();
                                                                        }});

                                                                    }
                                                                    
                                                                });
                                                            }
                                                            else
                                                            {
                                                                webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:'กรุณาป้อนข้อมูล<font color="#ce5545"><b>ในช่องสีแดง</b></font>ให้ถูกต้อง',callback:function(){$$('confirm_lot_no').focus();}});
                                                            }
                                                      
                                                        }
                                                    }                                         
                                                }
                                            ]
                                        },
                                        {
                                            rows:
                                            [
                                                {

                                                },
                                                {
                                                    view:"button",type:"form",value:"Confirm Picking",id:"confirm_pick",css:"bt_red",width:230,on:
                                                    {
                                                        onItemClick:function(id, e)
                                                        {
                                                            var dataT1 = $$("confirmPicking_dataT2");
                                                            if(dataT1.count() >0)
                                                            {
                                                                webix.confirm(
                                                                {
                                                                    title:"<b>ข้อความจากระบบ</b>",ok:'ใช่',cancel:"ไม่",text:"คุณต้องการ<font color='#ca4635'><br><b>บันทึกข้อมูล</b></font><br><font color='#27ae60'><b>ใช่</b></font> หรือ <font color='#3498db'><b>ไม่</b></font>",callback:function(result)
                                                                    {
                                                                    if(result)
                                                                        {
                                                                            var btn = $$('confirm_pick'),
                                                                            obj =  $$('confirmPicking_form1').getValues();
                                                                            btn.disable();
                                                                            $.post( "outbound/confirmPicking_save.php", {obj:obj,type:1})
                                                                            .done(function( data ) 
                                                                            {
                                                                                btn.enable();
                                                                                var data = eval('('+data+')');
                                                                                if(data.ch == 1)
                                                                                { 
                                                                                    webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});
                                                                                    $.post( "print/gtn.php", {data1:data.data})
                                                                                    .done(function( data ) 
                                                                                    {
                                                                                        var data = eval('('+data+')');
                                                                                        if(data.ch == 1)
                                                                                        {
                                                                                          webix.message({ type:"default",expire:7000, text:'ปริ้น GTN สำเร็จ'});
                                                                                        }
                                                                                        else webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});
                                                                                    });
                                                                                    $$('confirm_clear_all').callEvent("onItemClick", []);
                                                                        
                                                                                }
                                                                                else if(data.ch == 2){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                                                                                else if(data.ch == 9){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                                                                                else if(data.ch == 10){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){window.open("login.php","_self");}});}                    
                                                                            });
                                                                        }
                                                                    }
                                                                });


                                                            }
                                                            else{webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:'ไม่พบข้อมูลในตาราง',callback:function(){}});}
                                                        }
                                                    }                                         
                                                }
                                            ]
                                        },
                                        {

                                        }              
                                    ]
                                }
                            ]
                        },
                    ],
                    on:
                            {
                                "onSubmit":function(view,e)
                                {
                                    if(view.config.name =='confirm_so_no')
                                    {
                                        view.blur();
                                        $$('confirm_scan_soNO').callEvent("onItemClick", []);
                                    }
                                    else if (view.config.name == 'confirm_lot_no') 
                                    {
                                        view.blur();
                                        $$('confirm_scan_lot').callEvent("onItemClick", []);
                                    }
                                    else if(webix.UIManager.getNext(view).config.type == 'line')
                                    {
                                        webix.UIManager.setFocus(webix.UIManager.getNext(webix.UIManager.getNext(view)));
                                    }
                                    else
                                    {
                                        webix.UIManager.setFocus(webix.UIManager.getNext(view));
                                    }
                                }
                            }
                    
                },
               
                {
     
                    paddingX:5,
                    cols:
                    [
                        {
                            id:"tabel_1",
                            rows:
                                [
                                    {
                                        view:"datatable",
                                        id:"confirmPicking_dataT1",
                                        navigation:true,
                                        resizeColumn:true,
                                        autoheight:true,
                                        css:"my_style",
                                        datatype:"jsarray",
                                        headerRowHeight:55,
                                        width:570,
                                        columns:
                                        [                                           
                                            { id:"data0",header:"No.",css:"rank",width:35},
                                            { id:"data1",header:{text:"Part No", css:{"text-align":"center"}},css:"rank",width:220,css:{'text-align':'center'}},
                                            { id:"data2",header:{text:"Pick Actual", css:{"text-align":"center"}},css:"rank",width:100,css:{'text-align':'center'}},
                                            { id:"data3",header:{text:"Qty", css:{"text-align":"center"}},css:"rank",width:100,css:{'text-align':'center'}},
                                            { id:"data4",header:{text:"Box", css:{"text-align":"center"}},css:"rank",width:90,css:{'text-align':'center'}},
                                        ],                                     
                                    }
                                ]
                        },
                        {
                           
                                paddingX:20,
                                id:"tabel_2",
                                rows:
                                [
                                    {
                                        view:"datatable",
                                        id:"confirmPicking_dataT2",
                                        navigation:true,
                                        resizeColumn:true,
                                        autoheight:true,
                                        css:"my_style",
                                        datatype:"jsarray",
                                        headerRowHeight:55,
                                        width:650,
                                        columns:
                                        [
                                            { id:"data22",header:"&nbsp;",width:35,template: "<span style='cursor:pointer;' class='webix_icon fa-trash-o'></span>"},
                                            { id:"data0",header:"No.",css:"rank",width:35},
                                            { id:"data2",header:{text:"Part No", css:{"text-align":"center"}},css:"rank",width:200,css:{'text-align':'left'}},
                                            { id:"data3",header:{text:"Lot", css:{"text-align":"center"}},css:"rank",width:200,css:{'text-align':'center'}},
                                            { id:"data4",header:{text:"Box", css:{"text-align":"center"}},css:"rank",width:90,css:{'text-align':'center'}},
                                            { id:"data5",header:{text:"Qty", css:{"text-align":"center"}},css:"rank",width:90,css:{'text-align':'center'}},

                                        ],
                                        onClick:
                                        {                 
                                             "fa-trash-o":function(e,t)
                                            {
                                                var r = this.getItem(t),obj;
                                                webix.confirm({
                                                     title: "กรุณายืนยัน",ok:"ใช่", cancel:"ไม่",text:"คุณต้องการ<font color='#ca4635'><br><b>ลบ</b></font><br><font color='#27ae60'><b>ใช่</b></font> หรือ <font color='#3498db'><b>ไม่</b></font>",
                                                     callback:function(res)
                                                     {
                                                        if(res)
                                                        {
                                                            $.post("outbound/confirmPicking_delete.php",{obj:{out_body_id:r.data1,confirm_so_no:$$('confirm_so_no').getValue()},type:2})
                                                            .done(function( data ) 
                                                            {
                                                                data = eval('('+data+')');
                                                                if(data.ch==1)
                                                                {
                                                                    var dataT1 = $$('confirmPicking_dataT1');
                                                                    dataT1.clearAll();
                                                                    dataT1.parse(data.data1,"jsarray");

                                                                    var dataT1 = $$('confirmPicking_dataT2');
                                                                    dataT1.clearAll();
                                                                    dataT1.parse(data.data2,"jsarray");
                                                                }

                                                                else if(data.ch == 2)
                                                                {
                                                                    var dataT1 = $$('confirmPicking_dataT2');
                                                                    dataT1.clearAll();
                                                                }
                                                               
                                                            });
                                                        }
                                                     }
                                                });
                                            }
                                        }
                                    }
                                ]
                            
                        },{}
                    ]
                }
            ],on:
            {
                onHide:function()
                {
                    
                },
                onShow:function()
                {

                },
                onAddView:function()
                {

                    window['confirm_FN'] = function()  
                    {
                        setTimeout(function(){$$('confirm_so_no').focus();},500);
                        $$('row_2').hide();
                        $$('tabel_1').hide();
                        $$('tabel_2').hide();
                    };

                    window['confirm_FN_1'] = function()  
                    {               
                        $$('row_2').show();
                        $$('tabel_1').show();
                        $$('tabel_2').show();
                        $$('confirm_so_no').disable();
                        $$('confirm_scan_soNO').hide();
                        $$('confirm_lot_no').focus();                     
                    };

                    window['confirm_init'] = function()
                    {
                        var doctype = $$('confirm_doc_gtn'),list = doctype.getPopup().getList();
                        doctype.blockEvent();
                        list.clearAll();
                        doctype.setValue('');
                        webix.ajax().post("outbound/confirmPicking_data.php", {type:1}, function(text, data)
                        {
                            var data=eval('('+text+')'),htmlNode=$(doctype.$view);
                            list.parse(data);
                            doctype.unblockEvent();
                            if(data.length>0)
                            {
                                doctype.enable();
                                if(!htmlNode.hasClass('webix_invalid')) htmlNode.addClass('webix_invalid');
                                webix.message({ type:"error",expire:7000,text:'คุณมีงานค้างโปรดตรวจสอบ'});
                            }else
                            {
                                doctype.disable();
                                if(htmlNode.hasClass('webix_invalid')) htmlNode.removeClass('webix_invalid');
                            }
                        });
                    };

                    confirm_init();                   
                   window.confirm_FN();
                }
            }
        }
    };
};